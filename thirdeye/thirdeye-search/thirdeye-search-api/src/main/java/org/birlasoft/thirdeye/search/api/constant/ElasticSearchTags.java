package org.birlasoft.thirdeye.search.api.constant;

/**
 * Standard tag keys that are going to be used in the elastic search JSON objects
 * 
 * @author tej.sarup
 *
 */
public enum ElasticSearchTags {

	/**
	 * Used to mark the parameter array associated to an asset
	 */
	PARAMS("_params"),
	
	/**
	 * Mark the workspace to which an asset is linked
	 */
	WORKSPACE("ws"),
	
	TEMPLATECOLS("templateCols"),
	
	ASSETCLASS("assetClass"), 
	
	UID("uid");
	
	
	
	String tagKey;
	
	ElasticSearchTags (String tagKey){
		this.tagKey = tagKey;
	}

	public String getTagKey() {
		return tagKey;
	}

	
	
}
