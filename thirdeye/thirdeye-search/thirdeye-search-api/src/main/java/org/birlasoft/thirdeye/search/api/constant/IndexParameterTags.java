package org.birlasoft.thirdeye.search.api.constant;

/**
 * Standard tag keys that are going to be used in the elastic search JSON objects
 * 
 * @author tej.sarup
 *
 */
public enum IndexParameterTags {

	
	ID("id"),
	DISPLAYNAME("displayName"),
	UNIQUENAME("uniqueName"),
	TYPE("type"),
	CURRENTVALUE("currentValue"),
	VALUES("values");
	
	
	String tagKey;
	
	IndexParameterTags (String tagKey){
		this.tagKey = tagKey;
	}

	public String getTagKey() {
		return tagKey;
	}

	
	
}
