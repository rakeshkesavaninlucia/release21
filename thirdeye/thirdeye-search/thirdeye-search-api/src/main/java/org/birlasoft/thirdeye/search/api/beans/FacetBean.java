package org.birlasoft.thirdeye.search.api.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dhruv.sood
 *
 */
public class FacetBean{
	
	private String facetNumber;
	private String name;
	private List<FilterBean> listOfFilterBean = new ArrayList<>();
	
	
	public String getFacetNumber() {
		return facetNumber;
	}
	public void setFacetNumber(String facetNumber) {
		this.facetNumber = facetNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<FilterBean> getListOfFilterBean() {
		return listOfFilterBean;
	}
	
	public boolean addFilterBean(FilterBean filterBean) {
		if (filterBean == null) {
			return false;
		}
		
		return this.listOfFilterBean.add(filterBean);
	}
	
}