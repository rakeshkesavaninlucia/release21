package org.birlasoft.thirdeye.search.api.beans;


/**
 * Bean class for Aid sub block indexing
 * @author samar.gupta
 *
 */
public class IndexAidSubBlockBean {
	
	private String subBlockName;
	private String title;
	private String configValue;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getConfigValue() {
		return configValue;
	}
	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}
	public String getSubBlockName() {
		return subBlockName;
	}
	public void setSubBlockName(String subBlockName) {
		this.subBlockName = subBlockName;
	}
}
