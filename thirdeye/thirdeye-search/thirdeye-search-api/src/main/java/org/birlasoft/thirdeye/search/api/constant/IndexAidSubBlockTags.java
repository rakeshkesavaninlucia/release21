package org.birlasoft.thirdeye.search.api.constant;

/**
 * Standard tag keys that are going to be used in the elastic search JSON objects
 * 
 * @author samar.gupta
 *
 */
public enum IndexAidSubBlockTags {
	
	SUBBLOCKNAME("subBlockName"),
	TITLE("title"),
	CONFIGVALUE("configValue");
	
	
	String tagKey;
	
	IndexAidSubBlockTags (String tagKey){
		this.tagKey = tagKey;
	}

	public String getTagKey() {
		return tagKey;
	}
}
