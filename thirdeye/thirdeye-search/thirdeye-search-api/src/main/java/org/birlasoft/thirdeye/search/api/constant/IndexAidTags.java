package org.birlasoft.thirdeye.search.api.constant;


/**
 * Standard tag keys that are going to be used in the elastic search JSON objects
 * 
 * @author samar.gupta
 *
 */
public enum IndexAidTags {
	
	ID("id"),
	NAME("name"),
	DESCRIPTION("description"),
	AIDTEMPLATE("aidTemplate"),
	BLOCKS("blocks");
	
	
	String tagKey;
	
	IndexAidTags (String tagKey){
		this.tagKey = tagKey;
	}

	public String getTagKey() {
		return tagKey;
	}

	
	
}
