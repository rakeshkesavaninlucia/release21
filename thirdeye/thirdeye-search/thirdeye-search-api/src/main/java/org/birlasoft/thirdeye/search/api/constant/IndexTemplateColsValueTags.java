package org.birlasoft.thirdeye.search.api.constant;

/**
 * Standard tag keys that are going to be used in the elastic search JSON objects
 * 
 * @author tej.sarup
 *
 */
public enum IndexTemplateColsValueTags {

	SEQUENCENUMBER("sequenceNumber"),
	COLUMNDATATYPE("columnDataType"),
	VALUE("value"),
	FILTERABLE("filterable");
		
	String tagKey;
	
	IndexTemplateColsValueTags (String tagKey){
		this.tagKey = tagKey;
	}

	public String getTagKey() {
		return tagKey;
	}

	
	
}
