package org.birlasoft.thirdeye.search.api.beans;

import java.util.List;

/**
 * Bean class for relationship indexing
 * @author samar.gupta
 *
 */
public class IndexAssetParentChildBean {
	
	private List<IndexRelationshipAssetBean> parents;
	private List<IndexRelationshipAssetBean> children;
	
	public List<IndexRelationshipAssetBean> getParents() {
		return parents;
	}
	public void setParents(List<IndexRelationshipAssetBean> parents) {
		this.parents = parents;
	}
	public List<IndexRelationshipAssetBean> getChildren() {
		return children;
	}
	public void setChildren(List<IndexRelationshipAssetBean> children) {
		this.children = children;
	}
}
