package org.birlasoft.thirdeye.search.api.constant;

/**
 * Standard tag keys that are going to be used in the elastic search JSON objects
 * 
 * @author dhruv.sood
 *
 */
public enum IndexBCMParameterTags {

	PARAMETERID("parameterId"),
	QUESTIONNAIREID("questionnaireId"),
	DISPLAYNAME("displayName"),
	UNIQUENAME("uniqueName"),
	TYPE("type"),	
	VALUES("values");
	

	String tagKey;

	IndexBCMParameterTags (String tagKey){
		this.tagKey = tagKey;
	}

	public String getTagKey() {
		return tagKey;
	}	
}