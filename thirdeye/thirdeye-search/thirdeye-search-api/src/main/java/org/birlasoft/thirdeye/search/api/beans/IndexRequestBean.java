package org.birlasoft.thirdeye.search.api.beans;


public class IndexRequestBean{

	private String tenantURL;
	
	private String[] wsIds;
	
	
	public String getTenantURL() {
		return tenantURL;
	}

	public void setTenantURL(String tenantURL) {
		this.tenantURL = tenantURL;
	}

	public String[] getWsIds() {
		return wsIds;
	}

	public void setWsIds(String[] wsIds) {
		this.wsIds = wsIds;
	}
	
}
