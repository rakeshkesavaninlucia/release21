package org.birlasoft.thirdeye.search.api.constant;

/**
 * Standard tag keys that are going to be used in the elastic search JSON objects
 * 
 * @author dhruv.sood
 *
 */
public enum IndexBCMLevelTags {

	BCMLEVELID("bcmLevelId"),	
	BCMLEVELNAME("bcmLevelName"),
	LEVELNUMBER("levelNumber"),
	PARENTBCMLEVELID("parentBCMLevelId"),	
	CATEGORY("category"),
	DEFAULTBCMCOLOR("defaultBCMColor"),
	SEQUENCENUMBER("sequenceNumber"),	
	BCMCOLORS("bcmColors"),
	BCMEVALUATEDVALUE("bcmEvaluatedValue");

	String tagKey;

	IndexBCMLevelTags (String tagKey){
		this.tagKey = tagKey;
	}

	public String getTagKey() {
		return tagKey;
	}	
}