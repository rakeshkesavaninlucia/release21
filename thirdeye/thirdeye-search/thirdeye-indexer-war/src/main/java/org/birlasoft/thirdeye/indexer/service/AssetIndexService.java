/**
 * 
 */
package org.birlasoft.thirdeye.indexer.service;

import java.util.List;

import org.birlasoft.thirdeye.entity.Workspace;

/**
 * @author shaishav.dixit
 *
 */
public interface AssetIndexService {

	public void buildAssetIndex(List<Workspace> setOfWorkspaces, String tenantId);
	
	public void buildIncrementAssetIndex(List<Integer> assetIdlst, String tenantId) ;
	
	public void updateQustionnaireQues(Integer qid, String tenantId) ;
	
	public void buildIncrementAssetRelationIndex(List<Integer> assetIdlst, String tenantId) ;
	
	public void buildIncAssetAidDataIndex(Integer assetTemplateId, String tenantId);
	
	public void updateAssetTemplate(Integer assetTemplateId, String tenantId) ;
	
	public void deleteAsset(Integer assetId, String tenantId) ;
	
	public void updateAssetCostStucture(List<Integer> assetIdlst, String tenantId);
	
	public void updateAssetQuestionnaire(List<Integer> assetIdlst, String tenantId) ; 
	 
	
}
