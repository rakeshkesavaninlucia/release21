package org.birlasoft.thirdeye.indexer.service.impl;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.birlasoft.thirdeye.entity.Aid;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.indexer.service.AssetIndexService;
import org.birlasoft.thirdeye.search.api.beans.IndexParameterBean;
import org.birlasoft.thirdeye.service.AIDService;
import org.birlasoft.thirdeye.service.AssetTemplateService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AssetIndexServiceImplTest {
	
	private static final String TENANT_ID = "demo";
	private static final int ID1 = 1;
	private static final String WORKSPACE_NAME = "Demo workspace";
	private static final String WORKSPACE_DES = "Demo workspace Test";
	
	
	@Mock
	private AssetTemplateService assetTemplateService;
	@Mock
	private AIDService aidService;
	private	AssetIndexService assetIndexService = Mockito.mock(AssetIndexService.class);
	
	private List<Workspace> dummyListOfWorkspace = new ArrayList<>();
	private List<AssetTemplate> dummyListOfTemplates = new ArrayList<>();
	private List<Aid> dummyListOfAid = new ArrayList<>();
	
	private Workspace dummyWorkspace;
	private AssetTemplate dummyAssetTemplate;
	private Aid dummyAid;
	private Asset dummyAsset;
	private IndexParameterBean dummyIndexParameterBean;
	
	@Before
	public void setup(){
		
		setUpWorkspace();
		
		setUpAssetTemplate();
		
		setUpAid();
		
		setupAsset();
		
		setUpIndexParameterBean();
		
		// set workspace in dummyListOfWorkspace
		dummyListOfWorkspace.add(dummyWorkspace);
		
		// set workspace in dummyListOfWorkspace
		dummyListOfTemplates.add(dummyAssetTemplate);
		
		// set workspace in dummyListOfWorkspace
		dummyListOfAid.add(dummyAid);
	}
	
	private void setUpWorkspace(){
		dummyWorkspace = new Workspace();
		dummyWorkspace.setId(ID1);
		dummyWorkspace.setWorkspaceName(WORKSPACE_NAME);
		dummyWorkspace.setWorkspaceDescription(WORKSPACE_DES);
	}
	
	private void setUpAssetTemplate(){
		dummyAssetTemplate = new AssetTemplate() ;
		dummyAssetTemplate.setId(ID1);
		dummyAssetTemplate.setAssetTemplateName("Demo Template");
	}
	
	private void setUpAid(){
		dummyAid = new Aid();
		dummyAid.setId(ID1);
		dummyAid.setName("Demo Aid");
	}
	
	private void setupAsset() {
		dummyAsset = new Asset();
		dummyAsset.setId(ID1);
	}
	
	private void setUpIndexParameterBean(){
		dummyIndexParameterBean = new IndexParameterBean();
		dummyIndexParameterBean.setId(ID1);
	}
	
	private void mockServiceClassMethods() {
		when(assetTemplateService.findByWorkspaceIn(dummyListOfWorkspace)).thenReturn(dummyListOfTemplates);
		when(aidService.findByWorkspaceIn(new HashSet<Workspace>(dummyListOfWorkspace))).thenReturn(dummyListOfAid);
	}
	
	@Test
	public void testBuildAssetIndex(){
		mockServiceClassMethods();
		
		Mockito.doNothing().when(assetIndexService).buildAssetIndex(dummyListOfWorkspace, TENANT_ID);
	}
}
