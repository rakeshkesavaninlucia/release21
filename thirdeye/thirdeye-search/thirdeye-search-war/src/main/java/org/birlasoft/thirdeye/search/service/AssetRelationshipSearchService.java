package org.birlasoft.thirdeye.search.service;

import java.util.List;

import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;

/**
 * @author samar.gupta
 *
 */
public interface AssetRelationshipSearchService {

	/**
	 * Get asset relationship.
	 * @param searchConfig
	 * @return {@code List<IndexAssetBean>}
	 */
	public List<IndexAssetBean> getAssetsRelationship(SearchConfig searchConfig);

}
