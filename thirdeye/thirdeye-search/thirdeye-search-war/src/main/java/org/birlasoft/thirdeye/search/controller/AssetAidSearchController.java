package org.birlasoft.thirdeye.search.controller;

import java.util.Set;

import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.service.AssetAidSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * This controller class for asset aid elastic search.
 * @author samar.gupta
 *
 */
@RestController
public class AssetAidSearchController extends BaseSearchController {
	
	@Autowired
	private AssetAidSearchService assetAidSearchService;
	
	/**
	 * Search asset aid.
	 * @param searchConfig
	 * @return {@code IndexAssetBean}
	 */
	@RequestMapping( value = "/assetAid", method = RequestMethod.POST )
	public IndexAssetBean searchAssetAid(@RequestBody SearchConfig searchConfig){		
		return assetAidSearchService.getAssetAid(searchConfig);
	}

	/**
	 * Get asset ids for Aid
	 * @param searchConfig
	 * @return {@code Set<Integer>}
	 */
	@RequestMapping(value = "/assetAidId", method = RequestMethod.POST)
	public Set<Integer> aidInDisplay(@RequestBody SearchConfig searchConfig) {
		return assetAidSearchService.getAidAssetIds(searchConfig);
	}

	/**
	 * Search asset ids by asset template.
	 * @param searchConfig
	 * @return {@code Set<Integer>}
	 */
	@RequestMapping( value = "/assetTemplate", method = RequestMethod.POST )
	public Set<Integer> searchAssetIdsByAssetTemplate(@RequestBody SearchConfig searchConfig){		
		return assetAidSearchService.getAssetIdsByAssetTemplate(searchConfig);
	}
}
