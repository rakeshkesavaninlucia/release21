package org.birlasoft.thirdeye.search.config;

import java.util.List;

import org.birlasoft.thirdeye.search.interceptor.MultiTenancyInterceptor;
import org.birlasoft.thirdeye.search.support.SearchArgumentResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@EnableWebMvc
@Configuration
@Import({MasterPersistenceConfig.class, PropertyLoader.class})
public class ESWebConfig extends WebMvcConfigurerAdapter{
	
	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		argumentResolvers.add(new SearchArgumentResolver());
	}
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(getMultitenancyInterceptor());
	}

	@Bean
	public HandlerInterceptorAdapter getMultitenancyInterceptor() {
		return new MultiTenancyInterceptor();
	}
}
