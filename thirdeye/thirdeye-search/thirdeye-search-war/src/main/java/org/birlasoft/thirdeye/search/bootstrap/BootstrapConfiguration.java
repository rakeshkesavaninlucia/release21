package org.birlasoft.thirdeye.search.bootstrap;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.search.config.ESWebConfig;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/*@EnableAsync*/
@Configuration
@Import({ESWebConfig.class})
public class BootstrapConfiguration {

	private static Logger logger = Logger.getLogger(BootstrapConfiguration.class);

	@Value("#{systemProperties['" + Constants.ELASTICSEARCH_IP_ADDRESS + "']}")
	private String elasticSearchIpAddress;
	
	public BootstrapConfiguration() {
		logger.info("Initializing the search beans");
	}

	@Bean
	public Client elasticSearchClient(){
		Settings settings = Settings.builder().put("cluster.name", "3rdEye-elasticsearch").build();
		Client client = null;
		try {
			client = new PreBuiltTransportClient(settings)//TransportClient.builder().settings(settings).build()
					.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(elasticSearchIpAddress), 9300));
		} catch (UnknownHostException e) {
			logger.debug("Host not available " , e);
		}

		return client;
	}

}
