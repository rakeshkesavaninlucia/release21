package org.birlasoft.thirdeye.search.controller;

import java.util.List;

import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.api.beans.SearchCostStructureBean;
import org.birlasoft.thirdeye.search.api.beans.SearchQuestionBean;
import org.birlasoft.thirdeye.search.api.beans.TcoAssetBean;
import org.birlasoft.thirdeye.search.service.TcoReportSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TcoReportSearchController extends BaseSearchController {
	
	@Autowired
	private TcoReportSearchService tcoReportService;
	
	@RequestMapping( value = "/tco/cs", method = RequestMethod.POST )
	public List<SearchCostStructureBean> aggregateCostStructure(@RequestBody SearchConfig searchConfig){
		return tcoReportService.getCostStructureAggregateValue(searchConfig);
	}
	
	@RequestMapping( value = "/tco/ce", method = RequestMethod.POST )
	public List<SearchQuestionBean> aggregateCostElement(@RequestBody SearchConfig searchConfig){
		return tcoReportService.getCostElementAggregateValue(searchConfig);
	}
	
	@RequestMapping( value = "/tco/asset", method = RequestMethod.POST )
	public List<IndexAssetBean> getListOfIndexAssetBean(@RequestBody SearchConfig searchConfig){
		return tcoReportService.getListOfAssetbean(searchConfig);
	}
	
	@RequestMapping( value = "/tco/asset/total", method = RequestMethod.POST )
	public List<TcoAssetBean> getTotalCostOfAsset(@RequestBody SearchConfig searchConfig){
		return tcoReportService.getTotalCostOfAsset(searchConfig);
	}
}
