package org.birlasoft.thirdeye.controller.tco;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.beans.tco.TCOAssetWrapper;
import org.birlasoft.thirdeye.beans.tco.TCOSunburstWrapperBean;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.TcoReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
/**
 * Controller for TCO Reports
 * @author dhruv.sood
 *
 */
@Controller
@RequestMapping(value = "/tco/reports")
public class TcoReportController {	

	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private QuestionnaireService questionnaireService;
	@Autowired
	private TcoReportService tcoReportService;
	
	
	/**Method to list all {@code chartOfAccount} for current user
	 * @author dhruv.sood	 * 
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'VIEW_TCO'})")
	public String listOfChartOfAccount(Model model) {
		// List those questionnaires which the user has access to in the active workspace
		Set<Workspace> listOfWorkspaces = new HashSet<>();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
		if(!listOfWorkspaces.isEmpty()){
		    model.addAttribute("listOfQuestionnaire", questionnaireService.findByWorkspaceInAndQuestionnaireType(listOfWorkspaces,QuestionnaireType.TCO.toString()));
		}
		return "tco/viewChartOfAccountReports";
	}
	
	/**Method to view Sunburst Chart
	 * @author dhruv.sood
	 * @param model
	 * @param chartOfAccountId
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/sunburst", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'VIEW_TCO'})")
	public String viewSunburst(Model model, @PathVariable("id") Integer chartOfAccountId) {
		Questionnaire questionnaire = questionnaireService.findOne(chartOfAccountId);
		model.addAttribute("coaName",questionnaire.getName());
		model.addAttribute("chartOfAccountId", chartOfAccountId);		
		return "tco/viewSunburst";
	}
	
	/**
	 * Method for providing the JSON to show the Sunburst/TreeMap report
	 * @author dhruv.sood
	 * @param chartOfAccountID
	 * @return
	 */
	@RequestMapping( value = "/TCOSunburstWrapper", method = RequestMethod.GET )
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'VIEW_TCO'})")
	@ResponseBody
	public TCOSunburstWrapperBean getTCOSunburstWrapper(@RequestParam(value = "ChartOfAccountID") Integer chartOfAccountID) {
		return tcoReportService.getTcoSunburstWrapperBean(chartOfAccountID);
	}
	
	/**Method to view TreeMap
	 * @param model
	 * @param chartOfAccountId
	 * @return {@code String}
	 */
	
	@RequestMapping(value = "/{id}/treeMap", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'VIEW_TCO'})")
	public String viewTreeMap(Model model,  @PathVariable("id") Integer chartOfAccountId) {	
		Questionnaire questionnaire = questionnaireService.findOne(chartOfAccountId);
		model.addAttribute("coaName",questionnaire.getName());
		model.addAttribute("chartOfAccountId", chartOfAccountId);		
		return "tco/viewTreeMap";
	}
	/**
	 * Show tree of coa.
	 * @param model
	 * @param id chart of account
	 * @return {@code String}
	 * method for chart of account js tree.
	 */
	@RequestMapping(value="/{id}/tcoTreeLevels")
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'VIEW_TCO'})")
	public String showBCMLevelsPage(Model model, @PathVariable("id") Integer chartOfAccountID){
		
		TCOSunburstWrapperBean tcobean = tcoReportService.getTcoSunburstWrapperBean(chartOfAccountID);
		model.addAttribute("name", tcobean.getCostStructureName());
		model.addAttribute("listsubcostsrtucture", tcobean.getSubCostStructureList());
		model.addAttribute("cost", tcobean.getCost());
		
		return "tco/tcolevels :: chartOfAccountTreeView";
	}
	/**Method to view TreeMap AssetDetails 
	 * @param model
	 * @param chartOfAccountId
	 * @param costStructureId
	 * @return {@code String}
	 */
	
	@RequestMapping(value = "/treeMap/asset", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'VIEW_TCO'})")
	public String viewAssetoncostStructure(Model model,  @RequestParam(value ="coaId") Integer chartOfAccountId, @RequestParam(value ="id") Integer costStructureId) {	
		                    
		List<TCOAssetWrapper> tcoAssetWrapperList = tcoReportService.getAssetDetailsForTcoReport(chartOfAccountId, costStructureId);
	    model.addAttribute("tcoAssetWrapperList", tcoAssetWrapperList);
		return "tco/treeMapAssetFragment :: treeMapAsset";
	}
	
}
