package org.birlasoft.thirdeye.controller;

import java.util.Collections;
import java.util.List;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.comparator.SequenceNumberComparator;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionnaireConstants;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for {@code order} {@code questions} for {@code questionnaire}.
 */
@Controller
@RequestMapping("/questionnaire")
public class QuestionnaireOrderController {

	private QuestionnaireService questionnaireService;
	private QuestionnaireQuestionService questionnaireQuestionService;
	private final WorkspaceSecurityService workspaceSecurityService;
	
	@Autowired
	private ParameterService parameterService;
	/**
	 * Constructor to initial services
	 * @param questionnaireService
	 * @param questionnaireQuestionService
	 * @param workspaceSecurityService
	 */
	@Autowired
	public QuestionnaireOrderController(QuestionnaireService questionnaireService,
			QuestionnaireQuestionService questionnaireQuestionService,
			WorkspaceSecurityService workspaceSecurityService) {
		this.questionnaireService = questionnaireService;
		this.questionnaireQuestionService = questionnaireQuestionService;
		this.workspaceSecurityService = workspaceSecurityService;
	}

	/**
	 * Create {@code order}{@code questions} for {@code questionnaire}
	 * @param questionnaireId
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/orderquestions", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_MODIFY'})")
	public String orderQuestionsForQuestionnaire(@PathVariable(value = "id") Integer questionnaireId, Model model) {

		// For the questionnaire find existing qq available in the database.
		Questionnaire qe = questionnaireService.findOneFullyLoaded(questionnaireId);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(qe,QuestionnaireType.DEF.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(qe);
		questionnaireQuestionService.refreshQuestionnaireQuestions(qe);
		
		if(qe.getQuestionnaireParameters().isEmpty()){
			List<Parameter> parameterList = parameterService.findByQuestionnaireAndParameterByParentParameterIdIsNull(qe,true) ;
			// Get root parameters of QE
			List<ParameterBean> listOfRootParameters = parameterService.getParametersOfQuestionnaire(parameterList);
			model.addAttribute("error", 1);
			model.addAttribute("rootParameterList", listOfRootParameters);
			model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ID, questionnaireId);
			model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_TYPES, QuestionnaireType.DEF.toString());
			model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION, QuestionnaireType.DEF.getAction());
			model.addAttribute(Constants.PAGE_TITLE, "Questionnaire Management - Parameter Selection for Questionnaire");
			return "questionnaire/questionnaireParameters";
		}		
		
		
		// Fetch the inserted QQ
		List<QuestionnaireQuestion> questionsToBeDisplayed = questionnaireQuestionService.findByQuestionnaire(qe,true);
		
		Collections.sort(questionsToBeDisplayed, new SequenceNumberComparator());
		// By default we prepare the list asset wise
		model.addAttribute("listOfRows", questionnaireQuestionService.getQQListForOrderQuestions(questionsToBeDisplayed));
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ID, questionnaireId);
		
		return "questionnaire/questionnaireorder";
	}
}
