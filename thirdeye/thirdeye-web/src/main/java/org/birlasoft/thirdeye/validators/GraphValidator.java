package org.birlasoft.thirdeye.validators;

import java.util.List;

import org.birlasoft.thirdeye.constant.GraphType;
import org.birlasoft.thirdeye.entity.Graph;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.GraphService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 */
@Component
public class GraphValidator implements Validator {
	private static final String GRAPH_NAME = "graphName";
	
	@Autowired
	private Environment env;
	@Autowired
	private GraphService graphService;
	@Autowired
	private CustomUserDetailsService customUserDetailsService; 
	
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Graph.class.equals(clazz);
	}
                
	@Override
	public void validate(Object target, Errors errors) {
		  Graph graph= (Graph)target;
		  
		 if(graph.getGraphName() != null && graph.getGraphName().isEmpty()){
			    ValidationUtils.rejectIfEmptyOrWhitespace(errors, GRAPH_NAME, "error.graph.name", env.getProperty("error.graph.name"));
		 }else if(graph.getGraphName()!= null && graph.getGraphName().length() > 45){
			 errors.rejectValue(GRAPH_NAME, "error.graph.name.length", env.getProperty("error.graph.name.length")); 
		 }else{		  
			      List<Graph> graphList = graphService.findByWorkspaceAndGraphType(customUserDetailsService.getActiveWorkSpaceForUser(), GraphType.NW.toString());
			      for(Graph oneGraph : graphList){
			    	  if(graph.getId() == null && oneGraph.getGraphName().trim().equals(graph.getGraphName().trim())){
			    		  errors.rejectValue(GRAPH_NAME, "error.graph.name.unique",env.getProperty("error.graph.name.unique"));
			    		  break;
			    	  }
			      }
		  }
		  
	}

}
