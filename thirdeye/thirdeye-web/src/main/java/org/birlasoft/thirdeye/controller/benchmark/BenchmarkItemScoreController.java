package org.birlasoft.thirdeye.controller.benchmark;

import java.util.Set;

import org.birlasoft.thirdeye.entity.BenchmarkItem;
import org.birlasoft.thirdeye.entity.BenchmarkItemScore;
import org.birlasoft.thirdeye.service.BenchmarkItemScoreService;
import org.birlasoft.thirdeye.service.BenchmarkItemService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.birlasoft.thirdeye.validators.BenchmarkItemScoreValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author sunil1.gupta 
 */
@Controller
@RequestMapping(value="/benchmarkItemScore")
public class BenchmarkItemScoreController {
	
	private final WorkspaceSecurityService workspaceSecurityService;
	@Autowired
	private BenchmarkItemService  benchmarkItemService  ;
	@Autowired
	private BenchmarkItemScoreService benchmarkItemScoreService;
	@Autowired
	private BenchmarkItemScoreValidator benchmarkItemScoreValidator;

		
	/**
	 * Constructor to Initialize services
	 * @param workspaceSecurityService
	 */
	@Autowired
	public BenchmarkItemScoreController(WorkspaceSecurityService workspaceSecurityService) {		
		this.workspaceSecurityService = workspaceSecurityService;
	}

	/**
	 * method to create benchmark item Score.
	 * @return {@code String}
	 */
	@RequestMapping(value = "{id}/createItemScore", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BENCHMARK_MODIFY'})")
	public String createBenchmark(Model model,@PathVariable(value = "id") Integer benchmarkItemId) {
		BenchmarkItem benchmarkItem =  benchmarkItemService.findById(benchmarkItemId);
		model.addAttribute("benchmarkId", benchmarkItem.getBenchmark().getId());
		model.addAttribute("benchmarkItemBean", benchmarkItem);
		return "benchmark/createBenchmarkItemScore";
	}
	
	/**
	 * Create new benchmark item score row. 
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/columns/newRow/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BENCHMARK_MODIFY'})")
	public String createBenchmarkItemScoreRow(Model model, @PathVariable(value = "id") Integer idOfBenchmarkItem) {
		BenchmarkItemScore benchmarkItemScore = new BenchmarkItemScore();
		BenchmarkItem benchmarkItem = new BenchmarkItem();
		benchmarkItem.setId(idOfBenchmarkItem);
		benchmarkItemScore.setBenchmarkItem(benchmarkItem);
		model.addAttribute("benchmarkItemScoreBean", benchmarkItemScore);	
		return "benchmark/benchmarkItemScoreFragment :: createRow";
	}

	/**
	 * Edit benchmark item score row.
	 * @param model
	 * @param idOfColumn
	 * @param session
	 * @return {@code String}
	 */
	@RequestMapping(value = "/columns/editRow/{id}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BENCHMARK_MODIFY'})")
	public String editBenchmarkItemScore(Model model, @PathVariable(value = "id") Integer idOfColumn) {		
		
		BenchmarkItemScore benchmarkItemScore = benchmarkItemScoreService.findById(idOfColumn);		
		model.addAttribute("benchmarkItemScoreBean", new BenchmarkItemScore());
		model.addAttribute("oneRow", benchmarkItemScore);
		return "benchmark/benchmarkItemScoreFragment :: editBenchmarkItemScore";
	}

	/**
	 * Save Benchmark Item Score Row.
	 * @param benchmarkItemScoreBean
	 * @param result
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/columns/saveRow", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BENCHMARK_MODIFY'})")
	public String saveBenchmarkItemScoreRow(@ModelAttribute("benchmarkItemScoreBean") BenchmarkItemScore benchmarkItemScoreBean, BindingResult result, Model model) {
		
		Set<BenchmarkItemScore> benchmarkItemScorelist = benchmarkItemScoreService.findByBenchmarkItemId(benchmarkItemScoreBean.getBenchmarkItem().getId());
		if(benchmarkItemScorelist != null){
     	   benchmarkItemScoreBean.getBenchmarkItem().setBenchmarkItemScores(benchmarkItemScorelist);
		}
		benchmarkItemScoreValidator.validate(benchmarkItemScoreBean, result);
		if(result.hasErrors()){
			if(benchmarkItemScoreBean.getId() == null){
			   return "benchmark/benchmarkItemScoreFragment :: createRow";
			}else{
			   model.addAttribute("oneRow", benchmarkItemScoreBean);
			   return "benchmark/benchmarkItemScoreFragment :: editBenchmarkItemScore";
			}
		}
		
		BenchmarkItemScore benchmarkItemScore = benchmarkItemScoreService.saveOrUpdate(benchmarkItemScoreBean);
		model.addAttribute("colid", benchmarkItemScore.getId());	
		model.addAttribute("startYear",benchmarkItemScore.getStartYear());
		model.addAttribute("endYear", benchmarkItemScore.getEndYear());
		model.addAttribute("score", benchmarkItemScore.getScore());
		
		return "benchmark/benchmarkItemScoreFragment :: viewBenchmarkItemScore";
	}
	
	/**
	 * Delete benchmark Item
	 * @param idOfBenchmarkItemScore	
	 */
	@RequestMapping(value = "/columns/deleteRow", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BENCHMARK_MODIFY'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteBenchmarkItem(@RequestParam(value="id") Integer idOfBenchmarkItemScore) {
		benchmarkItemScoreService.deleteBenchmarkItemScore(idOfBenchmarkItemScore);
	}
	
	
}
