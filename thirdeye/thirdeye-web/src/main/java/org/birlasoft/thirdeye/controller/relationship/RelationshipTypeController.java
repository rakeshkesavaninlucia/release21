package org.birlasoft.thirdeye.controller.relationship;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.birlasoft.thirdeye.entity.RelationshipType;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.RelationshipTypeService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.birlasoft.thirdeye.validators.RelationshipTypeValidator;
import org.birlasoft.thirdeye.views.JSONView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/relationship")
public class RelationshipTypeController {
	
	private CustomUserDetailsService customUserDetailsService;
	private RelationshipTypeService relationshipTypeService;
	private RelationshipTypeValidator relationshipTypeValidator;
	private WorkspaceSecurityService workspaceSecurityService;

	@Autowired
	public RelationshipTypeController(CustomUserDetailsService customUserDetailsService,
			RelationshipTypeService relationshipTypeService,
			RelationshipTypeValidator relationshipTypeValidator,
			WorkspaceSecurityService workspaceSecurityService) {
		this.customUserDetailsService = customUserDetailsService;
		this.relationshipTypeService = relationshipTypeService;
		this.relationshipTypeValidator = relationshipTypeValidator;
		this.workspaceSecurityService = workspaceSecurityService;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'RELATIONSHIP_MODIFY'})")
	public String createRelationshipType (Model model, @RequestParam(value="id",required=false) Integer relationshipTypeId) {
		if(relationshipTypeId != null){
			// Check if the user can access this relationship type.
			workspaceSecurityService.authorizeRelationshipTypeAccess(relationshipTypeId);
			RelationshipType relationshipType = relationshipTypeService.findOne(relationshipTypeId);
			model.addAttribute("relationshipTypeForm", relationshipType);
		}else{
			model.addAttribute("relationshipTypeForm", new RelationshipType());
		}
		model.addAttribute("activeWorkspace", customUserDetailsService.getActiveWorkSpaceForUser());
		return "relationship/relationshipModalFragments :: createRelationshipType";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'RELATIONSHIP_MODIFY'})")
	public Object saveRelationshipType (@ModelAttribute("relationshipTypeForm") RelationshipType incomingRelationshipType,
			Model model, HttpServletResponse response, BindingResult result) {
		
		if(incomingRelationshipType.getId() != null && incomingRelationshipType.getId() > 0){
			// Check if the user can access this relationship type.
			workspaceSecurityService.authorizeRelationshipTypeAccess(incomingRelationshipType.getId());
		}
		relationshipTypeValidator.validate(incomingRelationshipType, result);
		if(result.hasErrors()){
			ModelAndView mav = new ModelAndView("relationship/relationshipModalFragments :: createRelationshipType");
			mav.addObject("activeWorkspace", customUserDetailsService.getActiveWorkSpaceForUser());
			return mav;
		}
		
		if(incomingRelationshipType.getId() == null){
			relationshipTypeService.saveOrUpdate(relationshipTypeService.createNewRelationshipType(incomingRelationshipType));
		}else{
			relationshipTypeService.saveOrUpdate(relationshipTypeService.updateRelationshipType(incomingRelationshipType));
		}
		// Now that the save is done simply return the JSON okay and redirect
		Map<String, String> jsonToReturn = new HashMap<String, String>();
		jsonToReturn.put("redirect", "relationship/list");
		
		return JSONView.render(jsonToReturn, response);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'RELATIONSHIP_VIEW'})")
	public String listOfRelationshipType (Model model) {		
		model.addAttribute("listOfRelationshipType", relationshipTypeService.findByWorkspaceIsNullOrWorkspace(customUserDetailsService.getActiveWorkSpaceForUser()));
		return "relationship/listRelationshipType";
	}
}
