package org.birlasoft.thirdeye.validators;

import java.util.List;

import org.birlasoft.thirdeye.beans.QualityGateConditionBean;
import org.birlasoft.thirdeye.entity.QualityGateCondition;
import org.birlasoft.thirdeye.repositories.QualityGateConditionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary}  fields,
 * logic of {@code validation} is here.
 * 
 * @author shaishav.dixit
 *
 */
@Component
public class QualityGateConditionValidator implements Validator {
	
	private static final String ERROR_DESCRIPTION_SAME = "Color Description should be unique";
	
	private static final String ERROR_DESCRIPTION_NULL = "Please enter color Description";

	private static final String ERROR_VALUES_SORTED = "Condition values should be in increasing order";

	private static final String ERROR_CONDITION_NULL = "Please enter all condition values";

	private static final String QUALITY_GATE_PARAMETER_ERROR = "Please select a parameter.";

	private static final String QUALITY_GATE_CONDITION_WEIGHT = "weight";
	
	private static final String ERROR_PARAMETER_DUPLICATE = "This Parameter is already added";
	
	@Autowired
	private Environment env;
	@Autowired
	private QualityGateConditionRepository qualityGateConditionRepo;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return QualityGateConditionBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		QualityGateConditionBean conditionBean = (QualityGateConditionBean) obj;
		if (conditionBean.isWeighted() && conditionBean.getWeight() == null){
			errors.rejectValue(QUALITY_GATE_CONDITION_WEIGHT ,"error.quality.gate.condition.weight",env.getProperty("error.quality.gate.condition.weight"));
		} else if (conditionBean.isWeighted() && isWeightOutOfRange(conditionBean)){
			errors.rejectValue(QUALITY_GATE_CONDITION_WEIGHT ,"error.quality.gate.condition.weight.limit",env.getProperty("error.quality.gate.condition.weight.limit"));
		}
	}

	/**
	 * @param conditionBean
	 * @return
	 */
	private boolean isWeightOutOfRange(QualityGateConditionBean conditionBean) {
		return conditionBean.getWeight() < 0 || conditionBean.getWeight() >1;
	}

	/**
	 * Validate if a condition is added corresponding to a parameter.
	 * @param idOfParam
	 * @param listOfDescription
	 * @return
	 */
	public String validateConditionAddition(Integer idOfParam, List<String> listOfDescription, Integer qualityGate) {
		if(!listOfDescription.isEmpty()){
			for (String oneDescription : listOfDescription) {
				if(oneDescription.isEmpty()){
					return ERROR_DESCRIPTION_NULL;
				}
			}
		}
		if(idOfParam == null){
			return QUALITY_GATE_PARAMETER_ERROR;
		}
		if(idOfParam!= null){
			List<QualityGateCondition> qualityGateCondition = qualityGateConditionRepo.findByQualityGateId(qualityGate);
			for(QualityGateCondition oneQualityGateCond : qualityGateCondition){
				if(oneQualityGateCond.getParameter().getId().equals(idOfParam)){
					return ERROR_PARAMETER_DUPLICATE;
				}
			}
		}
		return null;
	}

	/**
	 * Validate if the condition values are in increasing order.
	 * @param listOfValues
	 * @return
	 */
	public String validateConditionValue(List<Double> listOfValues) {
		if(!listOfValues.isEmpty()){
			for (Double oneValue : listOfValues) {
				if(oneValue == null){
					return ERROR_CONDITION_NULL;
				}
			}
			if(!isIncreasing(listOfValues)){
				return ERROR_VALUES_SORTED;
			}
		}
		return null;
	}

	private boolean isIncreasing(List<Double> listOfValues) {
		for(int i=1; i<listOfValues.size();i++)
	    {
	        if(listOfValues.get(i-1)>listOfValues.get(i))
	            return false;
	    }
	    return true;
	}
	
	/**
	 * Validate if the description is unique .
	 * @param listOfDescription
	 * @return
	 */
	public String validateDescription(List<String> listOfDescription) {
		if(!listOfDescription.isEmpty()){
			for (String oneDescription : listOfDescription) {
				if(oneDescription.isEmpty()){
					return ERROR_DESCRIPTION_NULL;
				}
			}
			if(!isUnique(listOfDescription)){
				return ERROR_DESCRIPTION_SAME;
			}
		}
		return null;
	}
	
	private boolean isUnique(List<String> listOfDescription) {
			for(int i=1; i<listOfDescription.size();i++)
		    {
		        if((listOfDescription.get(i-1)).equals(listOfDescription.get(i)))
		            return false;
		    }
	    return true;
	}
}
