package org.birlasoft.thirdeye.controller.colors;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.birlasoft.thirdeye.beans.JSONQualityGateDescriptionValueMapper;
import org.birlasoft.thirdeye.beans.QualityGateBean;
import org.birlasoft.thirdeye.beans.QualityGateConditionBean;
import org.birlasoft.thirdeye.colorscheme.constant.Condition;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.QualityGate;
import org.birlasoft.thirdeye.entity.QualityGateCondition;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QualityGateConditionService;
import org.birlasoft.thirdeye.service.QualityGateService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.birlasoft.thirdeye.validators.QualityGateConditionValidator;
import org.birlasoft.thirdeye.validators.QualityGateValidator;
import org.birlasoft.thirdeye.views.JSONView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Controller for creating new quality gate, viewing and updating existing quality gates 
 * and quality gate conditions. 
 * 
 * @author shaishav.dixit
 *
 */
@Controller
@RequestMapping("/qualityGates")
public class QualityGateController {
	
	private static final String QUALITY_GATE_COLORSCALE_MODEL = "colorScale";
	private static final String QUALITY_GATE_COLORSCALE_COUNT_MODEL = "colorScaleCount";
	private static final String QUALITY_GATE_DESCRIPTION_MODEL = "qualityGateDescription";
	private static final String QUALITY_GATE_CONDITION_MODEL = "qualityGateCondition";
	private static final String CREATE_QUALITY_GATE_FRAGMENT = "qualityGates/qualityGateFragments :: createQualityGate";
	private QualityGateValidator qualityGateValidator;
	private QualityGateService qualityGateService;
	private CustomUserDetailsService customUserDetailsService;
	private ParameterService parameterService;
	private QualityGateConditionService qualityGateConditionService;
	private QualityGateConditionValidator qualityGateConditionValidator;
	private WorkspaceSecurityService workspaceSecurityService;

	@Autowired	
	public QualityGateController(QualityGateValidator qualityGateValidator,
			QualityGateService qualityGateService,
			CustomUserDetailsService customUserDetailsService,
			ParameterService parameterService,
			QualityGateConditionService qualityGateConditionService,
			QualityGateConditionValidator qualityGateConditionValidator,
			WorkspaceSecurityService workspaceSecurityService) {
		this.qualityGateValidator = qualityGateValidator;
		this.qualityGateService = qualityGateService;
		this.customUserDetailsService = customUserDetailsService;
		this.parameterService = parameterService;
		this.qualityGateConditionService = qualityGateConditionService;
		this.qualityGateConditionValidator = qualityGateConditionValidator;
		this.workspaceSecurityService = workspaceSecurityService;
	}

	/**
	 * Create new quality gate
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'COLOR_SCHEME_VIEW'})")
	public String createQualityGate(Model model){
		model.addAttribute("qualityGateForm", new QualityGateBean());
		return CREATE_QUALITY_GATE_FRAGMENT;
	}

	/**
	 * View all asset quality gates. This list will not include any parameter level quality 
	 * gate.
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'COLOR_SCHEME_VIEW'})")
	public String viewAssetQualityGates(Model model){
		model.addAttribute("listOfQualityGates", qualityGateService.findAssetQualityGates());
		return "qualityGates/viewQualityGate";
	}

	/**
	 * Save or update quality gate.
	 * @param incomingQualityGate
	 * @param result
	 * @param model
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'COLOR_SCHEME_VIEW'})")
	public Object saveAssetQualityGates(@ModelAttribute("qualityGateForm") QualityGateBean incomingQualityGate, BindingResult result, HttpServletResponse response){
		qualityGateValidator.validate(incomingQualityGate, result);
		if(result.hasErrors()){
			return CREATE_QUALITY_GATE_FRAGMENT;
		}

		QualityGate qualityGate;
		if (incomingQualityGate.getId() == null){
			//create new quality gate
			qualityGate = qualityGateService.createQualityGateNewObject(incomingQualityGate);
		} else {
			//update existing quality gate
			qualityGate = qualityGateService.updateQualityGateObject(incomingQualityGate);
		}
		qualityGate = qualityGateService.save(qualityGate);

		Map<String, String> jsonToReturn = new HashMap<>();
		if (incomingQualityGate.getId() == null){
			//redirect to create condition screen after creation of new quality gate
			jsonToReturn.put("redirect", "qualityGates/editCondition/" + qualityGate.getId());
		} else {
			//redirect to quality gate list after updating
			jsonToReturn.put("redirect", "qualityGates/view");
		}

		return JSONView.render(jsonToReturn, response);
	}

	/**
	 * Add new row for quality gate condition.
	 * @param model
	 * @param idOfParam
	 * @param scale
	 * @param qualityGate
	 * @param listOfDescription
	 * @return
	 */
	@RequestMapping(value = "/newRow", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'COLOR_SCHEME_VIEW'})")
	public String newRowForQualityGateCondition(Model model, @RequestParam(value = "colordescription[]", required = false) List<String> listOfDescription, @RequestParam(value = "parameterId") Integer idOfParam, @RequestParam(value = "scale[]") List<String> scale,
			@RequestParam(value = "qualityGateId") Integer qualityGate) {
		String error = null;
		if(listOfDescription != null ){
		error = qualityGateConditionValidator.validateConditionAddition(idOfParam, listOfDescription, qualityGate);
		}
		if(error != null){
			model.addAttribute("paramError", error);
			return "qualityGates/qualityGateFragments :: parameterError";
		}
		model.addAttribute("parameter", parameterService.findOne(idOfParam));
		model.addAttribute(QUALITY_GATE_COLORSCALE_MODEL, scale);
		model.addAttribute(QUALITY_GATE_COLORSCALE_COUNT_MODEL, scale.size());
		QualityGateConditionBean qgc = generateConditionBeanForNewRow(idOfParam, qualityGate);
		model.addAttribute(QUALITY_GATE_CONDITION_MODEL, qgc);
		return "qualityGates/qualityGateFragments :: createRow";
	}

	/**
	 * @param idOfParam
	 * @param qualityGate
	 * @return
	 */
	private QualityGateConditionBean generateConditionBeanForNewRow(Integer idOfParam, Integer qualityGate) {
		QualityGateConditionBean qgc = new QualityGateConditionBean();
		qgc.setParameterId(idOfParam);
		qgc.setCondition(Condition.LESS);
		qgc.setQualityGateId(qualityGate);
		qgc.setWeighted(qualityGateConditionService.isWeightedCondition(qualityGate));
		return qgc;
	}

	/**
	 * Save single quality gate condition.
	 * @param qualityGateConditionBean
	 * @param result
	 * @param model
	 * @param listOfValues
	 * @param listOfColors
	 * @return
	 */
	@RequestMapping(value = "/saveRow", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'COLOR_SCHEME_VIEW'})")
	public String saveQualityGateCondition(@ModelAttribute(QUALITY_GATE_CONDITION_MODEL) QualityGateConditionBean qualityGateConditionBean, BindingResult result, 
			Model model, @RequestParam(value = "value[]") List<Double> listOfValues, @RequestParam(value = "color[]") List<String> listOfColors) {
		String error = qualityGateConditionValidator.validateConditionValue(listOfValues);
		if(error != null){
			model.addAttribute("conditionError", error);
			return "qualityGates/qualityGateFragments :: conditionError";
		}
		if(listOfValues.size() != listOfColors.size()){
			listOfValues.add(null);
		}
		qualityGateConditionBean = qualityGateConditionService.updateBean(qualityGateConditionBean);
		qualityGateConditionBean = qualityGateConditionService.insertValueMapperList(qualityGateConditionBean, listOfValues, listOfColors);
		qualityGateConditionValidator.validate(qualityGateConditionBean, result);
		if(result.hasErrors()){
			model.addAttribute("parameter", parameterService.findOne(qualityGateConditionBean.getParameterId()));
			model.addAttribute(QUALITY_GATE_COLORSCALE_MODEL, listOfColors);
			model.addAttribute(QUALITY_GATE_CONDITION_MODEL, qualityGateConditionBean);
			return "qualityGates/qualityGateFragments :: createRow";
		}
		QualityGateCondition qualityGateCondition;
		if (qualityGateConditionBean.getId() == null){
			qualityGateCondition = qualityGateConditionService.createConditionObject(listOfValues,listOfColors, qualityGateConditionBean);
		} else {
			qualityGateCondition = qualityGateConditionService.updateConditionObject(qualityGateConditionBean, listOfValues, listOfColors);
		}
		qualityGateCondition = qualityGateConditionService.save(qualityGateCondition);
		createConditionModel(qualityGateCondition, model);
		return "qualityGates/qualityGateFragments :: viewOnlyCondition";
	}

	private void createConditionModel(QualityGateCondition aqgc, Model model) {
		model.addAttribute("qgCondition", qualityGateConditionService.createConditionBean(aqgc));
	}

	/**
	 * Edit quality gate conditions. 
	 * @param model
	 * @param idOfQualityGate
	 * @return
	 */
	@RequestMapping(value = "/editCondition/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'COLOR_SCHEME_VIEW'})")
	public String editConditionForQualityGate(Model model, @PathVariable(value="id")Integer idOfQualityGate){

		checkAccessToQualityGate(idOfQualityGate);

		List<QualityGateConditionBean> listOfConditionBean = qualityGateConditionService.getAllConditions(idOfQualityGate);	

		int colorScale = 0;
		if (!listOfConditionBean.isEmpty()){
			colorScale = listOfConditionBean.get(0).getValueMapper().size();
		}

		List<JSONQualityGateDescriptionValueMapper> qgDescription = qualityGateService.getQGBean(idOfQualityGate).getDescription();

		int colorScaleDesc = 0; 
		if (qgDescription != null){
			colorScaleDesc = qgDescription.size();
		}

		Set<Workspace> setOfWorkspaces = new HashSet<> ();
		setOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());

		Set<Parameter> setOfParameter = parameterService.findByWorkspaceIsNullOrWorkspaceIn(setOfWorkspaces);

		model.addAttribute("colorScaleDesc", colorScaleDesc);
		model.addAttribute(QUALITY_GATE_COLORSCALE_COUNT_MODEL, colorScale);
		model.addAttribute("listOfConditions", listOfConditionBean);
		model.addAttribute("qualityGate", idOfQualityGate);
		model.addAttribute("qualityGateName", qualityGateService.findOne(idOfQualityGate).getName());
		model.addAttribute("listOfParameters", setOfParameter);
		return "qualityGates/qualityGateConditions";
	}

	/**
	 * Check quality gate for workspace security
	 * @param idOfQualityGate
	 */
	private void checkAccessToQualityGate(Integer idOfQualityGate) {
		QualityGate qg = qualityGateService.findOne(idOfQualityGate);
		workspaceSecurityService.authorizeQualityGate(qg);
	}

	/**
	 * Edit quality gate name.
	 * @param model
	 * @param idOfQualityGate
	 * @return
	 */
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'COLOR_SCHEME_VIEW'})")
	public String editQualityGate(Model model, @PathVariable(value="id")Integer idOfQualityGate){
		checkAccessToQualityGate(idOfQualityGate);

		QualityGateBean qualityGateBean = new QualityGateBean(qualityGateService.findOne(idOfQualityGate));
		model.addAttribute("qualityGateForm", qualityGateBean);
		return CREATE_QUALITY_GATE_FRAGMENT;
	}

	/**
	 * Edit quality gate condition. Can change color conditions and weight.
	 * @param model
	 * @param idOfQualityGateCondition
	 * @return
	 */
	@RequestMapping(value = "/editRow/{id}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'COLOR_SCHEME_VIEW'})")
	public String editQualityGateCondition(Model model, @PathVariable(value="id")Integer idOfQualityGateCondition) {
		QualityGateConditionBean qualityGateConditionBean = qualityGateConditionService.getConditionBean(idOfQualityGateCondition);
		model.addAttribute(QUALITY_GATE_CONDITION_MODEL, qualityGateConditionBean);
		return "qualityGates/qualityGateFragments :: editRow";
	}

	/**
	 * Delete single quality gate condition.
	 * @param idOfQualityGateCondition
	 */
	@RequestMapping(value = "/deleteRow", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'COLOR_SCHEME_VIEW'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteQualityGateCondition(@RequestParam(value="id")Integer idOfQualityGateCondition) {
		qualityGateConditionService.delete(idOfQualityGateCondition);
	}

	/**
	 * Delete quality gate and all it's associated conditions.
	 * @param idOfQualityGate
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'COLOR_SCHEME_VIEW'})")
	public String deleteQualityGate(@PathVariable(value="id")Integer idOfQualityGate) {
		checkAccessToQualityGate(idOfQualityGate);
		qualityGateService.delete(idOfQualityGate);
		return "redirect:/qualityGates/view";
	}

	/**
	 * Add color Description to quality gate
	 * @param model
	 * @param idOfQualityGate
	 * @param scale
	 * @param action1
	 * @return
	 */
	@RequestMapping(value = "/addDescription", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'COLOR_SCHEME_VIEW'})")
	public String addDescription(Model model, @RequestParam(value = "qualityGateId") Integer idOfQualityGate, @RequestParam(value = "scale[]") List<String> scale,
			@RequestParam(value = "action", required = false) String action, @RequestParam(value = "action1", required = false) String action1) {
		QualityGateBean qualityGateBean = qualityGateService.getQGBean(idOfQualityGate);
		if(action == null && qualityGateBean.getDescription() != null && action1 == null){
			model.addAttribute(QUALITY_GATE_DESCRIPTION_MODEL, qualityGateBean.getDescription());
			return "qualityGates/qualityGateFragments :: viewDescription";
		}
		
		model.addAttribute(QUALITY_GATE_COLORSCALE_MODEL, scale);
		model.addAttribute(QUALITY_GATE_COLORSCALE_COUNT_MODEL, scale.size());
		return "qualityGates/qualityGateFragments :: colorDescription";
	}

	/**
	 * Save quality gate description.
	 * @param model
	 * @param idOfQualityGate
	 * @param listOfDescription
	 * @param listOfColors
	 * @return
	 */
	@RequestMapping(value = "/saveDescription", method = {RequestMethod.POST,RequestMethod.GET})
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'COLOR_SCHEME_VIEW'})")
	public String saveDescription(Model model, @RequestParam(value = "qualityGateId") Integer idOfQualityGate, @RequestParam(value = "description[]") List<String> listOfDescription, @RequestParam(value = "color[]") List<String> listOfColors) {
		String error = qualityGateConditionValidator.validateDescription(listOfDescription);
		if(error != null){
			model.addAttribute("descriptionError", error);
			return "qualityGates/qualityGateFragments :: descriptionError";
		}

		QualityGate qg = qualityGateService.findOne(idOfQualityGate);
		qg = qualityGateService.save(qualityGateService.insertDescriptionMapperList(qg, listOfDescription, listOfColors));
		QualityGateBean qgb = qualityGateService.createQGBean(qg);

		model.addAttribute(QUALITY_GATE_DESCRIPTION_MODEL, qgb.getDescription());
		model.addAttribute("colorScaleCount", listOfColors.size());
		return "qualityGates/qualityGateFragments :: viewDescription";
	}

	/**
	 * Edit quality gate description. 
	 * @param model
	 * @param idOfQualityGate
	 * @return
	 */
	@RequestMapping(value = "/editDescription", method = {RequestMethod.POST,RequestMethod.GET})
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'COLOR_SCHEME_VIEW'})")
	public String editQualityGateDescription(Model model, @RequestParam(value = "qualityGateId") Integer idOfQualityGate) {
		QualityGateBean qualityGateBean = qualityGateService.getQGBean(idOfQualityGate);
		model.addAttribute(QUALITY_GATE_DESCRIPTION_MODEL, qualityGateBean.getDescription());
		return "qualityGates/qualityGateFragments :: editDescription";
	}
}
