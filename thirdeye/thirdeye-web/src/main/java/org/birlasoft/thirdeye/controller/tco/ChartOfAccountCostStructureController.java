package org.birlasoft.thirdeye.controller.tco;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionnaireConstants;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Controller for Chart Of Account {@code show costStructure} page to {@code load tree} , add a new {@code costSturcture}  to save , get {@code model}
 * content for {@code adding costStructure} , get {@code model} content for {@code adding questions} , get {@code model}
 * for {@code updating} {@code costStructure} {@code function}
 *
 */
@Controller
@RequestMapping(value="/tco")
public class ChartOfAccountCostStructureController {
	
	private ParameterService parameterService;
	private QuestionnaireService questionnaireService;
	private final WorkspaceSecurityService workspaceSecurityService;	
	private QuestionnaireParameterService qpService;

	/**
	 * Constructor to initialize services 
	 * @param parameterService
	 * @param parameterFunctionService
	 * @param questionnaireService
	 * @param questionService
	 * @param workspaceSecurityService
	 * @param qpService
	 */
	@Autowired
	public ChartOfAccountCostStructureController(ParameterService parameterService,			
			QuestionnaireService questionnaireService,			
			WorkspaceSecurityService workspaceSecurityService,QuestionnaireParameterService qpService){
		this.parameterService = parameterService;
		this.questionnaireService = questionnaireService;	
		this.workspaceSecurityService = workspaceSecurityService;
		this.qpService = qpService;
	}
	
	/**
	 * Show ChartOfAccount parameter page to load tree.
	 * @param model
	 * @param idOfChartOfAccount
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/coststructures", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	public String showCOACostStructurePage(Model model,  @PathVariable(value = "id") Integer idOfChartOfAccount) {
		
		// Prepare the view for the parameters that are on the questionnaire.
		Questionnaire coa = questionnaireService.findOne(idOfChartOfAccount);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(coa,QuestionnaireType.TCO.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(coa);
		List<Parameter> parameterList = parameterService.findByQuestionnaireAndParameterByParentParameterIdIsNull(coa,true) ;	
		// Get root parameters of QE
		List<ParameterBean> listOfRootParameters = parameterService.getParametersOfQuestionnaire(parameterList);
		
		model.addAttribute("rootParameterList", listOfRootParameters);
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ID, idOfChartOfAccount);
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_TYPES, QuestionnaireType.TCO.toString());
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION,QuestionnaireType.TCO.getAction());
		model.addAttribute(Constants.PAGE_TITLE, "TCO Management - Cost Structure Selection for Chart of Account");
		
		return "questionnaire/questionnaireParameters";
	}

	/** 
	 * Delete node from tree.
	 * @param model
	 * @param idOfChartOfAccount
	 * @param parentParameterId
	 * @param parameterId
	 * @param questionId
	 */
	@RequestMapping(value = "{id}/parameterquestion/deleteNode", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteNodeFromTree(@PathVariable(value = "id") Integer idOfChartOfAccount,
										@RequestParam(value = "paramId", required=false) Integer parameterId){		
	
		Questionnaire chartOfAccount = questionnaireService.findOne(idOfChartOfAccount);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(chartOfAccount,QuestionnaireType.TCO.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(chartOfAccount);		
		// Are we going to delete a question ?
		if (idOfChartOfAccount != null && parameterId != null){
			Parameter parameter = parameterService.findFullyLoaded(parameterId);
			Set<Parameter> parameters = new HashSet<>();
			parameters.add(parameter);
			questionnaireService.updateQuestionnaireEntity(chartOfAccount, null,parameters);
		}
	   
	
	}
	
    /**
     * method to save QuestionnaireParameter
     * @param model
     * @param result
     * @param chartOfAccountId
     * @param setOfParameters
     * @param session
     * @return
     */
	@RequestMapping(value="/{id}/qparameters/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	public String saveChartOfAccountCostStructure(Model model,@PathVariable(value = "id") Integer chartOfAccountId, @RequestParam(value = "parameter[]") Set<Integer> setOfParameters) {
				
		Questionnaire chartOfAccount = questionnaireService.findOneFullyLoaded(chartOfAccountId);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(chartOfAccount,QuestionnaireType.TCO.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(chartOfAccount);		
		List<Parameter> listOfParametersToUpdate = parameterService.findFullyLoadedParametersByIds(setOfParameters);		
		qpService.updateParametersOnQuestionnaire(chartOfAccount, listOfParametersToUpdate);	
		
		List<Parameter> parameterList = parameterService.findByQuestionnaireAndParameterByParentParameterIdIsNull(chartOfAccount,true) ;	
		// Get root parameters of QE
		List<ParameterBean> listOfRootParameters = parameterService.getParametersOfQuestionnaire(parameterList);
		model.addAttribute("rootParameterList", listOfRootParameters);
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ID, chartOfAccountId);
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_TYPES, QuestionnaireType.TCO.toString());
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION, QuestionnaireType.TCO.getAction());
		return "questionnaire/questionnaireParameters :: questionnaireParameterFragment(rootParameterList=${rootParameterList})";
	}

}
