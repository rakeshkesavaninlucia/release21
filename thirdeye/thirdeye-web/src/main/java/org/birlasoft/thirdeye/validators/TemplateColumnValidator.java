package org.birlasoft.thirdeye.validators;

import java.util.Set;

import org.birlasoft.thirdeye.constant.DataType;
import org.birlasoft.thirdeye.entity.AssetTemplateColumn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 */
@Component
public class TemplateColumnValidator implements Validator {
	
	private static final String LENGTH = "length";
	private static final String ERROR_TEMPLATE_COLUMN_NAME_UNIQUE = "error.template.column.name.unique";
	private static final String ASSET_TEMPLATE_COLUMN = "assetTemplateColName";
	private Environment env;

	/**
	 * Constructor autowiring
	 * @param env
	 */
	@Autowired
	public TemplateColumnValidator(Environment env) {
		this.env = env;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return AssetTemplateColumn.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		AssetTemplateColumn assetTemplateColumn = (AssetTemplateColumn) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, ASSET_TEMPLATE_COLUMN, "error.template.column.name", env.getProperty("error.template.column.name"));
		
		Set<AssetTemplateColumn> assetTemplateColumns = assetTemplateColumn.getAssetTemplate().getAssetTemplateColumns();
		for (AssetTemplateColumn assetTemplateColumn2 : assetTemplateColumns) {
			if (assetTemplateColumn.getId() == null) {
				if (assetTemplateColumn.getAssetTemplateColName().equalsIgnoreCase(assetTemplateColumn2.getAssetTemplateColName())) {
					errors.rejectValue(ASSET_TEMPLATE_COLUMN, ERROR_TEMPLATE_COLUMN_NAME_UNIQUE,env.getProperty(ERROR_TEMPLATE_COLUMN_NAME_UNIQUE));
				}

			} else if (assetTemplateColumn.getId() != null) {
				if (assetTemplateColumn.getId().equals(assetTemplateColumn2.getId()) && assetTemplateColumn.getAssetTemplateColName().equalsIgnoreCase(assetTemplateColumn2.getAssetTemplateColName())) {
					//do nothing
				} else if (assetTemplateColumn.getAssetTemplateColName().equalsIgnoreCase(assetTemplateColumn2.getAssetTemplateColName())) {
					errors.rejectValue(	ASSET_TEMPLATE_COLUMN, ERROR_TEMPLATE_COLUMN_NAME_UNIQUE,env.getProperty(ERROR_TEMPLATE_COLUMN_NAME_UNIQUE));
				}
			}

		}
		if (assetTemplateColumn.getId() == null && "select".equals(assetTemplateColumn.getDataType())) {
			errors.rejectValue("dataType", "error.template.datatype",env.getProperty("error.template.datatype"));
		}
		validateColumnLength(errors, assetTemplateColumn);
	}

	/**
	 * Validate template column length
	 * @param errors
	 * @param assetTemplateColumn
	 */
	private void validateColumnLength(Errors errors, AssetTemplateColumn assetTemplateColumn) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, LENGTH, "error.template.column.empty.length", env.getProperty("error.template.column.empty.length"));
		if (assetTemplateColumn.getLength()!= null && assetTemplateColumn.getLength() < 0) {
			errors.rejectValue(LENGTH, "error.template.length",env.getProperty("error.template.length"));
		}else if(assetTemplateColumn.getLength()!= null && !assetTemplateColumn.getDataType().equals(DataType.DATE.name()) && assetTemplateColumn.getLength() == 0){
			errors.rejectValue(LENGTH, "error.template.column.zero.length",env.getProperty("error.template.column.zero.length"));
		}
	}
	
}

