package org.birlasoft.thirdeye.validators;

import java.util.List;

import org.birlasoft.thirdeye.beans.AssetTemplateBean;
import org.birlasoft.thirdeye.constant.AssetTypes;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.service.AssetTemplateService;
import org.birlasoft.thirdeye.service.AssetTypeService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 */
@Component
public class TemplateValidator implements Validator {

	private static final String ERROR_TEMPLATE_NAME_UNIQUE = "error.template.name.unique";
	private static final String ASSET_TEMPLATE_NAME = "assetTemplateName";
	private AssetTemplateService assetTemplateService;
	private Environment env;
	private AssetTypeService assetTypeService;
	private CustomUserDetailsService customUserDetailsService;
	
	/**
	 * Constructor autowiring of dependencies
	 * @param assetTemplateService
	 * @param env
	 * @param assetTypeService
	 * @param customUserDetailsService
	 */
	@Autowired
	public TemplateValidator(AssetTemplateService assetTemplateService,
			Environment env,
			AssetTypeService assetTypeService,
			CustomUserDetailsService customUserDetailsService) {
		this.assetTemplateService = assetTemplateService;
		this.env = env;
		this.assetTypeService = assetTypeService;
		this.customUserDetailsService = customUserDetailsService;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return AssetTemplateBean.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		AssetTemplateBean assetTemplateBean = (AssetTemplateBean) target;
		if(assetTemplateBean.getAssetTypeId() == -1){
			errors.rejectValue("assetTypeId" ,"error.assetType.select",env.getProperty("error.assetType.select"));
		}else if(assetTemplateBean.getAssetTemplateName() != null && assetTemplateBean.getAssetTemplateName().isEmpty()){
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, ASSET_TEMPLATE_NAME, "error.template.name", env.getProperty("error.template.name"));
		}else if(assetTemplateBean.getAssetTemplateName()!= null && assetTemplateBean.getAssetTemplateName().length() > 45){
			
			 errors.rejectValue(ASSET_TEMPLATE_NAME, "error.template.name.length", env.getProperty("error.template.name.length")); 
		 }else {
			List<AssetTemplate> listOfTemplate = assetTemplateService.findByAssetTemplateNameAndAssetTypeAndUserByCreatedBy(assetTemplateBean.getAssetTemplateName(), assetTypeService.findOne(assetTemplateBean.getAssetTypeId()), customUserDetailsService.getCurrentUser());
			validateUniqueAssetTemplate(errors, assetTemplateBean, listOfTemplate);
		}
		
		if(assetTemplateBean.getAssetTypeId() > 0 && assetTypeService.findOne(assetTemplateBean.getAssetTypeId()).getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())){
			  if (assetTemplateBean.getParentAssetTypeId() == -1) {
				  errors.rejectValue("parentAssetTypeId", "rsuggestion.error.parentassettype");
			  }
			  if (assetTemplateBean.getRelationshipTypeId() == -1) {
				  errors.rejectValue("relationshipTypeId", "rsuggestion.error.relationshiptype");
			  }
			  if (assetTemplateBean.getChildAssetTypeId() == -1) {
				  errors.rejectValue("childAssetTypeId", "rsuggestion.error.childassettype");
			  }
		}
	}

	/**
	 * @param errors
	 * @param assetTemplateBean
	 * @param listOfTemplate
	 */
	private void validateUniqueAssetTemplate(Errors errors, AssetTemplateBean assetTemplateBean, List<AssetTemplate> listOfTemplate) {
		for (AssetTemplate oneTemplate : listOfTemplate) {
			if (isNewTemplateUnique(assetTemplateBean, oneTemplate) || isUpdatingTemplateUnique(assetTemplateBean, oneTemplate)) {
				errors.rejectValue(ASSET_TEMPLATE_NAME,ERROR_TEMPLATE_NAME_UNIQUE,env.getProperty(ERROR_TEMPLATE_NAME_UNIQUE));
				break;
			}
		}
	}

	/**
	 * @param assetTemplateBean
	 * @param oneTemplate
	 * @return
	 */
	private boolean isUpdatingTemplateUnique(
			AssetTemplateBean assetTemplateBean, AssetTemplate oneTemplate) {
		return assetTemplateBean.getId() != null && !assetTemplateBean.getId().equals(oneTemplate.getId()) 
				&& assetTemplateBean.getAssetTemplateName().trim().equalsIgnoreCase(oneTemplate.getAssetTemplateName());
	}

	/**
	 * @param assetTemplateBean
	 * @param oneTemplate
	 * @return
	 */
	private boolean isNewTemplateUnique(AssetTemplateBean assetTemplateBean,
			AssetTemplate oneTemplate) {
		return assetTemplateBean.getId() == null && oneTemplate.getAssetTemplateName().trim().equalsIgnoreCase(assetTemplateBean.getAssetTemplateName().trim());
	}
}
