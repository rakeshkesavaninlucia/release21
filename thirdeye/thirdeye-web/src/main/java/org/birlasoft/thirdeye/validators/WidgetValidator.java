package org.birlasoft.thirdeye.validators;
import org.birlasoft.thirdeye.beans.widgets.BaseWidgetJSONConfig;
import org.birlasoft.thirdeye.entity.Dashboard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 */
@Component
public class WidgetValidator implements Validator {
	
	@Autowired
	private Environment env;
	
	@Override
	public boolean supports(Class<?> clazz) {
	// TODO Auto-generated method stub
	    return BaseWidgetJSONConfig.class.equals(clazz);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
	    BaseWidgetJSONConfig newWidget = (BaseWidgetJSONConfig)target;
		if(StringUtils.isEmpty(newWidget.getTitle().trim())){
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "widget.errors.title.empty", env.getProperty("widget.errors.title.empty"));
	    }else
	    {		  
		 // Currently we are not validating on the unique name for dashboards
	  }
		
	  }

}
	