package org.birlasoft.thirdeye.config.packages;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@ComponentScan({"org.birlasoft.**.service"})
@EnableTransactionManagement
public class ServicesConfig {

}
