package org.birlasoft.thirdeye.controller.parameter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.asset.AssetChartRequestParamBean;
import org.birlasoft.thirdeye.beans.parameter.ParamDrillDownBean;
import org.birlasoft.thirdeye.service.AssetParameterHealthService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(value="/parameter/drill")
public class ParameterDrillDownModalController {

	@Autowired
	private AssetParameterHealthService aphService;
		
	@RequestMapping(value = "/down", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'ASSET_FILLDETAILS'})")
	public  String drillDownOnParam (@ModelAttribute("assetChartRequestParamBean") AssetChartRequestParamBean chartRequestParamBean, Model model){
		
		// TODO: Does the user have access to workspace of param - questionnaire
		// TODO: Need to add some permission for the drill down feature	
		 ParameterBean paramBean = aphService.getParameterBean(chartRequestParamBean);
		 if(paramBean.getParent() != null){
		   chartRequestParamBean.setParentParameterId(paramBean.getParent().getId());
		   if(paramBean.getParent().getParent() != null){
			   chartRequestParamBean.setParentParentParameterId(paramBean.getParent().getParent().getId());  
		   }
		 }
		 model.addAttribute("paramName",paramBean.getDisplayName());
		 model.addAttribute("cRParamBean",chartRequestParamBean);
		return "param/paramDrillDownModal :: drillDownModal";
	}
	
	@RequestMapping(value = "/down/response", method = { RequestMethod.POST, RequestMethod.GET })
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'ASSET_FILLDETAILS'})")
	@ResponseBody
	public  Map<String,Object> prepareParamResponse (@ModelAttribute("assetChartRequestParamBean") AssetChartRequestParamBean chartRequestParamBean){
		
		// TODO: Does the user have access to workspace of param - questionnaire
		// TODO: Need to add some permission for the drill down feature		
		Map<String,Object> mapOfJsondata = new HashMap<>();
		List<ParamDrillDownBean> paramDrillDownList = aphService.getAssetParamDrillDownData(chartRequestParamBean);
		mapOfJsondata.put("data",paramDrillDownList);
		
		return mapOfJsondata;
		
	}
	
	
	
	
	
}
