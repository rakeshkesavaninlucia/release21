package org.birlasoft.thirdeye.controller.admin;

import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.entity.Aid;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.Graph;
import org.birlasoft.thirdeye.entity.GraphAssetTemplate;
import org.birlasoft.thirdeye.service.AIDService;
import org.birlasoft.thirdeye.service.AdminReportsService;
import org.birlasoft.thirdeye.service.AssetTypeService;
import org.birlasoft.thirdeye.service.GraphService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AdminReportController {

	
	
	@Autowired
	private AIDService aidService;

	@Autowired
	private GraphService graphService;
	@Autowired
	private AssetTypeService assetTypeService;
	@Autowired
	private AdminReportsService adminReportsService;

	@RequestMapping(value = "/adminreports", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'ADMIN_VIEW'})")
	public String showAdminPage(Model model) {
      
		model.addAttribute("assetTypeList", assetTypeService.findAll());
		return "admin/reports";

	}

	@RequestMapping(value = "/adminaid", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'AID_VIEW'})")
	public String getAids(Model model, @RequestParam(value = "assetTypeId") Integer assetTypeId) {
		

		List<AssetTemplate> listOfAssetTemplate = adminReportsService.findByAssetTypeid(assetTypeId);

		List<Aid> listOfAids = new ArrayList<>();
		for (AssetTemplate assetTemplate : listOfAssetTemplate) {
			listOfAids.addAll(aidService.findByAssetTemplate(assetTemplate));

		}

		model.addAttribute("listOfAids", listOfAids);
		return "admin/listAdminAID";
	}
	
	@RequestMapping(value = "/adminadd", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'REPORT_ASSET_GRAPH'})")
	public String getAllGraphs(Model model, @RequestParam(value = "assetTypeId") Integer assetTypeId) {
		// Fetch the user's active workspaces

		List<AssetTemplate> listOfAssetTemplate = adminReportsService.findByAssetTypeid(assetTypeId);
		List<Graph> listOfGraphs = new ArrayList<>();
		List<GraphAssetTemplate> listOfGraphAssetTemplates = new ArrayList<>();
		for(AssetTemplate assetTemplate: listOfAssetTemplate){
			listOfGraphAssetTemplates.addAll(graphService.findGraphAssetTemplate(assetTemplate));
			
		}
		for(GraphAssetTemplate graphAssetTemplate: listOfGraphAssetTemplates){
			listOfGraphs.addAll(graphService.findByGraphAssetTemplates(graphAssetTemplate));
		}
		model.addAttribute("graphsInUserWorkspaces", listOfGraphs);
		return "admin/listAdminGraph";

	}

}