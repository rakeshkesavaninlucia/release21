package org.birlasoft.thirdeye.controller.questionnaire;

import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.constant.QuestionnaireConstants;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.QuestionnaireLifeCycleService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for view  the Questionnaire Status {@code viewQuestionnaireStatus}
 * 
 */
@Controller
@RequestMapping("/questionnaire")
public class QuestionnaireLifeCycleController {
	
	private QuestionnaireLifeCycleService questionnaireLifeCycleService ;
	
	private QuestionnaireService questionnaireService;
	
	private final WorkspaceSecurityService workspaceSecurityService;
	
	private CustomUserDetailsService customUserDetailsService;
	
	private static final String VIEW_QUESTIONNAIRE_STATUS = "questionnaire/viewQuestionnaireStatus";

	
	/**
	 * Constructor to initialize services
	 * @param questionnaireService
	 * @param questionnaireLifeCycleService
	 * @param workspaceSecurityService
	 * @param customUserDetailsService
	 */
	 
	@Autowired
	public QuestionnaireLifeCycleController(QuestionnaireService questionnaireService,
			QuestionnaireLifeCycleService questionnaireLifeCycleService,
			WorkspaceSecurityService workspaceSecurityService,
			CustomUserDetailsService customUserDetailsService){
 
		this.questionnaireService = questionnaireService;
		this.questionnaireLifeCycleService = questionnaireLifeCycleService;
		this.workspaceSecurityService = workspaceSecurityService;
		this.customUserDetailsService = customUserDetailsService;
	}
	

	
	/**
	 * method to view the Questionnaire Status
	 * @param idOfQuestionnaire
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/status", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_MODIFY'})")
	public String viewQuestionnaireStatus(@PathVariable(value = "id") Integer idOfQuestionnaire, Model model){
		
		Questionnaire qe = questionnaireService.findOneFullyLoaded(idOfQuestionnaire);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireAccess(qe);
		
		List<ParameterBean> listOfRootParameters  = questionnaireLifeCycleService.getRootParameterForQuestionnaire(qe);		
		Map<String,Integer> mapOfQuestionCategory = questionnaireLifeCycleService.getMapOfCategoryAndNoOfQuetion(qe);
		
		model.addAttribute("rootParameterList", listOfRootParameters);		
		model.addAttribute("mapOfQC", mapOfQuestionCategory);
		model.addAttribute("questionnaireForm", qe);
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_TYPES, QuestionnaireType.DEF.toString());
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION,QuestionnaireType.DEF.getAction());
		
		return VIEW_QUESTIONNAIRE_STATUS;
	}
	

	/**
	 * method to change the Questionnaire Status
	 * @param idOfQuestionnaire
	 * @param status
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/modify/{status}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_MODIFY'})")
	public String changeQuestionnaireStatus(@PathVariable(value = "id") Integer idOfQuestionnaire, @PathVariable(value = "status") String status, Model model){
		
		Questionnaire qe = questionnaireService.findOneFullyLoaded(idOfQuestionnaire);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireAccess(qe);
		qe.setStatus(status);
		Questionnaire savedQuestionnaire = questionnaireService.save(questionnaireService.updateQuestionnaireObject(qe, customUserDetailsService.getCurrentUser()));
		
		return  "redirect:/questionnaire/"+savedQuestionnaire.getId()+"/status";
	}
	
	
	
}

