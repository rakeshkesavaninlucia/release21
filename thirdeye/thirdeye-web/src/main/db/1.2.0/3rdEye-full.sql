-- MySQL dump 10.13  Distrib 5.6.23, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: 3rdi
-- ------------------------------------------------------
-- Server version	5.6.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aid`
--

DROP TABLE IF EXISTS `aid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `assetTemplateId` int(11) NOT NULL,
  `aidTemplate` varchar(25) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UQ_AID_Name_WorkspaceId` (`name`,`workspaceId`),
  KEY `FK_AID_WORKSPACEID_idx` (`workspaceId`),
  KEY `FK_AID_ASSETTEMPLATID_idx` (`assetTemplateId`),
  KEY `FK_CREATEDBY_USERID_idx` (`createdBy`),
  KEY `FK_UPDATEDBY_USERID_idx` (`updatedBy`),
  CONSTRAINT `FK_AID_ASSETTEMPLATID` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_AID_WORKSPACEID` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_CREATEDBY_USERID` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_UPDATEDBY_USERID` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aid`
--

LOCK TABLES `aid` WRITE;
/*!40000 ALTER TABLE `aid` DISABLE KEYS */;
/*!40000 ALTER TABLE `aid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aid_block`
--

DROP TABLE IF EXISTS `aid_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aid_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aidId` int(11) NOT NULL,
  `aidBlockType` varchar(20) NOT NULL,
  `sequenceNumber` int(11) NOT NULL,
  `blockJSONConfig` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_AIDID_AID_idx` (`aidId`),
  CONSTRAINT `FK_AIDID_AID` FOREIGN KEY (`aidId`) REFERENCES `aid` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aid_block`
--

LOCK TABLES `aid_block` WRITE;
/*!40000 ALTER TABLE `aid_block` DISABLE KEYS */;
/*!40000 ALTER TABLE `aid_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset`
--

DROP TABLE IF EXISTS `asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateId` int(11) NOT NULL,
  `uid` varchar(256) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_UNIQUE` (`uid`),
  KEY `ASSET_TEMPLATE_FK_idx` (`templateId`),
  KEY `ASSET_CREATED_BY_FK_idx` (`createdBy`),
  KEY `ASSET_UPDATED_BY_FK_idx` (`updatedBy`),
  CONSTRAINT `ASSET_CREATED_BY_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `ASSET_TEMPLATE_FK` FOREIGN KEY (`templateId`) REFERENCES `asset_template` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `ASSET_UPDATED_BY_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=250 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset`
--

LOCK TABLES `asset` WRITE;
/*!40000 ALTER TABLE `asset` DISABLE KEYS */;
INSERT INTO `asset` VALUES (1,69,'1',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(8,70,'8',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(9,70,'9',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(10,70,'10',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(11,70,'11',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(12,70,'12',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(13,70,'13',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(14,70,'14',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(15,70,'15',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(16,70,'16',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(17,70,'17',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(18,70,'18',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(19,70,'19',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(20,70,'20',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(21,71,'21',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(22,71,'22',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(23,71,'23',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(24,71,'24',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(25,71,'25',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(26,71,'26',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(27,71,'27',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(28,71,'28',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(29,71,'29',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(31,71,'31',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(35,71,'35',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(37,71,'37',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(41,71,'41',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(43,71,'43',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(55,71,'55',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(61,71,'61',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(63,71,'63',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(65,71,'65',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(67,71,'67',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(69,71,'69',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(71,71,'71',0,13,'2016-07-18 11:09:35',13,'2016-07-18 11:09:35'),(72,72,'72',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(73,72,'73',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(74,72,'74',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(75,72,'75',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(76,73,'76',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(77,72,'77',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(78,73,'78',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(79,72,'79',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(80,72,'80',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(81,73,'81',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(82,72,'82',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(83,72,'83',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(84,72,'84',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(85,73,'85',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(86,72,'86',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(87,73,'87',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(88,72,'88',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(89,73,'89',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(90,72,'90',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(91,73,'91',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(93,72,'93',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(94,73,'94',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(95,73,'95',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(96,73,'96',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(97,73,'97',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(98,73,'98',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(99,73,'99',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(100,73,'100',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(101,73,'101',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(102,73,'102',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(103,73,'103',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(104,73,'104',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(105,73,'105',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(106,73,'106',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(107,73,'107',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(108,73,'108',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(109,73,'109',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(110,73,'110',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(111,73,'111',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(112,73,'112',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(113,73,'113',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(114,75,'114',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(115,75,'115',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(116,75,'116',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(117,75,'117',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(118,75,'118',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(119,75,'119',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(120,75,'120',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(121,75,'121',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(122,75,'122',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(123,75,'123',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(124,75,'124',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(125,75,'125',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(126,75,'126',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(127,75,'127',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(128,75,'128',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(129,75,'129',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(130,75,'130',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(131,75,'131',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(132,75,'132',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(133,75,'133',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(134,75,'134',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(135,75,'135',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(136,75,'136',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(137,75,'137',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(138,75,'138',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(139,75,'139',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(140,75,'140',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(141,75,'141',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(142,75,'142',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(143,75,'143',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(144,75,'144',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(145,75,'145',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(146,75,'146',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(147,75,'147',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(148,75,'148',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(149,75,'149',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(150,75,'150',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(151,75,'151',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(152,75,'152',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(153,75,'153',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(154,75,'154',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(155,75,'155',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(156,75,'156',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(157,75,'157',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(158,75,'158',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(159,75,'159',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(160,75,'160',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(161,75,'161',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(162,75,'162',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(163,75,'163',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(164,75,'164',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(165,75,'165',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(166,75,'166',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(167,75,'167',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(168,75,'168',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(169,75,'169',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(170,75,'170',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(171,75,'171',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(172,75,'172',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(173,75,'173',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(174,75,'174',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(175,75,'175',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(176,75,'176',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(177,75,'177',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(178,75,'178',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(179,75,'179',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(181,75,'181',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(182,75,'182',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(183,75,'183',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(184,75,'184',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(185,75,'185',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(186,75,'186',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(187,75,'187',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(188,75,'188',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(189,75,'189',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(190,75,'190',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(191,75,'191',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(192,75,'192',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(193,75,'193',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(194,75,'194',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(195,75,'195',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(196,75,'196',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(197,75,'197',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(198,75,'198',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(200,75,'200',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(201,75,'201',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(202,75,'202',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(203,75,'203',0,12,'2016-07-18 11:09:35',12,'2016-07-18 11:09:35'),(204,74,'204',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(205,74,'205',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(206,74,'206',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(207,74,'207',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(208,74,'208',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(209,74,'209',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(210,74,'210',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(211,74,'211',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(212,74,'212',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(213,74,'213',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(214,74,'214',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(215,74,'215',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(216,74,'216',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(217,74,'217',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(218,74,'218',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(219,74,'219',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(220,74,'220',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(221,74,'221',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(222,74,'222',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(223,74,'223',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(224,74,'224',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(225,74,'225',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(228,74,'228',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(229,74,'229',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(230,74,'230',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(231,74,'231',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(234,74,'234',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(235,74,'235',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(236,74,'236',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(237,74,'237',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(238,74,'238',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(239,74,'239',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(240,74,'240',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(241,74,'241',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(242,74,'242',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(243,74,'243',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(244,74,'244',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(245,74,'245',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(246,74,'246',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(247,74,'247',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(248,74,'248',0,14,'2016-07-18 11:09:35',14,'2016-07-18 11:09:35'),(249,76,'249',0,15,'2016-07-18 11:09:35',15,'2016-07-18 11:09:35');
/*!40000 ALTER TABLE `asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_data`
--

DROP TABLE IF EXISTS `asset_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` text,
  `assetTemplateColId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `assetTemplateColIdFk_idx` (`assetTemplateColId`),
  KEY `AssetData_Asset_FK_idx` (`assetId`),
  CONSTRAINT `AssetData_Asset_FK` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `assetTemplateColIdFk` FOREIGN KEY (`assetTemplateColId`) REFERENCES `asset_template_column` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=991 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_data`
--

LOCK TABLES `asset_data` WRITE;
/*!40000 ALTER TABLE `asset_data` DISABLE KEYS */;
INSERT INTO `asset_data` VALUES (1,'7512',83,1),(2,'JQTB is used for displaying for different attractions like special food items and upcoming shows daily along with their their schedule and timings. Based on the information displayed through this application, visitors decide on various bookings which they need to do.',84,1),(3,'Edgar Claudio',85,1),(4,'Joy Quest Tip Board',82,1),(5,'Mathew J OverBerger',86,1),(36,'Hari Selvaraj',90,8),(37,'Keith Berger',91,8),(38,'Making member reservations for Joy cruises, trips, park tickets etc. Members of Joy Vacation Club have the option to use their DVC points for other vacations like Adventures by Joy for trips, cruises instead of staying in one of the Joy resorts. ',89,8),(39,'DVC Adventure Travel',87,8),(40,'21379',88,8),(41,'Keith Berger',91,9),(42,'9987',88,9),(43,'DVC Contract Printing (Contract Prep and Management)',87,9),(44,'Calls the document manager application to do printing of contract printing documents. Pulls Data from AS400 and send it to document manager for printing.',89,9),(45,'Vigneswaran',90,9),(46,'9988',88,10),(47,'DVC Credit Underwriting',87,10),(48,'Keith Berger',91,10),(49,'Calls the document manager apllication to do printing of  Credit Undewriting  documents. Pulls Data from AS400 and send it to document manager for printing.',89,10),(50,'Vigneswaran',90,10),(51,'Keith Berger',91,11),(52,'Vigneswaran',90,11),(53,'1767',88,11),(54,'Application is used for printing and managing contract document templates, which involves storing and retrieving data from SQL server and interacting with word plugin.',89,11),(55,'DVC Document Manager',87,11),(56,'Keith Berger',91,12),(57,'9993',88,12),(58,'Vigneswaran',90,12),(59,'DVC Title Insurance',87,12),(60,'Processing recorded member contracts for issuing title insurance and calls the document manager apllication to do printing of Title  documents. Pulls Data from AS400 and send it to document manager for printing',89,12),(61,'Keith Berger',91,13),(62,'The Dues application is used to provide all financial related information with memebers dues as the payment transacions , Member inforamtion.',89,13),(63,'Narender Kumar',90,13),(64,'DVC Dues Processing System',87,13),(65,'28790',88,13),(67,'Sai Krishna',90,14),(68,'This application is used for proposal preparation before contract been signed in the Sales Office.',89,14),(69,'Keith Berger',91,14),(70,'1200',88,14),(71,'Keith Berge',91,15),(72,'21311',88,15),(73,'DVC Sales Center Operations',87,15),(74,'Its an workflow based application. Entire Sales center process of the Guest will be handled by this application. It tracks step by step customer activities in a sales office.',89,15),(75,'Saikrishna',90,15),(76,'1203',88,16),(77,'SaiKrishna ',90,16),(78,'Keith Berger',91,16),(79,'DVC Member Website Services',87,16),(80,'This is a webservice application. All these services are  used by the Member web site (This is a common portal for all Joy Members) which fetches information like  Dues, Member information, booking ettc',89,16),(81,'SaiKrishna',90,17),(82,'This is an webservice application used by an stand alone application. The purpose is used to check the credit card validations.',89,17),(83,'2912',88,17),(84,'DVC Member Accounting Services UI',87,17),(85,'Keith Berger',91,17),(86,'DVC Mail/Referral Processing',87,18),(87,'Hari Selvaraj ',90,18),(88,'All the details which are all required for sending Secure FTP file to the Fullfillment team . Those file and Server location details are maintained in this application',89,18),(89,'Keith Berger',91,18),(90,'9989',88,18),(91,'This application is managing LOAN payment details for the member contracts.Members can take loan for the membership fees required for DVC contracts amount. Loan details like loan pending, loan paid back, loan sancationed etc are tracked through this application',89,19),(92,'Hari Selvaraj',90,19),(93,'1201',88,19),(94,'Keith Berger',91,19),(95,'DVC Loans Servicing (AS400)',87,19),(96,'Keith Berger',91,20),(97,'DVC Guest & Tour Entry (GATES)',87,20),(98,'Siva ',90,20),(99,'21309',88,20),(100,'This application mainly captures the New / Existing details of the customers who visit the park. At different booths for customer for promotiona and sales',89,20),(101,'AS400Z',92,21),(108,'Building Code - P_NS1012\nRoom - L01\nRack - CA01',95,21),(109,'AS400XO',92,22),(115,'Building Code - P_NS1012\nRoom - L01\nRack - CA01',95,22),(120,'AS400BO',92,23),(122,'Building Code - P_NS1012\nRoom - L01\nRack - CA01',95,23),(128,'Building Code - P_NS1012\nRoom - L01\nRack - CA01',95,24),(130,'AS400GO',92,24),(133,'AS400FO',92,25),(138,'Building Code - P_NS1012\nRoom - L01\nRack - CA01',95,25),(144,'Building Code - P_NS1012\nRoom - L01\nRack - CA01',95,26),(145,'AS400DO',92,26),(152,'Building Code - P_NS1012\nRoom - L01\nRack - CA01',95,27),(156,'AS400EO',92,27),(157,'AS400UO',92,28),(163,'Building Code - P_NS1012\nRoom - L01\nRack - CA01',95,28),(170,'Building Code - P_NS1012\nRoom - L01\nRack - CA01',95,29),(171,'AS400ZO',92,29),(181,'Orlando',95,31),(186,'dvcnet2.wdw.Joy.com',92,31),(217,'Orlando',95,35),(219,'docmgrsqlsvr.Joy.com ',92,35),(231,'fldcvpswa1242 ',92,37),(232,'Orlando',95,37),(261,'wm-flor-ap529',92,41),(262,'Orlando',95,41),(277,'fldcvpswa1280 ',92,43),(280,'Orlando',95,43),(373,'as400zk',92,55),(377,'Orlando',95,55),(423,'fldcvdsla1388',92,61),(427,'Orlando',95,61),(440,'Orlando',95,63),(441,'fldcvfsla1395',92,63),(454,'Orlando',95,65),(455,'fldcvisla1394',92,65),(469,'Orlando',95,67),(472,'wl-flor-ap038\nwl-flor-ap039',92,67),(489,'nckmvpsla0337\nnckmvpsla0338',92,69),(492,'Orlando',95,69),(502,'IIS 7.5',92,71),(507,'',95,71),(509,'Gonzalez  Jose L',103,72),(510,'Siebel CRM - Vault',100,72),(511,'29015',101,72),(512,'Siebel Vault is the CRM application used to store the credit card details of the guests.',102,72),(513,'Abresky Diane  ',104,72),(514,'Siebel CRM - IR',100,73),(515,'7324',101,73),(516,'Benefield Tamara ',104,73),(517,'Gonzalez Jose L',103,73),(518,'The Investor Relations application houses contact information for key Wall Street constituents including analysts and institutional investors. It serves as one of the key sources by which company financial communications (including earnings conference call invitations, investor relations newsletters, announcements, web cast alerts, etc.) are disseminated to the financial community. ',102,73),(519,'Craig Nordengren',104,74),(520,'Steve Repp',103,74),(521,'NICE Interaction Management (NIM) is used to record voice and screen of calls made to agents at the SATO Domestic Call Centers; DRC-Orlando, DRC-Tampa and DRTSC in Anaheim, Joy Shopping in Memphis and TWDC Corporate Travel in New York and Burbank, Ca.  The recordings are evaluated by third party Q/A team - HyperQuality.  The evaluations are used to rate and coach the agents and also can impact their incentives. In addition to the base system, SATO Domestic is also using Desktop Analytics, Interaction Analytics and Feedback modules.',102,74),(522,'SATO 4.1',100,74),(523,'29174',101,74),(524,'Julie Motzel',104,75),(525,'Gonzalez  Jose L',103,75),(526,'28208',101,75),(527,'Siebel Joy Special Activities (DSA) is used by the VIP Tours organization for Tour Management and Tour Guide management.',102,75),(528,'Siebel CRM Sup - DSA (28208)',100,75),(529,'Animal World',105,76),(530,'Susan Feltman',109,76),(531,'Barry Harfenes ',108,76),(532,'6204',106,76),(533,'Animal World application comprising of ZAP (Zoological Animal Programs), Zoomr (Zoo Medical Records) and ZooDiets is a comprehensive information management system for managing animals.  This is a single repository having information on each animal\'s taxonomy, birth, transactions, medical and diet related records and lot more.  This is a web based application available 24x7 on the Joy network with ability to generate different reports.  Used widely by the zoo staff including zoo managers, record keepers, veterinary doctors and veterinary dieticians.',107,76),(534,'Gonzalez  Jose L',103,77),(535,'Abresky Diane',104,77),(536,'SUP Groups and Sports application is a customized CRM application that mainly supports the group sales management, co-ordination of room booking & ticket purchases and event management.',102,77),(537,'Siebel Groups and Sports',100,77),(538,'28207',101,77),(539,'Frank Simmons',108,78),(540,'26054',106,78),(541,'Ariel',105,78),(542,'Nancy White',109,78),(543,'Ariel FMS (WDW) is a WDW developed .NET software package used at WDW by the Food and Beverage line of business.   Ariel is just a character name from the Joy movie Little Mermaid.  FMS stands for Food Management System and it allows its users to track the inventory used in creating  the food items sold at WDW (and Vero Beach).  With Ariel, cost analysis can be performed on the Recipes created and sold at WDW.',107,78),(544,'Live Person',100,79),(545,'Steve Repp',103,79),(546,'28234',101,79),(547,'Live person is vendor developed single unified desktop agent tool used by the DRC call center people. It is used as SAS and the chat client which is integrated in the Joy public websites facilitates the guest users to chat with the Joy cast members (agents) to get assistance in the reservations. The Joy cast members use the live person software available on their workstations to support the online customer interactions via chat, email and voice.',102,79),(548,'Gauthier  Thomas R',104,79),(549,'Gonzalez  Jose L',103,80),(550,'Joe Haughney',104,80),(551,'27217',101,80),(552,'Siebel Club 33 Application is used by members of the Club 33 at The Walt Joy Company for membership management and contact management.',102,80),(553,'Siebel Club 33',100,80),(554,'The purpose of the Computer Aided Labor Management(CALM) is to monitor and efficiently manage Labor in Ware House Management System and to generate proper report, know that how many work done and how many needs to be done, if anyone can leave early or have to stay longer.',107,81),(555,'Melanie Roehl',108,81),(556,'CALM (Computer Aided Labor Management )',105,81),(557,'Brice Schroedel',109,81),(558,'427',106,81),(559,'27719',101,82),(560,'Confirmit',100,82),(561,'Gonzalez  Jose L',104,82),(562,'Steve Repp',103,82),(563,'Confirmit is used for surveying from the Joy Guests that how the guest  experiencing/experienced the WDPR, and any issues can be updated through this application. After conducting the surveys this application is used for analysing the survyes. These surveys use traditional online surveys, handheld surveys, smartphones surveys, and paper surveys (Bellview Scan). And also for Analyzing the surveys.',102,82),(564,'7525',101,83),(565,'Co-branded Credit Card is a Siebel Call Center application managed by the Co-Branded Credit Card Services department of WDPR. This application allows users to track and update the credit card details of the GUEST.',102,83),(566,'Karen Burgess',104,83),(567,'Siebel CRM - C3',100,83),(568,'Gonzalez  Jose L',103,83),(569,'Kayko F Distler',104,84),(570,'27727',101,84),(571,'DGCS 3.5',100,84),(572,'Steve Repp',103,84),(573,'System used to record voice and screen of calls made to agents at Joy\'s Guest Connection Services (formerly the Resort Call Center).  The recordings are evaluated by an in house Q/A team.  The evaluations are used to rate and coach the agents.',102,84),(574,'CBORD (FMS) is a vendor software package used at Joyland that helps our Food and Beverage line of businesses .  FMS stands for Ã¢??Food Management SuiteÃ¢?? and  it allows us to order, purchase, inventory and track food items and products that are used and sold at our Resort restaurants.  It also allows us to maintain recipes and nutritional information for our menu items.',107,85),(575,'Wendi Lay, Brian Ford',109,85),(576,'CBORD - Food Management System (FMS)  DLR',105,85),(577,'2278',106,85),(578,'Fernando Jimenez',108,85),(579,'Siebel CRM 0 DASH (Joy Account Super Highway)',100,86),(580,'25711',101,86),(581,'Llewellyn Belinda L ',104,86),(582,'DASH Application is a long running stable application in JoyÃ¢??s Landscape originally built with the capability for the user community to maintain account relationships with DLR wholesale and travel agents.',102,86),(583,'Gonzalez  Jose L',103,86),(584,'CBORD Aulani (FMS) ',105,87),(585,'Fernando Jimenez',108,87),(586,'Wendi Lay, Brian Ford',109,87),(587,'27555',106,87),(588,'CBORD (FMS) is a vendor software package used at Joyland that helps our Food and Beverage line of businesses .  FMS stands for Ã¢??Food Management SuiteÃ¢?? and  it allows us to order, purchase, inventory and track food items and products that are used and sold at our Resort restaurants.  It also allows us to maintain recipes and nutritional information for our menu items.',107,87),(589,'Abresky Diane  ',104,88),(590,'Siebel CRM - Batch',100,88),(591,'28287',101,88),(592,'CRM Batch and Siebel Batch C3 applications explains the batch jobs that aid the systems to exchange the data. ',102,88),(593,'Gonzalez  Jose L',103,88),(594,'Fernando Jimenez',108,89),(595,'Wendi Lay, Brian Ford',109,89),(596,'CBORD Joy Regional Entertainment (DRE) Food Management System (FMS) ',105,89),(597,'21209',106,89),(598,'CBORD (FMS) is a vendor software package used at Joyland that helps our Food and Beverage line of businesses .  FMS stands for Ã¢??Food Management SuiteÃ¢?? and  it allows us to order, purchase, inventory and track food items and products that are used and sold at our Resort restaurants.  It also allows us to maintain recipes and nutritional information for our menu items.',107,89),(599,'CRM Batch and Siebel Batch C3 applications explains the batch jobs that aid the systems to exchange the data. ',102,90),(600,'Benefield Tamara ',104,90),(601,'Gonzalez  Jose L',103,90),(602,'Siebel CRM Batch C3',100,90),(603,'28288',101,90),(604,'COMMON DB',105,91),(605,'631',106,91),(606,'Shruthiraj Shetty ',108,91),(607,'The Common DB is a data store that is used throughout DLR applications.  It receives data from the SAP system via a series of Scheduled GECS jobs that load the data, ultimately to the Common DB instances.The Windows 2003 servers load data to 2003 R2 and SQL 2008 servers directly to the Common DB instances',107,91),(608,'Sharon Mason',109,91),(614,'DRE EventMaster (EM) is a vendor software package used at the JoyÃ¢??s Regional Entertainment (DRE ) ESPNZone restaurant in Anaheim, California that helps our Food and Beverage line of businesses .  EventMaster is a catering and event management software package.  It allows us to plan, book, and schedule resources and menu needs for events sold at our ESPNZone restaurants.',102,93),(615,'6807',101,93),(616,'Fernando Jimenez',103,93),(617,'Event Management DRE ',100,93),(618,'Wendi Lay',104,93),(619,'21164',106,94),(620,'deborah I simpson(+1-407-828-2843)',108,94),(621,'This application is used to authorized the access request for various applications.The application has an approval matrix for all the access request and finally approved by Finance Team ',107,94),(622,'brijesh singh(+1-407-256-2480)',109,94),(623,'Controlled Access Process System (CAPS)',105,94),(624,'544',106,95),(625,'Shruthiraj shetty -- Shruthiraj.B.Shetty.-ND@Joy.com',108,95),(626,'Melanie Roehl -- Melanie.Roehl@Joy.com',109,95),(627,'DBS',105,95),(628,'DBS is a replenishment system. DBS scans through the inventory and determines which inventories should be replenished and replenishes the inventory by placing orders for vendors.',107,95),(629,'\"web-based application which allows ticketing to control the business rule values and set up products in the DTI, along with request ticket creation and review prior transactions \"',107,96),(630,'Joy Ticketing Interface (DTI) Admin GUI - PROD',105,96),(631,'Joseph Roberts    (+1-4-7-827-5918)',109,96),(632,'28163',106,96),(633,'Todd Lewis           (+1-407-828-2466)',108,96),(634,'Todd Lewis           (+1-407-828-2466)',108,97),(635,'Joseph Roberts    (+1-4-7-827-5918)',109,97),(636,'Joy Ticketing Interface (DTI) Create Ticket-PROD',105,97),(637,'\"GUI driven tool to create large volumes of unentitled tickets in ATS, and the text file of the shell information for the plastics printers. \"',107,97),(638,'28166',106,97),(639,'This application used as an information system by the sellers to see the product availability and product catalog (Events, Rides etc) .This application manages Ticket booking, Ticket cancellation, Ticket Management, Ticket Pricing',107,98),(640,'Todd Lewis           (+1-407-828-2466)',108,98),(641,'Joseph Roberts    (+1-4-7-827-5918)',109,98),(642,'28168',106,98),(643,'Joy Ticketing Interface (DTI) OneCorp - PROD',105,98),(644,'Barry.Harfenes@Joy.com',108,99),(645,'This application is used to track the services and ticket distribution, merchandize, and give a wayÃ¢??s for Joy Cast Members (Joy Employees).  Used by Joyland, WDI, and Studios (Corporate).  ',107,99),(646,'450',106,99),(647,'sharon.mason@Joy.com',109,99),(648,'DLR CAST TRAC',105,99),(649,'Robert Rule -- Robert.M.Rule@Joy.com',109,100),(650,'6229',106,100),(651,'Jazz Call Accounting application is a vendor package used for managing resort telecommunications by call accounting and recording for pricing and accounting purpose. The application team does the daily monitoring of the server processes to esnure that JAZZ is running properly. ',107,100),(652,'Jazz',105,100),(653,'Shruthiraj shetty -- Shruthiraj.B.Shetty.-ND@Joy.com',108,100),(654,'Melanie Roehl, Melanie.Roehl@Joy.com, 407-828-1072, Orlando ',108,101),(655,'5833',106,101),(656,'David E. Greene,David.E.Greene@Joy.com, 407-939-4335, Orland',109,101),(657,'LabelMatrix - Production',105,101),(658,'Label Matrix is a 3rd party application from TekLynx used for printing label, which is used for warehouse shipping and whenever users need to print any barcode. Users create the label as per the regular format and sendÃ¢??s that for printing.',107,101),(659,'\"Rob Rule 407-248-6890  Robert.M.Rule@Joy.com\"',108,102),(660,'Lodging Management System(LMS) Property Management',105,102),(661,'Middleton, Christopher M Christopher.M.Middleton@Joy.com',109,102),(662,'699',106,102),(663,'Lodging Management System (LMS) is developed on AS400 platform used to take hotel reservations from multiple sources, and to operationally fulfill them at the various DLR hotel locations. The application is in use by users at multiple call centers, DLR Hotels, Finance, Revenue Management, and a multitude of other business units. Application supports hotel reservations, check-ins, check-outs and room charges',107,102),(664,'Merchandise Warehouse Conveyor/Sorter DLR',105,103),(665,'Melanie Roehl, Melanie.Roehl@Joy.com, 407-828-1072, Orlando ',108,103),(666,'David .E.Greene',109,103),(667,'Joy warehouse in California sends different packages to Joy stores. The packages are sent to Conveyor at a rate of 1 package per 2 sec during peak hours. The packages are scanned and the package information is written to SQL server DB as a record. There is only one conveyor which is controlled by PLC. Rockwell software application (which is a third party application and   is installed on a PC next to conveyor) interacts with PLC and allows the users to see the status of the conveyor and processes the data. The warehouse operates from 6 am to 6 pm PST. Merchandise Warehouse Conveyor/Sorter DLR ',107,103),(668,'5438',106,103),(669,'Melanie.Roehl@Joy.com)',108,104),(670,'To Manage the Joy Warehouse merchandise Warehouse operation which includes Receiving,Walkthrough Unit picking, Case Picking, Generate the Picking for Store Replenishment, Shippment , generate store ASN',107,104),(671,'Bill.Whitaker@Joy.com',109,104),(672,'Oracle Retail Warehouse Management System - DLR',105,104),(673,'25308',106,104),(674,'Melanie.Roehl@Joy.com)',108,105),(675,'Oracle Retail Warehouse Management System - WDW',105,105),(676,'Bill.Whitaker@Joy.com',109,105),(677,'To Manage the Joy Warehouse merchandise Warehouse operation which includes Receiving,Walkthrough Unit picking, Case Picking, Generate the Picking for Store Replenishment, Shippment , generate store ASN',107,105),(678,'27867',106,105),(679,'SC Logic is a third party application, which helps to track delivery of packages/merchandise, general supplies, mails meant for Joy employees/guests and movement of warehouse items within Joy campus. It provides both desktop and mobile interfaces.',107,106),(680,'Melanie Roehl, Melanie.Roehl@Joy.com, 407-828-1072, Orlando ',108,106),(681,'SCLogic - DLR - Production',105,106),(682,'Jenny Hedden, General Supplies, 407-934-7047, DART, Virginia',109,106),(683,'28087',106,106),(684,'28786',106,107),(685,'Melanie Roehl, Melanie.Roehl@Joy.com, 407-828-1072, Orlando ',108,107),(686,'It\'s a web interface to manage the Radio Frequency (RF) scanning device maintenance requests.  The system is designed to take in requests, print a ticket indicating its reference numbers for the device to be delivered to the IT team for it to be fixed, look up device repair history, as well as generate a report that provides a list of devices',107,107),(687,'Supply Chain and Logistics Support Tool',105,107),(688,'Jimmy Porter,Jimmy.D.Porter@Joy.com, 407-939-4541,  Orlando ',109,107),(689,' Kevin.M.Beach@Joy.com',109,108),(690,'To Control the warehouse Conveyor system,unit picking and shipping',107,108),(691,'Melanie.Roehl@Joy.com)',108,108),(692,'Warehouse Control System (WCS) ',105,108),(693,'25738',106,108),(694,'Shruthiraj shetty -- Shruthiraj.B.Shetty.-ND@Joy.com',108,109),(695,'DCS',105,109),(696,'Melanie Roehl -- Melanie.Roehl@Joy.com',109,109),(697,'DCS is a warehouse management system which manages items coming into a warehouse and moving out of a warehouse.  DCS within the realm of WDW is used for a multiple of Lines of Businesses (LOBs).  Those LOBs are:   Foods (which includes Dry Stock, Frozen Foods, Liquor, Produce, and Miscellaneous), and General Supplies',107,109),(698,'404',106,109),(699,'sharon.mason@Joy.com',109,110),(700,'448',106,110),(701,'DLR Cast Activities Service Awards (CASA)',105,110),(702,'CASA Application is used to track information about the Ã¢??Spirit Awards nomination processÃ¢??.  CASA is also used to help plan the Service Awards by enabling the business to distribute the spirit award by setting up the eligibility process',107,110),(703,'Barry.Harfenes@Joy.com',108,110),(704,'sharon.mason@Joy.com',109,111),(705,'Barry.Harfenes@Joy.com',108,111),(706,'Cast Parking Application tracks Cast Member parking privileges by parking location. Parking distribution for the veichles bought by CAST members',107,111),(707,'DLR Cast Parking System (CPS)',105,111),(708,'6354',106,111),(709,'REMY',105,112),(710,'25720',106,112),(711,'Remy is a vender software package used at WDW by the Food and Beverage line of business.  Remy is a character in the Joy movie Ratatouille.   Remy calculates the nutritional content of the recipes used at WDW',107,112),(712,'Jennifer Leiter - Jennifer.S.Leiter@Joy.com',109,112),(713,'\"Frank Simmons Frank.Simmon@Joy.com \"',108,112),(714,'SIM',105,113),(715,'Store inventory Management provides Store personnel to carry out Store Operations like Receiving, Stock Counts etc',107,113),(716,'Rennae Greenwood',109,113),(717,'25648',106,113),(718,'Melanie Roehl, Melanie.Roehl@Joy.com, 407-828-1072, Orlando ',108,113),(719,'FLDCVPSWA0952',112,114),(720,'Orlando',113,114),(721,'NCKMVPSWA0329',112,115),(722,'Orlando',113,115),(723,'Orlando',113,116),(724,'WMFLORCDS039V04\\sql04',112,116),(725,'Orlando',113,117),(726,'WM-FLOR-DS151',112,117),(727,'FLDCVFSWA0951',112,118),(728,'Orlando',113,118),(729,'nckm-vds0349v02\\inst02',112,119),(730,'Orlando',113,119),(731,'Orlando',113,120),(732,'WM-FLOR-DV137',112,120),(733,'FLDCVDSWA0719',112,121),(734,'Orlando',113,121),(735,'Orlando',113,122),(736,'wmflorcds052v08\\inst08',112,122),(737,'wmflorcds020v01',112,123),(738,'Orlando',113,123),(739,'Orlando',113,124),(740,'sena-apps.wdw.Joy.com',112,124),(741,'Orlando',113,125),(742,'sena-webapps.wdw.Joy.com',112,125),(743,'wm-flor-cds015a/b',112,126),(744,'Orlando',113,126),(745,'sena-devapps.wdw.Joy.com',112,127),(746,'Orlando',113,127),(747,'Orlando',113,128),(748,'sena-devwebapps.wdw.Joy.com',112,128),(749,'wm-flor-cds035a/b',112,129),(750,'Orlando',113,129),(751,'sena-appsqa.wdw.Joy.com',112,130),(752,'Orlando',113,130),(753,'wm-flor-ap486',112,131),(754,'Orlando',113,131),(755,'wm-flor-ap603.wdw.Joy.com',112,132),(756,'Orlando',113,132),(757,'Orlando',113,133),(758,'wmflorcds052v09\\Inst09',112,133),(759,'wm-flor-dvvm155.wdw.Joy.com',112,134),(760,'Orlando',113,134),(761,'Orlando',113,135),(762,'FLDC-VDS1698v09\\inst09',112,135),(763,'KMTC',113,136),(764,'\"nckmppcwds0284a.swna.wdpr.Joy.com nckmppcwds0284b.swna.wdpr.Joy.com\"',112,136),(765,'KMTC',113,137),(766,'nckmvpswa0279.swna.wdpr.Joy.com',112,137),(767,'nckmpdcwds0335a/ nckmpdcwds0335b',112,138),(768,'KMTC',113,138),(769,'KMTC',113,139),(770,'nckmvrswa0250.swna.wdpr.Joy.com',112,139),(771,'nckmvfswa0241.swna.wdpr.Joy.com',112,140),(772,'KMTC',113,140),(773,'KMTC',113,141),(774,'nckmvpswa0278.swna.wdpr.Joy.com',112,141),(775,'nckmvdswa0243.swna.wdpr.Joy.com',112,142),(776,'KMTC',113,142),(777,'Nckmvpswa5308.swna.wdpr.Joy.com',112,143),(778,'KMTC',113,143),(779,'KMTC',113,144),(780,'nckmvdswa5312.swna.wdpr.Joy.com',112,144),(781,'dm-caan-ap025',112,145),(782,'California',113,145),(783,'orldwswsql012/013',112,146),(784,'California',113,146),(785,'California',113,147),(786,'orldwswsql025/026',112,147),(787,'California',113,148),(788,'wm-flor-cds040a/b',112,148),(789,'California',113,149),(790,'wm-flor-cds043a/b',112,149),(791,'wm-flor-ds064/065',112,150),(792,'California',113,150),(793,'California',113,151),(794,'wm-flor-ds092',112,151),(795,'wm-flor-dv033/034',112,152),(796,'California',113,152),(797,'dm-caan-ds001',112,153),(798,'California',113,153),(799,'California',113,154),(800,'dm-caan-ds002',112,154),(801,'dm-caan-ds003',112,155),(802,'California',113,155),(803,'California',113,156),(804,'dm-caan-ds006',112,156),(805,'dm-caan-sqpr003',112,157),(806,'California',113,157),(807,'dm-caan-sqpr006',112,158),(808,'California',113,158),(809,'dm-caan-sqpr020',112,159),(810,'California',113,159),(811,'dm-caan-sqpr022',112,160),(812,'California',113,160),(813,'dm-caan-sqpr023',112,161),(814,'California',113,161),(815,'California',113,162),(816,'dm-caan-sqpr024',112,162),(817,'dm-caan-sqpr026',112,163),(818,'California',113,163),(819,'Orlando, Florida',113,164),(820,'cip2claw.noceast.dws.Joy.com',112,164),(821,'cip-claw-devl.noceast.dws.Joy.com',112,165),(822,'Orlando, Florida',113,165),(823,'fldcvfsla1009.wdw.Joy.com',112,166),(824,'Orlando',113,166),(825,'Orlando',113,167),(826,'nckmvpsla0300.wdw.Joy.com',112,167),(827,'nckmvpsla0301.wdw.Joy.com',112,168),(828,'Orlando',113,168),(829,'Orlando',113,169),(830,'fldcvpsla1699.wdw.Joy.com',112,169),(831,'Orlando',113,170),(832,'lnxu002dva-scan.wdw.Joy.com',112,170),(833,'lnxu002pr-scan.wdw.Joy.com',112,171),(834,'Orlando',113,171),(835,'Orlando',113,172),(836,'lnxu006prb-scan.wdw.Joy.com',112,172),(837,'Orlando',113,173),(838,'fldcvdsla1018.wdw.Joy.com',112,173),(839,'fldcvfsla1020.wdw.Joy.com',112,174),(840,'Orlando',113,174),(841,'Orlando(Disc building)',113,175),(842,'\"wm-flor-apvm249 jazzclient.wdw.Joy.com\"',112,175),(843,'WM-FLOR-AP250 ',112,176),(844,'Orlando(Disc building)',113,176),(845,'Orlando(Disc building)',113,177),(846,'AS400H(153.60.6.212)',112,177),(847,'AS400E(153.7.94.101)',112,178),(848,'Orlando(Disc building)',113,178),(849,'Orlando',113,179),(850,'WDWAPPADM04',112,179),(853,'wm-flor-ap603',112,181),(854,'Orlando',113,181),(855,'AS400L',112,182),(856,' Florida',113,182),(857,'AS400B',112,183),(858,'Florida',113,183),(859,'Florida',113,184),(860,'AS400Jo',112,184),(861,'AS400LO',112,185),(862,'Florida',113,185),(863,'AS400LK',112,186),(864,'KMTC',113,186),(865,'dm-caan-ds006,50001',112,187),(866,'California',113,187),(867,'California',113,188),(868,'dm-caan-sqpr023,50001',112,188),(869,'Orlando DC',113,189),(870,'OIANP085',112,189),(871,'Orlando DC',113,190),(872,'OIANP081',112,190),(873,'wdwappadm04',112,191),(874,'Orlando',113,191),(875,'WMFLORCDS038V05.wdw.Joy.com\\SQL05,50013 ',112,192),(876,'Orlando',113,192),(877,'Orlando',113,193),(878,'D:\\Inetpub\\wwwroot\\Enterprise\\SCLIntraPackage',112,193),(879,'fldcvdswa5141',112,194),(880,'Orlando',113,194),(881,'fldcvpswa5147',112,195),(882,'Orlando',113,195),(883,'WM-FLOR-AP580, WM-FLOR-AP569',112,196),(884,'Orlando DC',113,196),(885,'nckm-vds0335v08.swna.wdpr.Joy.com\\inst08,50008',112,197),(886,'Orlando',113,197),(887,'Orlando',113,198),(888,'a. WMFLORCDS040V04.wdw.Joy.com\\SQL04,50004',112,198),(891,'WMFLORCDS040V04\\SQL04  ',112,200),(892,'Orlando',113,200),(893,'nckm-vds0335v08\\inst08, 50008 ',112,201),(894,'Orlando',113,201),(895,'FLDC-VDS1232V07.wdw.Joy.com',112,202),(896,'Orlando',113,202),(897,'Orlando',113,203),(898,'sena-webappsqa.wdw.Joy.com',112,203),(899,'fldcvpswa5413',110,204),(900,'Lake Buena Vista, FL',111,204),(901,'Lake Buena Vista, FL',111,205),(902,'fldcvpswa5414',110,205),(903,'fldcvpswa5415 ',110,206),(904,'Lake Buena Vista, FL',111,206),(905,'fldcvpswa5416',110,207),(906,'Lake Buena Vista, FL',111,207),(907,'Florida',111,208),(908,'wm-flor-apvm176',110,208),(909,'Florida',111,209),(910,'wm-flor-apvm177',110,209),(911,'Florida',111,210),(912,'wm-flor-apvm178',110,210),(913,'wm-flor-apvm179',110,211),(914,'Florida',111,211),(915,'Florida',111,212),(916,'wl-flor-cds003a',110,212),(917,'wl-flor-cds003b',110,213),(918,'Florida',111,213),(919,'wm-flor-qavm001',110,214),(920,'Florida',111,214),(921,'Florida',111,215),(922,'wm-flor-qavm002',110,215),(923,'Florida',111,216),(924,'fldcvdswa5527',110,216),(925,'wl-flor-cds004a',110,217),(926,'Florida',111,217),(927,'wl-flor-cds004b',110,218),(928,'Florida',111,218),(929,'Orlando',111,219),(930,'oiond003',110,219),(931,'oiond004',110,220),(932,'Orlando',111,220),(933,'oionp004',110,221),(934,'Orlando',111,221),(935,'Florida',111,222),(936,'fldcvpswa1605 ',110,222),(937,'Florida',111,223),(938,'fldcvpswds0721',110,223),(939,'Kings Mountain',111,224),(940,'nckmvfswa0303',110,224),(941,'nckmvdswa0302',110,225),(942,'Kings Mountain',111,225),(947,'nckmpdcwds0347a',110,228),(948,'Kings Mountain',111,228),(949,'Kings Mountain',111,229),(950,'nckmpdcwds0347b',110,229),(951,'Florida',111,230),(952,'wm-flor-qavm003',110,230),(953,'wm-flor-dvvm179',110,231),(954,'Florida',111,231),(959,'wm-flor-cds035a',110,234),(960,'Florida',111,234),(961,'Florida',111,235),(962,'wm-flor-cds035b',110,235),(963,'Florida',111,236),(964,'WM-FLOR-APVM191',110,236),(965,'Florida',111,237),(966,'WM-FLOR-APVM192',110,237),(967,'fldcppcwds1698a',110,238),(968,'Florida',111,238),(969,'Florida',111,239),(970,'fldcppcwds1698b',110,239),(971,'wm-flor-apvm190',110,240),(972,'Orlando',111,240),(973,'wl-flor-cds014a ',110,241),(974,'Orlando',111,241),(975,'wl-flor-cds014b',110,242),(976,'Orlando',111,242),(977,'wl-flor-cds014c',110,243),(978,'Orlando',111,243),(979,'Orlando',111,244),(980,'fldcviswa5526',110,244),(981,'Orlando',111,245),(982,'wm-flor-dvvm162',110,245),(983,'wl-flor-cds015a',110,246),(984,'Orlando',111,246),(985,'wl-flor-cds015b',110,247),(986,'Orlando',111,247),(987,'Live Person vendor',110,248),(988,'Lake Buena Vista, FL',111,248),(989,'DVC Electronic Purchase Proposal',87,14),(990,'test',114,249);
/*!40000 ALTER TABLE `asset_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_template`
--

DROP TABLE IF EXISTS `asset_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetTemplateName` varchar(45) NOT NULL,
  `assetTypeId` int(11) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `description` text,
  `workspaceId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTemplateName_createdBy_UNIQUE` (`assetTemplateName`,`createdBy`,`assetTypeId`),
  UNIQUE KEY `UK_a35ol4p1y14cnqk9j9yqcoj13` (`assetTemplateName`,`createdBy`,`assetTypeId`),
  KEY `id_idx` (`assetTypeId`),
  KEY `updatedByFk2_idx` (`updatedBy`),
  KEY `createdBy_UNIQUE` (`createdBy`),
  KEY `FK_ASSETTEMPLATE_WORKSPACEID_idx` (`workspaceId`),
  CONSTRAINT `FK_ASSETTEMPLATE_WORKSPACEID` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `assetTypeIdFk` FOREIGN KEY (`assetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `createdByFk2` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `updatedByFk2` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_template`
--

LOCK TABLES `asset_template` WRITE;
/*!40000 ALTER TABLE `asset_template` DISABLE KEYS */;
INSERT INTO `asset_template` VALUES (69,'Application Inventory - Utilities',1,0,'Inventory for Utilities & Services',12,14,'2016-06-14 11:19:20',14,'2016-06-14 11:19:20'),(70,'Application Inventory - Global',1,0,'Application Inventory for Globalization',9,14,'2016-06-14 11:19:20',15,'2016-06-22 11:13:26'),(71,'Server Inventory - Global',2,0,'Server inventory for the global portfolio',9,13,'2016-06-14 11:21:44',13,'2016-06-14 11:21:50'),(72,'Application Inventory-S&M',1,0,'Inventory for Sales & Marketing',10,14,'2016-06-15 06:57:49',14,'2016-06-15 06:57:49'),(73,'Application Inventory- Ops & Ful ',1,0,'Application Inventory for Operations And Fulfillment',11,12,'2016-06-15 07:07:32',12,'2016-06-15 07:07:32'),(74,'Server Inventory-Sales & Marketing',2,0,'Inventory for server -Sales & Marketing',10,14,'2016-06-15 08:22:26',14,'2016-06-15 08:22:26'),(75,'Server Inventory -Ops',2,0,'',11,12,'2016-06-15 09:39:46',12,'2016-06-15 09:39:46'),(76,'Interface Inventory',7,0,'',9,15,'2016-06-22 11:12:15',15,'2016-06-22 11:14:11');
/*!40000 ALTER TABLE `asset_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_template_column`
--

DROP TABLE IF EXISTS `asset_template_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_template_column` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetTemplateColName` varchar(45) NOT NULL,
  `dataType` varchar(45) NOT NULL,
  `length` int(11) NOT NULL,
  `assetTemplateId` int(11) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `sequenceNumber` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTemplateColName_assetTemplateId_UNIQUE` (`assetTemplateColName`,`assetTemplateId`),
  UNIQUE KEY `UK_i7n89jntp5xq390elbs233689` (`assetTemplateColName`,`assetTemplateId`),
  KEY `createdByFk3_idx` (`createdBy`),
  KEY `updatedByFk3_idx` (`updatedBy`),
  KEY `assettemplateIdFk_idx` (`assetTemplateId`),
  CONSTRAINT `assetTemplateIdFk` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `createdByFk3` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `updatedByFk3` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_template_column`
--

LOCK TABLES `asset_template_column` WRITE;
/*!40000 ALTER TABLE `asset_template_column` DISABLE KEYS */;
INSERT INTO `asset_template_column` VALUES (82,'Name','TEXT',100,69,0,1,1,14,'2016-06-13 12:46:08',14,'2016-06-13 12:46:08'),(83,'Application ID','NUMBER',20,69,0,0,2,14,'2016-06-13 12:47:25',14,'2016-06-13 12:47:25'),(84,'Application Overview','TEXT',300,69,0,0,3,14,'2016-06-13 12:47:42',14,'2016-06-13 12:56:32'),(85,'Application IT Owner Name','TEXT',50,69,0,0,4,14,'2016-06-13 12:48:09',14,'2016-06-13 12:48:09'),(86,'Application Business Owner Name','TEXT',50,69,0,0,5,14,'2016-06-13 12:48:48',14,'2016-06-13 12:48:48'),(87,'Name','TEXT',100,70,0,1,1,14,'2016-06-13 12:59:54',14,'2016-06-13 12:59:54'),(88,'Application ID','NUMBER',20,70,0,0,2,14,'2016-06-13 13:00:42',14,'2016-06-13 13:00:42'),(89,'Application Overview','TEXT',300,70,0,0,3,14,'2016-06-13 13:00:54',14,'2016-06-13 13:00:54'),(90,'Application IT Owner Name','TEXT',50,70,0,0,4,14,'2016-06-13 13:02:07',14,'2016-06-13 13:02:07'),(91,'Application Business Owner Name','TEXT',50,70,0,0,5,14,'2016-06-13 13:02:48',14,'2016-06-13 13:02:48'),(92,'Name','TEXT',100,71,0,1,1,13,'2016-06-14 11:21:44',13,'2016-06-14 11:21:44'),(95,'Hosting Location','TEXT',50,71,0,1,4,13,'2016-06-14 16:35:29',13,'2016-06-14 16:35:29'),(100,'Name','TEXT',100,72,0,1,1,14,'2016-06-15 06:57:49',14,'2016-06-15 06:57:49'),(101,'Application ID','NUMBER',20,72,0,0,2,14,'2016-06-15 06:58:13',14,'2016-06-15 06:58:13'),(102,'Application Overview','TEXT',600,72,0,0,3,14,'2016-06-15 06:59:33',14,'2016-06-15 07:11:38'),(103,'Application IT Owner Name','TEXT',50,72,0,0,4,14,'2016-06-15 06:59:52',14,'2016-06-15 06:59:52'),(104,'Application Business Owner Name','TEXT',50,72,0,0,5,14,'2016-06-15 07:00:10',14,'2016-06-15 07:00:10'),(105,'Name','TEXT',100,73,0,1,1,12,'2016-06-15 07:07:32',12,'2016-06-15 07:07:32'),(106,'Application ID','NUMBER',20,73,0,0,2,12,'2016-06-15 07:14:16',12,'2016-06-15 07:14:16'),(107,'Application Overview','TEXT',600,73,0,0,3,12,'2016-06-15 07:14:50',12,'2016-06-15 07:14:50'),(108,'Application IT Owner','TEXT',60,73,0,0,4,12,'2016-06-15 07:15:21',12,'2016-06-15 07:15:21'),(109,'Application Business Owner Name  ','TEXT',60,73,0,0,5,12,'2016-06-15 07:16:01',12,'2016-06-15 07:16:01'),(110,'Name','TEXT',100,74,0,1,1,14,'2016-06-15 08:22:26',14,'2016-06-15 08:22:26'),(111,'Hosting Location','TEXT',100,74,0,0,2,14,'2016-06-15 08:52:33',14,'2016-06-15 08:52:33'),(112,'Name','TEXT',100,75,0,1,1,12,'2016-06-15 09:39:46',12,'2016-06-15 09:39:46'),(113,'Hosting Location','TEXT',100,75,0,0,2,12,'2016-06-15 09:41:14',12,'2016-06-15 09:41:14'),(114,'Name','TEXT',100,76,0,1,1,15,'2016-06-22 11:12:15',15,'2016-06-22 11:12:15');
/*!40000 ALTER TABLE `asset_template_column` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_type`
--

DROP TABLE IF EXISTS `asset_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetTypeName` varchar(45) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTypeName_UNIQUE` (`assetTypeName`),
  UNIQUE KEY `UK_q1gprhj5b79ufwicjvkvef4fw` (`assetTypeName`),
  KEY `createdByFk1_idx` (`createdBy`),
  KEY `updatedByFk1_idx` (`updatedBy`),
  CONSTRAINT `createdByFk1` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `updatedByFk1` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_type`
--

LOCK TABLES `asset_type` WRITE;
/*!40000 ALTER TABLE `asset_type` DISABLE KEYS */;
INSERT INTO `asset_type` VALUES (1,'Application',0,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(2,'Server',0,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(3,'Database',0,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(4,'Person',0,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(5,'Business Services',0,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(6,'IT Services',0,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(7,'Interfaces',0,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(8,'Relationship',0,1,'2016-06-23 06:47:04',1,'2016-06-23 06:47:04');
/*!40000 ALTER TABLE `asset_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_type_style`
--

DROP TABLE IF EXISTS `asset_type_style`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_type_style` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_type_id` int(11) NOT NULL,
  `asset_colour` varchar(45) NOT NULL,
  `asset_image` blob,
  `workspaceId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTypeId_workspace_uq` (`asset_type_id`,`workspaceId`),
  KEY `assetTypeId_fk_idx` (`asset_type_id`),
  KEY `id_idx` (`asset_type_id`),
  KEY `assetTypeStyle_workspaceId_fk_idx` (`workspaceId`),
  CONSTRAINT `assetTypeId_fk` FOREIGN KEY (`asset_type_id`) REFERENCES `asset_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `assetTypeStyle_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_type_style`
--

LOCK TABLES `asset_type_style` WRITE;
/*!40000 ALTER TABLE `asset_type_style` DISABLE KEYS */;
INSERT INTO `asset_type_style` VALUES (1,1,'#8AC007',NULL,NULL),(2,2,'#ADD8E6',NULL,NULL),(3,3,'#FFFF00',NULL,NULL),(4,4,'#FF8C00',NULL,NULL),(5,5,'#FF8C00',NULL,NULL),(6,6,'#FF8C00',NULL,NULL),(7,7,'#FF8C00',NULL,NULL);
/*!40000 ALTER TABLE `asset_type_style` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bcm`
--

DROP TABLE IF EXISTS `bcm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bcm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bcmId` int(11) DEFAULT NULL,
  `bcmName` varchar(255) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `bcm_workspaceId_idx` (`workspaceId`),
  KEY `bcm_createdBy_idx` (`createdBy`),
  KEY `bcm_updatedBy_idx` (`updatedBy`),
  KEY `bcm_bcmId_idx` (`bcmId`),
  CONSTRAINT `bcm_bcmId` FOREIGN KEY (`bcmId`) REFERENCES `bcm` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_createdBy` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_updatedBy` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_workspaceId` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bcm`
--

LOCK TABLES `bcm` WRITE;
/*!40000 ALTER TABLE `bcm` DISABLE KEYS */;
INSERT INTO `bcm` VALUES (1,NULL,'Globalization BCM',NULL,13,'2016-06-15 18:16:28',13,'2016-06-15 18:16:28'),(2,1,'Globalization BCM WS',9,13,'2016-06-15 18:56:21',13,'2016-06-15 18:56:21');
/*!40000 ALTER TABLE `bcm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bcm_level`
--

DROP TABLE IF EXISTS `bcm_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bcm_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentBcmLevelId` int(11) DEFAULT NULL,
  `bcmId` int(11) NOT NULL,
  `levelNumber` int(2) NOT NULL,
  `sequenceNumber` int(11) NOT NULL,
  `bcmLevelName` varchar(60) NOT NULL,
  `category` varchar(30) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `bcm_level_parentBcmLevelId_fk_idx` (`parentBcmLevelId`),
  KEY `bcm_level_bcmId_fk_idx` (`bcmId`),
  KEY `bcm_level_createdBy_fk_idx` (`createdBy`),
  KEY `bcm_level_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `bcm_level_bcmId_fk` FOREIGN KEY (`bcmId`) REFERENCES `bcm` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_level_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_level_parentBcmLevelId_fk` FOREIGN KEY (`parentBcmLevelId`) REFERENCES `bcm_level` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bcm_level_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=285 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bcm_level`
--

LOCK TABLES `bcm_level` WRITE;
/*!40000 ALTER TABLE `bcm_level` DISABLE KEYS */;
INSERT INTO `bcm_level` VALUES (1,NULL,1,0,1,'Sales','',13,'2016-06-15 18:17:01',13,'2016-06-15 18:17:01'),(2,NULL,1,0,2,'Marketing','',13,'2016-06-15 18:17:19',13,'2016-06-15 18:17:19'),(3,NULL,1,0,3,'Contract Management','',13,'2016-06-15 18:17:36',13,'2016-06-15 18:17:36'),(4,NULL,1,0,4,'Legal & Regulatory Affairs','',13,'2016-06-15 18:17:51',13,'2016-06-15 18:17:51'),(5,NULL,1,0,5,'Finance','',13,'2016-06-15 18:18:01',13,'2016-06-15 18:18:01'),(6,NULL,1,0,6,'Member Experiences / CRM','',13,'2016-06-15 18:18:23',13,'2016-06-15 18:18:23'),(7,NULL,1,0,7,'Facility Management','',13,'2016-06-15 18:18:50',13,'2016-06-15 18:18:50'),(8,1,1,1,1,'Sales Tracking','Front End',13,'2016-06-15 18:21:06',13,'2016-06-15 18:21:06'),(9,8,1,2,1,'Sales Inventory Control','',13,'2016-06-15 18:21:22',13,'2016-06-15 18:21:22'),(10,8,1,2,2,'Sales Payments','',13,'2016-06-15 18:21:33',13,'2016-06-15 18:21:33'),(12,8,1,2,3,'Monitor status of all sales','',13,'2016-06-15 18:21:51',13,'2016-06-15 18:21:51'),(13,8,1,2,4,'Upgrades/ DownGrades','',13,'2016-06-15 18:22:03',13,'2016-06-15 18:22:03'),(14,8,1,2,5,'Sales Reporting(Manual)','',13,'2016-06-15 18:22:17',13,'2016-06-15 18:22:17'),(15,8,1,2,6,'Staff Performance Tracking','',13,'2016-06-15 18:22:30',13,'2016-06-15 18:22:30'),(16,8,1,2,7,'Account Management','',13,'2016-06-15 18:22:41',13,'2016-06-15 18:22:41'),(17,2,1,1,1,'Lead Management and Fulfillment','Front End',13,'2016-06-15 18:23:07',13,'2016-06-15 18:23:07'),(18,17,1,2,1,'Collect lead from various Sources','',13,'2016-06-15 18:23:19',13,'2016-06-15 18:23:19'),(19,17,1,2,2,'Qualify and prioritize leads','',13,'2016-06-15 18:23:30',13,'2016-06-15 18:23:30'),(20,17,1,2,3,'Distribute leads to responsible parties','',13,'2016-06-15 18:23:38',13,'2016-06-15 18:23:38'),(21,17,1,2,4,'OPC Management','',13,'2016-06-15 18:23:45',13,'2016-06-15 18:23:45'),(22,17,1,2,5,'Telemarketing','',13,'2016-06-15 18:23:53',13,'2016-06-15 18:23:53'),(23,17,1,2,6,'Mini-Vac Fulfillment','',13,'2016-06-15 18:24:05',13,'2016-06-15 18:24:05'),(24,17,1,2,7,'DNC Scrubbing','',13,'2016-06-15 18:24:12',13,'2016-06-15 18:24:12'),(25,2,1,1,2,'Promotion Management','Front End',13,'2016-06-15 18:24:41',13,'2016-06-15 18:24:41'),(26,25,1,2,1,'Promotion Funds Management','',13,'2016-06-15 18:24:50',13,'2016-06-15 18:24:50'),(27,25,1,2,2,'Promotion Claims Management','',13,'2016-06-15 18:24:56',13,'2016-06-15 18:24:56'),(28,25,1,2,3,'Campaign Management','',13,'2016-06-15 18:25:07',13,'2016-06-15 18:25:07'),(29,25,1,2,4,'Advertising','',13,'2016-06-15 18:25:14',13,'2016-06-15 18:25:14'),(30,25,1,2,5,'Marketing Survey','',13,'2016-06-15 18:25:22',13,'2016-06-15 18:25:22'),(31,25,1,2,6,'Email  Notifications and Referals','',13,'2016-06-15 18:25:30',13,'2016-06-15 18:25:30'),(32,2,1,1,3,'Tour Management','Front End',13,'2016-06-15 18:25:58',13,'2016-06-15 18:25:58'),(33,32,1,2,1,'Product information','',13,'2016-06-15 18:26:08',13,'2016-06-15 18:26:08'),(34,32,1,2,2,'Financing Overview','',13,'2016-06-15 18:26:15',13,'2016-06-15 18:26:15'),(35,32,1,2,3,'Points Overview','',13,'2016-06-15 18:26:21',13,'2016-06-15 18:26:21'),(36,32,1,2,4,'Tour Reservations','',13,'2016-06-15 18:26:28',13,'2016-06-15 18:26:28'),(37,32,1,2,5,'Tour Flow','',13,'2016-06-15 18:26:35',13,'2016-06-15 18:26:35'),(38,32,1,2,6,'Tour Analysis','',13,'2016-06-15 18:26:42',13,'2016-06-15 18:26:42'),(39,32,1,2,7,'Tour Modification and Cancellation','',13,'2016-06-15 18:26:49',13,'2016-06-15 18:26:49'),(40,32,1,2,8,'Transportation Management','',13,'2016-06-15 18:26:57',13,'2016-06-15 18:26:57'),(41,2,1,1,4,'Premium Inventory','Front Office',13,'2016-06-15 18:27:12',13,'2016-06-15 18:27:12'),(42,41,1,2,1,'Gifting at Concierge/Exit Desk','',13,'2016-06-15 18:27:24',13,'2016-06-15 18:27:24'),(43,41,1,2,2,'Inventory','',13,'2016-06-15 18:27:30',13,'2016-06-15 18:27:30'),(44,41,1,2,3,'Re-orders','',13,'2016-06-15 18:27:39',13,'2016-06-15 18:27:39'),(45,41,1,2,4,'Purchasing Advice','',13,'2016-06-15 18:27:49',13,'2016-06-15 18:27:49'),(46,41,1,2,5,'Audit Trail','',13,'2016-06-15 18:27:56',13,'2016-06-15 18:27:56'),(47,3,1,1,1,'Contract Creation','Front Office',13,'2016-06-15 18:28:54',13,'2016-06-15 18:28:54'),(48,47,1,2,1,'Proposal Request','',13,'2016-06-15 18:29:05',13,'2016-06-15 18:29:05'),(49,47,1,2,2,'Contract Repository','',13,'2016-06-15 18:29:17',13,'2016-06-15 18:29:17'),(50,47,1,2,3,'Contract Agreement','',13,'2016-06-15 18:29:24',13,'2016-06-15 18:29:24'),(51,47,1,2,4,'Contract preparation','',13,'2016-06-15 18:29:34',13,'2016-06-15 18:29:34'),(52,47,1,2,5,'Contract Printing','',13,'2016-06-15 18:29:41',13,'2016-06-15 18:29:41'),(53,47,1,2,6,'Credit Underwriting','',13,'2016-06-15 18:29:48',13,'2016-06-15 18:29:48'),(54,47,1,2,7,'Contract Billing/Escrow','',13,'2016-06-15 18:29:54',13,'2016-06-15 18:29:54'),(55,47,1,2,8,'Contract Review and Approval','',13,'2016-06-15 18:30:01',13,'2016-06-15 18:30:01'),(56,47,1,2,9,'Regulatory Affairs','',13,'2016-06-15 18:30:09',13,'2016-06-15 18:30:09'),(57,47,1,2,10,'Wait List Management','',13,'2016-06-15 18:30:18',13,'2016-06-15 18:30:18'),(58,3,1,1,2,'Inventory Set Up','Back Office',13,'2016-06-15 18:30:39',13,'2016-06-15 18:30:39'),(59,58,1,2,1,'Resort Registration','',13,'2016-06-15 18:30:51',13,'2016-06-15 18:30:51'),(60,58,1,2,2,'Resort Set up','',13,'2016-06-15 18:30:59',13,'2016-06-15 18:30:59'),(61,58,1,2,3,'Resort Set up','',13,'2016-06-15 18:31:00',13,'2016-06-15 18:31:00'),(62,58,1,2,4,'Points and Units','',13,'2016-06-15 18:31:06',13,'2016-06-15 18:31:06'),(63,3,1,1,3,'Contract Closure','Back Office',13,'2016-06-15 18:31:35',13,'2016-06-15 18:31:35'),(64,63,1,2,1,'Insurance Issuance','',13,'2016-06-15 18:31:44',13,'2016-06-15 18:31:44'),(65,63,1,2,2,'External Registration','',13,'2016-06-15 18:32:40',13,'2016-06-15 18:32:40'),(66,63,1,2,3,'Final Documentation','',13,'2016-06-15 18:32:47',13,'2016-06-15 18:32:47'),(67,3,1,1,4,'Manage Contract','Back Office',13,'2016-06-15 18:33:04',13,'2016-06-15 18:33:04'),(68,67,1,2,1,'Contract Status','',13,'2016-06-15 18:33:26',13,'2016-06-15 18:33:26'),(69,67,1,2,2,'Resale','',13,'2016-06-15 18:33:32',13,'2016-06-15 18:33:32'),(70,67,1,2,3,'Contract Transfer','',13,'2016-06-15 18:33:40',13,'2016-06-15 18:33:40'),(71,67,1,2,4,'Contract Modifications','',13,'2016-06-15 18:33:48',13,'2016-06-15 18:33:48'),(72,3,1,1,5,'Documents','Back Office',13,'2016-06-15 18:34:08',13,'2016-06-15 18:34:08'),(73,72,1,2,1,'Sales Contracts','',13,'2016-06-15 18:34:14',13,'2016-06-15 18:34:14'),(74,72,1,2,2,'Electronic Signature','',13,'2016-06-15 18:34:20',13,'2016-06-15 18:34:20'),(75,72,1,2,3,'Payment Receipts Loan Notes','',13,'2016-06-15 18:34:27',13,'2016-06-15 18:34:27'),(76,72,1,2,4,'New Owner Letters','',13,'2016-06-15 18:34:32',13,'2016-06-15 18:34:32'),(77,72,1,2,5,'Statements','',13,'2016-06-15 18:34:39',13,'2016-06-15 18:34:39'),(78,72,1,2,6,'Correspondence History','',13,'2016-06-15 18:34:46',13,'2016-06-15 18:34:46'),(79,72,1,2,7,'Document Repository','',13,'2016-06-15 18:34:53',13,'2016-06-15 18:34:53'),(80,4,1,1,1,'Member Verifications','Back Office',13,'2016-06-15 18:35:17',13,'2016-06-15 18:35:17'),(81,80,1,2,1,'OFAC','',13,'2016-06-15 18:35:27',13,'2016-06-15 18:35:27'),(82,80,1,2,2,'Member Database Scan','',13,'2016-06-15 18:35:35',13,'2016-06-15 18:35:35'),(83,5,1,1,1,'Dues/Maintenance Fees','Back Office',13,'2016-06-15 18:36:20',13,'2016-06-15 18:36:20'),(84,83,1,2,1,'Assesments','',13,'2016-06-15 18:36:32',13,'2016-06-15 18:36:32'),(85,83,1,2,2,'Statements(email, print)','',13,'2016-06-15 18:36:39',13,'2016-06-15 18:36:39'),(86,83,1,2,3,'Automatic Billing','',13,'2016-06-15 18:36:45',13,'2016-06-15 18:36:45'),(87,83,1,2,4,'Autopay','',13,'2016-06-15 18:36:54',13,'2016-06-15 18:36:54'),(88,5,1,1,2,'Financial Accounting','Back Office',13,'2016-06-15 18:37:06',13,'2016-06-15 18:37:06'),(89,88,1,2,1,'Asset & Fund Management','',13,'2016-06-15 18:37:15',13,'2016-06-15 18:37:15'),(90,88,1,2,2,'Controlling','',13,'2016-06-15 18:37:22',13,'2016-06-15 18:37:22'),(91,88,1,2,3,'General Ledger','',13,'2016-06-15 18:37:28',13,'2016-06-15 18:37:28'),(92,88,1,2,4,'Reporting','',13,'2016-06-15 18:37:34',13,'2016-06-15 18:37:34'),(93,88,1,2,5,'Search','',13,'2016-06-15 18:37:41',13,'2016-06-15 18:37:41'),(94,88,1,2,6,'Notes','',13,'2016-06-15 18:37:48',13,'2016-06-15 18:37:48'),(95,88,1,2,7,'Reporting/queries','',13,'2016-06-15 18:37:54',13,'2016-06-15 18:37:54'),(96,5,1,1,3,'Receiveables','Back Office',13,'2016-06-15 18:38:17',13,'2016-06-15 18:38:17'),(98,96,1,2,1,'Transaction Management','',13,'2016-06-15 18:47:27',13,'2016-06-15 18:47:27'),(99,96,1,2,2,'Adjustments and Charges','',13,'2016-06-15 18:47:36',13,'2016-06-15 18:47:36'),(100,96,1,2,3,'Mail Invoices to Customer','',13,'2016-06-15 18:47:43',13,'2016-06-15 18:47:43'),(101,5,1,1,4,'Escrow','Back Office',13,'2016-06-15 18:48:20',13,'2016-06-15 18:48:20'),(102,101,1,2,1,'Downpayment Management','',13,'2016-06-15 18:48:30',13,'2016-06-15 18:48:30'),(103,101,1,2,2,'3rd Party Escrow','',13,'2016-06-15 18:48:43',13,'2016-06-15 18:48:43'),(104,101,1,2,3,'Check/wire payments','',13,'2016-06-15 18:48:54',13,'2016-06-15 18:48:54'),(105,5,1,1,5,'Treasury and Financial Risk Management','Back Office',13,'2016-06-15 18:49:13',13,'2016-06-15 18:49:13'),(106,105,1,2,1,'Payments and Bank Communications','',13,'2016-06-15 18:49:20',13,'2016-06-15 18:49:20'),(107,105,1,2,2,'Cash and Liquidity Management','',13,'2016-06-15 18:49:27',13,'2016-06-15 18:49:27'),(108,105,1,2,3,'Debt and Investment Management','',13,'2016-06-15 18:49:34',13,'2016-06-15 18:49:34'),(109,5,1,1,6,'Risk and Compliance','Back Office',13,'2016-06-15 18:50:00',13,'2016-06-15 18:50:00'),(110,109,1,2,1,'Enterprise Risk Management','',13,'2016-06-15 18:50:09',13,'2016-06-15 18:50:09'),(111,109,1,2,2,'Access Governance','',13,'2016-06-15 18:50:15',13,'2016-06-15 18:50:15'),(112,109,1,2,3,'International Trade Management','',13,'2016-06-15 18:50:22',13,'2016-06-15 18:50:22'),(113,109,1,2,4,'Audit Management','',13,'2016-06-15 18:50:29',13,'2016-06-15 18:50:29'),(114,6,1,1,1,'Member Services/ MS Operations','Front Office',13,'2016-06-15 18:50:48',13,'2016-06-15 18:50:48'),(115,6,1,1,2,'Event Management','Front Office',13,'2016-06-15 18:51:03',13,'2016-06-15 18:51:03'),(116,6,1,1,3,'Member Experiences','Front Office',13,'2016-06-15 18:51:38',13,'2016-06-15 18:51:38'),(117,114,1,2,1,'Training and Communication','',13,'2016-06-15 18:51:57',13,'2016-06-15 18:51:57'),(118,114,1,2,2,'Labour','',13,'2016-06-15 18:52:04',13,'2016-06-15 18:52:04'),(119,114,1,2,3,'MEM Sat/Teamline','',13,'2016-06-15 18:52:13',13,'2016-06-15 18:52:13'),(120,114,1,2,4,'Trip Insurance','',13,'2016-06-15 18:52:20',13,'2016-06-15 18:52:20'),(121,114,1,2,5,'Customer Portal','',13,'2016-06-15 18:52:28',13,'2016-06-15 18:52:28'),(122,115,1,2,1,'Event Registration','',13,'2016-06-15 18:52:38',13,'2016-06-15 18:52:38'),(123,115,1,2,2,'Event Planning','',13,'2016-06-15 18:52:44',13,'2016-06-15 18:52:44'),(124,115,1,2,3,'Event Catering Management','',13,'2016-06-15 18:52:51',13,'2016-06-15 18:52:51'),(125,114,1,2,6,'Guest Arrival Management','',13,'2016-06-15 18:52:58',13,'2016-06-15 18:52:58'),(126,116,1,2,1,'VPM','',13,'2016-06-15 18:53:23',13,'2016-06-15 18:53:23'),(127,7,1,1,1,'RESERVATIONS','Front Office',13,'2016-06-15 18:53:43',13,'2016-06-15 18:53:43'),(128,7,1,1,2,'Corporate Resort Management','Front Office',13,'2016-06-15 18:53:54',13,'2016-06-15 18:53:54'),(129,7,1,1,3,'PROPERTYÂ MANAGEMENT','Back Office',13,'2016-06-15 18:54:09',13,'2016-06-15 18:54:09'),(130,127,1,2,1,'Central Reservations','',13,'2016-06-15 18:54:19',13,'2016-06-15 18:54:19'),(131,127,1,2,2,'Control Reservation Inventory','',13,'2016-06-15 18:54:26',13,'2016-06-15 18:54:26'),(132,127,1,2,3,'Track Reservation','',13,'2016-06-15 18:54:32',13,'2016-06-15 18:54:32'),(133,127,1,2,4,'Contract Usage rules and restrictions','',13,'2016-06-15 18:54:40',13,'2016-06-15 18:54:40'),(134,127,1,2,5,'Owner Servicing','',13,'2016-06-15 18:54:46',13,'2016-06-15 18:54:46'),(135,127,1,2,6,'Rate Management','',13,'2016-06-15 18:54:53',13,'2016-06-15 18:54:53'),(136,128,1,2,1,'Resort Set up','',13,'2016-06-15 18:55:01',13,'2016-06-15 18:55:01'),(137,128,1,2,2,'Inventory Management','',13,'2016-06-15 18:55:07',13,'2016-06-15 18:55:07'),(138,129,1,2,1,'Concierge Services','',13,'2016-06-15 18:55:16',13,'2016-06-15 18:55:16'),(139,129,1,2,2,'Call Managemnt','',13,'2016-06-15 18:55:23',13,'2016-06-15 18:55:23'),(140,129,1,2,3,'Lease Management','',13,'2016-06-15 18:55:32',13,'2016-06-15 18:55:32'),(141,129,1,2,4,'Folio Management','',13,'2016-06-15 18:55:38',13,'2016-06-15 18:55:38'),(142,129,1,2,5,'Housekeeping','',13,'2016-06-15 18:55:45',13,'2016-06-15 18:55:45'),(143,129,1,2,6,'Tarrif Enquiry','',13,'2016-06-15 18:55:53',13,'2016-06-15 18:55:53'),(144,NULL,2,0,1,'Sales','',13,'2016-06-15 18:56:22',13,'2016-06-15 18:56:22'),(145,144,2,1,1,'Sales Tracking','Front End',13,'2016-06-15 18:56:23',13,'2016-06-15 18:56:23'),(146,145,2,2,1,'Sales Inventory Control','',13,'2016-06-15 18:56:23',13,'2016-06-15 18:56:23'),(147,145,2,2,2,'Sales Payments','',13,'2016-06-15 18:56:24',13,'2016-06-15 18:56:24'),(148,145,2,2,3,'Monitor status of all sales','',13,'2016-06-15 18:56:24',13,'2016-06-15 18:56:24'),(149,145,2,2,4,'Upgrades/ DownGrades','',13,'2016-06-15 18:56:25',13,'2016-06-15 18:56:25'),(150,145,2,2,5,'Sales Reporting(Manual)','',13,'2016-06-15 18:56:25',13,'2016-06-15 18:56:25'),(151,145,2,2,6,'Staff Performance Tracking','',13,'2016-06-15 18:56:26',13,'2016-06-15 18:56:26'),(152,145,2,2,7,'Account Management','',13,'2016-06-15 18:56:26',13,'2016-06-15 18:56:26'),(153,NULL,2,0,2,'Marketing','',13,'2016-06-15 18:56:27',13,'2016-06-15 18:56:27'),(154,153,2,1,1,'Lead Management and Fulfillment','Front End',13,'2016-06-15 18:56:27',13,'2016-06-15 18:56:27'),(155,154,2,2,1,'Collect lead from various Sources','',13,'2016-06-15 18:56:27',13,'2016-06-15 18:56:28'),(156,154,2,2,2,'Qualify and prioritize leads','',13,'2016-06-15 18:56:28',13,'2016-06-15 18:56:28'),(157,154,2,2,3,'Distribute leads to responsible parties','',13,'2016-06-15 18:56:28',13,'2016-06-15 18:56:29'),(158,154,2,2,4,'OPC Management','',13,'2016-06-15 18:56:29',13,'2016-06-15 18:56:29'),(159,154,2,2,5,'Telemarketing','',13,'2016-06-15 18:56:29',13,'2016-06-15 18:56:29'),(160,154,2,2,6,'Mini-Vac Fulfillment','',13,'2016-06-15 18:56:30',13,'2016-06-15 18:56:30'),(161,154,2,2,7,'DNC Scrubbing','',13,'2016-06-15 18:56:30',13,'2016-06-15 18:56:30'),(162,153,2,1,2,'Promotion Management','Front End',13,'2016-06-15 18:56:31',13,'2016-06-15 18:56:31'),(163,162,2,2,1,'Promotion Funds Management','',13,'2016-06-15 18:56:31',13,'2016-06-15 18:56:31'),(164,162,2,2,2,'Promotion Claims Management','',13,'2016-06-15 18:56:32',13,'2016-06-15 18:56:32'),(165,162,2,2,3,'Campaign Management','',13,'2016-06-15 18:56:32',13,'2016-06-15 18:56:32'),(166,162,2,2,4,'Advertising','',13,'2016-06-15 18:56:32',13,'2016-06-15 18:56:33'),(167,162,2,2,5,'Marketing Survey','',13,'2016-06-15 18:56:33',13,'2016-06-15 18:56:33'),(168,162,2,2,6,'Email  Notifications and Referals','',13,'2016-06-15 18:56:33',13,'2016-06-15 18:56:33'),(169,153,2,1,3,'Tour Management','Front End',13,'2016-06-15 18:56:34',13,'2016-06-15 18:56:34'),(170,169,2,2,1,'Product information','',13,'2016-06-15 18:56:35',13,'2016-06-15 18:56:35'),(171,169,2,2,2,'Financing Overview','',13,'2016-06-15 18:56:35',13,'2016-06-15 18:56:35'),(172,169,2,2,3,'Points Overview','',13,'2016-06-15 18:56:36',13,'2016-06-15 18:56:36'),(173,169,2,2,4,'Tour Reservations','',13,'2016-06-15 18:56:36',13,'2016-06-15 18:56:36'),(174,169,2,2,5,'Tour Flow','',13,'2016-06-15 18:56:36',13,'2016-06-15 18:56:37'),(175,169,2,2,6,'Tour Analysis','',13,'2016-06-15 18:56:37',13,'2016-06-15 18:56:37'),(176,169,2,2,7,'Tour Modification and Cancellation','',13,'2016-06-15 18:56:37',13,'2016-06-15 18:56:37'),(177,169,2,2,8,'Transportation Management','',13,'2016-06-15 18:56:38',13,'2016-06-15 18:56:38'),(178,153,2,1,4,'Premium Inventory','Front Office',13,'2016-06-15 18:56:38',13,'2016-06-15 18:56:38'),(179,178,2,2,1,'Gifting at Concierge/Exit Desk','',13,'2016-06-15 18:56:39',13,'2016-06-15 18:56:39'),(180,178,2,2,2,'Inventory','',13,'2016-06-15 18:56:39',13,'2016-06-15 18:56:39'),(181,178,2,2,3,'Re-orders','',13,'2016-06-15 18:56:39',13,'2016-06-15 18:56:40'),(182,178,2,2,4,'Purchasing Advice','',13,'2016-06-15 18:56:40',13,'2016-06-15 18:56:40'),(183,178,2,2,5,'Audit Trail','',13,'2016-06-15 18:56:41',13,'2016-06-15 18:56:41'),(184,NULL,2,0,3,'Contract Management','',13,'2016-06-15 18:56:41',13,'2016-06-15 18:56:41'),(185,184,2,1,1,'Contract Creation','Front Office',13,'2016-06-15 18:56:42',13,'2016-06-15 18:56:42'),(186,185,2,2,1,'Proposal Request','',13,'2016-06-15 18:56:42',13,'2016-06-15 18:56:42'),(187,185,2,2,2,'Contract Repository','',13,'2016-06-15 18:56:43',13,'2016-06-15 18:56:43'),(188,185,2,2,3,'Contract Agreement','',13,'2016-06-15 18:56:43',13,'2016-06-15 18:56:43'),(189,185,2,2,4,'Contract preparation','',13,'2016-06-15 18:56:44',13,'2016-06-15 18:56:44'),(190,185,2,2,5,'Contract Printing','',13,'2016-06-15 18:56:44',13,'2016-06-15 18:56:44'),(191,185,2,2,6,'Credit Underwriting','',13,'2016-06-15 18:56:45',13,'2016-06-15 18:56:45'),(192,185,2,2,7,'Contract Billing/Escrow','',13,'2016-06-15 18:56:45',13,'2016-06-15 18:56:45'),(193,185,2,2,8,'Contract Review and Approval','',13,'2016-06-15 18:56:45',13,'2016-06-15 18:56:45'),(194,185,2,2,9,'Regulatory Affairs','',13,'2016-06-15 18:56:46',13,'2016-06-15 18:56:46'),(195,185,2,2,10,'Wait List Management','',13,'2016-06-15 18:56:46',13,'2016-06-15 18:56:46'),(196,184,2,1,2,'Inventory Set Up','Back Office',13,'2016-06-15 18:56:47',13,'2016-06-15 18:56:47'),(197,196,2,2,1,'Resort Registration','',13,'2016-06-15 18:56:47',13,'2016-06-15 18:56:47'),(198,196,2,2,2,'Resort Set up','',13,'2016-06-15 18:56:48',13,'2016-06-15 18:56:48'),(199,196,2,2,3,'Resort Set up','',13,'2016-06-15 18:56:48',13,'2016-06-15 18:56:48'),(200,196,2,2,4,'Points and Units','',13,'2016-06-15 18:56:49',13,'2016-06-15 18:56:49'),(201,184,2,1,3,'Contract Closure','Back Office',13,'2016-06-15 18:56:49',13,'2016-06-15 18:56:49'),(202,201,2,2,1,'Insurance Issuance','',13,'2016-06-15 18:56:50',13,'2016-06-15 18:56:50'),(203,201,2,2,2,'External Registration','',13,'2016-06-15 18:56:50',13,'2016-06-15 18:56:50'),(204,201,2,2,3,'Final Documentation','',13,'2016-06-15 18:56:51',13,'2016-06-15 18:56:51'),(205,184,2,1,4,'Manage Contract','Back Office',13,'2016-06-15 18:56:51',13,'2016-06-15 18:56:51'),(206,205,2,2,1,'Contract Status','',13,'2016-06-15 18:56:51',13,'2016-06-15 18:56:52'),(207,205,2,2,2,'Resale','',13,'2016-06-15 18:56:52',13,'2016-06-15 18:56:52'),(208,205,2,2,3,'Contract Transfer','',13,'2016-06-15 18:56:52',13,'2016-06-15 18:56:53'),(209,205,2,2,4,'Contract Modifications','',13,'2016-06-15 18:56:53',13,'2016-06-15 18:56:53'),(210,184,2,1,5,'Documents','Back Office',13,'2016-06-15 18:56:53',13,'2016-06-15 18:56:53'),(211,210,2,2,1,'Sales Contracts','',13,'2016-06-15 18:56:54',13,'2016-06-15 18:56:54'),(212,210,2,2,2,'Electronic Signature','',13,'2016-06-15 18:56:54',13,'2016-06-15 18:56:54'),(213,210,2,2,3,'Payment Receipts Loan Notes','',13,'2016-06-15 18:56:55',13,'2016-06-15 18:56:55'),(214,210,2,2,4,'New Owner Letters','',13,'2016-06-15 18:56:55',13,'2016-06-15 18:56:55'),(215,210,2,2,5,'Statements','',13,'2016-06-15 18:56:56',13,'2016-06-15 18:56:56'),(216,210,2,2,6,'Correspondence History','',13,'2016-06-15 18:56:56',13,'2016-06-15 18:56:56'),(217,210,2,2,7,'Document Repository','',13,'2016-06-15 18:56:57',13,'2016-06-15 18:56:57'),(218,NULL,2,0,4,'Legal & Regulatory Affairs','',13,'2016-06-15 18:56:57',13,'2016-06-15 18:56:57'),(219,218,2,1,1,'Member Verifications','Back Office',13,'2016-06-15 18:56:57',13,'2016-06-15 18:56:58'),(220,219,2,2,1,'OFAC','',13,'2016-06-15 18:56:58',13,'2016-06-15 18:56:58'),(221,219,2,2,2,'Member Database Scan','',13,'2016-06-15 18:56:58',13,'2016-06-15 18:56:59'),(222,NULL,2,0,5,'Finance','',13,'2016-06-15 18:56:59',13,'2016-06-15 18:56:59'),(223,222,2,1,1,'Dues/Maintenance Fees','Back Office',13,'2016-06-15 18:56:59',13,'2016-06-15 18:56:59'),(224,223,2,2,1,'Assesments','',13,'2016-06-15 18:57:00',13,'2016-06-15 18:57:00'),(225,223,2,2,2,'Statements(email, print)','',13,'2016-06-15 18:57:00',13,'2016-06-15 18:57:00'),(226,223,2,2,3,'Automatic Billing','',13,'2016-06-15 18:57:01',13,'2016-06-15 18:57:01'),(227,223,2,2,4,'Autopay','',13,'2016-06-15 18:57:01',13,'2016-06-15 18:57:01'),(228,222,2,1,2,'Financial Accounting','Back Office',13,'2016-06-15 18:57:02',13,'2016-06-15 18:57:02'),(229,228,2,2,1,'Asset & Fund Management','',13,'2016-06-15 18:57:02',13,'2016-06-15 18:57:02'),(230,228,2,2,2,'Controlling','',13,'2016-06-15 18:57:03',13,'2016-06-15 18:57:03'),(231,228,2,2,3,'General Ledger','',13,'2016-06-15 18:57:03',13,'2016-06-15 18:57:03'),(232,228,2,2,4,'Reporting','',13,'2016-06-15 18:57:04',13,'2016-06-15 18:57:04'),(233,228,2,2,5,'Search','',13,'2016-06-15 18:57:04',13,'2016-06-15 18:57:04'),(234,228,2,2,6,'Notes','',13,'2016-06-15 18:57:05',13,'2016-06-15 18:57:05'),(235,228,2,2,7,'Reporting/queries','',13,'2016-06-15 18:57:05',13,'2016-06-15 18:57:05'),(236,222,2,1,3,'Receiveables','Back Office',13,'2016-06-15 18:57:06',13,'2016-06-15 18:57:06'),(237,236,2,2,1,'Transaction Management','',13,'2016-06-15 18:57:06',13,'2016-06-15 18:57:06'),(238,236,2,2,2,'Adjustments and Charges','',13,'2016-06-15 18:57:06',13,'2016-06-15 18:57:07'),(239,236,2,2,3,'Mail Invoices to Customer','',13,'2016-06-15 18:57:07',13,'2016-06-15 18:57:07'),(240,222,2,1,4,'Escrow','Back Office',13,'2016-06-15 18:57:07',13,'2016-06-15 18:57:07'),(241,240,2,2,1,'Downpayment Management','',13,'2016-06-15 18:57:08',13,'2016-06-15 18:57:08'),(242,240,2,2,2,'3rd Party Escrow','',13,'2016-06-15 18:57:08',13,'2016-06-15 18:57:08'),(243,240,2,2,3,'Check/wire payments','',13,'2016-06-15 18:57:09',13,'2016-06-15 18:57:09'),(244,222,2,1,5,'Treasury and Financial Risk Management','Back Office',13,'2016-06-15 18:57:09',13,'2016-06-15 18:57:09'),(245,244,2,2,1,'Payments and Bank Communications','',13,'2016-06-15 18:57:09',13,'2016-06-15 18:57:10'),(246,244,2,2,2,'Cash and Liquidity Management','',13,'2016-06-15 18:57:10',13,'2016-06-15 18:57:10'),(247,244,2,2,3,'Debt and Investment Management','',13,'2016-06-15 18:57:10',13,'2016-06-15 18:57:11'),(248,222,2,1,6,'Risk and Compliance','Back Office',13,'2016-06-15 18:57:11',13,'2016-06-15 18:57:11'),(249,248,2,2,1,'Enterprise Risk Management','',13,'2016-06-15 18:57:11',13,'2016-06-15 18:57:11'),(250,248,2,2,2,'Access Governance','',13,'2016-06-15 18:57:12',13,'2016-06-15 18:57:12'),(251,248,2,2,3,'International Trade Management','',13,'2016-06-15 18:57:12',13,'2016-06-15 18:57:12'),(252,248,2,2,4,'Audit Management','',13,'2016-06-15 18:57:12',13,'2016-06-15 18:57:13'),(253,NULL,2,0,6,'Member Experiences / CRM','',13,'2016-06-15 18:57:13',13,'2016-06-15 18:57:13'),(254,253,2,1,1,'Member Services/ MS Operations','Front Office',13,'2016-06-15 18:57:13',13,'2016-06-15 18:57:13'),(255,254,2,2,1,'Training and Communication','',13,'2016-06-15 18:57:14',13,'2016-06-15 18:57:14'),(256,254,2,2,2,'Labour','',13,'2016-06-15 18:57:14',13,'2016-06-15 18:57:14'),(257,254,2,2,3,'MEM Sat/Teamline','',13,'2016-06-15 18:57:15',13,'2016-06-15 18:57:15'),(258,254,2,2,4,'Trip Insurance','',13,'2016-06-15 18:57:15',13,'2016-06-15 18:57:15'),(259,254,2,2,5,'Customer Portal','',13,'2016-06-15 18:57:15',13,'2016-06-15 18:57:16'),(260,254,2,2,6,'Guest Arrival Management','',13,'2016-06-15 18:57:16',13,'2016-06-15 18:57:16'),(261,253,2,1,2,'Event Management','Front Office',13,'2016-06-15 18:57:16',13,'2016-06-15 18:57:16'),(262,261,2,2,1,'Event Registration','',13,'2016-06-15 18:57:17',13,'2016-06-15 18:57:17'),(263,261,2,2,2,'Event Planning','',13,'2016-06-15 18:57:17',13,'2016-06-15 18:57:17'),(264,261,2,2,3,'Event Catering Management','',13,'2016-06-15 18:57:18',13,'2016-06-15 18:57:18'),(265,253,2,1,3,'Member Experiences','Front Office',13,'2016-06-15 18:57:18',13,'2016-06-15 18:57:18'),(266,265,2,2,1,'VPM','',13,'2016-06-15 18:57:18',13,'2016-06-15 18:57:19'),(267,NULL,2,0,7,'Facility Management','',13,'2016-06-15 18:57:19',13,'2016-06-15 18:57:19'),(268,267,2,1,1,'RESERVATIONS','Front Office',13,'2016-06-15 18:57:19',13,'2016-06-15 18:57:19'),(269,268,2,2,1,'Central Reservations','',13,'2016-06-15 18:57:20',13,'2016-06-15 18:57:20'),(270,268,2,2,2,'Control Reservation Inventory','',13,'2016-06-15 18:57:20',13,'2016-06-15 18:57:20'),(271,268,2,2,3,'Track Reservation','',13,'2016-06-15 18:57:21',13,'2016-06-15 18:57:21'),(272,268,2,2,4,'Contract Usage rules and restrictions','',13,'2016-06-15 18:57:21',13,'2016-06-15 18:57:21'),(273,268,2,2,5,'Owner Servicing','',13,'2016-06-15 18:57:22',13,'2016-06-15 18:57:22'),(274,268,2,2,6,'Rate Management','',13,'2016-06-15 18:57:22',13,'2016-06-15 18:57:22'),(275,267,2,1,2,'Corporate Resort Management','Front Office',13,'2016-06-15 18:57:23',13,'2016-06-15 18:57:23'),(276,275,2,2,1,'Resort Set up','',13,'2016-06-15 18:57:23',13,'2016-06-15 18:57:23'),(277,275,2,2,2,'Inventory Management','',13,'2016-06-15 18:57:24',13,'2016-06-15 18:57:24'),(278,267,2,1,3,'PROPERTYÂ MANAGEMENT','Back Office',13,'2016-06-15 18:57:24',13,'2016-06-15 18:57:24'),(279,278,2,2,1,'Concierge Services','',13,'2016-06-15 18:57:24',13,'2016-06-15 18:57:25'),(280,278,2,2,2,'Call Managemnt','',13,'2016-06-15 18:57:25',13,'2016-06-15 18:57:25'),(281,278,2,2,3,'Lease Management','',13,'2016-06-15 18:57:25',13,'2016-06-15 18:57:25'),(282,278,2,2,4,'Folio Management','',13,'2016-06-15 18:57:26',13,'2016-06-15 18:57:26'),(283,278,2,2,5,'Housekeeping','',13,'2016-06-15 18:57:26',13,'2016-06-15 18:57:26'),(284,278,2,2,6,'Tarrif Enquiry','',13,'2016-06-15 18:57:27',13,'2016-06-15 18:57:27');
/*!40000 ALTER TABLE `bcm_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `category_createdBy_fk_idx` (`createdBy`),
  KEY `category_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `category_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `category_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Functional Assessment',NULL,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(2,'Application Evolution',NULL,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(3,'Technology Assessment',NULL,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(4,'Application Management',NULL,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(5,'Interface Assessment',NULL,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(6,'Server Assessment',NULL,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(7,'Cloud Readiness',NULL,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(8,'Business Benefit',NULL,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(9,'Maintenance Quotient',NULL,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(10,'Criticality Quotient',NULL,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(11,'Sourcing Alignment Index',NULL,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(12,'Utilization Quotient for Servers',NULL,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(13,'Infra Health Quotient',NULL,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(14,'Governance Quality',NULL,1,'2016-06-08 11:29:14',1,'2016-06-08 11:29:14'),(15,'Data Related Queries',NULL,14,'2016-06-13 10:25:32',14,'2016-06-13 10:25:32'),(16,'Business Criticality ',NULL,12,'2016-06-15 11:56:56',12,'2016-06-15 11:56:56'),(17,'Management Quotient',NULL,14,'2016-06-13 11:25:22',14,'2016-06-13 11:25:22'),(18,'Management Assessment',NULL,12,'2016-06-13 11:33:02',12,'2016-06-13 11:33:02'),(19,'Miscellaneous Queries',NULL,14,'2016-06-13 11:38:09',14,'2016-06-13 11:38:09'),(20,'User and Support Queries',NULL,12,'2016-06-14 07:11:14',12,'2016-06-14 07:11:14'),(21,'General Queries',NULL,12,'2016-06-14 14:06:30',12,'2016-06-14 14:06:30'),(22,'__SYS_TCO',NULL,1,'2016-09-26 10:18:15',1,'2016-09-26 10:18:15');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard`
--

DROP TABLE IF EXISTS `dashboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dashboardName` varchar(45) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_DASHBOARD_WORKSPACE_idx` (`workspaceId`),
  CONSTRAINT `FK_DASHBOARD_WORKSPACE` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard`
--

LOCK TABLES `dashboard` WRITE;
/*!40000 ALTER TABLE `dashboard` DISABLE KEYS */;
INSERT INTO `dashboard` VALUES (1,'Server view',9),(2,'Application View',9);
/*!40000 ALTER TABLE `dashboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `export_log`
--

DROP TABLE IF EXISTS `export_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `export_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(45) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `purpose` varchar(45) NOT NULL,
  `refId` int(11) NOT NULL,
  `timeDown` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `timeUp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `export_userId_fk_idx` (`userId`),
  KEY `export_workspaceId_fk_idx` (`workspaceId`),
  CONSTRAINT `export_userId_fk` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `export_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `export_log`
--

LOCK TABLES `export_log` WRITE;
/*!40000 ALTER TABLE `export_log` DISABLE KEYS */;
INSERT INTO `export_log` VALUES (1,'b4b9b0504a32710d2b844fa27b585b6f',12,14,'INV',69,'2016-06-13 12:49:00',NULL),(2,'d26714b3808724bcfdc2b4337656d411',12,14,'INV',69,'2016-06-13 12:53:00','2016-06-13 12:56:49'),(3,'481ce41e404b02ab29524313ae56c32f',9,14,'INV',70,'2016-06-13 13:03:00','2016-06-13 13:16:30'),(4,'8ad80ec9de1ff215195bb40d1bbe73bb',9,12,'INV',70,'2016-06-14 05:49:30',NULL),(5,'d7ae1af5f525355b4cebe7c2dd110374',9,13,'INV',71,'2016-06-14 16:39:11','2016-06-14 16:52:20'),(6,'1b38c0d79f3a229b87fe93ee51f72c48',10,14,'INV',72,'2016-06-15 07:18:45',NULL),(7,'7b913782e2a625631c983bd5c3b0154e',10,14,'INV',74,'2016-06-15 11:00:45',NULL),(8,'4c950b9f15f48341a21edd14b1488aab',10,14,'INV',74,'2016-06-15 11:19:55','2016-06-15 11:20:18'),(9,'8de2184bcfc8ac783f7408d070e7996e',10,14,'INV',74,'2016-06-15 11:22:01','2016-06-15 11:22:33'),(10,'79220d1df11950b5beacfa4d3cf2d25f',10,14,'INV',74,'2016-06-15 11:31:17','2016-06-15 11:31:48'),(11,'931582997f84222a565a32492babbb58',10,14,'INV',74,'2016-06-15 11:35:17','2016-06-15 11:35:40'),(12,'f7e7501d5d99dbcc7aa9c39293d94a2e',10,14,'INV',74,'2016-06-15 11:38:13','2016-06-15 11:38:34'),(13,'625fcf17acf2ee29db2d819171473beb',10,14,'INV',74,'2016-06-15 11:40:56','2016-06-15 11:41:11'),(14,'186c8db240fb95a9590ce96914243ba5',10,14,'INV',74,'2016-06-15 11:46:27','2016-06-15 11:46:43'),(15,'6db661fe75dcb5b26e022af96e887cdb',9,12,'QE',1,'2016-06-15 13:01:06',NULL),(16,'1f9b799ac99af15bc2b6a5c7e5dc8d32',9,12,'QE',1,'2016-06-15 13:25:55',NULL),(17,'9e241001ea03a6a1a4e426c696b06997',9,13,'QE',1,'2016-06-15 16:41:39',NULL),(18,'e5e240379a78988b81915adc467f1a20',9,13,'QE',1,'2016-06-15 17:02:30','2016-06-15 17:12:11'),(19,'83a549d03baf3d9ecc4109029f1827c8',9,13,'FM',1,'2016-06-15 19:02:15','2016-06-15 19:08:03'),(20,'ea154c4f83f86e24f66a8c3fcf744ee5',9,12,'FM',1,'2016-06-16 04:25:16',NULL),(21,'d2359ccc969cc1bb8fe477bdf40e1171',9,12,'FM',2,'2016-06-16 05:44:29','2016-06-16 09:32:19'),(22,'5d4e8e511cd6ee76add49f7d4490cdaf',9,14,'QE',3,'2016-06-16 06:12:43',NULL),(23,'7bc79d2d6edd99cfcc2e0b0ee5716c3f',9,14,'QE',3,'2016-06-16 06:13:23',NULL),(24,'33d705bc0326b31d54ff9a9ee6dc27c9',9,12,'QE',3,'2016-06-16 07:03:35',NULL),(25,'250fa0cd03565578bed8e7109aa85bf4',9,12,'QE',3,'2016-06-16 07:04:23',NULL),(26,'99bd3d6b8d92a17b25ca94bad6b9b3dd',9,12,'QE',3,'2016-06-16 07:05:39',NULL),(27,'03914bc46646f0558782a157fef0343c',9,12,'FM',2,'2016-06-16 09:01:21',NULL),(28,'4db6893bb9fbfef38d0fe3e971a809dd',9,12,'QE',3,'2016-06-16 09:01:48',NULL),(29,'6298b8a29cfab091b60ca4a6fbbeb959',9,12,'QE',3,'2016-06-16 09:02:30',NULL),(30,'0b9a71d9ca997fd664b2562a3a2ffc7a',9,12,'QE',3,'2016-06-16 09:54:26',NULL),(31,'48b73c68d298d8dff5ecf54a71db36d1',9,12,'QE',3,'2016-06-16 10:11:00',NULL),(32,'c2dd29bcb5fa6490e989c4bf6a0a234e',9,14,'QE',3,'2016-06-16 10:19:54',NULL),(33,'756a0db6c6679f33bdd465ac0a167854',9,12,'QE',3,'2016-06-16 10:49:46',NULL),(34,'3dbcabe786e65b508759ac7f58b674da',9,12,'QE',3,'2016-06-16 10:51:31',NULL),(35,'fcfc50338946f05fbc962e79993befa5',9,12,'QE',3,'2016-06-16 11:00:48',NULL),(36,'bda305616844bd0e98aef4707e641e8a',9,12,'QE',3,'2016-06-16 12:32:34',NULL),(37,'46deabfb989a3fc6123f4c324c55ba40',9,12,'QE',3,'2016-06-16 13:04:53','2016-06-17 06:14:31'),(38,'de9a992717c33fb6217e9825ddaea7a9',9,13,'FM',2,'2016-06-20 05:22:27',NULL),(39,'2aa69d5411d5bb980bfccbf458d422fb',9,12,'FM',2,'2016-06-20 05:51:48',NULL);
/*!40000 ALTER TABLE `export_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `functional_map`
--

DROP TABLE IF EXISTS `functional_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `functional_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `bcmId` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  `assetTemplateId` int(11) NOT NULL,
  `questionId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fm_bcm_idx` (`bcmId`),
  KEY `fm_createdBy_idx` (`createdBy`),
  KEY `fm_updatedBy_idx` (`updatedBy`),
  KEY `fm_workspaceId_idx` (`workspaceId`),
  KEY `fm_assetTemplate_idx` (`assetTemplateId`),
  KEY `fm_question_idx` (`questionId`),
  CONSTRAINT `fm_assetTemplate` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fm_bcm` FOREIGN KEY (`bcmId`) REFERENCES `bcm` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fm_createdBy` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fm_question` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fm_updatedBy` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fm_workspaceId` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `functional_map`
--

LOCK TABLES `functional_map` WRITE;
/*!40000 ALTER TABLE `functional_map` DISABLE KEYS */;
INSERT INTO `functional_map` VALUES (1,'Capability Mapping for Globalization V1',2,'L3',9,70,69,13,'2016-06-15 19:00:44',13,'2016-06-15 19:00:44'),(2,'Capability Mapping for Globalization V2',2,'L3',9,70,69,12,'2016-06-16 05:38:19',12,'2016-06-16 05:38:19');
/*!40000 ALTER TABLE `functional_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `functional_map_data`
--

DROP TABLE IF EXISTS `functional_map_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `functional_map_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `functionalMapId` int(11) NOT NULL,
  `bcmLevelId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  `data` text,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `questionnaireId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fmd_bmfId_idx` (`functionalMapId`),
  KEY `fmd_bcmLevel_idx` (`bcmLevelId`),
  KEY `fmd_asset_idx` (`assetId`),
  KEY `fmd_userId_fk_idx` (`updatedBy`),
  KEY `questionnaireId` (`questionnaireId`),
  CONSTRAINT `fmd_asset_fk` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fmd_bcmLevel_fk` FOREIGN KEY (`bcmLevelId`) REFERENCES `bcm_level` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fmd_fmId_fk` FOREIGN KEY (`functionalMapId`) REFERENCES `functional_map` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fmd_userId_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `functional_map_data_ibfk_1` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `functional_map_data`
--

LOCK TABLES `functional_map_data` WRITE;
/*!40000 ALTER TABLE `functional_map_data` DISABLE KEYS */;
INSERT INTO `functional_map_data` VALUES (1,1,225,13,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:07:57',NULL),(2,1,226,13,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:07:57',NULL),(3,1,227,13,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:07:58',NULL),(4,1,229,13,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:07:58',NULL),(5,1,230,13,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:07:58',NULL),(6,1,231,13,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:07:58',NULL),(7,1,232,13,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:07:58',NULL),(8,1,233,13,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:07:58',NULL),(9,1,234,13,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:07:59',NULL),(10,1,235,13,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:07:59',NULL),(11,1,237,13,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:07:59',NULL),(12,1,237,19,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"Not Supported But Desired\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:07:59',NULL),(13,1,238,13,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:07:59',NULL),(14,1,238,19,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"Not Supported But Desired\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:07:59',NULL),(15,1,239,13,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:07:59',NULL),(16,1,239,19,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:08:00',NULL),(17,1,241,19,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:08:00',NULL),(18,1,242,19,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:08:00',NULL),(19,1,249,19,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:08:00',NULL),(20,1,250,19,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:08:00',NULL),(21,1,251,19,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:08:00',NULL),(22,1,252,19,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',13,'2016-06-15 19:08:00',NULL),(23,2,225,17,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(24,2,226,19,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(25,2,227,19,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(26,2,227,16,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(27,2,232,13,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(28,2,234,17,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"Not Supported But Desired\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(29,2,235,19,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(30,2,237,8,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(31,2,237,19,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(32,2,238,19,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(33,2,239,19,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(34,2,241,13,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(35,2,242,13,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(36,2,243,13,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(37,2,257,8,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(38,2,260,20,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(39,2,260,15,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(40,2,269,8,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(41,2,270,8,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(42,2,271,8,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(43,2,272,8,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(44,2,274,8,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(45,2,277,8,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(46,2,158,20,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(47,2,158,15,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(48,2,173,20,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(49,2,173,15,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(50,2,174,20,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(51,2,174,15,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(52,2,176,20,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(53,2,176,15,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(54,2,179,20,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(55,2,179,15,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(56,2,186,14,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(57,2,190,9,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(58,2,190,11,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(59,2,190,12,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(60,2,195,14,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(61,2,202,12,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(62,2,203,12,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(63,2,204,12,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(64,2,208,12,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"Not Supported But Desired\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(65,2,209,12,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"Not Supported But Desired\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL),(66,2,211,9,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}',12,'2016-06-16 09:32:19',NULL);
/*!40000 ALTER TABLE `functional_map_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graph`
--

DROP TABLE IF EXISTS `graph`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graph` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `graphName` varchar(45) NOT NULL,
  `description` text,
  `workspaceId` int(11) NOT NULL,
  `graphType` varchar(45) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_GRAPHNAME_WORKSPACE` (`graphName`,`workspaceId`,`graphType`),
  KEY `Graph_Workspace_FK_idx` (`workspaceId`),
  KEY `Graph_User_FK_idx` (`createdBy`),
  KEY `Graph_Updated_By_FK_idx` (`updatedBy`),
  CONSTRAINT `Graph_Created_By_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Graph_Updated_By_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Graph_Workspace_FK` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graph`
--

LOCK TABLES `graph` WRITE;
/*!40000 ALTER TABLE `graph` DISABLE KEYS */;
INSERT INTO `graph` VALUES (1,'Application Deployment Infra','',9,'NW',0,13,'2016-06-14 17:25:03',13,'2016-06-14 17:25:03');
/*!40000 ALTER TABLE `graph` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graph_asset_template`
--

DROP TABLE IF EXISTS `graph_asset_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graph_asset_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `graphId` int(11) NOT NULL,
  `assetTemplateId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_GRAPHID_ASSETTEMPLATEID` (`graphId`,`assetTemplateId`),
  KEY `FK_GRAPHID_GRAPHID_idx` (`graphId`),
  KEY `FK_ASSETTEMPLATEID_ASSETTEMPLATEID_idx` (`assetTemplateId`),
  CONSTRAINT `FK_ASSETTEMPLATEID_ASSETTEMPLATEID` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_GRAPHID_GRAPHID` FOREIGN KEY (`graphId`) REFERENCES `graph` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graph_asset_template`
--

LOCK TABLES `graph_asset_template` WRITE;
/*!40000 ALTER TABLE `graph_asset_template` DISABLE KEYS */;
INSERT INTO `graph_asset_template` VALUES (2,1,70),(1,1,71);
/*!40000 ALTER TABLE `graph_asset_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interface`
--

DROP TABLE IF EXISTS `interface`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interface` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interfaceAssetId` int(11) NOT NULL,
  `assetSourceId` int(11) NOT NULL,
  `assetDestId` int(11) NOT NULL,
  `interfaceDirection` varchar(45) NOT NULL,
  `dataType` varchar(20) DEFAULT NULL,
  `frequency` varchar(20) DEFAULT NULL,
  `communicationMethod` varchar(20) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_INTERFACE_SRC_DEST` (`interfaceAssetId`,`assetSourceId`,`assetDestId`),
  KEY `Interface_Asset_FK_idx` (`assetSourceId`),
  KEY `Interface_Asset_Dest_FK_idx` (`assetDestId`),
  KEY `Interface_Created_By_FK_idx` (`createdBy`),
  KEY `Interface_Updated_By_FK_idx` (`updatedBy`),
  KEY `FK_INTERFACEASSETID_ASSETID_idx` (`interfaceAssetId`),
  CONSTRAINT `FK_INTERFACEASSETID_ASSETID` FOREIGN KEY (`interfaceAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Asset_Dest_FK` FOREIGN KEY (`assetDestId`) REFERENCES `asset` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Asset_Source_FK` FOREIGN KEY (`assetSourceId`) REFERENCES `asset` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Created_By_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Updated_By_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interface`
--

LOCK TABLES `interface` WRITE;
/*!40000 ALTER TABLE `interface` DISABLE KEYS */;
INSERT INTO `interface` VALUES (1,249,8,9,'BIDI','XML','MONTHLY','ASYNCHRONOUS',15,'2016-06-22 11:15:59',15,'2016-06-22 11:15:59');
/*!40000 ALTER TABLE `interface` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter`
--

DROP TABLE IF EXISTS `parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniqueName` varchar(45) NOT NULL,
  `displayName` varchar(250) NOT NULL,
  `description` text,
  `type` varchar(4) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `parentParameterId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `parameter_uniqueName_workspaceId_uq` (`uniqueName`,`workspaceId`),
  KEY `parameter_workspaceId_fk_idx` (`workspaceId`),
  KEY `parameter_createdBy_fk_idx` (`createdBy`),
  KEY `parameter_updatedBy_fk_idx` (`updatedBy`),
  KEY `parameter_parentParameterId_idx` (`parentParameterId`),
  CONSTRAINT `parameter_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_parentParameterId` FOREIGN KEY (`parentParameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter`
--

LOCK TABLES `parameter` WRITE;
/*!40000 ALTER TABLE `parameter` DISABLE KEYS */;
INSERT INTO `parameter` VALUES (6,'SRVR_GENERAL','Server General Queries','General queries on the servers which is to be used for the dimensions that need to be captured for a particular server. This would not be used health assessments.','LP',NULL,NULL,13,'2016-06-15 08:30:57',13,'2016-06-15 08:30:57'),(7,' Know_Mgmnt',' Knowledge Management','','LP',NULL,NULL,12,'2016-06-15 09:24:16',12,'2016-06-15 09:24:16'),(8,'DRBC','Disaster Recovery Business Continuity ','Queries related to Disaster recovery Business Continuity .','LP',NULL,NULL,12,'2016-06-15 09:52:13',12,'2016-06-15 09:52:13'),(9,'Application_Security ','Application Security ','Queries related to Application Security.','LP',NULL,NULL,12,'2016-06-15 09:56:00',12,'2016-06-15 09:56:00'),(10,'App _Governance ','Application Governance','','LP',NULL,NULL,12,'2016-06-15 10:02:02',12,'2016-06-15 10:02:02'),(11,'Tech_Currency','Technology Currency','','LP',NULL,NULL,12,'2016-06-15 10:26:23',12,'2016-06-15 10:26:23'),(12,'Tech_Support','Technology  Support','','LP',NULL,NULL,12,'2016-06-15 10:28:53',12,'2016-06-15 10:28:53'),(13,'App_Architecture ','Application Architecture ','','LP',NULL,NULL,12,'2016-06-15 10:37:13',12,'2016-06-15 10:37:13'),(14,'App_Complexity ','Application Complexity ','','LP',NULL,NULL,12,'2016-06-15 10:55:34',12,'2016-06-15 10:55:34'),(15,'Tech_Stability ','Technology  Stability ','','LP',NULL,NULL,12,'2016-06-15 11:10:16',12,'2016-06-15 11:10:16'),(16,'Functional_Usability ','Functional Usability ','','LP',NULL,NULL,12,'2016-06-15 11:14:42',12,'2016-06-15 11:14:42'),(17,'Functional_Stability','Functional Stability','','LP',NULL,NULL,12,'2016-06-15 11:17:08',12,'2016-06-15 11:17:08'),(18,'Functional_Flexibility','Functional Flexibility','','LP',NULL,NULL,12,'2016-06-15 11:18:28',12,'2016-06-15 11:18:28'),(19,'Functional_Performance','Functional Performance','','LP',NULL,NULL,12,'2016-06-15 11:22:13',12,'2016-06-15 11:22:13'),(20,'Business_Criticality','Business Criticality','','LP',NULL,NULL,12,'2016-06-15 12:06:27',12,'2016-06-15 12:06:27'),(21,'BusCrit_Uptime Requirements','Business Criticality Uptime Requirements','','LP',NULL,NULL,12,'2016-06-15 12:07:54',12,'2016-06-15 12:07:54'),(22,'BusCrit_Application Category','Business Criticality Application Category','','LP',NULL,NULL,12,'2016-06-15 12:09:39',12,'2016-06-15 12:09:39'),(23,'Samp_param','General Queries ','','LP',NULL,NULL,12,'2016-06-15 12:12:04',12,'2016-06-15 12:12:04'),(24,'Functional_Coverage','Functional Coverage','','LP',NULL,NULL,12,'2016-06-15 12:27:46',12,'2016-06-15 12:27:46'),(25,'Functional Quotient','Functional Quotient','','AP',NULL,NULL,12,'2016-06-15 12:29:26',12,'2016-06-15 12:29:26'),(26,'Technology Quotient','Technology Quotient','','AP',NULL,NULL,12,'2016-06-15 12:36:37',12,'2016-06-15 12:36:37'),(27,'Management Quotient','Management Quotient','','AP',NULL,NULL,12,'2016-06-15 12:37:31',12,'2016-06-15 12:37:31'),(28,'Business Criticality','Business Criticality','','AP',NULL,NULL,12,'2016-06-15 12:38:23',12,'2016-06-15 12:38:23'),(29,'SRVR_General_v2','Server General Assessment','Not to be used for calculations','LP',NULL,NULL,13,'2016-06-15 17:00:28',13,'2016-06-15 17:00:28'),(30,'FC_V1','FC_V1-Trial','','FC',9,NULL,13,'2016-06-15 19:09:09',13,'2016-06-15 19:09:10'),(31,'FC_V2_Final','Functional Coverage','Functional coverage for the applications defined at the L2 level','FC',9,NULL,12,'2016-06-16 09:48:47',12,'2016-06-16 09:48:47');
/*!40000 ALTER TABLE `parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter_config`
--

DROP TABLE IF EXISTS `parameter_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parameterId` int(11) NOT NULL,
  `parameterConfig` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pc_parameterId_idx` (`parameterId`),
  CONSTRAINT `pc_parameterId` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter_config`
--

LOCK TABLES `parameter_config` WRITE;
/*!40000 ALTER TABLE `parameter_config` DISABLE KEYS */;
INSERT INTO `parameter_config` VALUES (1,30,'{\"functionalMapId\":1,\"functionalMapLevelType\":\"L2\"}'),(2,31,'{\"functionalMapId\":2,\"functionalMapLevelType\":\"L2\"}');
/*!40000 ALTER TABLE `parameter_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter_function`
--

DROP TABLE IF EXISTS `parameter_function`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter_function` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentParameterId` int(11) NOT NULL,
  `weight` decimal(10,3) NOT NULL,
  `constant` int(11) NOT NULL DEFAULT '1',
  `questionId` int(11) DEFAULT NULL,
  `mandatoryQuestion` tinyint(1) NOT NULL DEFAULT '0',
  `childparameterId` int(11) DEFAULT NULL,
  `sequenceNumber` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pf_parentParam_que_childParam_uq` (`parentParameterId`,`questionId`,`childparameterId`),
  KEY `parameter_function_parentParameterId_fk_idx` (`parentParameterId`),
  KEY `parameter_function_childParameterId_fk_idx` (`childparameterId`),
  KEY `parameter_function_questionId_fk_idx` (`questionId`),
  CONSTRAINT `parameter_function_childParameterId_fk` FOREIGN KEY (`childparameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_function_parentParameterId_fk` FOREIGN KEY (`parentParameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_function_questionId_fk` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter_function`
--

LOCK TABLES `parameter_function` WRITE;
/*!40000 ALTER TABLE `parameter_function` DISABLE KEYS */;
INSERT INTO `parameter_function` VALUES (4,6,0.250,0,63,0,NULL,NULL),(5,6,0.250,0,64,0,NULL,NULL),(6,6,0.250,0,65,0,NULL,NULL),(7,6,0.250,0,66,0,NULL,NULL),(8,7,0.200,0,8,0,NULL,NULL),(9,7,0.100,0,9,0,NULL,NULL),(10,7,0.200,0,45,0,NULL,NULL),(11,7,0.300,0,46,0,NULL,NULL),(12,7,0.200,0,47,0,NULL,NULL),(13,8,0.300,0,14,0,NULL,NULL),(14,8,0.300,0,15,0,NULL,NULL),(15,8,0.400,0,43,0,NULL,NULL),(16,9,0.400,0,12,0,NULL,NULL),(17,9,0.100,0,16,0,NULL,NULL),(18,9,0.100,0,39,0,NULL,NULL),(19,9,0.400,0,44,0,NULL,NULL),(20,10,0.250,0,20,0,NULL,NULL),(21,10,0.250,0,21,0,NULL,NULL),(22,10,0.500,0,31,0,NULL,NULL),(23,11,0.400,0,19,0,NULL,NULL),(24,11,0.300,0,23,0,NULL,NULL),(25,11,0.300,0,24,0,NULL,NULL),(26,12,0.100,0,42,0,NULL,NULL),(27,12,0.400,0,48,0,NULL,NULL),(28,12,0.400,0,49,0,NULL,NULL),(29,12,0.100,0,51,0,NULL,NULL),(30,13,0.400,0,25,0,NULL,NULL),(31,13,0.600,0,67,0,NULL,NULL),(32,14,0.300,0,2,0,NULL,NULL),(33,14,0.400,0,22,0,NULL,NULL),(34,14,0.300,0,47,0,NULL,NULL),(35,15,0.400,0,32,0,NULL,NULL),(36,15,0.300,0,33,0,NULL,NULL),(37,15,0.150,0,34,0,NULL,NULL),(38,15,0.150,0,35,0,NULL,NULL),(39,16,1.000,0,29,0,NULL,NULL),(40,17,0.400,0,36,0,NULL,NULL),(41,17,0.350,0,37,0,NULL,NULL),(42,17,0.250,0,38,0,NULL,NULL),(43,18,1.000,0,28,0,NULL,NULL),(44,19,0.200,0,26,0,NULL,NULL),(45,19,0.800,0,27,0,NULL,NULL),(46,20,1.000,0,68,0,NULL,NULL),(47,21,1.000,0,30,0,NULL,NULL),(48,22,1.000,0,53,0,NULL,NULL),(49,23,0.500,0,52,0,NULL,NULL),(50,23,0.500,0,54,0,NULL,NULL),(51,24,1.000,0,69,0,NULL,NULL),(52,25,0.200,0,NULL,0,16,NULL),(53,25,0.150,0,NULL,0,17,NULL),(54,25,0.150,0,NULL,0,18,NULL),(55,25,0.200,0,NULL,0,19,NULL),(57,26,0.400,0,NULL,0,11,NULL),(58,26,0.200,0,NULL,0,12,NULL),(59,26,0.200,0,NULL,0,13,NULL),(60,26,0.100,0,NULL,0,14,NULL),(61,26,0.100,0,NULL,0,15,NULL),(62,27,0.400,0,NULL,0,7,NULL),(63,27,0.300,0,NULL,0,8,NULL),(64,27,0.150,0,NULL,0,9,NULL),(65,27,0.150,0,NULL,0,10,NULL),(66,28,0.600,0,NULL,0,20,NULL),(67,28,0.200,0,NULL,0,21,NULL),(68,28,0.200,0,NULL,0,22,NULL),(69,29,0.190,0,63,0,NULL,NULL),(70,29,0.010,0,64,0,NULL,NULL),(71,29,0.200,0,65,0,NULL,NULL),(72,29,0.200,0,66,0,NULL,NULL),(73,29,0.200,0,70,0,NULL,NULL),(74,29,0.200,0,71,0,NULL,NULL),(75,25,0.300,0,NULL,0,31,NULL);
/*!40000 ALTER TABLE `parameter_function` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter_quality_gate`
--

DROP TABLE IF EXISTS `parameter_quality_gate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter_quality_gate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parameterId` int(11) NOT NULL,
  `qualityGateId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `qualityGate_parameter_fk_idx` (`parameterId`),
  KEY `qgp_qualityGate_fk_idx` (`qualityGateId`),
  CONSTRAINT `qgp_parameter_fk` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qgp_qualityGate_fk` FOREIGN KEY (`qualityGateId`) REFERENCES `quality_gate` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter_quality_gate`
--

LOCK TABLES `parameter_quality_gate` WRITE;
/*!40000 ALTER TABLE `parameter_quality_gate` DISABLE KEYS */;
INSERT INTO `parameter_quality_gate` VALUES (3,6,6),(4,7,7),(5,8,8),(6,9,9),(7,10,10),(8,11,11),(9,12,12),(10,13,13),(11,14,14),(12,15,15),(13,16,16),(14,17,17),(15,18,18),(16,19,19),(17,20,20),(18,21,21),(19,22,22),(20,23,23),(21,24,24),(22,25,25),(23,26,26),(24,27,27),(25,28,28),(26,29,29),(27,30,30),(28,31,31);
/*!40000 ALTER TABLE `parameter_quality_gate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quality_gate`
--

DROP TABLE IF EXISTS `quality_gate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quality_gate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `qg_assetType_parameter_workspace_uq` (`workspaceId`,`name`),
  KEY `qg_workspace_fk_idx` (`workspaceId`),
  KEY `qg_createdBy_fk_idx` (`createdBy`),
  KEY `qg_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `qg_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qg_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qg_workspace_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quality_gate`
--

LOCK TABLES `quality_gate` WRITE;
/*!40000 ALTER TABLE `quality_gate` DISABLE KEYS */;
INSERT INTO `quality_gate` VALUES (6,'QG_SRVR_GENERAL',9,13,'2016-06-15 08:30:57',13,'2016-06-15 08:30:57'),(7,'QG_ Know_Mgmnt',9,12,'2016-06-15 09:24:16',12,'2016-06-15 09:24:16'),(8,'QG_DRBC',9,12,'2016-06-15 09:52:13',12,'2016-06-15 09:52:13'),(9,'QG_Application_Security ',9,12,'2016-06-15 09:56:00',12,'2016-06-15 09:56:00'),(10,'QG_App _Governance ',9,12,'2016-06-15 10:02:02',12,'2016-06-15 10:02:02'),(11,'QG_Tech_Currency',9,12,'2016-06-15 10:26:23',12,'2016-06-15 10:26:23'),(12,'QG_Tech_Support',9,12,'2016-06-15 10:28:53',12,'2016-06-15 10:28:53'),(13,'QG_App_Architecture ',9,12,'2016-06-15 10:37:13',12,'2016-06-15 10:37:13'),(14,'QG_App_Complexity ',9,12,'2016-06-15 10:55:34',12,'2016-06-15 10:55:34'),(15,'QG_Tech_Stability ',9,12,'2016-06-15 11:10:16',12,'2016-06-15 11:10:16'),(16,'QG_Functional_Usability ',9,12,'2016-06-15 11:14:43',12,'2016-06-15 11:14:43'),(17,'QG_Functional_Stability',9,12,'2016-06-15 11:17:08',12,'2016-06-15 11:17:08'),(18,'QG_Functional_Flexibility',9,12,'2016-06-15 11:18:28',12,'2016-06-15 11:18:28'),(19,'QG_Functional_Performance',9,12,'2016-06-15 11:22:13',12,'2016-06-15 11:22:13'),(20,'QG_Business_Criticality',9,12,'2016-06-15 12:06:27',12,'2016-06-15 12:06:27'),(21,'QG_BusCrit_Uptime Requirements',9,12,'2016-06-15 12:07:54',12,'2016-06-15 12:07:54'),(22,'QG_BusCrit_Application Category',9,12,'2016-06-15 12:09:39',12,'2016-06-15 12:09:39'),(23,'QG_Samp_param',9,12,'2016-06-15 12:12:04',12,'2016-06-15 12:12:04'),(24,'QG_Functional_Coverage',9,12,'2016-06-15 12:27:46',12,'2016-06-15 12:27:46'),(25,'QG_Functional Quotient',9,12,'2016-06-15 12:29:26',12,'2016-06-15 12:29:26'),(26,'QG_Technology Quotient',9,12,'2016-06-15 12:36:37',12,'2016-06-15 12:36:37'),(27,'QG_Management Quotient',9,12,'2016-06-15 12:37:31',12,'2016-06-15 12:37:31'),(28,'QG_Business Criticality',9,12,'2016-06-15 12:38:23',12,'2016-06-15 12:38:23'),(29,'QG_SRVR_General_v2',9,13,'2016-06-15 17:00:28',13,'2016-06-15 17:00:28'),(30,'QG_FC_V1',9,13,'2016-06-15 19:09:10',13,'2016-06-15 19:09:10'),(31,'QG_FC_V2_Final',9,12,'2016-06-16 05:42:26',12,'2016-06-16 05:42:26');
/*!40000 ALTER TABLE `quality_gate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quality_gate_condition`
--

DROP TABLE IF EXISTS `quality_gate_condition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quality_gate_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qualityGateId` int(11) NOT NULL,
  `parameterId` int(11) NOT NULL,
  `weight` decimal(10,3) DEFAULT NULL,
  `gateCondition` varchar(45) NOT NULL,
  `conditionValues` text,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `qgc_parameter_fk_idx` (`parameterId`),
  KEY `qgc_qg_fk_idx` (`qualityGateId`),
  KEY `qgc_createdBy_fk_idx` (`createdBy`),
  KEY `qgc_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `qgc_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qgc_parameter_fk` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qgc_qg_fk` FOREIGN KEY (`qualityGateId`) REFERENCES `quality_gate` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qgc_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quality_gate_condition`
--

LOCK TABLES `quality_gate_condition` WRITE;
/*!40000 ALTER TABLE `quality_gate_condition` DISABLE KEYS */;
INSERT INTO `quality_gate_condition` VALUES (6,6,6,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',13,'2016-06-15 08:30:57',13,'2016-06-15 08:30:57'),(7,7,7,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 09:24:16',12,'2016-06-15 09:24:16'),(8,8,8,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 09:52:13',12,'2016-06-15 09:52:13'),(9,9,9,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 09:56:01',12,'2016-06-15 09:56:01'),(10,10,10,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 10:02:02',12,'2016-06-15 10:02:02'),(11,11,11,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 10:26:23',12,'2016-06-15 10:26:23'),(12,12,12,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 10:28:53',12,'2016-06-15 10:28:53'),(13,13,13,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 10:37:13',12,'2016-06-15 10:37:13'),(14,14,14,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 10:55:34',12,'2016-06-15 10:55:34'),(15,15,15,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 11:10:17',12,'2016-06-15 11:10:17'),(16,16,16,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 11:14:43',12,'2016-06-15 11:14:43'),(17,17,17,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 11:17:08',12,'2016-06-15 11:17:08'),(18,18,18,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 11:18:28',12,'2016-06-15 11:18:28'),(19,19,19,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 11:22:14',12,'2016-06-15 11:22:14'),(20,20,20,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 12:06:27',12,'2016-06-15 12:06:27'),(21,21,21,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 12:07:54',12,'2016-06-15 12:07:54'),(22,22,22,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 12:09:39',12,'2016-06-15 12:09:39'),(23,23,23,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 12:12:04',12,'2016-06-15 12:12:04'),(24,24,24,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 12:27:46',12,'2016-06-15 12:27:46'),(25,25,25,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 12:29:26',12,'2016-06-15 12:29:26'),(26,26,26,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 12:36:37',12,'2016-06-15 12:36:37'),(27,27,27,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 12:37:31',12,'2016-06-15 12:37:31'),(28,28,28,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-15 12:38:23',12,'2016-06-15 12:38:23'),(29,29,29,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":2.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#91cf60\",\"value\":null}]}',13,'2016-06-15 17:00:28',13,'2016-06-15 17:00:28'),(30,30,30,NULL,'LESS','{\"values\":[{\"color\":\"#d7191c\",\"value\":3.0},{\"color\":\"#fdae61\",\"value\":4.0},{\"color\":\"#ffffbf\",\"value\":5.0},{\"color\":\"#a6d96a\",\"value\":7.0},{\"color\":\"#1a9641\",\"value\":null}]}',13,'2016-06-15 19:09:10',13,'2016-06-16 04:12:56'),(31,31,31,NULL,'LESS','{\"values\":[{\"color\":\"#fc8d59\",\"value\":0.9},{\"color\":\"#ffffbf\",\"value\":2.5},{\"color\":\"#91cf60\",\"value\":null}]}',12,'2016-06-16 05:42:26',12,'2016-06-16 05:42:26');
/*!40000 ALTER TABLE `quality_gate_condition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentQuesId` int(11) DEFAULT NULL,
  `questionMode` varchar(20) NOT NULL,
  `title` text NOT NULL,
  `displayName` varchar(256) NOT NULL,
  `helpText` text,
  `workspaceId` int(11) DEFAULT NULL,
  `questionType` varchar(10) NOT NULL,
  `queTypeText` text,
  `level` smallint(6) NOT NULL,
  `questionCategoryId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ques_workspace_displayName_uq` (`workspaceId`,`displayName`),
  KEY `QUESTIONNAIRE_WORKSPACE_ID_FK_idx` (`workspaceId`),
  KEY `QUESTIONNAIRE_CREATED_BY_FK_idx` (`createdBy`),
  KEY `QUESTIONNAIRE_UPDATED_BY_FK_idx` (`updatedBy`),
  KEY `question_questioncategoryid_categoryid_idx` (`questionCategoryId`),
  KEY `ques_quesId_fk_idx` (`parentQuesId`),
  CONSTRAINT `QUESTIONNAIRE_CREATED_BY_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `QUESTIONNAIRE_UPDATED_BY_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `QUESTIONNAIRE_WORKSPACE_ID_FK` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ques_quesId_fk` FOREIGN KEY (`parentQuesId`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `question_questioncategoryid_categoryid` FOREIGN KEY (`questionCategoryId`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,NULL,'MODIFIABLE','Is the application Custom Built or Packaged?','Is the application Custom Built or Packaged?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Packaged Vendor\"},{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"Custom Built- Inhouse\"},{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,3,12,'2016-06-13 10:07:40',12,'2016-06-13 10:07:40'),(2,NULL,'MODIFIABLE','Degree of Customization for packages/COTS ','Degree of Customization for packages/COTS ','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"High\"},{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Moderate\"},{\"sequenceNumber\":3,\"quantifier\":8.0,\"text\":\"low\"},{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":5,\"quantifier\":5.0,\"text\":\"Not Applicable\"},{\"sequenceNumber\":6,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,3,12,'2016-06-13 10:19:33',12,'2016-06-13 10:19:33'),(6,NULL,'MODIFIABLE','Is the application a host for any master data?  If so, what?','Is the application a host for any master data?  If so, what?','',NULL,'PARATEXT',NULL,0,15,14,'2016-06-13 11:32:25',14,'2016-06-13 11:32:25'),(7,NULL,'MODIFIABLE','Does the application create any master data?  If so, what?','Does the application create any master data?  If so, what?','',NULL,'PARATEXT',NULL,0,15,14,'2016-06-13 11:32:46',14,'2016-06-13 11:32:46'),(8,NULL,'MODIFIABLE','Do we have the regression test scripts available and updated?','Do we have the regression test scripts available and updated_WDPR?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"},{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Not Applicable\"},{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,18,12,'2016-06-14 04:39:51',12,'2016-06-14 04:39:51'),(9,NULL,'MODIFIABLE','Availability of in-line documentation for the application?','Availability of in-line documentation for the application?_WDPR','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"},{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Not Applicable\"},{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,18,12,'2016-06-14 04:39:52',12,'2016-06-14 04:39:52'),(10,NULL,'MODIFIABLE','What are the current projects planned, which will impact this application?','What are the current projects planned, which will impact this application?','',NULL,'TEXT',NULL,0,19,14,'2016-06-13 11:38:09',14,'2016-06-13 11:38:09'),(11,NULL,'MODIFIABLE','What are the key challenges with this application as stated by Business users?','What are the key challenges with this application as stated by Business users?','',NULL,'TEXT',NULL,0,19,14,'2016-06-13 11:38:32',14,'2016-06-13 11:38:32'),(12,NULL,'MODIFIABLE','Are the Information Security requirements for the application being met?','Are the Information Security requirements for the application being met?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"},{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Not Applicable\"},{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,18,12,'2016-06-13 11:38:49',12,'2016-06-13 11:38:49'),(13,NULL,'MODIFIABLE','What are the key challenges with this application as stated by IT Team?','What are the key challenges with this application as stated by IT Team?','',NULL,'PARATEXT',NULL,0,19,14,'2016-06-14 14:38:47',14,'2016-06-14 14:38:47'),(14,NULL,'MODIFIABLE','Are there any Data Retention policies defined for this application?','Are there any Data Retention policies defined for this application?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - fully covered\"},{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Yes - partially covered\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Not Known\"},{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,18,14,'2016-06-13 12:10:44',14,'2016-06-13 12:10:44'),(15,NULL,'MODIFIABLE','Are there any Data Archival processes defined for this application?','Are there any Data Archival processes defined for this application?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - fully covered\"},{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Yes - partially covered\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Not Known\"},{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,18,14,'2016-06-13 12:12:04',14,'2016-06-13 12:12:04'),(16,NULL,'MODIFIABLE','Is customer data / other sensitive data accessed from this application?','Is customer data / other sensitive data accessed from this application?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes-Confidentiality maintained through role base access\"},{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"Yes-No process for Maintaining confidentiality\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,18,14,'2016-06-13 12:15:30',14,'2016-06-13 12:15:30'),(17,NULL,'MODIFIABLE','Is there a Backup Validation Policy in place?','Is there a Backup Validation Policy in place?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes-Confidentiality maintained through role base access\"},{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"Yes-No process for Maintaining confidentiality\"},{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,18,14,'2016-06-13 12:34:27',14,'2016-06-13 12:34:27'),(18,NULL,'MODIFIABLE','Any other information?','Any other information','',NULL,'TEXT',NULL,0,19,14,'2016-06-13 12:35:14',14,'2016-06-13 12:35:14'),(19,NULL,'MODIFIABLE','What is the technology used for Application Development Language?','What is the technology used for Application Development Language?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":7.0,\"text\":\"Java 1.6\"},{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\".Net 4.5\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Oracle Forms 6i\"},{\"sequenceNumber\":5,\"quantifier\":1.0,\"text\":\"Delphi 7\"},{\"sequenceNumber\":6,\"quantifier\":1.0,\"text\":\"Cobol\"},{\"sequenceNumber\":8,\"quantifier\":4.0,\"text\":\"Share point 2007\"},{\"sequenceNumber\":10,\"quantifier\":1.0,\"text\":\"Java 1.5\"},{\"sequenceNumber\":11,\"quantifier\":1.0,\"text\":\"VB 2012\"},{\"sequenceNumber\":12,\"quantifier\":8.0,\"text\":\"VB6\"},{\"sequenceNumber\":13,\"quantifier\":1.0,\"text\":\".Net 3.5\"},{\"sequenceNumber\":15,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":17,\"quantifier\":4.0,\"text\":\".NET 2.0\"},{\"sequenceNumber\":18,\"quantifier\":1.0,\"text\":\"Java 1.4\"},{\"sequenceNumber\":24,\"quantifier\":1.0,\"text\":\".NET 4.2\"},{\"sequenceNumber\":25,\"quantifier\":10.0,\"text\":\"Java 1.7\"},{\"sequenceNumber\":30,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,3,12,'2016-06-16 12:31:27',12,'2016-06-16 12:31:27'),(20,NULL,'MODIFIABLE','Does the application address all relevant compliance needs?','Does the application address all relevant compliance needs?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":7.0,\"text\":\"Partially\"},{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\"Completely\"},{\"sequenceNumber\":3,\"quantifier\":4.0,\"text\":\"Sparsely\"},{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,3,12,'2016-06-13 13:01:47',12,'2016-06-13 13:01:47'),(21,NULL,'MODIFIABLE','If this application was not available on a permanent basis, what is the compliance impact?','If this application was not available on a permanent basis, what is the compliance impact?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Major\"},{\"sequenceNumber\":2,\"quantifier\":8.0,\"text\":\"Minor\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":4,\"quantifier\":10.0,\"text\":\"None\"},{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,3,12,'2016-06-14 06:32:08',12,'2016-06-14 06:32:08'),(22,NULL,'MODIFIABLE','Number of Application Interfaces?','Number of Application Interfaces?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-5\"},{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"6-10\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\">10\"},{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,3,12,'2016-06-14 06:36:59',12,'2016-06-14 06:36:59'),(23,NULL,'MODIFIABLE','What is the technology used for Operating System (OS)?','What is the technology used for Operating System (OS)?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"IBM AIX 5.1\"},{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"IBM AIX 5.3\"},{\"sequenceNumber\":3,\"quantifier\":9.0,\"text\":\"IBM AIX 6.1\"},{\"sequenceNumber\":4,\"quantifier\":10.0,\"text\":\"Linux 6.2\"},{\"sequenceNumber\":5,\"quantifier\":8.0,\"text\":\"Linux 5.11\"},{\"sequenceNumber\":6,\"quantifier\":6.0,\"text\":\"Linux 5.5\"},{\"sequenceNumber\":7,\"quantifier\":6.0,\"text\":\"OS/400 V7R1\"},{\"sequenceNumber\":8,\"quantifier\":5.0,\"text\":\"Vendor Package\"},{\"sequenceNumber\":9,\"quantifier\":1.0,\"text\":\"Windows Server 2003\"},{\"sequenceNumber\":10,\"quantifier\":1.0,\"text\":\"Windows Server 2003 R2\"},{\"sequenceNumber\":11,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":12,\"quantifier\":4.0,\"text\":\"Windows Server 2008\"},{\"sequenceNumber\":13,\"quantifier\":5.0,\"text\":\"Windows Server 2008 R2\"},{\"sequenceNumber\":14,\"quantifier\":1.0,\"text\":\"Zos 1.11\"},{\"sequenceNumber\":15,\"quantifier\":7.0,\"text\":\"Linux 5.10\"},{\"sequenceNumber\":16,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,3,12,'2016-06-14 14:38:01',12,'2016-06-14 14:38:01'),(24,NULL,'MODIFIABLE','What is the technology used for Database (DBMS)?','What is the technology used for Database (DBMS)?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"DB2 V7R1\"},{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"Firebird 2.0\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Oracle 10g\"},{\"sequenceNumber\":4,\"quantifier\":4.0,\"text\":\"Oracle 11g\"},{\"sequenceNumber\":5,\"quantifier\":5.0,\"text\":\"Oracle 11g R2\"},{\"sequenceNumber\":6,\"quantifier\":1.0,\"text\":\"Oracle 9i\"},{\"sequenceNumber\":7,\"quantifier\":4.0,\"text\":\"SQL Server 2005\"},{\"sequenceNumber\":8,\"quantifier\":4.0,\"text\":\"SQL Server 2008\"},{\"sequenceNumber\":9,\"quantifier\":5.0,\"text\":\"SQL SERVER 2008 R2\"},{\"sequenceNumber\":10,\"quantifier\":10.0,\"text\":\"SQL Server 2012\"},{\"sequenceNumber\":11,\"quantifier\":5.0,\"text\":\"Vendor Package\"},{\"sequenceNumber\":12,\"quantifier\":1.0,\"text\":\"VSAM\"},{\"sequenceNumber\":13,\"quantifier\":1.0,\"text\":\"Informix 10.x\"},{\"sequenceNumber\":14,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":15,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,3,12,'2016-06-14 06:50:54',12,'2016-06-14 06:50:54'),(25,NULL,'MODIFIABLE','What is the technology used for  the UI?','What is the technology used for  the UI?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Green Screen\"},{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Desktop Based\"},{\"sequenceNumber\":3,\"quantifier\":10.0,\"text\":\"Internet Browser Based\"},{\"sequenceNumber\":4,\"quantifier\":10.0,\"text\":\"Mobile Browser Based\"},{\"sequenceNumber\":5,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":6,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,3,12,'2016-06-14 06:56:16',12,'2016-06-14 06:56:16'),(26,NULL,'MODIFIABLE','What is the performance of this application over the different channels of access?','What is the performance of this application over the different channels of access?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Good\"},{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Some Improvement Required\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Significant improvement Required\"},{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information not Avallable\"},{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 07:11:14',12,'2016-06-14 07:11:14'),(27,NULL,'MODIFIABLE','Does the application performance meet business requirement?','Does the application performance meet business requirement?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Completely\"},{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"Partially\"},{\"sequenceNumber\":3,\"quantifier\":4.0,\"text\":\"Sparsely\"},{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 07:13:29',12,'2016-06-14 07:13:29'),(28,NULL,'MODIFIABLE','Do the business users have a workaround in case of non availability of this application? If yes, indicate the nature of the workaround.','Do the business users have a workaround in case of non availability of this application? If yes, indicate the nature of the workaround.','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Covered\"},{\"sequenceNumber\":2,\"quantifier\":4.0,\"text\":\"Partially Covered\"},{\"sequenceNumber\":3,\"quantifier\":7.0,\"text\":\"Critical Areas Covered\"},{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 07:16:09',12,'2016-06-14 07:16:09'),(29,NULL,'MODIFIABLE','How would you rate the application usability?','How would you rate the application usability?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Easy to Use\"},{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"Requires Minor usability improvement\"},{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"Requires Major usability improvement\"},{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Not Known\"},{\"sequenceNumber\":5,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":6,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 07:19:33',12,'2016-06-14 07:19:33'),(30,NULL,'MODIFIABLE','What are the uptime requirements?','What are the uptime requirements?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"%Availability >99.5% & 24X7\"},{\"sequenceNumber\":2,\"quantifier\":8.0,\"text\":\"%Availability >98.5% & Extended Hrs\"},{\"sequenceNumber\":3,\"quantifier\":6.0,\"text\":\"%Availability >97.5% & Business Hrs\"},{\"sequenceNumber\":4,\"quantifier\":2.0,\"text\":\"Others\"},{\"sequenceNumber\":5,\"quantifier\":6.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":6,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 07:25:36',12,'2016-06-14 07:25:36'),(31,NULL,'MODIFIABLE','How is the SLA measured, monitored and governed?','How is the SLA measured, monitored and governed?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes-Process in place\"},{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"Yes- Adhoc Process\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"No-there is no process to measure SLA\"},{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 07:28:43',12,'2016-06-14 07:28:43'),(32,NULL,'MODIFIABLE','Number of Severity 1 incidents in last 1 year? ','Number of Severity 1 incidents in last 1 year? ','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-5\"},{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"6-10\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\">10\"},{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 07:31:54',12,'2016-06-14 07:31:54'),(33,NULL,'MODIFIABLE','Number of Severity 2 incidents in last 1 year? ','Number of Severity 2 incidents in last 1 year? ','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-10\"},{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"11-30\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\">30\"},{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 07:33:18',12,'2016-06-14 07:33:18'),(34,NULL,'MODIFIABLE','Number of Severity 3 incidents in last 1 year? ','Number of Severity 3 incidents in last 1 year? ','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-30\"},{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"30-100\"},{\"sequenceNumber\":3,\"quantifier\":4.0,\"text\":\">100\"},{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 07:34:34',12,'2016-06-14 07:34:34'),(35,NULL,'MODIFIABLE','Number of Severity 4 incidents in last 1 year? ','Number of Severity 4 incidents in last 1 year? ','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-100\"},{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"100-300\"},{\"sequenceNumber\":3,\"quantifier\":4.0,\"text\":\">300\"},{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 07:37:11',12,'2016-06-14 07:37:11'),(36,NULL,'MODIFIABLE','Number of Major CRs/Project A associated with application in last one year?','Number of Major CRs/Project A associated with application in last one year?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":7.0,\"text\":\"0\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"2.1\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"5.1\"},{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 07:40:15',12,'2016-06-14 07:40:15'),(37,NULL,'MODIFIABLE','Number of Medium CRs/Project B associated with application in last one year?','Number of Medium CRs/Project B associated with application in last one year?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":8.0,\"text\":\"0\"},{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"4.1\"},{\"sequenceNumber\":3,\"quantifier\":3.0,\"text\":\"8.1\"},{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 07:42:31',12,'2016-06-14 07:42:31'),(38,NULL,'MODIFIABLE','Number of Minor CRs/Project C associated with application in last one year?','Number of Minor CRs/Project C associated with application in last one year?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"0\"},{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"8.1\"},{\"sequenceNumber\":3,\"quantifier\":3.0,\"text\":\"15.1\"},{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 07:44:06',12,'2016-06-14 07:44:06'),(39,NULL,'MODIFIABLE','Does the Application have an Audit Trail built in it?','Does the Application have an Audit Trail built in it?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - Strong Mechanism\"},{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Yes - Not Strong\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"No\"},{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 12:22:17',12,'2016-06-14 12:22:17'),(40,NULL,'MODIFIABLE','Who owns source code for this application?','Who owns source code for this application?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":6.0,\"text\":\"Vendor\"},{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Third Party\"},{\"sequenceNumber\":3,\"quantifier\":10.0,\"text\":\"Company Specific\"},{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 12:30:19',12,'2016-06-14 12:30:19'),(42,NULL,'MODIFIABLE','What is the support level for Infrastructure?','What is the support level for Infrastructure?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Out of Vendor support\"},{\"sequenceNumber\":2,\"quantifier\":2.0,\"text\":\"End of Life\"},{\"sequenceNumber\":3,\"quantifier\":6.0,\"text\":\"Has additional Vendor support\"},{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"Less than 2 patches behind\"},{\"sequenceNumber\":5,\"quantifier\":1.0,\"text\":\"More than 2 patches behind\"},{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"},{\"sequenceNumber\":7,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":8,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 13:22:39',12,'2016-06-14 13:22:39'),(43,NULL,'MODIFIABLE','What are the Business Continuity and Data Recovery Plans for the Application?','What are the Business Continuity and Data Recovery Plans for the Application?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Hot\"},{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"Warm\"},{\"sequenceNumber\":3,\"quantifier\":4.0,\"text\":\"Cold\"},{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 13:25:30',12,'2016-06-14 13:25:30'),(44,NULL,'MODIFIABLE','What is the user security module(s) with respect to user authentication and authorization?','What is the user security module(s) with respect to user authentication and authorization?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":8.0,\"text\":\"Active Directory\"},{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"Password based user authentication\"},{\"sequenceNumber\":3,\"quantifier\":6.0,\"text\":\"SAP Security Profiling\"},{\"sequenceNumber\":4,\"quantifier\":10.0,\"text\":\"SSO User based Authentication\"},{\"sequenceNumber\":5,\"quantifier\":5.0,\"text\":\"Other\"},{\"sequenceNumber\":6,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":7,\"quantifier\":8.0,\"text\":\"LDAP\"},{\"sequenceNumber\":8,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 13:28:03',12,'2016-06-14 13:28:03'),(45,NULL,'MODIFIABLE','Availabilty of Training document. Also please provide location details where documents are stored?','Availabilty of Training document. Also please provide location details where documents are stored?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"},{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Exists and partially usable\"},{\"sequenceNumber\":3,\"quantifier\":3.0,\"text\":\"Exists but Unusable\"},{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"No Documentation\"},{\"sequenceNumber\":5,\"quantifier\":5.0,\"text\":\"Not Known\"},{\"sequenceNumber\":6,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":7,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 13:31:07',12,'2016-06-14 13:31:07'),(46,NULL,'MODIFIABLE','Availability of User Document. Also please provide location details where documents are stored?','Availability of User Document. Also please provide location details where documents are stored?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"},{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Exists and partially usable\"},{\"sequenceNumber\":3,\"quantifier\":3.0,\"text\":\"Exists but Unusable\"},{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"No Documentation\"},{\"sequenceNumber\":5,\"quantifier\":5.0,\"text\":\"Not Known\"},{\"sequenceNumber\":6,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":7,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 13:32:41',12,'2016-06-14 13:32:41'),(47,NULL,'MODIFIABLE','What is the complexity of the business logic in the application?','What is the complexity of the business logic in the application?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"},{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Exists and partially usable\"},{\"sequenceNumber\":3,\"quantifier\":3.0,\"text\":\"Exists but Unusable\"},{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"No Documentation\"},{\"sequenceNumber\":5,\"quantifier\":5.0,\"text\":\"Not Known\"},{\"sequenceNumber\":6,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":7,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 13:34:31',12,'2016-06-14 13:34:31'),(48,NULL,'MODIFIABLE','What is support level for database?','What is support level for database?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Out of Vendor support\"},{\"sequenceNumber\":2,\"quantifier\":2.0,\"text\":\"End of Life\"},{\"sequenceNumber\":3,\"quantifier\":6.0,\"text\":\"Has additional Vendor support\"},{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"Less than 2 patches behind\"},{\"sequenceNumber\":5,\"quantifier\":1.0,\"text\":\"More than 2 patches behind\"},{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"},{\"sequenceNumber\":7,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":8,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 13:37:14',12,'2016-06-14 13:37:14'),(49,NULL,'MODIFIABLE','What is the support level for operating system?','What is the support level for operating system?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Out of Vendor support\"},{\"sequenceNumber\":2,\"quantifier\":2.0,\"text\":\"End of Life\"},{\"sequenceNumber\":3,\"quantifier\":6.0,\"text\":\"Has additional Vendor support\"},{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"Less than 2 patches behind\"},{\"sequenceNumber\":5,\"quantifier\":1.0,\"text\":\"More than 2 patches behind\"},{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"},{\"sequenceNumber\":7,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":8,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 13:38:54',12,'2016-06-14 13:38:54'),(50,NULL,'MODIFIABLE','Availability of system documentation. Also please provide location details where documents are stored?','Availability of system documentation. Also please provide location details where documents are stored?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"},{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Exists and partially usable\"},{\"sequenceNumber\":3,\"quantifier\":3.0,\"text\":\"Exists but Unusable\"},{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"No Documentation\"},{\"sequenceNumber\":5,\"quantifier\":5.0,\"text\":\"Not Known\"},{\"sequenceNumber\":6,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":7,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 13:41:08',12,'2016-06-14 13:41:08'),(51,NULL,'MODIFIABLE','If this is a package application, what is the health/maturity of the vendor and is this application considered an important product within their vendor portfolio?','If this is a package application, what is the health/maturity of the vendor and is this application considered an important product within their vendor portfolio?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Vendor has proven track record and this is a Key product in their portfolio\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Vendor has proven track record and this is End of Life product\"},{\"sequenceNumber\":3,\"quantifier\":10.0,\"text\":\"New vendor, key product and strategic business differentiator\"},{\"sequenceNumber\":4,\"quantifier\":10.0,\"text\":\"Not a Package\"},{\"sequenceNumber\":5,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":6,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,3,12,'2016-06-14 13:58:40',12,'2016-06-14 13:58:40'),(52,NULL,'MODIFIABLE','What is the current Lifecycle Status?','What is the current Lifecycle Status?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":4.0,\"text\":\"Planned to Retirement\"},{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Retired\"},{\"sequenceNumber\":3,\"quantifier\":10.0,\"text\":\"Active\"},{\"sequenceNumber\":4,\"quantifier\":6.0,\"text\":\"Under developemnt\"},{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,21,12,'2016-06-14 14:06:30',12,'2016-06-14 14:06:30'),(53,NULL,'MODIFIABLE','Please classify this application as Core/Support.','Please classify this application as Core/Support.','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Core\"},{\"sequenceNumber\":2,\"quantifier\":8.0,\"text\":\"Support\"},{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,21,12,'2016-06-14 14:08:07',12,'2016-06-14 14:08:07'),(54,NULL,'MODIFIABLE','What is the planned release cycle for this application?','What is the planned release cycle for this application?','',NULL,'PARATEXT',NULL,0,21,12,'2016-06-14 14:11:54',12,'2016-06-14 14:11:54'),(55,NULL,'MODIFIABLE','If Packaged Software / COTS, please specify the vendor name and package/ module name with version?','If Packaged Software / COTS, please specify the vendor name and package/ module name with version?','',NULL,'PARATEXT',NULL,0,3,12,'2016-06-14 14:13:11',12,'2016-06-14 14:13:11'),(56,NULL,'MODIFIABLE','Are you considering any Package Solution(s) or Product(s) to migrate to? If so, what are they (selection list)?','Are you considering any Package Solution(s) or Product(s) to migrate to? If so, what are they (selection list)?','Mention any Off the Shelf/Package Solution to which this application is considered for migration. Mention Not Applicable in case no migration or product evaluation is planned.',NULL,'PARATEXT',NULL,0,3,12,'2016-06-14 14:17:32',12,'2016-06-14 14:17:32'),(57,NULL,'MODIFIABLE','Number of Business Users Accessing the Application/Service?','Number of Business Users Accessing the Application/Service?','Please specify number of Users/ Registered Users of the Application',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":6.0,\"text\":\"<100\"},{\"sequenceNumber\":2,\"quantifier\":8.0,\"text\":\"100-200\"},{\"sequenceNumber\":3,\"quantifier\":10.0,\"text\":\">200\"},{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,20,12,'2016-06-14 14:25:40',12,'2016-06-14 14:25:40'),(58,NULL,'MODIFIABLE','Specify top 5 Locations (cities) in terms of number of business users.','Specify top 5 Locations (cities) in terms of number of business users.','',NULL,'PARATEXT',NULL,0,3,12,'2016-06-14 14:28:47',12,'2016-06-14 14:28:47'),(59,NULL,'MODIFIABLE','Specify L1 Support provided by   In-house IT team/ Vendor name.','Specify L1 Support provided by   In-house IT team/ Vendor name.','Please specify the group providing L1 IT support ',NULL,'PARATEXT',NULL,0,3,12,'2016-06-14 14:31:30',12,'2016-06-14 14:31:30'),(60,NULL,'MODIFIABLE','Specify L2 Support provided by In-house IT team/ Vendor name.','Specify L2 Support provided by In-house IT team/ Vendor name.','Please specify group providing L2 IT support.',NULL,'PARATEXT',NULL,0,3,12,'2016-06-14 14:33:03',12,'2016-06-14 14:33:03'),(61,NULL,'MODIFIABLE','Specify L3 Support provided by In-house IT team/ Vendor name.','Specify L3 Support provided by In-house IT team/ Vendor name.','Please specify the group providing L3 IT support.',NULL,'PARATEXT',NULL,0,3,12,'2016-06-14 14:34:00',12,'2016-06-14 14:34:00'),(62,NULL,'MODIFIABLE','Specify Support Location ','Specify Support Location ','Please specify the primary location for providing application support.',NULL,'PARATEXT',NULL,0,3,12,'2016-06-14 14:35:00',12,'2016-06-14 14:35:00'),(63,NULL,'MODIFIABLE','Please specify the application environment.','Please specify the application environment.','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Prod\"},{\"sequenceNumber\":2,\"quantifier\":8.0,\"text\":\"Dev\"},{\"sequenceNumber\":3,\"quantifier\":8.0,\"text\":\"QA\"},{\"sequenceNumber\":4,\"quantifier\":7.0,\"text\":\"System Test\"},{\"sequenceNumber\":5,\"quantifier\":6.0,\"text\":\"UAT\"},{\"sequenceNumber\":6,\"quantifier\":5.0,\"text\":\"DR\"},{\"sequenceNumber\":7,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,6,12,'2016-06-15 06:24:56',12,'2016-06-15 06:24:56'),(64,NULL,'MODIFIABLE','Please specify the server type.','Please specify the server type.','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":8.0,\"text\":\"Database\"},{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\"Application\"},{\"sequenceNumber\":3,\"quantifier\":9.0,\"text\":\"Web\"},{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,6,12,'2016-06-15 06:26:09',12,'2016-06-15 06:26:09'),(65,NULL,'MODIFIABLE','Mention whether server  is Physical/Virtual.','Mention whether server  is Physical/Virtual.','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"},{\"sequenceNumber\":2,\"quantifier\":8.0,\"text\":\"Virtual\"},{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,6,12,'2016-06-15 06:28:28',12,'2016-06-15 06:28:28'),(66,NULL,'MODIFIABLE','Mention whether server is Dedicated/Shared.','Mention whether server is Dedicated/Shared.','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Dedicated\"},{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Shared\"},{\"sequenceNumber\":3,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,6,12,'2016-06-15 06:30:11',12,'2016-06-15 06:30:11'),(67,NULL,'MODIFIABLE','What is the application layering architecture?','What is the application layering architecture?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Service Oriented Architecture(SOA)\"},{\"sequenceNumber\":2,\"quantifier\":8.0,\"text\":\"Direct to Web (Non SOA enabled Web Based)\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Non Web Based n-tier\"},{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"Non Web Based Monolithic\"},{\"sequenceNumber\":5,\"quantifier\":9.0,\"text\":\"Hybrid (e.g. mixture of SOA and Direct to Web)\"},{\"sequenceNumber\":6,\"quantifier\":2.0,\"text\":\"Functional logic in Stored Procedures\"},{\"sequenceNumber\":7,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":8,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,3,12,'2016-06-15 10:36:33',12,'2016-06-15 10:36:33'),(68,NULL,'MODIFIABLE','Specify Business Criticality. ','Specify Business Criticality.','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"High\"},{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Medium\"},{\"sequenceNumber\":3,\"quantifier\":3.0,\"text\":\"low\"},{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"},{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,16,12,'2016-06-15 11:57:36',12,'2016-06-15 11:57:36'),(69,NULL,'MODIFIABLE','Does the application effectively support the business needs it is intended to support and are you satisfied with the application functionality? ','Does the application effectively support the business needs it is intended to support and are you satisfied with the application functionality? ','Application to Business Functional Mapping',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Supported\"},{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Partially Supported\"},{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"Not Supported But Desired\"},{\"sequenceNumber\":4,\"quantifier\":6.0,\"text\":\"Not Used But Desired\"},{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,1,12,'2016-06-15 12:26:19',12,'2016-06-15 12:26:19'),(70,NULL,'MODIFIABLE','Server OS','SRVR_OS','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":5.0,\"text\":\"Windows Server 2008\"},{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"OS/400 V7R1\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"microsoft windows server 2008 r2 enterprise edition\"},{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"OS/400\"},{\"sequenceNumber\":5,\"quantifier\":5.0,\"text\":\"Linux\"},{\"sequenceNumber\":6,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,6,13,'2016-06-15 16:55:30',13,'2016-06-15 16:55:30'),(71,NULL,'MODIFIABLE','Server Database','SRVR_Database','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":5.0,\"text\":\"Microsoft SQL 2008 R2 & DB2\"},{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"DB2 V7R1\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Microsoft SQL 2008 R2\"},{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"DB2\"},{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOption\":false}',0,6,13,'2016-06-15 16:57:13',13,'2016-06-15 16:57:13');
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire`
--

DROP TABLE IF EXISTS `questionnaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text,
  `workspaceId` int(11) NOT NULL,
  `status` varchar(10) NOT NULL,
  `assetTypeId` int(11) NOT NULL,
  `questionnaireType` varchar(5) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `questionnaire_name_workspace_unique` (`name`,`workspaceId`),
  KEY `questionnaire_workspaceId_fk_idx` (`workspaceId`),
  KEY `questionnaire_createdBy_fk_idx` (`createdBy`),
  KEY `questionnaire_updatedBy_fk_idx` (`updatedBy`),
  KEY `questionnaire_assetTypeId_fk_idx` (`assetTypeId`),
  CONSTRAINT `questionnaire_assetTypeId_fk` FOREIGN KEY (`assetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire`
--

LOCK TABLES `questionnaire` WRITE;
/*!40000 ALTER TABLE `questionnaire` DISABLE KEYS */;
INSERT INTO `questionnaire` VALUES (1,'Server General Assessment','',9,'EDITABLE',1,'DEF',13,'2016-09-26 09:37:31',13,'2016-09-26 09:37:31'),(2,'samp','',9,'EDITABLE',1,'DEF',12,'2016-09-26 09:37:31',12,'2016-09-26 09:37:31'),(3,'Application General Assessment','',9,'EDITABLE',1,'DEF',14,'2016-09-26 09:37:31',14,'2016-09-26 09:37:31');
/*!40000 ALTER TABLE `questionnaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire_asset`
--

DROP TABLE IF EXISTS `questionnaire_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `questionnaire_asset_questionnaireAssetId_uq` (`questionnaireId`,`assetId`),
  KEY `questionnaire_asset_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `questionnaire_asset_assetId_fk_idx` (`assetId`),
  CONSTRAINT `questionnaire_asset_assetId_fk` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_asset_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire_asset`
--

LOCK TABLES `questionnaire_asset` WRITE;
/*!40000 ALTER TABLE `questionnaire_asset` DISABLE KEYS */;
INSERT INTO `questionnaire_asset` VALUES (16,1,21),(17,1,22),(18,1,23),(19,1,24),(20,1,25),(21,1,26),(22,1,27),(23,1,28),(24,1,29),(25,1,31),(1,1,35),(2,1,37),(6,1,41),(7,1,43),(8,1,55),(9,1,61),(10,1,63),(11,1,65),(13,1,67),(14,1,69),(15,1,71),(26,2,8),(27,2,9),(28,2,10),(29,2,11),(30,2,12),(31,2,13),(32,2,14),(33,2,15),(34,2,16),(35,2,17),(36,2,18),(37,2,19),(38,2,20),(39,3,8),(40,3,9),(41,3,10),(42,3,11),(43,3,12),(44,3,13),(45,3,14),(46,3,15),(47,3,16),(48,3,17),(49,3,18),(50,3,19),(51,3,20);
/*!40000 ALTER TABLE `questionnaire_asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire_parameter`
--

DROP TABLE IF EXISTS `questionnaire_parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire_parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) NOT NULL,
  `parameterId` int(11) NOT NULL,
  `rootParameterId` int(11) NOT NULL,
  `parentParameterId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `qp_questionnaireId_parameterId_parentParameterId_uq` (`questionnaireId`,`parameterId`,`parentParameterId`,`rootParameterId`),
  KEY `qp_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `qp_parameterId_fk_idx` (`parameterId`),
  KEY `qp_rootParameterId_fk_idx` (`rootParameterId`),
  KEY `qp_parentParameterId_fk_idx` (`parentParameterId`),
  CONSTRAINT `qp_parameterId_fk` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qp_parentParameterId_fk` FOREIGN KEY (`parentParameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qp_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qp_rootParameterId_fk` FOREIGN KEY (`rootParameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire_parameter`
--

LOCK TABLES `questionnaire_parameter` WRITE;
/*!40000 ALTER TABLE `questionnaire_parameter` DISABLE KEYS */;
INSERT INTO `questionnaire_parameter` VALUES (2,1,29,29,NULL),(16,3,7,27,27),(17,3,8,27,27),(19,3,9,27,27),(18,3,10,27,27),(11,3,11,26,26),(13,3,12,26,26),(10,3,13,26,26),(14,3,14,26,26),(12,3,15,26,26),(8,3,16,25,25),(5,3,17,25,25),(6,3,18,25,25),(4,3,19,25,25),(23,3,20,28,28),(21,3,21,28,28),(22,3,22,28,28),(3,3,25,25,NULL),(9,3,26,26,NULL),(15,3,27,27,NULL),(20,3,28,28,NULL),(7,3,31,25,25);
/*!40000 ALTER TABLE `questionnaire_parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire_question`
--

DROP TABLE IF EXISTS `questionnaire_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) DEFAULT NULL,
  `questionId` int(11) NOT NULL,
  `questionnaireAssetId` int(11) DEFAULT NULL,
  `questionnaireParameterId` int(11) NOT NULL,
  `sequenceNumber` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `qq_QEId_QId_QAId_QPId_uq` (`questionnaireId`,`questionId`,`questionnaireAssetId`,`questionnaireParameterId`),
  KEY `questionnaire_question_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `questionnaire_question_questionId_fk_idx` (`questionId`),
  KEY `questionnaire_question_assetId_fk_idx` (`questionnaireAssetId`),
  KEY `qq_questionnaireParameterId_fk_idx` (`questionnaireParameterId`),
  CONSTRAINT `qq_questionnaireParameterId_fk` FOREIGN KEY (`questionnaireParameterId`) REFERENCES `questionnaire_parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_question_questionId_fk` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_question_questionnaireAssetId_fk` FOREIGN KEY (`questionnaireAssetId`) REFERENCES `questionnaire_asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_question_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=760 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire_question`
--

LOCK TABLES `questionnaire_question` WRITE;
/*!40000 ALTER TABLE `questionnaire_question` DISABLE KEYS */;
INSERT INTO `questionnaire_question` VALUES (101,1,63,1,2,1),(102,1,64,1,2,2),(103,1,65,1,2,3),(104,1,66,1,2,4),(105,1,70,1,2,5),(106,1,71,1,2,6),(107,1,63,2,2,7),(108,1,64,2,2,8),(109,1,65,2,2,9),(110,1,66,2,2,10),(111,1,70,2,2,11),(112,1,71,2,2,12),(113,1,63,6,2,13),(114,1,64,6,2,14),(115,1,65,6,2,15),(116,1,66,6,2,16),(117,1,70,6,2,17),(118,1,71,6,2,18),(119,1,63,7,2,19),(120,1,64,7,2,20),(121,1,65,7,2,21),(122,1,66,7,2,22),(123,1,70,7,2,23),(124,1,71,7,2,24),(125,1,63,8,2,25),(126,1,64,8,2,26),(127,1,65,8,2,27),(128,1,66,8,2,28),(129,1,70,8,2,29),(130,1,71,8,2,30),(131,1,63,9,2,31),(132,1,64,9,2,32),(133,1,65,9,2,33),(134,1,66,9,2,34),(135,1,70,9,2,35),(136,1,71,9,2,36),(137,1,63,10,2,37),(138,1,64,10,2,38),(139,1,65,10,2,39),(140,1,66,10,2,40),(141,1,70,10,2,41),(142,1,71,10,2,42),(143,1,63,11,2,43),(144,1,64,11,2,44),(145,1,65,11,2,45),(146,1,66,11,2,46),(147,1,70,11,2,47),(148,1,71,11,2,48),(149,1,63,13,2,49),(150,1,64,13,2,50),(151,1,65,13,2,51),(152,1,66,13,2,52),(153,1,70,13,2,53),(154,1,71,13,2,54),(155,1,63,14,2,55),(156,1,64,14,2,56),(157,1,65,14,2,57),(158,1,66,14,2,58),(159,1,70,14,2,59),(160,1,71,14,2,60),(161,1,63,15,2,61),(162,1,64,15,2,62),(163,1,65,15,2,63),(164,1,66,15,2,64),(165,1,70,15,2,65),(166,1,71,15,2,66),(167,1,63,16,2,67),(168,1,64,16,2,68),(169,1,65,16,2,69),(170,1,66,16,2,70),(171,1,70,16,2,71),(172,1,71,16,2,72),(173,1,63,17,2,73),(174,1,64,17,2,74),(175,1,65,17,2,75),(176,1,66,17,2,76),(177,1,70,17,2,77),(178,1,71,17,2,78),(179,1,63,18,2,79),(180,1,64,18,2,80),(181,1,65,18,2,81),(182,1,66,18,2,82),(183,1,70,18,2,83),(184,1,71,18,2,84),(185,1,63,19,2,85),(186,1,64,19,2,86),(187,1,65,19,2,87),(188,1,66,19,2,88),(189,1,70,19,2,89),(190,1,71,19,2,90),(191,1,63,20,2,91),(192,1,64,20,2,92),(193,1,65,20,2,93),(194,1,66,20,2,94),(195,1,70,20,2,95),(196,1,71,20,2,96),(197,1,63,21,2,97),(198,1,64,21,2,98),(199,1,65,21,2,99),(200,1,66,21,2,100),(201,1,70,21,2,101),(202,1,71,21,2,102),(203,1,63,22,2,103),(204,1,64,22,2,104),(205,1,65,22,2,105),(206,1,66,22,2,106),(207,1,70,22,2,107),(208,1,71,22,2,108),(209,1,63,23,2,109),(210,1,64,23,2,110),(211,1,65,23,2,111),(212,1,66,23,2,112),(213,1,70,23,2,113),(214,1,71,23,2,114),(215,1,63,24,2,115),(216,1,64,24,2,116),(217,1,65,24,2,117),(218,1,66,24,2,118),(219,1,70,24,2,119),(220,1,71,24,2,120),(221,1,63,25,2,121),(222,1,64,25,2,122),(223,1,65,25,2,123),(224,1,66,25,2,124),(225,1,70,25,2,125),(226,1,71,25,2,126),(227,3,26,39,4,1),(228,3,27,39,4,2),(229,3,36,39,5,3),(230,3,37,39,5,4),(231,3,38,39,5,5),(232,3,28,39,6,6),(233,3,29,39,8,7),(234,3,25,39,10,8),(235,3,67,39,10,9),(236,3,19,39,11,10),(237,3,23,39,11,11),(238,3,24,39,11,12),(239,3,32,39,12,13),(240,3,33,39,12,14),(241,3,34,39,12,15),(242,3,35,39,12,16),(243,3,42,39,13,17),(244,3,48,39,13,18),(245,3,49,39,13,19),(246,3,51,39,13,20),(247,3,2,39,14,21),(248,3,22,39,14,22),(249,3,47,39,14,23),(250,3,8,39,16,24),(251,3,9,39,16,25),(252,3,45,39,16,26),(253,3,46,39,16,27),(254,3,47,39,16,28),(255,3,14,39,17,29),(256,3,15,39,17,30),(257,3,43,39,17,31),(258,3,20,39,18,32),(259,3,21,39,18,33),(260,3,31,39,18,34),(261,3,12,39,19,35),(262,3,16,39,19,36),(263,3,39,39,19,37),(264,3,44,39,19,38),(265,3,30,39,21,39),(266,3,53,39,22,40),(267,3,68,39,23,41),(268,3,26,40,4,42),(269,3,27,40,4,43),(270,3,36,40,5,44),(271,3,37,40,5,45),(272,3,38,40,5,46),(273,3,28,40,6,47),(274,3,29,40,8,48),(275,3,25,40,10,49),(276,3,67,40,10,50),(277,3,19,40,11,51),(278,3,23,40,11,52),(279,3,24,40,11,53),(280,3,32,40,12,54),(281,3,33,40,12,55),(282,3,34,40,12,56),(283,3,35,40,12,57),(284,3,42,40,13,58),(285,3,48,40,13,59),(286,3,49,40,13,60),(287,3,51,40,13,61),(288,3,2,40,14,62),(289,3,22,40,14,63),(290,3,47,40,14,64),(291,3,8,40,16,65),(292,3,9,40,16,66),(293,3,45,40,16,67),(294,3,46,40,16,68),(295,3,47,40,16,69),(296,3,14,40,17,70),(297,3,15,40,17,71),(298,3,43,40,17,72),(299,3,20,40,18,73),(300,3,21,40,18,74),(301,3,31,40,18,75),(302,3,12,40,19,76),(303,3,16,40,19,77),(304,3,39,40,19,78),(305,3,44,40,19,79),(306,3,30,40,21,80),(307,3,53,40,22,81),(308,3,68,40,23,82),(309,3,26,41,4,83),(310,3,27,41,4,84),(311,3,36,41,5,85),(312,3,37,41,5,86),(313,3,38,41,5,87),(314,3,28,41,6,88),(315,3,29,41,8,89),(316,3,25,41,10,90),(317,3,67,41,10,91),(318,3,19,41,11,92),(319,3,23,41,11,93),(320,3,24,41,11,94),(321,3,32,41,12,95),(322,3,33,41,12,96),(323,3,34,41,12,97),(324,3,35,41,12,98),(325,3,42,41,13,99),(326,3,48,41,13,100),(327,3,49,41,13,101),(328,3,51,41,13,102),(329,3,2,41,14,103),(330,3,22,41,14,104),(331,3,47,41,14,105),(332,3,8,41,16,106),(333,3,9,41,16,107),(334,3,45,41,16,108),(335,3,46,41,16,109),(336,3,47,41,16,110),(337,3,14,41,17,111),(338,3,15,41,17,112),(339,3,43,41,17,113),(340,3,20,41,18,114),(341,3,21,41,18,115),(342,3,31,41,18,116),(343,3,12,41,19,117),(344,3,16,41,19,118),(345,3,39,41,19,119),(346,3,44,41,19,120),(347,3,30,41,21,121),(348,3,53,41,22,122),(349,3,68,41,23,123),(350,3,26,42,4,124),(351,3,27,42,4,125),(352,3,36,42,5,126),(353,3,37,42,5,127),(354,3,38,42,5,128),(355,3,28,42,6,129),(356,3,29,42,8,130),(357,3,25,42,10,131),(358,3,67,42,10,132),(359,3,19,42,11,133),(360,3,23,42,11,134),(361,3,24,42,11,135),(362,3,32,42,12,136),(363,3,33,42,12,137),(364,3,34,42,12,138),(365,3,35,42,12,139),(366,3,42,42,13,140),(367,3,48,42,13,141),(368,3,49,42,13,142),(369,3,51,42,13,143),(370,3,2,42,14,144),(371,3,22,42,14,145),(372,3,47,42,14,146),(373,3,8,42,16,147),(374,3,9,42,16,148),(375,3,45,42,16,149),(376,3,46,42,16,150),(377,3,47,42,16,151),(378,3,14,42,17,152),(379,3,15,42,17,153),(380,3,43,42,17,154),(381,3,20,42,18,155),(382,3,21,42,18,156),(383,3,31,42,18,157),(384,3,12,42,19,158),(385,3,16,42,19,159),(386,3,39,42,19,160),(387,3,44,42,19,161),(388,3,30,42,21,162),(389,3,53,42,22,163),(390,3,68,42,23,164),(391,3,26,43,4,165),(392,3,27,43,4,166),(393,3,36,43,5,167),(394,3,37,43,5,168),(395,3,38,43,5,169),(396,3,28,43,6,170),(397,3,29,43,8,171),(398,3,25,43,10,172),(399,3,67,43,10,173),(400,3,19,43,11,174),(401,3,23,43,11,175),(402,3,24,43,11,176),(403,3,32,43,12,177),(404,3,33,43,12,178),(405,3,34,43,12,179),(406,3,35,43,12,180),(407,3,42,43,13,181),(408,3,48,43,13,182),(409,3,49,43,13,183),(410,3,51,43,13,184),(411,3,2,43,14,185),(412,3,22,43,14,186),(413,3,47,43,14,187),(414,3,8,43,16,188),(415,3,9,43,16,189),(416,3,45,43,16,190),(417,3,46,43,16,191),(418,3,47,43,16,192),(419,3,14,43,17,193),(420,3,15,43,17,194),(421,3,43,43,17,195),(422,3,20,43,18,196),(423,3,21,43,18,197),(424,3,31,43,18,198),(425,3,12,43,19,199),(426,3,16,43,19,200),(427,3,39,43,19,201),(428,3,44,43,19,202),(429,3,30,43,21,203),(430,3,53,43,22,204),(431,3,68,43,23,205),(432,3,26,44,4,206),(433,3,27,44,4,207),(434,3,36,44,5,208),(435,3,37,44,5,209),(436,3,38,44,5,210),(437,3,28,44,6,211),(438,3,29,44,8,212),(439,3,25,44,10,213),(440,3,67,44,10,214),(441,3,19,44,11,215),(442,3,23,44,11,216),(443,3,24,44,11,217),(444,3,32,44,12,218),(445,3,33,44,12,219),(446,3,34,44,12,220),(447,3,35,44,12,221),(448,3,42,44,13,222),(449,3,48,44,13,223),(450,3,49,44,13,224),(451,3,51,44,13,225),(452,3,2,44,14,226),(453,3,22,44,14,227),(454,3,47,44,14,228),(455,3,8,44,16,229),(456,3,9,44,16,230),(457,3,45,44,16,231),(458,3,46,44,16,232),(459,3,47,44,16,233),(460,3,14,44,17,234),(461,3,15,44,17,235),(462,3,43,44,17,236),(463,3,20,44,18,237),(464,3,21,44,18,238),(465,3,31,44,18,239),(466,3,12,44,19,240),(467,3,16,44,19,241),(468,3,39,44,19,242),(469,3,44,44,19,243),(470,3,30,44,21,244),(471,3,53,44,22,245),(472,3,68,44,23,246),(473,3,26,45,4,247),(474,3,27,45,4,248),(475,3,36,45,5,249),(476,3,37,45,5,250),(477,3,38,45,5,251),(478,3,28,45,6,252),(479,3,29,45,8,253),(480,3,25,45,10,254),(481,3,67,45,10,255),(482,3,19,45,11,256),(483,3,23,45,11,257),(484,3,24,45,11,258),(485,3,32,45,12,259),(486,3,33,45,12,260),(487,3,34,45,12,261),(488,3,35,45,12,262),(489,3,42,45,13,263),(490,3,48,45,13,264),(491,3,49,45,13,265),(492,3,51,45,13,266),(493,3,2,45,14,267),(494,3,22,45,14,268),(495,3,47,45,14,269),(496,3,8,45,16,270),(497,3,9,45,16,271),(498,3,45,45,16,272),(499,3,46,45,16,273),(500,3,47,45,16,274),(501,3,14,45,17,275),(502,3,15,45,17,276),(503,3,43,45,17,277),(504,3,20,45,18,278),(505,3,21,45,18,279),(506,3,31,45,18,280),(507,3,12,45,19,281),(508,3,16,45,19,282),(509,3,39,45,19,283),(510,3,44,45,19,284),(511,3,30,45,21,285),(512,3,53,45,22,286),(513,3,68,45,23,287),(514,3,26,46,4,288),(515,3,27,46,4,289),(516,3,36,46,5,290),(517,3,37,46,5,291),(518,3,38,46,5,292),(519,3,28,46,6,293),(520,3,29,46,8,294),(521,3,25,46,10,295),(522,3,67,46,10,296),(523,3,19,46,11,297),(524,3,23,46,11,298),(525,3,24,46,11,299),(526,3,32,46,12,300),(527,3,33,46,12,301),(528,3,34,46,12,302),(529,3,35,46,12,303),(530,3,42,46,13,304),(531,3,48,46,13,305),(532,3,49,46,13,306),(533,3,51,46,13,307),(534,3,2,46,14,308),(535,3,22,46,14,309),(536,3,47,46,14,310),(537,3,8,46,16,311),(538,3,9,46,16,312),(539,3,45,46,16,313),(540,3,46,46,16,314),(541,3,47,46,16,315),(542,3,14,46,17,316),(543,3,15,46,17,317),(544,3,43,46,17,318),(545,3,20,46,18,319),(546,3,21,46,18,320),(547,3,31,46,18,321),(548,3,12,46,19,322),(549,3,16,46,19,323),(550,3,39,46,19,324),(551,3,44,46,19,325),(552,3,30,46,21,326),(553,3,53,46,22,327),(554,3,68,46,23,328),(555,3,26,47,4,329),(556,3,27,47,4,330),(557,3,36,47,5,331),(558,3,37,47,5,332),(559,3,38,47,5,333),(560,3,28,47,6,334),(561,3,29,47,8,335),(562,3,25,47,10,336),(563,3,67,47,10,337),(564,3,19,47,11,338),(565,3,23,47,11,339),(566,3,24,47,11,340),(567,3,32,47,12,341),(568,3,33,47,12,342),(569,3,34,47,12,343),(570,3,35,47,12,344),(571,3,42,47,13,345),(572,3,48,47,13,346),(573,3,49,47,13,347),(574,3,51,47,13,348),(575,3,2,47,14,349),(576,3,22,47,14,350),(577,3,47,47,14,351),(578,3,8,47,16,352),(579,3,9,47,16,353),(580,3,45,47,16,354),(581,3,46,47,16,355),(582,3,47,47,16,356),(583,3,14,47,17,357),(584,3,15,47,17,358),(585,3,43,47,17,359),(586,3,20,47,18,360),(587,3,21,47,18,361),(588,3,31,47,18,362),(589,3,12,47,19,363),(590,3,16,47,19,364),(591,3,39,47,19,365),(592,3,44,47,19,366),(593,3,30,47,21,367),(594,3,53,47,22,368),(595,3,68,47,23,369),(596,3,26,48,4,370),(597,3,27,48,4,371),(598,3,36,48,5,372),(599,3,37,48,5,373),(600,3,38,48,5,374),(601,3,28,48,6,375),(602,3,29,48,8,376),(603,3,25,48,10,377),(604,3,67,48,10,378),(605,3,19,48,11,379),(606,3,23,48,11,380),(607,3,24,48,11,381),(608,3,32,48,12,382),(609,3,33,48,12,383),(610,3,34,48,12,384),(611,3,35,48,12,385),(612,3,42,48,13,386),(613,3,48,48,13,387),(614,3,49,48,13,388),(615,3,51,48,13,389),(616,3,2,48,14,390),(617,3,22,48,14,391),(618,3,47,48,14,392),(619,3,8,48,16,393),(620,3,9,48,16,394),(621,3,45,48,16,395),(622,3,46,48,16,396),(623,3,47,48,16,397),(624,3,14,48,17,398),(625,3,15,48,17,399),(626,3,43,48,17,400),(627,3,20,48,18,401),(628,3,21,48,18,402),(629,3,31,48,18,403),(630,3,12,48,19,404),(631,3,16,48,19,405),(632,3,39,48,19,406),(633,3,44,48,19,407),(634,3,30,48,21,408),(635,3,53,48,22,409),(636,3,68,48,23,410),(637,3,26,49,4,411),(638,3,27,49,4,412),(639,3,36,49,5,413),(640,3,37,49,5,414),(641,3,38,49,5,415),(642,3,28,49,6,416),(643,3,29,49,8,417),(644,3,25,49,10,418),(645,3,67,49,10,419),(646,3,19,49,11,420),(647,3,23,49,11,421),(648,3,24,49,11,422),(649,3,32,49,12,423),(650,3,33,49,12,424),(651,3,34,49,12,425),(652,3,35,49,12,426),(653,3,42,49,13,427),(654,3,48,49,13,428),(655,3,49,49,13,429),(656,3,51,49,13,430),(657,3,2,49,14,431),(658,3,22,49,14,432),(659,3,47,49,14,433),(660,3,8,49,16,434),(661,3,9,49,16,435),(662,3,45,49,16,436),(663,3,46,49,16,437),(664,3,47,49,16,438),(665,3,14,49,17,439),(666,3,15,49,17,440),(667,3,43,49,17,441),(668,3,20,49,18,442),(669,3,21,49,18,443),(670,3,31,49,18,444),(671,3,12,49,19,445),(672,3,16,49,19,446),(673,3,39,49,19,447),(674,3,44,49,19,448),(675,3,30,49,21,449),(676,3,53,49,22,450),(677,3,68,49,23,451),(678,3,26,50,4,452),(679,3,27,50,4,453),(680,3,36,50,5,454),(681,3,37,50,5,455),(682,3,38,50,5,456),(683,3,28,50,6,457),(684,3,29,50,8,458),(685,3,25,50,10,459),(686,3,67,50,10,460),(687,3,19,50,11,461),(688,3,23,50,11,462),(689,3,24,50,11,463),(690,3,32,50,12,464),(691,3,33,50,12,465),(692,3,34,50,12,466),(693,3,35,50,12,467),(694,3,42,50,13,468),(695,3,48,50,13,469),(696,3,49,50,13,470),(697,3,51,50,13,471),(698,3,2,50,14,472),(699,3,22,50,14,473),(700,3,47,50,14,474),(701,3,8,50,16,475),(702,3,9,50,16,476),(703,3,45,50,16,477),(704,3,46,50,16,478),(705,3,47,50,16,479),(706,3,14,50,17,480),(707,3,15,50,17,481),(708,3,43,50,17,482),(709,3,20,50,18,483),(710,3,21,50,18,484),(711,3,31,50,18,485),(712,3,12,50,19,486),(713,3,16,50,19,487),(714,3,39,50,19,488),(715,3,44,50,19,489),(716,3,30,50,21,490),(717,3,53,50,22,491),(718,3,68,50,23,492),(719,3,26,51,4,493),(720,3,27,51,4,494),(721,3,36,51,5,495),(722,3,37,51,5,496),(723,3,38,51,5,497),(724,3,28,51,6,498),(725,3,29,51,8,499),(726,3,25,51,10,500),(727,3,67,51,10,501),(728,3,19,51,11,502),(729,3,23,51,11,503),(730,3,24,51,11,504),(731,3,32,51,12,505),(732,3,33,51,12,506),(733,3,34,51,12,507),(734,3,35,51,12,508),(735,3,42,51,13,509),(736,3,48,51,13,510),(737,3,49,51,13,511),(738,3,51,51,13,512),(739,3,2,51,14,513),(740,3,22,51,14,514),(741,3,47,51,14,515),(742,3,8,51,16,516),(743,3,9,51,16,517),(744,3,45,51,16,518),(745,3,46,51,16,519),(746,3,47,51,16,520),(747,3,14,51,17,521),(748,3,15,51,17,522),(749,3,43,51,17,523),(750,3,20,51,18,524),(751,3,21,51,18,525),(752,3,31,51,18,526),(753,3,12,51,19,527),(754,3,16,51,19,528),(755,3,39,51,19,529),(756,3,44,51,19,530),(757,3,30,51,21,531),(758,3,53,51,22,532),(759,3,68,51,23,533);
/*!40000 ALTER TABLE `questionnaire_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_asset`
--

DROP TABLE IF EXISTS `relationship_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relationshipSuggestionId` int(11) NOT NULL,
  `parentAssetId` int(11) NOT NULL,
  `childAssetId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `relationship_asset_relationshipSuggestionId_fk_idx` (`relationshipSuggestionId`),
  KEY `relationship_asset_parentAssetId_fk_idx` (`parentAssetId`),
  KEY `relationship_asset_childAssetId_fk_idx` (`childAssetId`),
  KEY `relationship_asset_createdBy_fk_idx` (`createdBy`),
  KEY `relationship_asset_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `ra_childAssetId_fk` FOREIGN KEY (`childAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ra_parentAssetId_fk` FOREIGN KEY (`parentAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_asset_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_asset_relationshipSuggestionId_fk` FOREIGN KEY (`relationshipSuggestionId`) REFERENCES `relationship_suggestion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_asset_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_asset`
--

LOCK TABLES `relationship_asset` WRITE;
/*!40000 ALTER TABLE `relationship_asset` DISABLE KEYS */;
INSERT INTO `relationship_asset` VALUES (1,1,8,21,13,'2016-06-14 17:09:59',13,'2016-06-14 17:09:59'),(2,1,8,23,13,'2016-06-14 17:10:27',13,'2016-06-14 17:10:27'),(3,1,8,22,13,'2016-06-14 17:10:55',13,'2016-06-14 17:10:55'),(4,1,8,24,13,'2016-06-14 17:11:22',13,'2016-06-14 17:11:23'),(5,1,8,25,13,'2016-06-14 17:11:46',13,'2016-06-14 17:11:47'),(6,1,8,26,13,'2016-06-14 17:12:12',13,'2016-06-14 17:12:13'),(7,1,8,27,13,'2016-06-14 17:12:43',13,'2016-06-14 17:12:43'),(8,1,8,28,13,'2016-06-14 17:13:26',13,'2016-06-14 17:13:26'),(9,1,8,29,13,'2016-06-14 17:14:07',13,'2016-06-14 17:14:07'),(11,1,13,71,13,'2016-06-14 17:27:24',13,'2016-06-14 17:27:24'),(12,1,18,21,13,'2016-06-14 17:28:26',13,'2016-06-14 17:28:27'),(13,1,18,22,13,'2016-06-14 17:28:48',13,'2016-06-14 17:28:49'),(14,1,18,24,13,'2016-06-14 17:29:13',13,'2016-06-14 17:29:13'),(15,1,18,25,13,'2016-06-14 17:29:35',13,'2016-06-14 17:29:36'),(16,1,18,26,13,'2016-06-14 17:29:57',13,'2016-06-14 17:29:58'),(17,1,19,21,13,'2016-06-14 17:30:37',13,'2016-06-14 17:30:37'),(18,1,19,27,13,'2016-06-14 17:30:56',13,'2016-06-14 17:30:56'),(19,1,19,28,13,'2016-06-14 17:31:18',13,'2016-06-14 17:31:18'),(20,1,19,29,13,'2016-06-14 17:31:48',13,'2016-06-14 17:31:48'),(21,1,19,55,13,'2016-06-14 17:32:16',13,'2016-06-14 17:32:16'),(22,1,11,35,13,'2016-06-14 17:33:30',13,'2016-06-14 17:33:30'),(23,1,12,35,13,'2016-06-14 17:33:53',13,'2016-06-14 17:33:53'),(24,1,11,31,13,'2016-06-14 17:34:39',13,'2016-06-14 17:34:39'),(25,1,10,31,13,'2016-06-14 17:35:01',13,'2016-06-14 17:35:01'),(26,1,12,31,13,'2016-06-14 17:35:32',13,'2016-06-14 17:35:32'),(27,1,9,31,13,'2016-06-14 17:35:53',13,'2016-06-14 17:35:53'),(28,1,17,61,13,'2016-06-14 17:38:38',13,'2016-06-14 17:38:39'),(29,1,16,61,13,'2016-06-14 17:39:12',13,'2016-06-14 17:39:12'),(30,2,19,9,13,'2016-06-15 17:44:29',13,'2016-06-15 17:44:29'),(31,2,9,11,13,'2016-06-15 17:46:03',13,'2016-06-15 17:46:03'),(32,2,10,11,13,'2016-06-15 17:48:44',13,'2016-06-15 17:48:45'),(33,2,14,9,13,'2016-06-15 17:50:08',13,'2016-06-15 17:50:08');
/*!40000 ALTER TABLE `relationship_asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_asset_data`
--

DROP TABLE IF EXISTS `relationship_asset_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_asset_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentAssetId` int(11) NOT NULL,
  `childAssetId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rad_parentAssetId_fk_idx` (`parentAssetId`),
  KEY `rad_childAssetId_fk_idx` (`childAssetId`),
  KEY `rad_assetId_fk_idx` (`assetId`),
  CONSTRAINT `rad_assetId_fk` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rad_childAssetId_fk` FOREIGN KEY (`childAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rad_parentAssetId_fk` FOREIGN KEY (`parentAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_asset_data`
--

LOCK TABLES `relationship_asset_data` WRITE;
/*!40000 ALTER TABLE `relationship_asset_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `relationship_asset_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_attribute_data`
--

DROP TABLE IF EXISTS `relationship_attribute_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_attribute_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relationshipSuggestionAttributeId` int(11) NOT NULL,
  `relationshipAssetId` int(11) NOT NULL,
  `attributeData` text,
  PRIMARY KEY (`id`),
  KEY `rad_relationshipSuggestionAttributeId_fk_idx` (`relationshipSuggestionAttributeId`),
  KEY `rad_relationshipAssetId_fk_idx` (`relationshipAssetId`),
  CONSTRAINT `rad_relationshipAssetId_fk` FOREIGN KEY (`relationshipAssetId`) REFERENCES `relationship_asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rad_relationshipSuggestionAttributeId_fk` FOREIGN KEY (`relationshipSuggestionAttributeId`) REFERENCES `relationship_suggestion_attribute` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_attribute_data`
--

LOCK TABLES `relationship_attribute_data` WRITE;
/*!40000 ALTER TABLE `relationship_attribute_data` DISABLE KEYS */;
INSERT INTO `relationship_attribute_data` VALUES (1,1,1,' '),(2,1,2,' '),(3,1,3,' '),(4,1,4,' '),(5,1,5,' '),(6,1,6,' '),(7,1,7,' '),(8,1,8,' '),(9,1,9,' '),(11,1,11,' '),(12,1,12,' '),(13,1,13,' '),(14,1,14,' '),(15,1,15,' '),(16,1,16,' '),(17,1,17,' '),(18,1,18,' '),(19,1,19,' '),(20,1,20,' '),(21,1,21,' '),(22,1,22,' '),(23,1,23,' '),(24,1,24,' '),(25,1,25,' '),(26,1,26,' '),(27,1,27,' '),(28,1,28,' '),(29,1,29,' '),(30,5,30,'CSV'),(31,2,30,'Contract Prep'),(32,3,30,'Both'),(33,4,30,'On Demand'),(34,4,31,'Real Time'),(35,3,31,'Outbound'),(36,2,31,'Document Manager printing Module'),(37,5,31,'URL Query'),(38,5,32,'URL Query'),(39,2,32,'Document Manager printing '),(40,3,32,'Outbound'),(41,4,32,'On Demand'),(42,3,33,'Inbound'),(43,4,33,'Real Time'),(44,2,33,'EPP'),(45,5,33,'Flat file');
/*!40000 ALTER TABLE `relationship_attribute_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_suggestion`
--

DROP TABLE IF EXISTS `relationship_suggestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_suggestion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentAssetTypeId` int(11) NOT NULL,
  `relationshipTypeId` int(11) NOT NULL,
  `childAssetTypeId` int(11) NOT NULL,
  `displayName` varchar(150) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rs_displayName_workspaceId_uq` (`workspaceId`,`displayName`),
  KEY `rs_parentAssetTypeId_idx` (`parentAssetTypeId`),
  KEY `rs_childAssetTypeId_idx` (`childAssetTypeId`),
  KEY `rs_relationshipTypeId_idx` (`relationshipTypeId`),
  KEY `rs_workspaceId_idx` (`workspaceId`),
  KEY `rs_createdBy_idx` (`createdBy`),
  KEY `rs_updatedBy_idx` (`updatedBy`),
  CONSTRAINT `rs_childAssetTypeId` FOREIGN KEY (`childAssetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rs_createdBy` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rs_parentAssetTypeId` FOREIGN KEY (`parentAssetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rs_relationshipTypeId` FOREIGN KEY (`relationshipTypeId`) REFERENCES `relationship_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rs_updatedBy` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rs_workspaceId` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_suggestion`
--

LOCK TABLES `relationship_suggestion` WRITE;
/*!40000 ALTER TABLE `relationship_suggestion` DISABLE KEYS */;
INSERT INTO `relationship_suggestion` VALUES (1,1,1,2,'Application Runs on :: Runs Server',NULL,0,13,'2016-06-14 17:07:36',13,'2016-06-14 17:07:37'),(2,1,2,1,'Application Uses :: Used By Application',9,0,13,'2016-06-15 17:39:52',13,'2016-06-15 17:39:52');
/*!40000 ALTER TABLE `relationship_suggestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_suggestion_attribute`
--

DROP TABLE IF EXISTS `relationship_suggestion_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_suggestion_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attributeName` varchar(45) NOT NULL,
  `dataType` varchar(45) NOT NULL,
  `length` int(5) DEFAULT NULL,
  `relationshipSuggestionId` int(11) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `sequenceNumber` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ra_uq_attributeName_rSuggestionId1` (`relationshipSuggestionId`,`attributeName`),
  KEY `ra_relationshipSuggestionId_idx` (`relationshipSuggestionId`),
  KEY `ra_createdBy_fk_idx` (`createdBy`),
  KEY `ra_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `ra_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ra_relationshipSuggestionId_fk` FOREIGN KEY (`relationshipSuggestionId`) REFERENCES `relationship_suggestion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ra_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_suggestion_attribute`
--

LOCK TABLES `relationship_suggestion_attribute` WRITE;
/*!40000 ALTER TABLE `relationship_suggestion_attribute` DISABLE KEYS */;
INSERT INTO `relationship_suggestion_attribute` VALUES (1,'Name','TEXT',100,1,0,1,1,13,'2016-06-14 17:07:37',13,'2016-06-14 17:07:37'),(2,'Name','TEXT',100,2,0,1,1,13,'2016-06-15 17:39:53',13,'2016-06-15 17:39:53'),(3,'Interface Type','TEXT',15,2,0,1,2,13,'2016-06-15 17:40:44',13,'2016-06-15 17:40:52'),(4,'Frequency','TEXT',15,2,0,0,3,13,'2016-06-15 17:41:17',13,'2016-06-15 17:41:17'),(5,'Data Type','TEXT',15,2,0,0,4,13,'2016-06-15 17:42:21',13,'2016-06-15 17:42:21');
/*!40000 ALTER TABLE `relationship_suggestion_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_template`
--

DROP TABLE IF EXISTS `relationship_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentAssetTypeId` int(11) NOT NULL,
  `relationshipTypeId` int(11) NOT NULL,
  `childAssetTypeId` int(11) NOT NULL,
  `displayName` varchar(150) NOT NULL,
  `assetTemplateId` int(11) NOT NULL,
  `hasAttribute` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `relationship_template_displayName_assetTemplateId_uq` (`displayName`,`assetTemplateId`),
  KEY `relationship_template_parentAssetTypeId_fk_idx` (`parentAssetTypeId`),
  KEY `relationship_template_childAssetTypeId_fk_idx` (`childAssetTypeId`),
  KEY `relationship_template_relationshipTypeId_fk_idx` (`relationshipTypeId`),
  KEY `relationship_template_assetTemplateId_fk_idx` (`assetTemplateId`),
  CONSTRAINT `relationship_template_assetTemplateId_fk` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_template_childAssetTypeId_fk` FOREIGN KEY (`childAssetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_template_parentAssetTypeId_fk` FOREIGN KEY (`parentAssetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_template_relationshipTypeId_fk` FOREIGN KEY (`relationshipTypeId`) REFERENCES `relationship_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_template`
--

LOCK TABLES `relationship_template` WRITE;
/*!40000 ALTER TABLE `relationship_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `relationship_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship_type`
--

DROP TABLE IF EXISTS `relationship_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentDescriptor` varchar(50) NOT NULL,
  `childDescriptor` varchar(50) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `displayName` varchar(150) NOT NULL,
  `direction` varchar(4) NOT NULL DEFAULT 'UNI',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `relationship_type_displayName_workspaceId_uq` (`workspaceId`,`displayName`),
  KEY `relationship_type_createdBy_idx` (`createdBy`),
  KEY `relationship_type_updatedBy_idx` (`updatedBy`),
  KEY `relationship_type_workspaceId_idx` (`workspaceId`),
  CONSTRAINT `relationship_type_createdBy` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_type_updatedBy` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_type_workspaceId` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship_type`
--

LOCK TABLES `relationship_type` WRITE;
/*!40000 ALTER TABLE `relationship_type` DISABLE KEYS */;
INSERT INTO `relationship_type` VALUES (1,'Runs on','Runs',NULL,0,'Runs on :: Runs','UNI',13,'2016-06-14 16:53:29',15,'2016-06-23 06:43:03'),(2,'Uses','Used By',9,0,'Uses :: Used By','UNI',13,'2016-07-07 07:24:29',15,'2016-07-07 07:24:29'),(3,'Runs','Runs on',9,0,'Runs :: Runs on','UNI',15,'2016-06-22 12:02:27',15,'2016-06-23 06:42:59'),(4,'Runs','Runs on',NULL,0,'Runs :: Runs on','UNI',15,'2016-06-22 12:28:04',15,'2016-06-22 12:28:04'),(5,'Allocated from','Allocated to',NULL,0,'Allocated from :: Allocated to','UNI',15,'2016-06-23 06:43:18',15,'2016-06-23 06:43:23');
/*!40000 ALTER TABLE `relationship_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `response`
--

DROP TABLE IF EXISTS `response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `response` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `response_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `response_createdBy_fk_idx` (`createdBy`),
  KEY `response_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `response_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `response_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `response_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `response`
--

LOCK TABLES `response` WRITE;
/*!40000 ALTER TABLE `response` DISABLE KEYS */;
INSERT INTO `response` VALUES (1,1,12,'2016-06-15 13:01:05',12,'2016-06-15 13:01:05'),(2,3,14,'2016-06-16 06:12:42',14,'2016-06-17 07:37:37');
/*!40000 ALTER TABLE `response` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `response_data`
--

DROP TABLE IF EXISTS `response_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `response_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireQuestionId` int(11) NOT NULL,
  `responseId` int(11) NOT NULL,
  `responseData` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UQ_response_data_questionnaireQuestionId_responseId` (`questionnaireQuestionId`,`responseId`),
  KEY `response_data_questionnaireId_fk_idx` (`questionnaireQuestionId`),
  KEY `response_data_responseId_fk_idx` (`responseId`),
  CONSTRAINT `response_data_questionnaireQuestionId_fk` FOREIGN KEY (`questionnaireQuestionId`) REFERENCES `questionnaire_question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `response_data_responseId_fk` FOREIGN KEY (`responseId`) REFERENCES `response` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=680 DEFAULT CHARSET=latin1 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `response_data`
--

LOCK TABLES `response_data` WRITE;
/*!40000 ALTER TABLE `response_data` DISABLE KEYS */;
INSERT INTO `response_data` VALUES (1,103,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(2,109,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(3,115,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(4,121,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(5,127,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(6,133,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(7,139,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(8,145,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(9,151,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(10,157,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(11,163,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(12,169,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(13,175,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(14,181,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(15,187,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(16,193,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(17,199,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(18,205,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(19,211,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(20,217,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(21,223,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Physical\"}],\"otherOptionResponseText\":null,\"score\":null}'),(22,104,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Dedicated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(23,110,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Dedicated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(24,116,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Dedicated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(25,122,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Dedicated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(26,128,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Shared\"}],\"otherOptionResponseText\":null,\"score\":null}'),(27,134,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Shared\"}],\"otherOptionResponseText\":null,\"score\":null}'),(28,140,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Shared\"}],\"otherOptionResponseText\":null,\"score\":null}'),(29,146,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Shared\"}],\"otherOptionResponseText\":null,\"score\":null}'),(30,152,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Shared\"}],\"otherOptionResponseText\":null,\"score\":null}'),(31,158,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Shared\"}],\"otherOptionResponseText\":null,\"score\":null}'),(32,164,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Dedicated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(33,170,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Dedicated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(34,176,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Dedicated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(35,182,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Dedicated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(36,188,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Dedicated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(37,194,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Dedicated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(38,200,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Dedicated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(39,206,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Dedicated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(40,212,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Dedicated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(41,218,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Dedicated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(42,224,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Dedicated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(43,105,1,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"microsoft windows server 2008 r2 enterprise edition\"}],\"otherOptionResponseText\":null,\"score\":null}'),(44,111,1,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"microsoft windows server 2008 r2 enterprise edition\"}],\"otherOptionResponseText\":null,\"score\":null}'),(45,117,1,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"microsoft windows server 2008 r2 enterprise edition\"}],\"otherOptionResponseText\":null,\"score\":null}'),(46,123,1,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"microsoft windows server 2008 r2 enterprise edition\"}],\"otherOptionResponseText\":null,\"score\":null}'),(47,129,1,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"OS/400\"}],\"otherOptionResponseText\":null,\"score\":null}'),(48,135,1,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":5.0,\"text\":\"Linux\"}],\"otherOptionResponseText\":null,\"score\":null}'),(49,141,1,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":5.0,\"text\":\"Linux\"}],\"otherOptionResponseText\":null,\"score\":null}'),(50,147,1,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":5.0,\"text\":\"Linux\"}],\"otherOptionResponseText\":null,\"score\":null}'),(51,153,1,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":5.0,\"text\":\"Linux\"}],\"otherOptionResponseText\":null,\"score\":null}'),(52,159,1,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":5.0,\"text\":\"Linux\"}],\"otherOptionResponseText\":null,\"score\":null}'),(53,165,1,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"microsoft windows server 2008 r2 enterprise edition\"}],\"otherOptionResponseText\":null,\"score\":null}'),(54,171,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"OS/400 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(55,177,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"OS/400 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(56,183,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"OS/400 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(57,189,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"OS/400 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(58,195,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"OS/400 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(59,201,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"OS/400 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(60,207,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"OS/400 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(61,213,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"OS/400 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(62,219,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"OS/400 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(63,225,1,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"microsoft windows server 2008 r2 enterprise edition\"}],\"otherOptionResponseText\":null,\"score\":null}'),(64,106,1,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(65,112,1,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Microsoft SQL 2008 R2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(66,118,1,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(67,124,1,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Microsoft SQL 2008 R2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(68,130,1,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"DB2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(69,136,1,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"DB2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(70,142,1,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"DB2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(71,148,1,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"DB2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(72,154,1,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"DB2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(73,160,1,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"DB2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(74,166,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":5.0,\"text\":\"Microsoft SQL 2008 R2 & DB2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(75,172,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(76,178,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(77,184,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(78,190,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(79,196,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(80,202,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(81,208,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(82,214,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(83,220,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(84,226,1,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Microsoft SQL 2008 R2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(85,101,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Prod\"}],\"otherOptionResponseText\":null,\"score\":null}'),(86,107,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":8.0,\"text\":\"Dev\"}],\"otherOptionResponseText\":null,\"score\":null}'),(87,113,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":8.0,\"text\":\"Dev\"}],\"otherOptionResponseText\":null,\"score\":null}'),(88,119,1,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":8.0,\"text\":\"QA\"}],\"otherOptionResponseText\":null,\"score\":null}'),(89,125,1,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":5.0,\"text\":\"DR\"}],\"otherOptionResponseText\":null,\"score\":null}'),(90,131,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":8.0,\"text\":\"Dev\"}],\"otherOptionResponseText\":null,\"score\":null}'),(91,137,1,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":8.0,\"text\":\"QA\"}],\"otherOptionResponseText\":null,\"score\":null}'),(92,143,1,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":6.0,\"text\":\"UAT\"}],\"otherOptionResponseText\":null,\"score\":null}'),(93,149,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Prod\"}],\"otherOptionResponseText\":null,\"score\":null}'),(94,155,1,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":5.0,\"text\":\"DR\"}],\"otherOptionResponseText\":null,\"score\":null}'),(95,161,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Prod\"}],\"otherOptionResponseText\":null,\"score\":null}'),(96,167,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Prod\"}],\"otherOptionResponseText\":null,\"score\":null}'),(97,173,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":8.0,\"text\":\"Dev\"}],\"otherOptionResponseText\":null,\"score\":null}'),(98,179,1,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":6.0,\"text\":\"UAT\"}],\"otherOptionResponseText\":null,\"score\":null}'),(99,185,1,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":6.0,\"text\":\"UAT\"}],\"otherOptionResponseText\":null,\"score\":null}'),(100,191,1,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":6.0,\"text\":\"UAT\"}],\"otherOptionResponseText\":null,\"score\":null}'),(101,197,1,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":6.0,\"text\":\"UAT\"}],\"otherOptionResponseText\":null,\"score\":null}'),(102,203,1,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":6.0,\"text\":\"UAT\"}],\"otherOptionResponseText\":null,\"score\":null}'),(103,209,1,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":7.0,\"text\":\"System Test\"}],\"otherOptionResponseText\":null,\"score\":null}'),(104,215,1,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":6.0,\"text\":\"UAT\"}],\"otherOptionResponseText\":null,\"score\":null}'),(105,221,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Prod\"}],\"otherOptionResponseText\":null,\"score\":null}'),(106,102,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":8.0,\"text\":\"Database\"}],\"otherOptionResponseText\":null,\"score\":null}'),(107,108,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\"Application\"}],\"otherOptionResponseText\":null,\"score\":null}'),(108,114,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":8.0,\"text\":\"Database\"}],\"otherOptionResponseText\":null,\"score\":null}'),(109,120,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\"Application\"}],\"otherOptionResponseText\":null,\"score\":null}'),(110,126,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\"Application\"}],\"otherOptionResponseText\":null,\"score\":null}'),(111,132,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\"Application\"}],\"otherOptionResponseText\":null,\"score\":null}'),(112,138,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\"Application\"}],\"otherOptionResponseText\":null,\"score\":null}'),(113,144,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\"Application\"}],\"otherOptionResponseText\":null,\"score\":null}'),(114,150,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\"Application\"}],\"otherOptionResponseText\":null,\"score\":null}'),(115,156,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\"Application\"}],\"otherOptionResponseText\":null,\"score\":null}'),(116,162,1,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":9.0,\"text\":\"Web\"}],\"otherOptionResponseText\":null,\"score\":null}'),(117,168,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":8.0,\"text\":\"Database\"}],\"otherOptionResponseText\":null,\"score\":null}'),(118,174,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":8.0,\"text\":\"Database\"}],\"otherOptionResponseText\":null,\"score\":null}'),(119,180,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":8.0,\"text\":\"Database\"}],\"otherOptionResponseText\":null,\"score\":null}'),(120,186,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":8.0,\"text\":\"Database\"}],\"otherOptionResponseText\":null,\"score\":null}'),(121,192,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":8.0,\"text\":\"Database\"}],\"otherOptionResponseText\":null,\"score\":null}'),(122,198,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":8.0,\"text\":\"Database\"}],\"otherOptionResponseText\":null,\"score\":null}'),(123,204,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":8.0,\"text\":\"Database\"}],\"otherOptionResponseText\":null,\"score\":null}'),(124,210,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":8.0,\"text\":\"Database\"}],\"otherOptionResponseText\":null,\"score\":null}'),(125,216,1,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":8.0,\"text\":\"Database\"}],\"otherOptionResponseText\":null,\"score\":null}'),(126,222,1,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\"Application\"}],\"otherOptionResponseText\":null,\"score\":null}'),(127,229,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"5.1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(128,240,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-10\"}],\"otherOptionResponseText\":null,\"score\":null}'),(129,281,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-10\"}],\"otherOptionResponseText\":null,\"score\":null}'),(130,322,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-10\"}],\"otherOptionResponseText\":null,\"score\":null}'),(131,363,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-10\"}],\"otherOptionResponseText\":null,\"score\":null}'),(132,404,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-10\"}],\"otherOptionResponseText\":null,\"score\":null}'),(133,445,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(134,486,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(135,527,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(136,568,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(137,609,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(138,650,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(139,691,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(140,732,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(141,241,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-30\"}],\"otherOptionResponseText\":null,\"score\":null}'),(142,282,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-30\"}],\"otherOptionResponseText\":null,\"score\":null}'),(143,323,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-30\"}],\"otherOptionResponseText\":null,\"score\":null}'),(144,364,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-30\"}],\"otherOptionResponseText\":null,\"score\":null}'),(145,405,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-30\"}],\"otherOptionResponseText\":null,\"score\":null}'),(146,446,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-30\"}],\"otherOptionResponseText\":null,\"score\":null}'),(147,487,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-30\"}],\"otherOptionResponseText\":null,\"score\":null}'),(148,528,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-30\"}],\"otherOptionResponseText\":null,\"score\":null}'),(149,569,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-30\"}],\"otherOptionResponseText\":null,\"score\":null}'),(150,610,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-30\"}],\"otherOptionResponseText\":null,\"score\":null}'),(151,651,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-30\"}],\"otherOptionResponseText\":null,\"score\":null}'),(152,692,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-30\"}],\"otherOptionResponseText\":null,\"score\":null}'),(153,733,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-30\"}],\"otherOptionResponseText\":null,\"score\":null}'),(154,242,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"100-300\"}],\"otherOptionResponseText\":null,\"score\":null}'),(155,283,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-100\"}],\"otherOptionResponseText\":null,\"score\":null}'),(156,324,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-100\"}],\"otherOptionResponseText\":null,\"score\":null}'),(157,365,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-100\"}],\"otherOptionResponseText\":null,\"score\":null}'),(158,406,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-100\"}],\"otherOptionResponseText\":null,\"score\":null}'),(159,447,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"100-300\"}],\"otherOptionResponseText\":null,\"score\":null}'),(160,488,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-100\"}],\"otherOptionResponseText\":null,\"score\":null}'),(161,529,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-100\"}],\"otherOptionResponseText\":null,\"score\":null}'),(162,570,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-100\"}],\"otherOptionResponseText\":null,\"score\":null}'),(163,611,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-100\"}],\"otherOptionResponseText\":null,\"score\":null}'),(164,652,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-100\"}],\"otherOptionResponseText\":null,\"score\":null}'),(165,693,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-100\"}],\"otherOptionResponseText\":null,\"score\":null}'),(166,734,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-100\"}],\"otherOptionResponseText\":null,\"score\":null}'),(167,270,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"5.1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(168,311,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"5.1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(169,352,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"5.1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(170,393,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"5.1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(171,434,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"5.1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(172,475,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(173,516,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"5.1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(174,557,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"5.1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(175,598,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"5.1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(176,639,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"5.1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(177,680,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"5.1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(178,721,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"5.1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(179,230,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":8.0,\"text\":\"0\"}],\"otherOptionResponseText\":null,\"score\":null}'),(180,271,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(181,312,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(182,353,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(183,394,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(184,435,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":8.0,\"text\":\"0\"}],\"otherOptionResponseText\":null,\"score\":null}'),(185,476,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(186,517,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(187,558,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(188,599,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(189,640,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":8.0,\"text\":\"0\"}],\"otherOptionResponseText\":null,\"score\":null}'),(190,681,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":8.0,\"text\":\"0\"}],\"otherOptionResponseText\":null,\"score\":null}'),(191,722,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(192,231,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"0\"}],\"otherOptionResponseText\":null,\"score\":null}'),(193,272,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(194,313,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(195,354,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(196,395,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(197,436,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"0\"}],\"otherOptionResponseText\":null,\"score\":null}'),(198,477,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(199,518,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(200,559,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(201,600,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(202,641,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"0\"}],\"otherOptionResponseText\":null,\"score\":null}'),(203,682,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"0\"}],\"otherOptionResponseText\":null,\"score\":null}'),(204,723,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(205,263,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - Strong Mechanism\"}],\"otherOptionResponseText\":null,\"score\":null}'),(206,304,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - Strong Mechanism\"}],\"otherOptionResponseText\":null,\"score\":null}'),(207,345,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - Strong Mechanism\"}],\"otherOptionResponseText\":null,\"score\":null}'),(208,386,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - Strong Mechanism\"}],\"otherOptionResponseText\":null,\"score\":null}'),(209,427,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - Strong Mechanism\"}],\"otherOptionResponseText\":null,\"score\":null}'),(210,468,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(211,509,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - Strong Mechanism\"}],\"otherOptionResponseText\":null,\"score\":null}'),(212,550,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - Strong Mechanism\"}],\"otherOptionResponseText\":null,\"score\":null}'),(213,591,2,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(214,632,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - Strong Mechanism\"}],\"otherOptionResponseText\":null,\"score\":null}'),(215,673,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Yes - Not Strong\"}],\"otherOptionResponseText\":null,\"score\":null}'),(216,714,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - Strong Mechanism\"}],\"otherOptionResponseText\":null,\"score\":null}'),(217,755,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - Strong Mechanism\"}],\"otherOptionResponseText\":null,\"score\":null}'),(218,243,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(219,284,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Out of Vendor support\"}],\"otherOptionResponseText\":null,\"score\":null}'),(220,325,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":6.0,\"text\":\"Has additional Vendor support\"}],\"otherOptionResponseText\":null,\"score\":null}'),(221,366,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":6.0,\"text\":\"Has additional Vendor support\"}],\"otherOptionResponseText\":null,\"score\":null}'),(222,407,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Out of Vendor support\"}],\"otherOptionResponseText\":null,\"score\":null}'),(223,448,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":6.0,\"text\":\"Has additional Vendor support\"}],\"otherOptionResponseText\":null,\"score\":null}'),(224,489,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":6.0,\"text\":\"Has additional Vendor support\"}],\"otherOptionResponseText\":null,\"score\":null}'),(225,530,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":6.0,\"text\":\"Has additional Vendor support\"}],\"otherOptionResponseText\":null,\"score\":null}'),(226,571,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":6.0,\"text\":\"Has additional Vendor support\"}],\"otherOptionResponseText\":null,\"score\":null}'),(227,612,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":6.0,\"text\":\"Has additional Vendor support\"}],\"otherOptionResponseText\":null,\"score\":null}'),(228,653,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":6.0,\"text\":\"Has additional Vendor support\"}],\"otherOptionResponseText\":null,\"score\":null}'),(229,694,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":6.0,\"text\":\"Has additional Vendor support\"}],\"otherOptionResponseText\":null,\"score\":null}'),(230,735,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":6.0,\"text\":\"Has additional Vendor support\"}],\"otherOptionResponseText\":null,\"score\":null}'),(231,257,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Hot\"}],\"otherOptionResponseText\":null,\"score\":null}'),(232,298,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Hot\"}],\"otherOptionResponseText\":null,\"score\":null}'),(233,339,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Hot\"}],\"otherOptionResponseText\":null,\"score\":null}'),(234,380,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Hot\"}],\"otherOptionResponseText\":null,\"score\":null}'),(235,421,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Hot\"}],\"otherOptionResponseText\":null,\"score\":null}'),(236,462,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"Warm\"}],\"otherOptionResponseText\":null,\"score\":null}'),(237,503,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"Warm\"}],\"otherOptionResponseText\":null,\"score\":null}'),(238,544,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"Warm\"}],\"otherOptionResponseText\":null,\"score\":null}'),(239,585,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"Warm\"}],\"otherOptionResponseText\":null,\"score\":null}'),(240,626,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"Warm\"}],\"otherOptionResponseText\":null,\"score\":null}'),(241,667,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Hot\"}],\"otherOptionResponseText\":null,\"score\":null}'),(242,708,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Hot\"}],\"otherOptionResponseText\":null,\"score\":null}'),(243,749,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.0,\"text\":\"Warm\"}],\"otherOptionResponseText\":null,\"score\":null}'),(244,264,2,'{\"options\":[{\"sequenceNumber\":8,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(245,305,2,'{\"options\":[{\"sequenceNumber\":8,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(246,346,2,'{\"options\":[{\"sequenceNumber\":8,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(247,387,2,'{\"options\":[{\"sequenceNumber\":8,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(248,428,2,'{\"options\":[{\"sequenceNumber\":8,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(249,469,2,'{\"options\":[{\"sequenceNumber\":8,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(250,510,2,'{\"options\":[{\"sequenceNumber\":8,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(251,551,2,'{\"options\":[{\"sequenceNumber\":8,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(252,592,2,'{\"options\":[{\"sequenceNumber\":8,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(253,633,2,'{\"options\":[{\"sequenceNumber\":8,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(254,674,2,'{\"options\":[{\"sequenceNumber\":8,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(255,715,2,'{\"options\":[{\"sequenceNumber\":8,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(256,756,2,'{\"options\":[{\"sequenceNumber\":8,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(257,252,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(258,293,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(259,334,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(260,375,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(261,416,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(262,457,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(263,498,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(264,539,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(265,580,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(266,621,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(267,662,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Exists and partially usable\"}],\"otherOptionResponseText\":null,\"score\":null}'),(268,703,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Exists and partially usable\"}],\"otherOptionResponseText\":null,\"score\":null}'),(269,744,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(270,253,2,'{\"options\":[{\"sequenceNumber\":7,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(271,294,2,'{\"options\":[{\"sequenceNumber\":7,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(272,335,2,'{\"options\":[{\"sequenceNumber\":7,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(273,376,2,'{\"options\":[{\"sequenceNumber\":7,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(274,417,2,'{\"options\":[{\"sequenceNumber\":7,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(275,458,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(276,499,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"No Documentation\"}],\"otherOptionResponseText\":null,\"score\":null}'),(277,540,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"No Documentation\"}],\"otherOptionResponseText\":null,\"score\":null}'),(278,581,2,'{\"options\":[{\"sequenceNumber\":7,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(279,622,2,'{\"options\":[{\"sequenceNumber\":7,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(280,663,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"No Documentation\"}],\"otherOptionResponseText\":null,\"score\":null}'),(281,704,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"No Documentation\"}],\"otherOptionResponseText\":null,\"score\":null}'),(282,745,2,'{\"options\":[{\"sequenceNumber\":7,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(283,254,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(284,295,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(285,336,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(286,377,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(287,418,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(288,459,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(289,500,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(290,541,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(291,582,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(292,623,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(293,664,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(294,705,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(295,746,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(296,244,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(297,285,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(298,326,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(299,367,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(300,408,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(301,449,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(302,490,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(303,531,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(304,572,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(305,613,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(306,654,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(307,695,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(308,736,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(309,245,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(310,286,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(311,327,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(312,368,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(313,409,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(314,450,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(315,491,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(316,532,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(317,573,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(318,614,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(319,655,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(320,696,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(321,737,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":10.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}'),(322,246,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":10.0,\"text\":\"Not a Package\"}],\"otherOptionResponseText\":null,\"score\":null}'),(323,287,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":10.0,\"text\":\"Not a Package\"}],\"otherOptionResponseText\":null,\"score\":null}'),(324,328,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":10.0,\"text\":\"Not a Package\"}],\"otherOptionResponseText\":null,\"score\":null}'),(325,369,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":10.0,\"text\":\"Not a Package\"}],\"otherOptionResponseText\":null,\"score\":null}'),(326,410,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":10.0,\"text\":\"Not a Package\"}],\"otherOptionResponseText\":null,\"score\":null}'),(327,451,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":10.0,\"text\":\"Not a Package\"}],\"otherOptionResponseText\":null,\"score\":null}'),(328,492,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":10.0,\"text\":\"Not a Package\"}],\"otherOptionResponseText\":null,\"score\":null}'),(329,533,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":10.0,\"text\":\"Not a Package\"}],\"otherOptionResponseText\":null,\"score\":null}'),(330,574,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":10.0,\"text\":\"Not a Package\"}],\"otherOptionResponseText\":null,\"score\":null}'),(331,615,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":10.0,\"text\":\"Not a Package\"}],\"otherOptionResponseText\":null,\"score\":null}'),(332,656,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":10.0,\"text\":\"Not a Package\"}],\"otherOptionResponseText\":null,\"score\":null}'),(333,697,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":10.0,\"text\":\"Not a Package\"}],\"otherOptionResponseText\":null,\"score\":null}'),(334,738,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":10.0,\"text\":\"Not a Package\"}],\"otherOptionResponseText\":null,\"score\":null}'),(335,266,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":8.0,\"text\":\"Support\"}],\"otherOptionResponseText\":null,\"score\":null}'),(336,307,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Core\"}],\"otherOptionResponseText\":null,\"score\":null}'),(337,348,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Core\"}],\"otherOptionResponseText\":null,\"score\":null}'),(338,389,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Core\"}],\"otherOptionResponseText\":null,\"score\":null}'),(339,430,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Core\"}],\"otherOptionResponseText\":null,\"score\":null}'),(340,471,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":8.0,\"text\":\"Support\"}],\"otherOptionResponseText\":null,\"score\":null}'),(341,512,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Core\"}],\"otherOptionResponseText\":null,\"score\":null}'),(342,553,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Core\"}],\"otherOptionResponseText\":null,\"score\":null}'),(343,594,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Core\"}],\"otherOptionResponseText\":null,\"score\":null}'),(344,635,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":8.0,\"text\":\"Support\"}],\"otherOptionResponseText\":null,\"score\":null}'),(345,676,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":8.0,\"text\":\"Support\"}],\"otherOptionResponseText\":null,\"score\":null}'),(346,717,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":8.0,\"text\":\"Support\"}],\"otherOptionResponseText\":null,\"score\":null}'),(347,758,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":8.0,\"text\":\"Support\"}],\"otherOptionResponseText\":null,\"score\":null}'),(348,247,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(349,288,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(350,329,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(351,370,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(352,411,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(353,452,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(354,493,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(355,534,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(356,575,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(357,616,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(358,657,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(359,698,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(360,739,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(361,235,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"Non Web Based Monolithic\"}],\"otherOptionResponseText\":null,\"score\":null}'),(362,276,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Service Oriented Architecture(SOA)\"}],\"otherOptionResponseText\":null,\"score\":null}'),(363,317,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Service Oriented Architecture(SOA)\"}],\"otherOptionResponseText\":null,\"score\":null}'),(364,358,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Service Oriented Architecture(SOA)\"}],\"otherOptionResponseText\":null,\"score\":null}'),(365,399,2,'{\"options\":[{\"sequenceNumber\":7,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(366,440,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":2.0,\"text\":\"Functional logic in Stored Procedures\"}],\"otherOptionResponseText\":null,\"score\":null}'),(367,481,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Service Oriented Architecture(SOA)\"}],\"otherOptionResponseText\":null,\"score\":null}'),(368,522,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Service Oriented Architecture(SOA)\"}],\"otherOptionResponseText\":null,\"score\":null}'),(369,563,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Service Oriented Architecture(SOA)\"}],\"otherOptionResponseText\":null,\"score\":null}'),(370,604,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Service Oriented Architecture(SOA)\"}],\"otherOptionResponseText\":null,\"score\":null}'),(371,645,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"Non Web Based Monolithic\"}],\"otherOptionResponseText\":null,\"score\":null}'),(372,686,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"Non Web Based Monolithic\"}],\"otherOptionResponseText\":null,\"score\":null}'),(373,727,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Service Oriented Architecture(SOA)\"}],\"otherOptionResponseText\":null,\"score\":null}'),(374,267,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"High\"}],\"otherOptionResponseText\":null,\"score\":null}'),(375,308,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"High\"}],\"otherOptionResponseText\":null,\"score\":null}'),(376,349,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":3.0,\"text\":\"low\"}],\"otherOptionResponseText\":null,\"score\":null}'),(377,390,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"High\"}],\"otherOptionResponseText\":null,\"score\":null}'),(378,431,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Medium\"}],\"otherOptionResponseText\":null,\"score\":null}'),(379,472,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"High\"}],\"otherOptionResponseText\":null,\"score\":null}'),(380,513,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Medium\"}],\"otherOptionResponseText\":null,\"score\":null}'),(381,554,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Medium\"}],\"otherOptionResponseText\":null,\"score\":null}'),(382,595,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"High\"}],\"otherOptionResponseText\":null,\"score\":null}'),(383,636,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Medium\"}],\"otherOptionResponseText\":null,\"score\":null}'),(384,677,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Medium\"}],\"otherOptionResponseText\":null,\"score\":null}'),(385,718,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"High\"}],\"otherOptionResponseText\":null,\"score\":null}'),(386,759,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Medium\"}],\"otherOptionResponseText\":null,\"score\":null}'),(387,250,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(388,291,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(389,332,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(390,373,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Not Applicable\"}],\"otherOptionResponseText\":null,\"score\":null}'),(391,414,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(392,455,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(393,496,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(394,537,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(395,578,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(396,619,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(397,660,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(398,701,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(399,742,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(400,251,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(401,292,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(402,333,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(403,374,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(404,415,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(405,456,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(406,497,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(407,538,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(408,579,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(409,620,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(410,661,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(411,702,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(412,743,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(413,261,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(414,302,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(415,343,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(416,384,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(417,425,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(418,466,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(419,507,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(420,548,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(421,589,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(422,630,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(423,671,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(424,712,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(425,753,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(426,255,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - fully covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(427,296,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(428,337,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(429,378,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(430,419,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(431,460,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - fully covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(432,501,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(433,542,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(434,583,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(435,624,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(436,665,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - fully covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(437,706,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - fully covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(438,747,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(439,256,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - fully covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(440,297,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(441,338,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(442,379,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(443,420,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(444,461,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - fully covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(445,502,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - fully covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(446,543,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - fully covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(447,584,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - fully covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(448,625,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - fully covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(449,666,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - fully covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(450,707,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - fully covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(451,748,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes - fully covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(452,262,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(453,303,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(454,344,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(455,385,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(456,426,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(457,467,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(458,508,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(459,549,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(460,590,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(461,631,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(462,672,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes-Confidentiality maintained through role base access\"}],\"otherOptionResponseText\":null,\"score\":null}'),(463,713,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes-Confidentiality maintained through role base access\"}],\"otherOptionResponseText\":null,\"score\":null}'),(464,754,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(465,236,2,'{\"options\":[{\"sequenceNumber\":15,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(466,277,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":7.0,\"text\":\"Java 1.6\"}],\"otherOptionResponseText\":null,\"score\":null}'),(467,318,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\".Net 4.5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(468,359,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\".Net 4.5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(469,400,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\".Net 4.5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(470,441,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\".Net 4.5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(471,482,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":7.0,\"text\":\"Java 1.6\"}],\"otherOptionResponseText\":null,\"score\":null}'),(472,523,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":7.0,\"text\":\"Java 1.6\"}],\"otherOptionResponseText\":null,\"score\":null}'),(473,564,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":7.0,\"text\":\"Java 1.6\"}],\"otherOptionResponseText\":null,\"score\":null}'),(474,605,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":7.0,\"text\":\"Java 1.6\"}],\"otherOptionResponseText\":null,\"score\":null}'),(475,646,2,'{\"options\":[{\"sequenceNumber\":15,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(476,687,2,'{\"options\":[{\"sequenceNumber\":8,\"quantifier\":4.0,\"text\":\"Share point 2007\"}],\"otherOptionResponseText\":null,\"score\":null}'),(477,728,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":7.0,\"text\":\"Java 1.6\"}],\"otherOptionResponseText\":null,\"score\":null}'),(478,258,2,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(479,299,2,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(480,340,2,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(481,381,2,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(482,422,2,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(483,463,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\"Completely\"}],\"otherOptionResponseText\":null,\"score\":null}'),(484,504,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\"Completely\"}],\"otherOptionResponseText\":null,\"score\":null}'),(485,545,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(486,586,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\"Completely\"}],\"otherOptionResponseText\":null,\"score\":null}'),(487,627,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\"Completely\"}],\"otherOptionResponseText\":null,\"score\":null}'),(488,668,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(489,709,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":10.0,\"text\":\"Completely\"}],\"otherOptionResponseText\":null,\"score\":null}'),(490,750,2,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(491,259,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":8.0,\"text\":\"Minor\"}],\"otherOptionResponseText\":null,\"score\":null}'),(492,300,2,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(493,341,2,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(494,382,2,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(495,423,2,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(496,464,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Major\"}],\"otherOptionResponseText\":null,\"score\":null}'),(497,505,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Major\"}],\"otherOptionResponseText\":null,\"score\":null}'),(498,546,2,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(499,587,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":8.0,\"text\":\"Minor\"}],\"otherOptionResponseText\":null,\"score\":null}'),(500,628,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":8.0,\"text\":\"Minor\"}],\"otherOptionResponseText\":null,\"score\":null}'),(501,669,2,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(502,710,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Major\"}],\"otherOptionResponseText\":null,\"score\":null}'),(503,751,2,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(504,248,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(505,289,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(506,330,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(507,371,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(508,412,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(509,453,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(510,494,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(511,535,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(512,576,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(513,617,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(514,658,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(515,699,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(516,740,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(517,237,2,'{\"options\":[{\"sequenceNumber\":7,\"quantifier\":6.0,\"text\":\"OS/400 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(518,278,2,'{\"options\":[{\"sequenceNumber\":12,\"quantifier\":4.0,\"text\":\"Windows Server 2008\"}],\"otherOptionResponseText\":null,\"score\":null}'),(519,319,2,'{\"options\":[{\"sequenceNumber\":12,\"quantifier\":4.0,\"text\":\"Windows Server 2008\"}],\"otherOptionResponseText\":null,\"score\":null}'),(520,360,2,'{\"options\":[{\"sequenceNumber\":12,\"quantifier\":4.0,\"text\":\"Windows Server 2008\"}],\"otherOptionResponseText\":null,\"score\":null}'),(521,401,2,'{\"options\":[{\"sequenceNumber\":12,\"quantifier\":4.0,\"text\":\"Windows Server 2008\"}],\"otherOptionResponseText\":null,\"score\":null}'),(522,442,2,'{\"options\":[{\"sequenceNumber\":12,\"quantifier\":4.0,\"text\":\"Windows Server 2008\"}],\"otherOptionResponseText\":null,\"score\":null}'),(523,483,2,'{\"options\":[{\"sequenceNumber\":7,\"quantifier\":6.0,\"text\":\"OS/400 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(524,524,2,'{\"options\":[{\"sequenceNumber\":7,\"quantifier\":6.0,\"text\":\"OS/400 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(525,565,2,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":8.0,\"text\":\"Linux 5.11\"}],\"otherOptionResponseText\":null,\"score\":null}'),(526,606,2,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":8.0,\"text\":\"Linux 5.11\"}],\"otherOptionResponseText\":null,\"score\":null}'),(527,647,2,'{\"options\":[{\"sequenceNumber\":7,\"quantifier\":6.0,\"text\":\"OS/400 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(528,688,2,'{\"options\":[{\"sequenceNumber\":7,\"quantifier\":6.0,\"text\":\"OS/400 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(529,729,2,'{\"options\":[{\"sequenceNumber\":7,\"quantifier\":6.0,\"text\":\"OS/400 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(530,238,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(531,279,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(532,320,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(533,361,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(534,402,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(535,443,2,'{\"options\":[{\"sequenceNumber\":8,\"quantifier\":4.0,\"text\":\"SQL Server 2008\"}],\"otherOptionResponseText\":null,\"score\":null}'),(536,484,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(537,525,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(538,566,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(539,607,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(540,648,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(541,689,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(542,730,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"DB2 V7R1\"}],\"otherOptionResponseText\":null,\"score\":null}'),(543,234,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Green Screen\"}],\"otherOptionResponseText\":null,\"score\":null}'),(544,275,2,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(545,316,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":10.0,\"text\":\"Internet Browser Based\"}],\"otherOptionResponseText\":null,\"score\":null}'),(546,357,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":10.0,\"text\":\"Internet Browser Based\"}],\"otherOptionResponseText\":null,\"score\":null}'),(547,398,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":10.0,\"text\":\"Internet Browser Based\"}],\"otherOptionResponseText\":null,\"score\":null}'),(548,439,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Desktop Based\"}],\"otherOptionResponseText\":null,\"score\":null}'),(549,480,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":10.0,\"text\":\"Internet Browser Based\"}],\"otherOptionResponseText\":null,\"score\":null}'),(550,521,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":10.0,\"text\":\"Internet Browser Based\"}],\"otherOptionResponseText\":null,\"score\":null}'),(551,562,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":10.0,\"text\":\"Internet Browser Based\"}],\"otherOptionResponseText\":null,\"score\":null}'),(552,603,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":10.0,\"text\":\"Internet Browser Based\"}],\"otherOptionResponseText\":null,\"score\":null}'),(553,644,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Green Screen\"}],\"otherOptionResponseText\":null,\"score\":null}'),(554,685,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Green Screen\"}],\"otherOptionResponseText\":null,\"score\":null}'),(555,726,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":10.0,\"text\":\"Internet Browser Based\"}],\"otherOptionResponseText\":null,\"score\":null}'),(556,227,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Good\"}],\"otherOptionResponseText\":null,\"score\":null}'),(557,268,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Good\"}],\"otherOptionResponseText\":null,\"score\":null}'),(558,309,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Good\"}],\"otherOptionResponseText\":null,\"score\":null}'),(559,350,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Good\"}],\"otherOptionResponseText\":null,\"score\":null}'),(560,391,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Good\"}],\"otherOptionResponseText\":null,\"score\":null}'),(561,432,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Some Improvement Required\"}],\"otherOptionResponseText\":null,\"score\":null}'),(562,473,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Good\"}],\"otherOptionResponseText\":null,\"score\":null}'),(563,514,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Good\"}],\"otherOptionResponseText\":null,\"score\":null}'),(564,555,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Good\"}],\"otherOptionResponseText\":null,\"score\":null}'),(565,596,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Good\"}],\"otherOptionResponseText\":null,\"score\":null}'),(566,637,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Good\"}],\"otherOptionResponseText\":null,\"score\":null}'),(567,678,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Good\"}],\"otherOptionResponseText\":null,\"score\":null}'),(568,719,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Good\"}],\"otherOptionResponseText\":null,\"score\":null}'),(569,228,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Completely\"}],\"otherOptionResponseText\":null,\"score\":null}'),(570,269,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Completely\"}],\"otherOptionResponseText\":null,\"score\":null}'),(571,310,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Completely\"}],\"otherOptionResponseText\":null,\"score\":null}'),(572,351,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Completely\"}],\"otherOptionResponseText\":null,\"score\":null}'),(573,392,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Completely\"}],\"otherOptionResponseText\":null,\"score\":null}'),(574,433,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Completely\"}],\"otherOptionResponseText\":null,\"score\":null}'),(575,474,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Completely\"}],\"otherOptionResponseText\":null,\"score\":null}'),(576,515,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Completely\"}],\"otherOptionResponseText\":null,\"score\":null}'),(577,556,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Completely\"}],\"otherOptionResponseText\":null,\"score\":null}'),(578,597,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Completely\"}],\"otherOptionResponseText\":null,\"score\":null}'),(579,638,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Completely\"}],\"otherOptionResponseText\":null,\"score\":null}'),(580,679,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Completely\"}],\"otherOptionResponseText\":null,\"score\":null}'),(581,720,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Completely\"}],\"otherOptionResponseText\":null,\"score\":null}'),(582,232,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(583,273,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(584,314,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(585,355,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(586,396,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(587,437,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(588,478,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":4.0,\"text\":\"Partially Covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(589,519,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(590,560,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":4.0,\"text\":\"Partially Covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(591,601,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":4.0,\"text\":\"Partially Covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(592,642,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(593,683,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Fully Covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(594,724,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":4.0,\"text\":\"Partially Covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(595,233,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Easy to Use\"}],\"otherOptionResponseText\":null,\"score\":null}'),(596,274,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(597,315,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Easy to Use\"}],\"otherOptionResponseText\":null,\"score\":null}'),(598,356,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Easy to Use\"}],\"otherOptionResponseText\":null,\"score\":null}'),(599,397,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Easy to Use\"}],\"otherOptionResponseText\":null,\"score\":null}'),(600,438,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Easy to Use\"}],\"otherOptionResponseText\":null,\"score\":null}'),(601,479,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Easy to Use\"}],\"otherOptionResponseText\":null,\"score\":null}'),(602,520,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Easy to Use\"}],\"otherOptionResponseText\":null,\"score\":null}'),(603,561,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Easy to Use\"}],\"otherOptionResponseText\":null,\"score\":null}'),(604,602,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Easy to Use\"}],\"otherOptionResponseText\":null,\"score\":null}'),(605,643,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Easy to Use\"}],\"otherOptionResponseText\":null,\"score\":null}'),(606,684,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Easy to Use\"}],\"otherOptionResponseText\":null,\"score\":null}'),(607,725,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Easy to Use\"}],\"otherOptionResponseText\":null,\"score\":null}'),(608,265,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"%Availability >99.5% & 24X7\"}],\"otherOptionResponseText\":null,\"score\":null}'),(609,306,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"%Availability >99.5% & 24X7\"}],\"otherOptionResponseText\":null,\"score\":null}'),(610,347,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"%Availability >99.5% & 24X7\"}],\"otherOptionResponseText\":null,\"score\":null}'),(611,388,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"%Availability >99.5% & 24X7\"}],\"otherOptionResponseText\":null,\"score\":null}'),(612,429,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"%Availability >99.5% & 24X7\"}],\"otherOptionResponseText\":null,\"score\":null}'),(613,470,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":2.0,\"text\":\"Others\"}],\"otherOptionResponseText\":null,\"score\":null}'),(614,511,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"%Availability >99.5% & 24X7\"}],\"otherOptionResponseText\":null,\"score\":null}'),(615,552,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"%Availability >99.5% & 24X7\"}],\"otherOptionResponseText\":null,\"score\":null}'),(616,593,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"%Availability >99.5% & 24X7\"}],\"otherOptionResponseText\":null,\"score\":null}'),(617,634,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"%Availability >99.5% & 24X7\"}],\"otherOptionResponseText\":null,\"score\":null}'),(618,675,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"%Availability >99.5% & 24X7\"}],\"otherOptionResponseText\":null,\"score\":null}'),(619,716,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"%Availability >99.5% & 24X7\"}],\"otherOptionResponseText\":null,\"score\":null}'),(620,757,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"%Availability >99.5% & 24X7\"}],\"otherOptionResponseText\":null,\"score\":null}'),(621,260,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes-Process in place\"}],\"otherOptionResponseText\":null,\"score\":null}'),(622,301,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes-Process in place\"}],\"otherOptionResponseText\":null,\"score\":null}'),(623,342,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes-Process in place\"}],\"otherOptionResponseText\":null,\"score\":null}'),(624,383,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes-Process in place\"}],\"otherOptionResponseText\":null,\"score\":null}'),(625,424,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes-Process in place\"}],\"otherOptionResponseText\":null,\"score\":null}'),(626,465,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes-Process in place\"}],\"otherOptionResponseText\":null,\"score\":null}'),(627,506,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes-Process in place\"}],\"otherOptionResponseText\":null,\"score\":null}'),(628,547,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes-Process in place\"}],\"otherOptionResponseText\":null,\"score\":null}'),(629,588,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes-Process in place\"}],\"otherOptionResponseText\":null,\"score\":null}'),(630,629,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes-Process in place\"}],\"otherOptionResponseText\":null,\"score\":null}'),(631,670,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes-Process in place\"}],\"otherOptionResponseText\":null,\"score\":null}'),(632,711,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes-Process in place\"}],\"otherOptionResponseText\":null,\"score\":null}'),(633,752,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Yes-Process in place\"}],\"otherOptionResponseText\":null,\"score\":null}'),(634,239,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(635,280,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(636,321,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(637,362,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(638,403,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(639,444,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(640,485,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(641,526,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(642,567,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(643,608,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(644,649,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(645,690,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"0-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(646,731,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":-1.0,\"text\":\"N/A\"}],\"otherOptionResponseText\":null,\"score\":null}'),(647,741,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(653,700,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(654,249,2,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"No Documentation\"}],\"otherOptionResponseText\":null,\"score\":null}'),(660,290,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Exists and partially usable\"}],\"otherOptionResponseText\":null,\"score\":null}'),(666,331,2,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":3.0,\"text\":\"Exists but Unusable\"}],\"otherOptionResponseText\":null,\"score\":null}'),(672,372,2,'{\"options\":[{\"sequenceNumber\":6,\"quantifier\":5.0,\"text\":\"Information Not Available\"}],\"otherOptionResponseText\":null,\"score\":null}'),(673,413,2,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":6.0,\"text\":\"Exists and partially usable\"}],\"otherOptionResponseText\":null,\"score\":null}'),(674,454,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(675,495,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(676,536,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(677,577,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(678,618,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}'),(679,659,2,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":10.0,\"text\":\"Exists and Updated\"}],\"otherOptionResponseText\":null,\"score\":null}');
/*!40000 ALTER TABLE `response_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(45) NOT NULL,
  `roleDescription` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ROLE_APP_ADMIN',NULL),(2,'ROLE_APP_USER',NULL),(3,'ROLE_CONSULTANT',NULL),(4,'ROLE_CUSTOMER',NULL);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_permission`
--

DROP TABLE IF EXISTS `role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_permission` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `permissionname` varchar(45) NOT NULL,
  `roleid` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_ROLE_PERMISSION` (`permissionname`,`roleid`),
  KEY `FK_ROLEPERM_ROLEID_idx` (`roleid`),
  CONSTRAINT `FK_ROLEPERM_ROLEID` FOREIGN KEY (`roleid`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=485 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_permission`
--

LOCK TABLES `role_permission` WRITE;
/*!40000 ALTER TABLE `role_permission` DISABLE KEYS */;
INSERT INTO `role_permission` VALUES (455,'AID_MODIFY',1),(478,'AID_VIEW',1),(483,'APP_LOGIN',1),(465,'ASSET_FILLDETAILS',1),(452,'BCM_MODIFY',1),(470,'BCM_VIEW',1),(453,'COLOR_SCHEME_MODIFY',1),(448,'COLOR_SCHEME_VIEW',1),(449,'FUNCTIONAL_MAP_MODIFY',1),(474,'FUNCTIONAL_MAP_VIEW',1),(477,'INDEX_CREATION',1),(457,'MODIFY_TCO',1),(459,'PARAMETER_MODIFY',1),(466,'PARAMETER_VIEW',1),(456,'QUESTIONNAIRE_MODIFY',1),(451,'QUESTIONNAIRE_RESPOND',1),(471,'QUESTIONNAIRE_VIEW',1),(454,'QUESTION_MODIFY',1),(463,'QUESTION_VIEW',1),(464,'RELATIONSHIP_MODIFY',1),(475,'RELATIONSHIP_VIEW',1),(462,'REPORT_ASSET_GRAPH',1),(468,'REPORT_FACET_GRAPH',1),(482,'REPORT_FUNCTIONAL_REDUNDANCY',1),(450,'REPORT_PHP',1),(480,'REPORT_SCATTER_GRAPH',1),(469,'TEMPLATE_MODIFY',1),(460,'TEMPLATE_VIEW',1),(473,'USER_MODIFY',1),(484,'USER_ROLE_MODIFY',1),(476,'USER_ROLE_VIEW',1),(472,'USER_VIEW',1),(479,'VIEW_TCO',1),(458,'WORKSPACES_MODIFY',1),(461,'WORKSPACES_MODIFY_DASHBOARD',1),(481,'WORKSPACES_VIEW',1),(467,'WORKSPACES_VIEW_DASHBOARD',1);
/*!40000 ALTER TABLE `role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmptable`
--

DROP TABLE IF EXISTS `tmptable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmptable` (
  `logmessage` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmptable`
--

LOCK TABLES `tmptable` WRITE;
/*!40000 ALTER TABLE `tmptable` DISABLE KEYS */;
INSERT INTO `tmptable` VALUES ('myvar is '),('myvar is '),('myvar is '),('myvar is end'),('myvar is '),('myvar is end'),('myvar is '),('length of children'),('myvar is end'),('myvar is '),('length of children'),('myvar is end'),('myvar is '),('length of children4'),('length of children'),('myvar is end4');
/*!40000 ALTER TABLE `tmptable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `emailAddress` varchar(45) NOT NULL,
  `password` varchar(60) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `accountExpired` tinyint(1) NOT NULL DEFAULT '0',
  `accountLocked` tinyint(1) NOT NULL DEFAULT '0',
  `credentialExpired` tinyint(1) NOT NULL DEFAULT '0',
  `accountEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `emailAddress_UNIQUE` (`emailAddress`),
  UNIQUE KEY `UK_t40ack6c3x1y4m2nhaju018jg` (`emailAddress`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Admin','Admin','thirdeye-admin@birlasoft.com','$2a$10$2E3ud9YmM9U4EAcJd16lmusYswSzmx5DGhQ3tk6L9g4A/5eyvxCpO',0,0,0,0,0,-1,'2016-06-08 11:38:38',-1,'2016-06-08 11:38:38'),(12,'Mehak','Guglani','mehak.guglani@birlasoft.com','$2a$10$RlZBiyi5X7ErylkMKi//Cem45ean/Aol0u5VvjLK/yRGYYN1oGRl.',0,0,0,0,0,1,'2016-06-08 11:41:41',1,'2016-06-08 11:41:41'),(13,'Tej','Sarup','tej.sarup@birlasoft.com','$2a$10$Ix8qkKheN4YxP9KDfddz6e6HsJWzIYeNUXVttqfnHCopxHWM6uvn.',0,0,0,0,0,12,'2016-06-09 09:57:24',12,'2016-06-09 09:57:24'),(14,'Ananta','Sethi','ananta.sethi@birlasoft.com','$2a$10$EGUHDlh3tsQgxiyZ.31gfOP7NJjZ1L8uFFBWMNwfhC9gmo0uu.kRa',0,0,0,0,0,12,'2016-06-09 09:58:16',12,'2016-06-09 09:58:16'),(15,'Samar','Gupta','samar.gupta@birlasoft.com','$2a$10$CFJfryoQZYviNvc2ATQFou42pprd8DPoaJvAOxN/9hnRtohw3zMpS',0,0,0,0,0,12,'2016-06-16 13:39:28',12,'2016-06-16 13:39:28');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_role_unique` (`userId`,`roleId`),
  KEY `roleIdFk_idx` (`roleId`),
  KEY `userIdFk_idx` (`userId`),
  CONSTRAINT `roleIdFk` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `userIdFk` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,1,1),(38,12,1),(39,12,2),(40,13,1),(41,13,2),(42,14,1),(43,14,2),(44,15,1);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_workspace`
--

DROP TABLE IF EXISTS `user_workspace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_workspace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_USER_WORKSPACE` (`userId`,`workspaceId`),
  KEY `FK_WORKSPACEID_WORKSPACEID_JOIN_idx` (`workspaceId`),
  CONSTRAINT `FK_USERID_USERID_JOIN` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_WORKSPACEID_WORKSPACEID_JOIN` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_workspace`
--

LOCK TABLES `user_workspace` WRITE;
/*!40000 ALTER TABLE `user_workspace` DISABLE KEYS */;
INSERT INTO `user_workspace` VALUES (1,1,1),(23,12,1),(24,12,9),(27,12,10),(32,12,11),(33,12,12),(36,12,13),(25,13,9),(29,13,10),(31,13,11),(35,13,12),(26,14,9),(28,14,10),(30,14,11),(34,14,12),(37,15,9);
/*!40000 ALTER TABLE `user_workspace` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `widget`
--

DROP TABLE IF EXISTS `widget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `widgetType` varchar(20) NOT NULL,
  `dashboardId` int(11) NOT NULL,
  `sequenceNumber` int(11) NOT NULL,
  `widgetConfig` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_WIDGET_DASHBOARD_idx` (`dashboardId`),
  CONSTRAINT `FK_WIDGET_DASHBOARD` FOREIGN KEY (`dashboardId`) REFERENCES `dashboard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `widget`
--

LOCK TABLES `widget` WRITE;
/*!40000 ALTER TABLE `widget` DISABLE KEYS */;
INSERT INTO `widget` VALUES (1,'FACETGRAPH',1,1,'{\"id\":1,\"width\":0,\"height\":0,\"title\":\"Environments\",\"questionnaireId\":1,\"questionId\":63,\"paramId\":0,\"graphType\":\"pie\"}'),(2,'FACETGRAPH',1,2,'{\"id\":2,\"width\":0,\"height\":0,\"title\":\"Server Operating System\",\"questionnaireId\":1,\"questionId\":70,\"paramId\":0,\"graphType\":\"bar\"}'),(3,'FACETGRAPH',1,3,'{\"id\":3,\"width\":0,\"height\":0,\"title\":\"Shared Servers\",\"questionnaireId\":1,\"questionId\":66,\"paramId\":0,\"graphType\":\"pie\"}'),(5,'FACETGRAPH',1,4,'{\"id\":5,\"width\":0,\"height\":0,\"title\":\"DB Servers\",\"questionnaireId\":1,\"questionId\":71,\"paramId\":0,\"graphType\":\"pie\"}');
/*!40000 ALTER TABLE `widget` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workspace`
--

DROP TABLE IF EXISTS `workspace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workspace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workspaceName` varchar(45) NOT NULL,
  `workspaceDescription` text,
  `deletedStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_WORKSPACE_USER_idx` (`createdBy`),
  KEY `FK_WORKSPACE_USER_UPDATE_idx` (`updatedBy`),
  CONSTRAINT `FK_WORKSPACE_USER_CREATE` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_WORKSPACE_USER_UPDATE` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workspace`
--

LOCK TABLES `workspace` WRITE;
/*!40000 ALTER TABLE `workspace` DISABLE KEYS */;
INSERT INTO `workspace` VALUES (1,'MyCust','Workspace for MyCust',0,1,'2016-06-08 11:29:15',1,'2016-06-08 11:29:15'),(9,'Globalization','',0,12,'2016-06-09 09:56:00',12,'2016-06-09 09:56:00'),(10,'Sales and Marketing','',0,12,'2016-06-09 10:00:15',12,'2016-06-09 10:00:15'),(11,'Operations and Fulfillment','',0,12,'2016-06-09 10:03:27',12,'2016-06-09 10:03:27'),(12,'Utilities And Services','',0,12,'2016-06-09 10:06:26',12,'2016-06-09 10:06:26'),(13,'Sample','',0,12,'2016-06-10 09:33:09',12,'2016-06-10 09:33:09');
/*!40000 ALTER TABLE `workspace` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workspace_dashboard`
--

DROP TABLE IF EXISTS `workspace_dashboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workspace_dashboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workspaceId` int(11) NOT NULL,
  `dashboardId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_WORKSPACE_DASHBOARD` (`workspaceId`,`dashboardId`),
  KEY `FK_WORKSPACE_ID_idx` (`workspaceId`),
  KEY `FK_DASHBOARD_ID_idx` (`dashboardId`),
  CONSTRAINT `FK_DASHBOARD_ID` FOREIGN KEY (`dashboardId`) REFERENCES `dashboard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_WORKSPACE_ID` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workspace_dashboard`
--

LOCK TABLES `workspace_dashboard` WRITE;
/*!40000 ALTER TABLE `workspace_dashboard` DISABLE KEYS */;
/*!40000 ALTER TABLE `workspace_dashboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database '3rdi'
--
/*!50003 DROP FUNCTION IF EXISTS `GetParameterTree` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `GetParameterTree`(GivenID INT) RETURNS varchar(1024) CHARSET latin1
    DETERMINISTIC
BEGIN

    DECLARE rv,q,queue,queue_children VARCHAR(1024);
    DECLARE queue_length,front_id,pos INT;

    SET rv = '';
    SET queue = GivenID;
    SET queue_length = 1;

    WHILE queue_length > 0 DO
        
        IF queue_length = 1 THEN
            SET front_id = FORMAT(queue,0);
            SET queue = '';
        ELSE
			SET front_id = SUBSTR(queue,1,LOCATE(',',queue)-1);
            SET pos = LOCATE(',',queue) + 1;
            SET q = SUBSTR(queue,pos);
            SET queue = q;
        END IF;
        SET queue_length = queue_length - 1;

        SELECT IFNULL(qc,'') INTO queue_children
        FROM (SELECT GROUP_CONCAT(childparameterId) qc
        FROM parameter_function WHERE parentParameterId = front_id) A;

        IF LENGTH(queue_children) = 0 THEN
            IF LENGTH(queue) = 0 THEN
                SET queue_length = 0;
            END IF;
        ELSE
            IF LENGTH(rv) = 0 THEN
                SET rv = queue_children;
            ELSE
                SET rv = CONCAT(rv,',',queue_children);
            END IF;
            IF LENGTH(queue) = 0 THEN
                SET queue = queue_children;
            ELSE
                SET queue = CONCAT(queue,',',queue_children);
            END IF;
            SET queue_length = LENGTH(queue) - LENGTH(REPLACE(queue,',','')) + 1;
        END IF;
    END WHILE;

    RETURN rv;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getParameterTree` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getParameterTree`(IN GivenID int, OUT rv VARCHAR(1024))
BEGIN

    set rv = getParameterTree(GivenID);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-26 15:49:51
