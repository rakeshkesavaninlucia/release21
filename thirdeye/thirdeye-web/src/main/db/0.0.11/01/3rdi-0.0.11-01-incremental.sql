-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE TABLE `3rdi`.`relationship_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relationshipSuggestionId` int(11) NOT NULL,
  `parentAssetId` int(11) NOT NULL,
  `childAssetId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `relationship_asset_relationshipSuggestionId_fk_idx` (`relationshipSuggestionId`),
  KEY `relationship_asset_parentAssetId_fk_idx` (`parentAssetId`),
  KEY `relationship_asset_childAssetId_fk_idx` (`childAssetId`),
  KEY `relationship_asset_createdBy_fk_idx` (`createdBy`),
  KEY `relationship_asset_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `ra_childAssetId_fk` FOREIGN KEY (`childAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ra_parentAssetId_fk` FOREIGN KEY (`parentAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_asset_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_asset_relationshipSuggestionId_fk` FOREIGN KEY (`relationshipSuggestionId`) REFERENCES `relationship_suggestion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `relationship_asset_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
  
  CREATE TABLE `3rdi`.`relationship_attribute_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relationshipSuggestionAttributeId` int(11) NOT NULL,
  `relationshipAssetId` int(11) NOT NULL,
  `attributeData` text,
  PRIMARY KEY (`id`),
  KEY `rad_relationshipSuggestionAttributeId_fk_idx` (`relationshipSuggestionAttributeId`),
  KEY `rad_relationshipAssetId_fk_idx` (`relationshipAssetId`),
  CONSTRAINT `rad_relationshipAssetId_fk` FOREIGN KEY (`relationshipAssetId`) REFERENCES `relationship_asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rad_relationshipSuggestionAttributeId_fk` FOREIGN KEY (`relationshipSuggestionAttributeId`) REFERENCES `relationship_suggestion_attribute` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
