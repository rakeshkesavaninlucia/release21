-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE TABLE IF NOT EXISTS `3rdi`.`asset_type_style` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `asset_type_id` INT(11) NOT NULL,
  `asset_colour` VARCHAR(45) NOT NULL,
  `asset_image` BLOB NULL DEFAULT NULL,
  `workspaceId` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `assetTypeId_workspace_uq` (`asset_type_id` ASC, `workspaceId` ASC),
  INDEX `assetTypeId_fk_idx` (`asset_type_id` ASC),
  INDEX `id_idx` (`asset_type_id` ASC),
  INDEX `assetTypeStyle_workspaceId_fk_idx` (`workspaceId` ASC),
  CONSTRAINT `assetTypeId_fk`
    FOREIGN KEY (`asset_type_id`)
    REFERENCES `3rdi`.`asset_type` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `assetTypeStyle_workspaceId_fk`
    FOREIGN KEY (`workspaceId`)
    REFERENCES `3rdi`.`workspace` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

ALTER TABLE `3rdi`.`question` 
ADD COLUMN `parentQuesId` INT(11) NULL DEFAULT NULL AFTER `id`,
ADD COLUMN `questionMode` VARCHAR(20) NOT NULL AFTER `parentQuesId`,
ADD COLUMN `displayName` VARCHAR(256) NOT NULL AFTER `title`;

--Updating display name column with unique data for existing questions
update question set displayName=concat(SUBSTR(title, 1, 50),'-',createdDate);

ALTER TABLE `3rdi`.`question` 
ADD UNIQUE INDEX `ques_workspace_displayName_uq` (`workspaceId` ASC, `displayName` ASC),
ADD INDEX `ques_quesId_fk_idx` (`parentQuesId` ASC),
ADD CONSTRAINT `ques_quesId_fk`
  FOREIGN KEY (`parentQuesId`)
  REFERENCES `3rdi`.`question` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
