-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE `3rdi`.`interface` 
ADD COLUMN `dataType` VARCHAR(20) NULL DEFAULT NULL AFTER `interfaceDirection`,
ADD COLUMN `frequency` VARCHAR(20) NULL DEFAULT NULL AFTER `dataType`,
ADD COLUMN `communicationMethod` VARCHAR(20) NULL DEFAULT NULL AFTER `frequency`;

CREATE TABLE IF NOT EXISTS `3rdi`.`relationship_type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `parentDescriptor` VARCHAR(50) NOT NULL,
  `childDescriptor` VARCHAR(50) NOT NULL,
  `createdBy` INT(11) NOT NULL,
  `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` INT(11) NOT NULL,
  `updatedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `relationship_type_uq_parentDesc_childDesc` (`parentDescriptor` ASC, `childDescriptor` ASC),
  INDEX `relationship_type_createdBy_idx` (`createdBy` ASC),
  INDEX `relationship_type_updatedBy_idx` (`updatedBy` ASC),
  CONSTRAINT `relationship_type_createdBy`
    FOREIGN KEY (`createdBy`)
    REFERENCES `3rdi`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `relationship_type_updatedBy`
    FOREIGN KEY (`updatedBy`)
    REFERENCES `3rdi`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
