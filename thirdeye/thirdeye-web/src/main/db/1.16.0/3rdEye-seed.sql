/*************User role view permission */

REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('USER_ROLE_VIEW',3), ('USER_ROLE_VIEW',2), ('USER_ROLE_VIEW',4);


REPLACE INTO `role_permission` (`permissionname`, `roleid`) VALUES ('APP_LOGIN',2), ('APP_LOGIN',3),('APP_LOGIN',4);

/*************WORKSPACE ROLES */
REPLACE INTO `role_permission` (`permissionname`, `roleid`) VALUES ('WORKSPACES_MODIFY',4);
REPLACE INTO `role_permission` (`permissionname`, `roleid`) VALUES ('WORKSPACES_VIEW',4);

/*OPTIONAL*/
REPLACE INTO `role_permission` (`permissionname`, `roleid`) VALUES ('WORKSPACES_MODIFY',3);
REPLACE INTO `role_permission` (`permissionname`, `roleid`) VALUES ('WORKSPACES_VIEW',3);


/*******************TEMPLATE MANAGEMENT*******/
REPLACE INTO `role_permission` (`permissionname`, `roleid`) VALUES ('TEMPLATE_MODIFY',2), ('TEMPLATE_VIEW',2),('TEMPLATE_MODIFY',3), ('TEMPLATE_VIEW',3), ('TEMPLATE_MODIFY',4), ('TEMPLATE_VIEW',4);





/*******************FUNCTIONAL MAPS*********************/
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('BCM_VIEW',2), ('BCM_VIEW',3),('BCM_VIEW',4);



/*bcm modify*/
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('BCM_MODIFY',3);

/*functional map view and modify*/
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('FUNCTIONAL_MAP_VIEW',2),('FUNCTIONAL_MAP_VIEW',3),('FUNCTIONAL_MAP_VIEW',4);

REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('FUNCTIONAL_MAP_MODIFY',4),('FUNCTIONAL_MAP_MODIFY',3);

/*****************************parameter ********************/
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('PARAMETER_VIEW',4),('PARAMETER_VIEW',3);
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('PARAMETER_MODIFY',4),('PARAMETER_MODIFY',3);




/******************TCO MANAGEMENT******************/

REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('VIEW_TCO',2), ('VIEW_TCO',3), ('VIEW_TCO',4);

REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('MODIFY_TCO',3), ('MODIFY_TCO',4);


/********RELATIONSHIP*****************************/

REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('RELATIONSHIP_VIEW',2),('RELATIONSHIP_VIEW',3), ('RELATIONSHIP_VIEW',4);

REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('RELATIONSHIP_MODIFY',3), ('RELATIONSHIP_MODIFY',4);

/*******COLOR SCHEME**********/

REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('COLOR_SCHEME_VIEW',3), ('COLOR_SCHEME_VIEW',4);
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES  ('COLOR_SCHEME_MODIFY',3), ('COLOR_SCHEME_MODIFY',4);


/**********REPORTS*****************************/
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('REPORT_ASSET_GRAPH',3), ('REPORT_ASSET_GRAPH',4);

/*AID*/
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('AID_VIEW',2), ('AID_VIEW',3), ('AID_VIEW',4);

REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('AID_MODIFY',3), ('AID_MODIFY',4);

/*scatter*/
REPLACE INTO `role_permission` (`permissionname`,`roleid`) values ('REPORT_SCATTER_GRAPH',2),('REPORT_SCATTER_GRAPH',3),('REPORT_SCATTER_GRAPH',4);

/*php*/
REPLACE INTO `role_permission` (`permissionname`,`roleid`) values ('REPORT_PHP',2),('REPORT_PHP',3),('REPORT_PHP',4);

/*functional redundancy*/
REPLACE INTO `role_permission` (`permissionname`,`roleid`) values ('REPORT_FUNCTIONAL_REDUNDANCY',2),('REPORT_FUNCTIONAL_REDUNDANCY',3),('REPORT_FUNCTIONAL_REDUNDANCY',4);


/********************dashboard**********************/

REPLACE INTO `role_permission` (`permissionname`, `roleid`) values ('WORKSPACES_VIEW_DASHBOARD',2),('WORKSPACES_VIEW_DASHBOARD',3),('WORKSPACES_VIEW_DASHBOARD',4);

REPLACE INTO `role_permission` (`permissionname`, `roleid`) values ('WORKSPACES_MODIFY_DASHBOARD',2),('WORKSPACES_MODIFY_DASHBOARD',3),('WORKSPACES_MODIFY_DASHBOARD',4);

REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('REPORT_FACET_GRAPH',2), ('REPORT_FACET_GRAPH',3), ('REPORT_FACET_GRAPH',4);


/*****************TICKET MANAGEMENT*****************************/
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('SNOW_VIEW',2), ('SNOW_VIEW',3), ('SNOW_VIEW',4);



/*******ADMIN REPORTS**********************/
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('ADMIN_VIEW',3), ('ADMIN_VIEW',4);


/*************CREATE AND VIEW ASSETS************************/
REPLACE INTO `role_permission` (`permissionname`,`roleId`) VALUES ('ASSET_FILLDETAILS',2);
REPLACE INTO `role_permission` (`permissionname`,`roleId`) VALUES ('ASSET_FILLDETAILS',3);
REPLACE INTO `role_permission` (`permissionname`,`roleId`) VALUES ('ASSET_FILLDETAILS',4);

/**************QUESTIONNAIRE MANAGEMENT*******************************************/

/**create and modify questionnaire*/
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('QUESTIONNAIRE_MODIFY',3);
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('QUESTIONNAIRE_MODIFY',4);

/*view questionnaire*/
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('QUESTIONNAIRE_VIEW',2);
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('QUESTIONNAIRE_VIEW',3);
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('QUESTIONNAIRE_VIEW',4);



/****************BENCHMARK ****************************************/
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('BENCHMARK_VIEW',2);
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('BENCHMARK_VIEW',3);
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('BENCHMARK_VIEW',4);


REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('BENCHMARK_MODIFY',3);



/******************Question management******************************/
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('QUESTION_VIEW',2);
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('QUESTION_VIEW',3);
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('QUESTION_VIEW',4);


REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('QUESTION_MODIFY',3);
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('QUESTION_MODIFY',4);


REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('QUESTIONNAIRE_RESPOND',2);
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('QUESTIONNAIRE_RESPOND',3);
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('QUESTIONNAIRE_RESPOND',4);


REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('BOOST_EVALUATED_VALUE',3);
