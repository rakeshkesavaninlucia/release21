CREATE DATABASE  IF NOT EXISTS `3rdi` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `3rdi`;
-- MySQL dump 10.13  Distrib 5.6.23, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: 3rdi
-- ------------------------------------------------------
-- Server version	5.6.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `asset`
--

DROP TABLE IF EXISTS `asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateId` int(11) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `ASSET_TEMPLATE_FK_idx` (`templateId`),
  KEY `ASSET_CREATED_BY_FK_idx` (`createdBy`),
  KEY `ASSET_UPDATED_BY_FK_idx` (`updatedBy`),
  CONSTRAINT `ASSET_CREATED_BY_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `ASSET_TEMPLATE_FK` FOREIGN KEY (`templateId`) REFERENCES `asset_template` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `ASSET_UPDATED_BY_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset`
--

LOCK TABLES `asset` WRITE;
/*!40000 ALTER TABLE `asset` DISABLE KEYS */;
INSERT INTO `asset` VALUES (1,1,0,2,'2015-08-13 17:57:34',2,'2015-08-13 17:57:34'),(2,1,0,2,'2015-08-13 18:02:00',2,'2015-08-13 18:02:00'),(3,1,0,2,'2015-08-13 18:03:32',2,'2015-08-13 18:03:32'),(4,1,0,2,'2015-08-13 18:04:29',2,'2015-08-13 18:04:29'),(5,1,0,2,'2015-08-13 18:05:52',2,'2015-08-13 18:05:52'),(6,1,0,2,'2015-08-13 18:07:35',2,'2015-08-13 18:07:35'),(7,1,0,2,'2015-08-13 18:08:51',2,'2015-08-13 18:08:51'),(8,1,0,2,'2015-08-13 18:10:23',2,'2015-08-13 18:10:23'),(9,1,0,2,'2015-08-13 18:12:38',2,'2015-08-13 18:12:38'),(10,3,0,2,'2015-08-13 18:29:57',2,'2015-08-13 18:29:57'),(11,3,0,2,'2015-08-13 18:30:19',2,'2015-08-13 18:30:19'),(12,3,0,2,'2015-08-13 18:30:59',2,'2015-08-13 18:30:59'),(13,3,0,2,'2015-08-13 18:31:48',2,'2015-08-13 18:31:48'),(14,3,0,2,'2015-08-13 18:32:47',2,'2015-08-13 18:32:47'),(15,1,0,2,'2015-08-13 18:45:07',2,'2015-08-13 18:45:07'),(16,1,0,2,'2015-08-13 18:45:45',2,'2015-08-13 18:45:45');
/*!40000 ALTER TABLE `asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_data`
--

DROP TABLE IF EXISTS `asset_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` text,
  `assetTemplateColId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `assetTemplateColIdFk_idx` (`assetTemplateColId`),
  KEY `AssetData_Asset_FK_idx` (`assetId`),
  CONSTRAINT `AssetData_Asset_FK` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `assetTemplateColIdFk` FOREIGN KEY (`assetTemplateColId`) REFERENCES `asset_template_column` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_data`
--

LOCK TABLES `asset_data` WRITE;
/*!40000 ALTER TABLE `asset_data` DISABLE KEYS */;
INSERT INTO `asset_data` VALUES (1,'',3,1),(2,'2010-08-05',7,1),(3,'Timothy Morrison ',6,1),(4,'Ranjan Mitra',5,1),(5,'Handling after sales data processing',2,1),(6,'After sales',4,1),(7,'Automobile Managem System ',1,1),(8,'Michael Flanke',6,2),(9,'Middleware',4,2),(10,'2010-05-01',7,2),(11,'IA-AG-01',1,2),(12,'Vohn Gutenberg',5,2),(13,'Integration architecture system bus in Germany',2,2),(14,'',3,2),(15,'Bondial',1,3),(16,'Frank Ziedle',6,3),(17,'',3,3),(18,'',7,3),(19,'Automobile policy data warehouse',2,3),(20,'Warranty',4,3),(21,'Frank Ziedle',5,3),(22,'Gerhard Miller',6,4),(23,'',7,4),(24,'',3,4),(25,'Claims processing factory system',2,4),(26,'Warranty',4,4),(27,'Frankhoff Gief',5,4),(28,'FAGA2',1,4),(29,'T-Systems',5,5),(30,'',3,5),(31,'2010-08-01',7,5),(32,'PMD BLI',1,5),(33,'Automobile ordering system',2,5),(34,'Sales',4,5),(35,'Amanda Connors',6,5),(36,'Frank Ziedle',6,6),(37,'Factory',4,6),(38,'SpyderCat',1,6),(39,'T-Systems',5,6),(40,'',3,6),(41,'2010-01-20',7,6),(42,'Factory design system',2,6),(43,'Middleware',4,7),(44,'',3,7),(45,'UK-ESB-01',1,7),(46,'2013-07-07',7,7),(47,'Birlasoft',5,7),(48,'UK Middleware service bus',2,7),(49,'John Miekleham',6,7),(50,'2009-07-07',7,8),(51,'Common Business Partner',1,8),(52,'Phillis Tarry',6,8),(53,'Customer Relations',4,8),(54,'Customer data warehouse',2,8),(55,'',3,8),(56,'Birlasoft',5,8),(57,'T-Systems',5,9),(58,'Roadside Support',4,9),(59,'',3,9),(60,'On The Go',1,9),(61,'Johnathon R',6,9),(62,'Mobility support for customers',2,9),(63,'2015-02-20',7,9),(64,'',13,10),(65,'Realitime',14,10),(66,'CBP Synchronization',12,10),(67,'Automobile Information',12,11),(68,'Realtime',14,11),(69,'',13,11),(70,'Realtime',14,12),(71,'Automobile Owner Service',12,12),(72,'Provides ownership details based on VIN',13,12),(73,'Automobile Information Service UK',12,13),(74,'',13,13),(75,'Realtime',14,13),(76,'PUK32-FAGA2',12,14),(77,'Batch',14,14),(78,'',13,14),(79,'T-Systems',5,15),(80,'Middleware',4,15),(81,'2010-04-01',7,15),(82,'',2,15),(83,'SFTP Germany',1,15),(84,'',3,15),(85,'Gerhard Miller',6,15),(86,'',3,16),(87,'Birlasoft',5,16),(88,'2010-02-04',7,16),(89,'',2,16),(90,'James Brown',6,16),(91,'SFTP UK',1,16),(92,'Middleware',4,16);
/*!40000 ALTER TABLE `asset_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_template`
--

DROP TABLE IF EXISTS `asset_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetTemplateName` varchar(45) NOT NULL,
  `assetTypeId` int(11) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `description` text,
  `workspaceId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTemplateName_createdBy_UNIQUE` (`assetTemplateName`,`createdBy`,`assetTypeId`),
  UNIQUE KEY `UK_a35ol4p1y14cnqk9j9yqcoj13` (`assetTemplateName`,`createdBy`,`assetTypeId`),
  KEY `id_idx` (`assetTypeId`),
  KEY `updatedByFk2_idx` (`updatedBy`),
  KEY `createdBy_UNIQUE` (`createdBy`),
  KEY `FK_ASSETTEMPLATE_WORKSPACEID_idx` (`workspaceId`),
  CONSTRAINT `FK_ASSETTEMPLATE_WORKSPACEID` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `assetTypeIdFk` FOREIGN KEY (`assetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `createdByFk2` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `updatedByFk2` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_template`
--

LOCK TABLES `asset_template` WRITE;
/*!40000 ALTER TABLE `asset_template` DISABLE KEYS */;
INSERT INTO `asset_template` VALUES (1,'Application Inventory',1,0,'Inventory template for application portfolio',1,2,'2015-08-13 10:52:32',2,'2015-08-13 10:52:32'),(2,'Server Inventory',2,0,'',1,2,'2015-08-13 17:49:12',2,'2015-08-13 17:49:12'),(3,'Integration Services Repository',5,0,'',1,2,'2015-08-13 18:26:36',2,'2015-08-13 18:26:36'),(4,'Demo for DK',6,0,'',1,2,'2015-08-14 04:12:52',2,'2015-08-14 04:12:52'),(5,'template test',1,0,'et',1,2,'2015-08-26 11:46:33',2,'2015-08-26 11:46:33'),(6,'sdaffsdf',4,0,'sdfdffdf',1,2,'2015-08-26 11:47:04',2,'2015-08-26 11:47:04'),(7,'dcfsddsf',1,0,'dffdfgfdfg',1,2,'2015-08-26 11:53:46',2,'2015-08-26 11:53:46'),(8,'template 21',5,0,'test',1,2,'2015-08-26 12:07:57',2,'2015-08-26 12:07:57'),(9,'template test',5,0,'',1,2,'2015-08-26 12:09:27',2,'2015-08-26 12:09:27'),(10,'Test template in WS 123',1,0,'here we go!',4,2,'2015-08-30 09:31:41',2,'2015-08-30 09:31:41'),(11,'One template 123',1,0,'',4,2,'2015-08-30 09:32:25',2,'2015-08-30 09:32:25'),(12,'template in ws 123',2,0,'',4,2,'2015-08-30 09:33:43',2,'2015-08-30 09:33:43'),(13,'Template for application',1,0,'',1,2,'2015-08-31 11:16:05',2,'2015-08-31 11:16:05'),(14,'GG\'s template',3,0,'',4,2,'2015-08-31 11:50:03',2,'2015-08-31 11:50:03');
/*!40000 ALTER TABLE `asset_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_template_column`
--

DROP TABLE IF EXISTS `asset_template_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_template_column` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetTemplateColName` varchar(45) NOT NULL,
  `dataType` varchar(45) NOT NULL,
  `length` int(11) NOT NULL,
  `assetTemplateId` int(11) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `sequenceNumber` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTemplateColName_assetTemplateId_UNIQUE` (`assetTemplateColName`,`assetTemplateId`),
  UNIQUE KEY `UK_i7n89jntp5xq390elbs233689` (`assetTemplateColName`,`assetTemplateId`),
  KEY `createdByFk3_idx` (`createdBy`),
  KEY `updatedByFk3_idx` (`updatedBy`),
  KEY `assettemplateIdFk_idx` (`assetTemplateId`),
  CONSTRAINT `assetTemplateIdFk` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `createdByFk3` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `updatedByFk3` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_template_column`
--

LOCK TABLES `asset_template_column` WRITE;
/*!40000 ALTER TABLE `asset_template_column` DISABLE KEYS */;
INSERT INTO `asset_template_column` VALUES (1,'Application Name','TEXT',25,1,0,1,1,2,'2015-08-13 10:52:48',2,'2015-08-13 10:52:48'),(2,'Description','TEXT',50,1,0,0,2,2,'2015-08-13 10:53:06',2,'2015-08-13 10:53:06'),(3,'Category 1','TEXT',25,1,0,0,3,2,'2015-08-13 10:53:19',2,'2015-08-13 10:53:56'),(4,'Service Area / Function / Department','TEXT',25,1,0,1,4,2,'2015-08-13 10:53:51',2,'2015-08-13 10:53:51'),(5,'Application Lead','TEXT',50,1,0,1,5,2,'2015-08-13 10:54:15',2,'2015-08-13 10:54:15'),(6,'IT Manager','TEXT',50,1,0,1,6,2,'2015-08-13 10:55:12',2,'2015-08-13 10:55:12'),(7,'Date of Commissioning','DATE',1,1,0,0,7,2,'2015-08-13 10:55:34',2,'2015-08-13 10:55:34'),(8,'Server Name','TEXT',50,2,0,1,1,2,'2015-08-13 17:49:38',2,'2015-08-13 17:49:38'),(9,'FQDN','TEXT',50,2,0,1,2,2,'2015-08-13 17:49:58',2,'2015-08-13 17:49:58'),(10,'Operating System','TEXT',100,2,0,1,3,2,'2015-08-13 17:50:47',2,'2015-08-13 17:50:47'),(11,'Purpose','TEXT',200,2,0,0,4,2,'2015-08-13 17:51:12',2,'2015-08-13 17:51:12'),(12,'Service Name','TEXT',50,3,0,1,1,2,'2015-08-13 18:27:03',2,'2015-08-13 18:27:03'),(13,'Service Description','TEXT',100,3,0,0,2,2,'2015-08-13 18:27:37',2,'2015-08-13 18:27:37'),(14,'Realtime / Batch','TEXT',10,3,0,1,3,2,'2015-08-13 18:28:13',2,'2015-08-13 18:28:13'),(15,'Server Type','TEXT',15,2,0,1,5,2,'2015-08-13 18:29:25',2,'2015-08-13 18:29:25'),(16,'Service name','TEXT',50,4,0,1,1,2,'2015-08-14 04:13:06',2,'2015-08-14 04:13:06'),(17,'column 1','TEXT',50,10,0,1,1,2,'2015-08-30 09:39:16',2,'2015-08-30 09:39:16'),(18,'column 2','TEXT',25,10,0,0,2,2,'2015-08-30 09:39:31',2,'2015-08-30 09:39:31'),(19,'c1','TEXT',4,6,0,1,1,2,'2015-08-30 09:42:34',2,'2015-08-30 09:42:34'),(20,'x2','NUMBER',33,6,0,1,2,2,'2015-08-30 09:42:44',2,'2015-08-30 09:42:44'),(21,'db name','TEXT',25,14,0,1,1,2,'2015-08-31 11:50:17',2,'2015-08-31 11:50:17'),(22,'db platform','TEXT',25,14,0,0,2,2,'2015-08-31 11:50:28',2,'2015-08-31 11:50:28');
/*!40000 ALTER TABLE `asset_template_column` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_type`
--

DROP TABLE IF EXISTS `asset_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetTypeName` varchar(45) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTypeName_UNIQUE` (`assetTypeName`),
  UNIQUE KEY `UK_q1gprhj5b79ufwicjvkvef4fw` (`assetTypeName`),
  KEY `createdByFk1_idx` (`createdBy`),
  KEY `updatedByFk1_idx` (`updatedBy`),
  CONSTRAINT `createdByFk1` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `updatedByFk1` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_type`
--

LOCK TABLES `asset_type` WRITE;
/*!40000 ALTER TABLE `asset_type` DISABLE KEYS */;
INSERT INTO `asset_type` VALUES (1,'Application',0,1,'2015-08-13 10:51:31',1,'2015-08-13 10:51:31'),(2,'Server',0,1,'2015-08-13 10:51:31',1,'2015-08-13 10:51:31'),(3,'Database',0,1,'2015-08-13 10:51:31',1,'2015-08-13 10:51:31'),(4,'Person',0,1,'2015-08-13 10:51:31',1,'2015-08-13 10:51:31'),(5,'Business Services',0,1,'2015-08-13 10:51:31',1,'2015-08-13 10:51:31'),(6,'IT Services',0,1,'2015-08-13 10:51:31',1,'2015-08-13 10:51:31'),(7,'Interfaces',0,1,'2015-08-13 10:51:31',1,'2015-08-13 10:51:31');
/*!40000 ALTER TABLE `asset_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `category_createdBy_fk_idx` (`createdBy`),
  KEY `category_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `category_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `category_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (5,'Functional Assessment',NULL,1,'2015-08-13 10:36:33',1,'2015-08-13 10:36:33'),(6,'Application Evolution',NULL,1,'2015-08-13 10:36:33',1,'2015-08-13 10:36:33'),(7,'Technology Assessment',NULL,1,'2015-08-13 10:36:33',1,'2015-08-13 10:36:33'),(8,'Application Management',NULL,1,'2015-08-13 10:36:33',1,'2015-08-13 10:36:33');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graph`
--

DROP TABLE IF EXISTS `graph`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graph` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `graphName` varchar(45) NOT NULL,
  `description` text,
  `workspaceId` int(11) NOT NULL,
  `graphType` varchar(45) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_GRAPHNAME_WORKSPACE` (`graphName`,`workspaceId`,`graphType`),
  KEY `Graph_Workspace_FK_idx` (`workspaceId`),
  KEY `Graph_User_FK_idx` (`createdBy`),
  KEY `Graph_Updated_By_FK_idx` (`updatedBy`),
  CONSTRAINT `Graph_Created_By_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Graph_Updated_By_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Graph_Workspace_FK` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graph`
--

LOCK TABLES `graph` WRITE;
/*!40000 ALTER TABLE `graph` DISABLE KEYS */;
INSERT INTO `graph` VALUES (29,'Application Network view','Relation of the applications in the portfolio',1,'NW',0,2,'2015-08-13 18:13:49',2,'2015-09-02 04:12:30'),(30,'test graph','',4,'PC',0,2,'2015-09-01 05:11:47',2,'2015-09-01 05:11:47'),(31,'test graph demo for caterpille ws','',1,'PC',0,2,'2015-09-01 05:12:39',2,'2015-09-01 05:12:39'),(32,'test graph for desc text','',1,'PC',0,2,'2015-09-01 07:05:21',2,'2015-09-01 07:05:21'),(33,'test graph for desc text','',4,'PC',0,2,'2015-09-01 07:05:49',2,'2015-09-01 07:05:49'),(34,'test graph for desc text1','ere',4,'PC',0,2,'2015-09-01 07:06:16',2,'2015-09-01 07:06:16');
/*!40000 ALTER TABLE `graph` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graph_asset`
--

DROP TABLE IF EXISTS `graph_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graph_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `graphId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_GRAFH_ASSET` (`graphId`,`assetId`),
  KEY `Graph_Asset_Graph_FK_idx` (`graphId`),
  KEY `Graph_Asset_Asset_FK_idx` (`assetId`),
  CONSTRAINT `Graph_Asset_Asset_FK` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Graph_Asset_Graph_FK` FOREIGN KEY (`graphId`) REFERENCES `graph` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graph_asset`
--

LOCK TABLES `graph_asset` WRITE;
/*!40000 ALTER TABLE `graph_asset` DISABLE KEYS */;
INSERT INTO `graph_asset` VALUES (42,29,1),(24,29,2),(23,29,3),(22,29,4),(21,29,5),(19,29,7),(41,29,8),(40,29,9),(39,29,10),(38,29,11),(37,29,12),(36,29,13),(35,29,14),(34,29,15),(33,29,16);
/*!40000 ALTER TABLE `graph_asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interface`
--

DROP TABLE IF EXISTS `interface`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interface` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interfaceName` varchar(45) DEFAULT NULL,
  `assetSourceId` int(11) NOT NULL,
  `assetDestId` int(11) NOT NULL,
  `graphId` int(11) NOT NULL,
  `interfaceType` varchar(45) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_SRC_DEST_GRAPH_TYPE` (`assetSourceId`,`assetDestId`,`graphId`,`interfaceType`),
  KEY `Interface_Asset_FK_idx` (`assetSourceId`),
  KEY `Interface_Asset_Dest_FK_idx` (`assetDestId`),
  KEY `Interface_Created_By_FK_idx` (`createdBy`),
  KEY `Interface_Updated_By_FK_idx` (`updatedBy`),
  KEY `Interface_Graph_FK_idx` (`graphId`),
  CONSTRAINT `Interface_Asset_Dest_FK` FOREIGN KEY (`assetDestId`) REFERENCES `asset` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Asset_Source_FK` FOREIGN KEY (`assetSourceId`) REFERENCES `asset` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Created_By_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Graph_FK` FOREIGN KEY (`graphId`) REFERENCES `graph` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Updated_By_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interface`
--

LOCK TABLES `interface` WRITE;
/*!40000 ALTER TABLE `interface` DISABLE KEYS */;
INSERT INTO `interface` VALUES (11,'',3,2,29,'NW',2,'2015-08-13 18:14:19',2,'2015-08-25 11:18:03'),(13,'ghghgh',1,12,29,'NW',2,'2015-08-13 18:15:38',2,'2015-09-02 05:42:05'),(14,'',7,2,29,'NW',2,'2015-08-13 18:16:32',2,'2015-08-13 18:16:32'),(15,'',8,2,29,'NW',2,'2015-08-13 18:22:25',2,'2015-08-13 18:22:25'),(16,'',6,5,29,'NW',2,'2015-08-13 18:24:21',2,'2015-08-13 18:24:21'),(17,'',5,1,29,'NW',2,'2015-08-13 18:24:43',2,'2015-08-13 18:24:43'),(18,'',11,1,29,'NW',2,'2015-08-13 18:34:10',2,'2015-08-13 18:34:10'),(19,'',11,4,29,'NW',2,'2015-08-13 18:34:32',2,'2015-09-01 12:18:08'),(20,'',7,13,29,'NW',2,'2015-08-13 18:37:28',2,'2015-08-13 18:37:28'),(21,'',13,9,29,'NW',2,'2015-08-13 18:37:38',2,'2015-08-13 18:37:38'),(22,'ghghg',14,14,29,'NW',2,'2015-08-13 18:46:04',2,'2015-09-02 05:42:14'),(23,'',14,15,29,'NW',2,'2015-08-13 18:47:23',2,'2015-08-13 18:47:23'),(24,'',5,14,29,'NW',2,'2015-08-13 18:47:37',2,'2015-08-13 18:47:37'),(25,'',15,16,29,'NW',2,'2015-08-13 18:47:45',2,'2015-08-13 18:47:45'),(26,'',16,2,29,'NW',2,'2015-08-13 18:47:56',2,'2015-08-26 07:18:28'),(27,'',1,15,29,'NW',2,'2015-08-13 18:48:25',2,'2015-08-13 18:48:25'),(28,'',11,2,29,'NW',2,'2015-09-01 12:18:11',2,'2015-09-01 12:18:34');
/*!40000 ALTER TABLE `interface` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter`
--

DROP TABLE IF EXISTS `parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text,
  `type` varchar(4) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `questionnaireId` int(11) DEFAULT NULL,
  `templateFlag` tinyint(4) NOT NULL DEFAULT '0',
  `parentTemplateId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`,`questionnaireId`),
  KEY `parameter_workspaceId_fk_idx` (`workspaceId`),
  KEY `parameter_parentTemplateId_fk_idx` (`parentTemplateId`),
  KEY `parameter_createdBy_fk_idx` (`createdBy`),
  KEY `parameter_updatedBy_fk_idx` (`updatedBy`),
  KEY `parameter_questionnairId_fk_idx` (`questionnaireId`),
  CONSTRAINT `parameter_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_parentTemplateId_fk` FOREIGN KEY (`parentTemplateId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_questionnairId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter`
--

LOCK TABLES `parameter` WRITE;
/*!40000 ALTER TABLE `parameter` DISABLE KEYS */;
INSERT INTO `parameter` VALUES (80,'Functional Quotient','','LP',1,23,0,NULL,2,'2015-08-14 03:39:19',2,'2015-08-14 03:39:19'),(81,'Technical Quotient','','LP',1,23,0,NULL,2,'2015-08-14 03:39:32',2,'2015-08-14 03:39:32'),(82,'Management Quotient','','LP',1,23,0,NULL,2,'2015-08-14 03:39:54',2,'2015-08-14 03:39:54'),(83,'FQ 1','','LP',1,24,0,NULL,2,'2015-08-14 04:20:22',2,'2015-08-14 04:20:22'),(84,'TQ1','','AP',1,24,0,NULL,2,'2015-08-14 04:20:29',2,'2015-08-14 04:20:29'),(85,'FQ1.1','','LP',1,24,0,NULL,2,'2015-08-14 04:21:03',2,'2015-08-14 04:21:03'),(86,'P1','','LP',1,25,0,NULL,2,'2015-08-20 11:30:09',2,'2015-08-20 11:30:09'),(87,'Fq 1','','LP',1,27,0,NULL,2,'2015-08-31 11:24:19',2,'2015-08-31 11:24:19'),(88,'FQ2','','LP',1,27,0,NULL,2,'2015-08-31 11:24:24',2,'2015-08-31 11:24:24');
/*!40000 ALTER TABLE `parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter_function`
--

DROP TABLE IF EXISTS `parameter_function`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter_function` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentParameterId` int(11) NOT NULL,
  `weight` decimal(10,3) NOT NULL,
  `constant` int(11) NOT NULL,
  `questionId` int(11) DEFAULT NULL,
  `childparameterId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parameter_function_parentParameterId_fk_idx` (`parentParameterId`),
  KEY `parameter_function_childParameterId_fk_idx` (`childparameterId`),
  KEY `parameter_function_questionId_fk_idx` (`questionId`),
  CONSTRAINT `parameter_function_childParameterId_fk` FOREIGN KEY (`childparameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_function_parentParameterId_fk` FOREIGN KEY (`parentParameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_function_questionId_fk` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter_function`
--

LOCK TABLES `parameter_function` WRITE;
/*!40000 ALTER TABLE `parameter_function` DISABLE KEYS */;
INSERT INTO `parameter_function` VALUES (77,80,1.500,1,86,NULL),(78,80,1.000,1,88,NULL),(79,80,1.000,1,90,NULL),(80,81,1.000,1,94,NULL),(81,81,1.300,1,95,NULL),(82,81,0.900,1,96,NULL),(83,82,1.000,1,106,NULL),(84,82,1.000,1,109,NULL),(85,82,1.000,1,110,NULL),(86,84,1.000,1,NULL,85),(87,85,1.000,1,87,NULL),(88,85,1.000,1,88,NULL),(89,85,1.000,1,89,NULL),(90,80,1.000,1,102,NULL),(91,80,1.000,1,104,NULL),(92,80,1.000,1,93,NULL),(93,87,0.000,1,86,NULL),(94,87,1.000,1,87,NULL),(95,87,1.000,1,88,NULL),(96,80,1.000,1,130,NULL),(97,80,1.000,1,131,NULL),(98,80,1.000,1,132,NULL),(99,80,1.000,1,125,NULL),(100,80,1.000,1,126,NULL),(101,80,1.000,1,127,NULL),(102,80,1.000,1,129,NULL);
/*!40000 ALTER TABLE `parameter_function` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `helpText` text,
  `workspaceId` int(11) DEFAULT NULL,
  `questionType` varchar(10) NOT NULL,
  `queTypeText` text,
  `level` smallint(6) NOT NULL,
  `questionCategoryId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `QUESTIONNAIRE_WORKSPACE_ID_FK_idx` (`workspaceId`),
  KEY `QUESTIONNAIRE_CREATED_BY_FK_idx` (`createdBy`),
  KEY `QUESTIONNAIRE_UPDATED_BY_FK_idx` (`updatedBy`),
  KEY `question_questioncategoryid_categoryid_idx` (`questionCategoryId`),
  CONSTRAINT `QUESTIONNAIRE_CREATED_BY_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `QUESTIONNAIRE_UPDATED_BY_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `QUESTIONNAIRE_WORKSPACE_ID_FK` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `question_questioncategoryid_categoryid` FOREIGN KEY (`questionCategoryId`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (86,'Please list the number of functions or business processes supported by the application? ','',1,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"0 - Only Supports peripherals\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"1-2\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"3-5\"},{\"sequenceNumber\":4,\"quantifier\":7.0,\"text\":\"6-10\"}]}',1,5,2,'2015-09-01 06:44:55',2,'2015-09-01 06:44:55'),(87,'What is the type of application?','',1,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"In house developed application\"},{\"sequenceNumber\":2,\"quantifier\":2.0,\"text\":\"Packaged application\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Third party developed application\"},{\"sequenceNumber\":4,\"quantifier\":1.5,\"text\":\"Others\"}]}',1,5,2,'2015-09-01 06:44:55',2,'2015-09-01 06:44:55'),(88,'What is the actual functional coverage vis-Ã -vis the planned coverage? ','Please state whether any more business functions/ processes were intended to be covered by this application as part of its implementation scope?',1,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":5.0,\"text\":\"All envisaged functionality covered\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Peripheral functionality implemented\"}]}',1,5,2,'2015-09-01 06:44:55',2,'2015-09-01 06:44:55'),(89,'Are the business functions/ processes supported by this application also supported any other application implemented in this company?  ','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Yes\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"No\"}]}',1,5,2,'2015-08-13 13:40:17',2,'2015-08-13 13:40:17'),(90,'Are there any functionality redundancies / overlap of functionality with other applications? ','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Significant overlap in functionality\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Minor functionality overlap\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"No functionality overlap\"}],\"otherOption\":true}',1,5,2,'2015-09-03 09:42:11',2,'2015-09-03 09:42:11'),(91,'How would you rate the ability of the application to support ad-hoc reporting requirements?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":5.0,\"text\":\"The application is flexible and is easy to obtain ad-hoc reports\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"The application is not very flexible and support from IT is required for ad-hoc reporting\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Code level changes are required to support ad-hoc reporting requirements\"}]}',1,5,2,'2015-08-13 13:44:57',2,'2015-08-13 13:44:57'),(92,'How would you rate the ability of the application to meet the overall current needs of users?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"New application required\"},{\"sequenceNumber\":2,\"quantifier\":2.0,\"text\":\"Major enhancement required\"},{\"sequenceNumber\":3,\"quantifier\":3.0,\"text\":\"Significant modifications required\"},{\"sequenceNumber\":4,\"quantifier\":4.0,\"text\":\"Minor Modifications required\"},{\"sequenceNumber\":5,\"quantifier\":5.0,\"text\":\"Acceptable as it is\"}]}',1,5,2,'2015-08-13 13:48:16',2,'2015-08-13 13:48:16'),(93,'What are the names of the modules / sub systems within the application?','',1,'PARATEXT',NULL,1,7,2,'2015-08-13 14:19:41',2,'2015-09-01 11:57:00'),(94,'What is the level of integration with other applications? ','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"},{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}]}',1,7,2,'2015-08-13 14:20:47',2,'2015-08-13 14:20:47'),(95,'Does the application import or export data to other applications? ','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"},{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}]}',1,7,2,'2015-08-13 14:21:50',2,'2015-08-13 14:21:50'),(96,'Is the application centrally deployed or is it distributed?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Centrally\"},{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"Distributed\"},{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"Others\"}]}',1,7,2,'2015-08-13 14:22:50',2,'2015-08-13 14:22:50'),(97,'If distributed how is data synchronized across multiple locations?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":5.0,\"text\":\"Real time synchronization\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Day end batch synchronization\"},{\"sequenceNumber\":3,\"quantifier\":-3.0,\"text\":\"No data synchronization\"},{\"sequenceNumber\":4,\"quantifier\":0.0,\"text\":\"Others\"}]}',1,7,2,'2015-08-13 14:24:37',2,'2015-08-13 14:24:37'),(98,'What are the response times of the application during the day / week? ','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":5.0,\"text\":\"< 1 sec\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"2 - 4\"},{\"sequenceNumber\":3,\"quantifier\":0.0,\"text\":\">5 sec\"}]}',1,7,2,'2015-08-13 14:28:03',2,'2015-08-13 14:28:03'),(99,'What is the usage of system resources by the application?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"The application is very resource intensive and significant changes are required to make the application more efficient\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"The application is resource intensive but minor changes are required to make the application more efficient\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"The application uses resources optimally and does not load other IT resources\"}]}',1,7,2,'2015-08-13 14:30:13',2,'2015-08-13 14:30:13'),(100,'Has the application design considered good practices like following open architectures and standards etc.? Please provide a list of good practices followed.','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"},{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}]}',1,7,2,'2015-08-13 14:32:47',2,'2015-08-13 14:32:47'),(101,'How easily can the application be ported from the current environment to a new environment?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"The application would need to be designed and developed once again\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Major modifications would be required to port the application to another platform\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Minor modifications would be required to port the application\"}]}',1,7,2,'2015-08-13 14:34:12',2,'2015-08-13 14:34:12'),(102,'What are the modelling tools used for designing the system?','',NULL,'CHECKBOXES','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Rational Rose\"},{\"sequenceNumber\":2,\"quantifier\":2.0,\"text\":\"Erwin\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Designer 2000\"},{\"sequenceNumber\":4,\"quantifier\":3.0,\"text\":\"System Architect\"},{\"sequenceNumber\":5,\"quantifier\":1.5,\"text\":\"Others\"}]}',1,7,2,'2015-08-13 14:36:24',2,'2015-08-13 14:36:24'),(103,'Please state the level of vendor support for the tools deployed?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":4.0,\"text\":\"On site 24X7 support\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"On site only during office hours\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"On call\"},{\"sequenceNumber\":4,\"quantifier\":2.0,\"text\":\"Off shore monitoring & onsite remedial\"},{\"sequenceNumber\":5,\"quantifier\":1.0,\"text\":\"No support\"}]}',1,7,2,'2015-08-13 14:38:26',2,'2015-08-13 14:38:26'),(104,'What is the type of production support? ','',NULL,'CHECKBOXES','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Helpdesk\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Hands-on support to some of the production activities like generating MIS reports\"},{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"batch execution\"},{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"System maintenance support to maintain application\"}]}',1,7,2,'2015-08-13 14:41:17',2,'2015-08-13 14:41:17'),(105,'Does the application have IPR issues that may be a hindrance for outsourcing?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Yes\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"No\"}]}',1,8,2,'2015-08-13 14:46:40',2,'2015-08-13 14:48:21'),(106,'Does the application have any licensing issues that could be a hindrance in migrating offshore? If yes, please provide details.','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Yes\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"No\"}]}',1,8,2,'2015-08-13 14:53:17',2,'2015-08-13 14:53:17'),(107,'For the change control  process also ascertain whether the: ','',NULL,'NUMBER','{\"min\":null,\"max\":null,\"limitTo\":true}',1,8,2,'2015-09-09 09:21:35',2,'2015-09-09 09:21:35'),(108,'Are change request registers maintained and updated regularly? ','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":5.0,\"text\":\"Change request registers are maintained and updated with all change requests\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Change request registers are maintained but only some change requests are logged\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Change request registers are not maintained\"}]}',3,8,2,'2015-08-13 14:59:18',2,'2015-08-13 14:59:18'),(109,'What % of application budget is allocated for third party service providers?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"< 5%\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"5 - 15%\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"15 - 30%\"},{\"sequenceNumber\":4,\"quantifier\":6.0,\"text\":\"> 30%\"}]}',3,8,2,'2015-08-13 15:01:46',2,'2015-08-13 15:01:46'),(110,'Are documentation standards clearly defined?  Are these standards diligently followed while making enhancements/ changes?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":5.0,\"text\":\"Documentation standards defined and followed\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Documentation standards defined but not followed\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Documentation standards not defined\"}]}',3,8,2,'2015-08-13 15:03:56',2,'2015-08-13 15:03:56'),(111,'what is your next week looking like','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Option 1\"},{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Option 2\"},{\"sequenceNumber\":3,\"quantifier\":6.0,\"text\":\"Option 3\"},{\"sequenceNumber\":4,\"quantifier\":8.0,\"text\":\"Option 4\"}],\"otherOption\":true}',1,NULL,2,'2015-08-14 04:18:41',2,'2015-09-04 09:42:51'),(112,'this is the question for DC ws','',1,'TEXT',NULL,1,5,2,'2015-09-01 09:28:57',2,'2015-09-01 09:30:39'),(113,'this is the question for 123 ws','',4,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":4.0,\"text\":\"454\"},{\"sequenceNumber\":2,\"quantifier\":4.0,\"text\":\"4654\"}]}',2,5,2,'2015-09-02 12:58:06',2,'2015-09-02 12:58:06'),(114,'this is the question for 123 ws 1','',4,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":4.0,\"text\":\"454\"},{\"sequenceNumber\":2,\"quantifier\":4.0,\"text\":\"4654\"}],\"otherOption\":true}',2,6,2,'2015-09-02 12:57:19',2,'2015-09-04 09:43:08'),(115,'test','',4,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"123\"},{\"sequenceNumber\":2,\"quantifier\":2.0,\"text\":\"465\"}],\"otherOption\":true}',4,6,2,'2015-09-02 13:30:22',2,'2015-09-02 13:30:22'),(116,'test1','',4,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":7.0,\"text\":\"6544\"},{\"sequenceNumber\":2,\"quantifier\":45.0,\"text\":\"465\"}],\"otherOption\":true}',3,NULL,2,'2015-09-02 13:30:22',2,'2015-09-04 09:42:47'),(117,'test2 multichoice','',4,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":4.0,\"text\":\"4565\"}],\"otherOption\":true}',3,NULL,2,'2015-09-02 13:30:22',2,'2015-09-04 09:42:56'),(118,'test 3','hello',4,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":23.0,\"text\":\"465\"}],\"otherOption\":true}',4,6,2,'2015-09-02 13:28:47',2,'2015-09-02 13:28:47'),(119,'test','',4,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":7.0,\"text\":\"4654\"}],\"otherOption\":false}',2,5,2,'2015-09-02 13:29:25',2,'2015-09-02 13:29:25'),(120,'test','',4,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":7.0,\"text\":\"4654\"}],\"otherOption\":false}',2,5,2,'2015-09-02 13:29:31',2,'2015-09-02 13:29:31'),(121,'test 2 for checkboxes','',4,'CHECKBOXES','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"123\"},{\"sequenceNumber\":2,\"quantifier\":2.0,\"text\":\"1213\"}]}',2,5,2,'2015-09-02 13:35:19',2,'2015-09-02 13:35:19'),(122,'test 3 for checkboxes','',4,'CHECKBOXES','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":4.0,\"text\":\"123\"},{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"45\"}]}',2,NULL,2,'2015-09-02 13:36:58',2,'2015-09-04 09:43:02'),(123,'test 4 for j','',4,'CHECKBOXES','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"123\"},{\"sequenceNumber\":2,\"quantifier\":2.0,\"text\":\"23\"}]}',2,NULL,2,'2015-09-02 13:38:20',2,'2015-09-02 13:38:21'),(124,'test ws123 6','',1,'NUMBER','{\"min\":4,\"max\":2,\"limitTo\":true}',2,NULL,2,'2015-09-09 09:21:35',2,'2015-09-09 09:21:35'),(125,'this is 6','',1,'NUMBER','{\"min\":4,\"max\":10,\"limitTo\":false}',3,NULL,2,'2015-09-09 09:21:35',2,'2015-09-09 09:21:35'),(126,'this 123 l','',1,'NUMBER','{\"min\":null,\"max\":null,\"limitTo\":false}',2,NULL,2,'2015-09-09 09:21:35',2,'2015-09-09 09:21:35'),(127,'this is 12344','',1,'NUMBER','{\"min\":4,\"max\":10,\"limitTo\":true}',2,NULL,2,'2015-09-09 09:21:35',2,'2015-09-09 09:21:35'),(128,'test 188899','',1,'NUMBER','{\"min\":null,\"max\":null,\"limitTo\":true}',2,5,2,'2015-09-09 09:21:35',2,'2015-09-09 09:21:35'),(129,'test 1214345456','',1,'NUMBER','{\"min\":4,\"max\":null, \"limitTo\":true}',3,NULL,2,'2015-09-09 09:21:35',2,'2015-09-09 09:21:35'),(130,'test789y9','9',1,'NUMBER','{\"min\":null,\"max\":10,\"limitTo\":true}',4,7,2,'2015-09-09 09:21:35',2,'2015-09-09 09:21:35'),(131,'Date test','',1,'DATE',NULL,2,8,2,'2015-09-03 12:46:40',2,'2015-09-04 07:12:16'),(132,'test 128','',1,'TEXT',NULL,2,7,2,'2015-09-03 12:46:40',2,'2015-09-03 12:46:40'),(133,'test para','',NULL,'PARATEXT',NULL,2,5,2,'2015-09-03 05:55:01',2,'2015-09-03 05:55:01'),(134,'test independent multi choice','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"123\"},{\"sequenceNumber\":2,\"quantifier\":2.0,\"text\":\"1234\"},{\"sequenceNumber\":3,\"quantifier\":12.0,\"text\":\"3\"}],\"otherOption\":true}',2,NULL,2,'2015-09-03 05:56:17',2,'2015-09-03 05:56:17'),(135,'test checkboxes','',4,'CHECKBOXES','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":4.0,\"text\":\"123\"},{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"45\"}]}',2,NULL,2,'2015-09-03 05:57:07',2,'2015-09-03 05:57:07'),(136,'wewew','This is Test',1,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"123\"},{\"sequenceNumber\":2,\"quantifier\":2.0,\"text\":\"1234\"},{\"sequenceNumber\":3,\"quantifier\":12.0,\"text\":\"3\"}],\"otherOption\":true}',3,7,2,'2015-09-04 12:41:43',2,'2015-09-04 12:41:43'),(137,'Test 1','',1,'NUMBER','{\"min\":-3,\"max\":15,\"limitTo\":true}',2,5,2,'2015-09-09 09:21:35',2,'2015-09-09 09:21:35'),(138,'test date','',NULL,'DATE',NULL,2,NULL,2,'2015-09-09 05:56:44',2,'2015-09-09 05:56:44'),(139,'test multi choice','',1,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"123\"},{\"sequenceNumber\":2,\"quantifier\":2.0,\"text\":\"1234\"}],\"otherOption\":true}',2,NULL,2,'2015-09-09 05:57:57',2,'2015-09-09 05:57:57');
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire`
--

DROP TABLE IF EXISTS `questionnaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text,
  `workspaceId` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `questionnaire_name_workspace_unique` (`name`,`workspaceId`),
  KEY `questionnaire_workspaceId_fk_idx` (`workspaceId`),
  KEY `questionnaire_createdBy_fk_idx` (`createdBy`),
  KEY `questionnaire_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `questionnaire_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire`
--

LOCK TABLES `questionnaire` WRITE;
/*!40000 ALTER TABLE `questionnaire` DISABLE KEYS */;
INSERT INTO `questionnaire` VALUES (23,'IT Portfolio Survey - 1','',1,1,2,'2015-08-14 03:38:18',2,'2015-09-04 12:25:29'),(24,'Questionnaire for Aditya\'s first day1 :)','',1,1,2,'2015-08-14 04:19:55',2,'2015-08-19 11:46:12'),(25,'This is for test.','',1,0,2,'2015-08-19 11:46:38',2,'2015-08-24 06:12:57'),(26,'First for 123','testing',4,1,2,'2015-09-01 06:30:47',2,'2015-09-02 10:29:16'),(27,'Demo for ajit','',1,0,2,'2015-08-31 11:23:54',2,'2015-08-31 11:23:54'),(28,'Test questionnaire','test',4,0,2,'2015-09-01 12:26:52',2,'2015-09-01 12:27:17'),(29,'Test questionniare in demo for caterpillar','test',1,0,2,'2015-09-02 03:50:59',2,'2015-09-02 10:30:26'),(30,'Test questionniare in demo for caterpillar','test',4,0,2,'2015-09-02 03:51:21',2,'2015-09-02 03:51:21');
/*!40000 ALTER TABLE `questionnaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire_asset`
--

DROP TABLE IF EXISTS `questionnaire_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `questionnaire_asset_questionnaireAssetId_uq` (`questionnaireId`,`assetId`),
  KEY `questionnaire_asset_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `questionnaire_asset_assetId_fk_idx` (`assetId`),
  CONSTRAINT `questionnaire_asset_assetId_fk` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_asset_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire_asset`
--

LOCK TABLES `questionnaire_asset` WRITE;
/*!40000 ALTER TABLE `questionnaire_asset` DISABLE KEYS */;
INSERT INTO `questionnaire_asset` VALUES (199,23,1),(200,23,2),(201,23,3),(202,23,4),(203,23,5),(204,23,6),(205,23,7),(206,23,8),(207,23,9),(194,23,10),(195,23,11),(196,23,12),(197,23,13),(198,23,14),(208,23,15),(209,23,16),(64,24,2),(65,24,3),(66,24,4),(67,24,5),(88,25,1),(89,25,2),(87,25,14),(122,27,10),(123,27,11),(124,27,12),(125,27,13),(126,27,14),(127,29,1),(128,29,2),(129,29,3);
/*!40000 ALTER TABLE `questionnaire_asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire_question`
--

DROP TABLE IF EXISTS `questionnaire_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) DEFAULT NULL,
  `questionId` int(11) NOT NULL,
  `questionnaireAssetId` int(11) DEFAULT NULL,
  `parameterId` int(11) NOT NULL,
  `sequenceNumber` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `questionnaire_question_questionnaireId_questionId_parameterId` (`questionnaireId`,`parameterId`,`questionId`,`questionnaireAssetId`),
  KEY `questionnaire_question_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `questionnaire_question_questionId_fk_idx` (`questionId`),
  KEY `questionnaire_question_assetId_fk_idx` (`questionnaireAssetId`),
  KEY `questionnaire_question_parameterId_fk_idx` (`parameterId`),
  CONSTRAINT `questionnaire_question_parameterId_fk` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_question_questionId_fk` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_question_questionnaireAssetId_fk` FOREIGN KEY (`questionnaireAssetId`) REFERENCES `questionnaire_asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_question_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2664 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire_question`
--

LOCK TABLES `questionnaire_question` WRITE;
/*!40000 ALTER TABLE `questionnaire_question` DISABLE KEYS */;
INSERT INTO `questionnaire_question` VALUES (2360,23,132,204,80,1),(2361,23,127,204,80,2),(2362,23,104,204,80,3),(2363,23,131,204,80,4),(2364,23,129,204,80,5),(2365,23,86,204,80,6),(2366,23,90,204,80,7),(2367,23,102,204,80,8),(2368,23,125,204,80,9),(2369,23,93,204,80,10),(2370,23,130,204,80,11),(2371,23,88,204,80,12),(2372,23,126,204,80,13),(2373,23,95,204,81,14),(2374,23,96,204,81,15),(2375,23,94,204,81,16),(2376,23,106,204,82,17),(2377,23,109,204,82,18),(2378,23,110,204,82,19),(2379,23,132,205,80,20),(2380,23,127,205,80,21),(2381,23,104,205,80,22),(2382,23,131,205,80,23),(2383,23,129,205,80,24),(2384,23,86,205,80,25),(2385,23,90,205,80,26),(2386,23,102,205,80,27),(2387,23,125,205,80,28),(2388,23,93,205,80,29),(2389,23,130,205,80,30),(2390,23,88,205,80,31),(2391,23,126,205,80,32),(2392,23,95,205,81,33),(2393,23,96,205,81,34),(2394,23,94,205,81,35),(2395,23,106,205,82,36),(2396,23,109,205,82,37),(2397,23,110,205,82,38),(2398,23,132,207,80,39),(2399,23,127,207,80,40),(2400,23,104,207,80,41),(2401,23,131,207,80,42),(2402,23,129,207,80,43),(2403,23,86,207,80,44),(2404,23,90,207,80,45),(2405,23,102,207,80,46),(2406,23,125,207,80,47),(2407,23,93,207,80,48),(2408,23,130,207,80,49),(2409,23,88,207,80,50),(2410,23,126,207,80,51),(2411,23,95,207,81,52),(2412,23,96,207,81,53),(2413,23,94,207,81,54),(2414,23,106,207,82,55),(2415,23,109,207,82,56),(2416,23,110,207,82,57),(2417,23,132,199,80,58),(2418,23,127,199,80,59),(2419,23,104,199,80,60),(2420,23,131,199,80,61),(2421,23,129,199,80,62),(2422,23,86,199,80,63),(2423,23,90,199,80,64),(2424,23,102,199,80,65),(2425,23,125,199,80,66),(2426,23,93,199,80,67),(2427,23,130,199,80,68),(2428,23,88,199,80,69),(2429,23,126,199,80,70),(2430,23,95,199,81,71),(2431,23,96,199,81,72),(2432,23,94,199,81,73),(2433,23,106,199,82,74),(2434,23,109,199,82,75),(2435,23,110,199,82,76),(2436,23,132,203,80,77),(2437,23,127,203,80,78),(2438,23,104,203,80,79),(2439,23,131,203,80,80),(2440,23,129,203,80,81),(2441,23,86,203,80,82),(2442,23,90,203,80,83),(2443,23,102,203,80,84),(2444,23,125,203,80,85),(2445,23,93,203,80,86),(2446,23,130,203,80,87),(2447,23,88,203,80,88),(2448,23,126,203,80,89),(2449,23,95,203,81,90),(2450,23,96,203,81,91),(2451,23,94,203,81,92),(2452,23,106,203,82,93),(2453,23,109,203,82,94),(2454,23,110,203,82,95),(2455,23,132,208,80,96),(2456,23,127,208,80,97),(2457,23,104,208,80,98),(2458,23,131,208,80,99),(2459,23,129,208,80,100),(2460,23,86,208,80,101),(2461,23,90,208,80,102),(2462,23,102,208,80,103),(2463,23,125,208,80,104),(2464,23,93,208,80,105),(2465,23,130,208,80,106),(2466,23,88,208,80,107),(2467,23,126,208,80,108),(2468,23,95,208,81,109),(2469,23,96,208,81,110),(2470,23,94,208,81,111),(2471,23,106,208,82,112),(2472,23,109,208,82,113),(2473,23,110,208,82,114),(2474,23,132,194,80,115),(2475,23,127,194,80,116),(2476,23,104,194,80,117),(2477,23,131,194,80,118),(2478,23,129,194,80,119),(2479,23,86,194,80,120),(2480,23,90,194,80,121),(2481,23,102,194,80,122),(2482,23,125,194,80,123),(2483,23,93,194,80,124),(2484,23,130,194,80,125),(2485,23,88,194,80,126),(2486,23,126,194,80,127),(2487,23,95,194,81,128),(2488,23,96,194,81,129),(2489,23,94,194,81,130),(2490,23,106,194,82,131),(2491,23,109,194,82,132),(2492,23,110,194,82,133),(2493,23,132,209,80,134),(2494,23,127,209,80,135),(2495,23,104,209,80,136),(2496,23,131,209,80,137),(2497,23,129,209,80,138),(2498,23,86,209,80,139),(2499,23,90,209,80,140),(2500,23,102,209,80,141),(2501,23,125,209,80,142),(2502,23,93,209,80,143),(2503,23,130,209,80,144),(2504,23,88,209,80,145),(2505,23,126,209,80,146),(2506,23,95,209,81,147),(2507,23,96,209,81,148),(2508,23,94,209,81,149),(2509,23,106,209,82,150),(2510,23,109,209,82,151),(2511,23,110,209,82,152),(2512,23,132,195,80,153),(2513,23,127,195,80,154),(2514,23,104,195,80,155),(2515,23,131,195,80,156),(2516,23,129,195,80,157),(2517,23,86,195,80,158),(2518,23,90,195,80,159),(2519,23,102,195,80,160),(2520,23,125,195,80,161),(2521,23,93,195,80,162),(2522,23,130,195,80,163),(2523,23,88,195,80,164),(2524,23,126,195,80,165),(2525,23,95,195,81,166),(2526,23,96,195,81,167),(2527,23,94,195,81,168),(2528,23,106,195,82,169),(2529,23,109,195,82,170),(2530,23,110,195,82,171),(2531,23,132,198,80,172),(2532,23,127,198,80,173),(2533,23,104,198,80,174),(2534,23,131,198,80,175),(2535,23,129,198,80,176),(2536,23,86,198,80,177),(2537,23,90,198,80,178),(2538,23,102,198,80,179),(2539,23,125,198,80,180),(2540,23,93,198,80,181),(2541,23,130,198,80,182),(2542,23,88,198,80,183),(2543,23,126,198,80,184),(2544,23,95,198,81,185),(2545,23,96,198,81,186),(2546,23,94,198,81,187),(2547,23,106,198,82,188),(2548,23,109,198,82,189),(2549,23,110,198,82,190),(2550,23,132,196,80,191),(2551,23,127,196,80,192),(2552,23,104,196,80,193),(2553,23,131,196,80,194),(2554,23,129,196,80,195),(2555,23,86,196,80,196),(2556,23,90,196,80,197),(2557,23,102,196,80,198),(2558,23,125,196,80,199),(2559,23,93,196,80,200),(2560,23,130,196,80,201),(2561,23,88,196,80,202),(2562,23,126,196,80,203),(2563,23,95,196,81,204),(2564,23,96,196,81,205),(2565,23,94,196,81,206),(2566,23,106,196,82,207),(2567,23,109,196,82,208),(2568,23,110,196,82,209),(2569,23,132,197,80,210),(2570,23,127,197,80,211),(2571,23,104,197,80,212),(2572,23,131,197,80,213),(2573,23,129,197,80,214),(2574,23,86,197,80,215),(2575,23,90,197,80,216),(2576,23,102,197,80,217),(2577,23,125,197,80,218),(2578,23,93,197,80,219),(2579,23,130,197,80,220),(2580,23,88,197,80,221),(2581,23,126,197,80,222),(2582,23,95,197,81,223),(2583,23,96,197,81,224),(2584,23,94,197,81,225),(2585,23,106,197,82,226),(2586,23,109,197,82,227),(2587,23,110,197,82,228),(2588,23,132,200,80,229),(2589,23,127,200,80,230),(2590,23,104,200,80,231),(2591,23,131,200,80,232),(2592,23,129,200,80,233),(2593,23,86,200,80,234),(2594,23,90,200,80,235),(2595,23,102,200,80,236),(2596,23,125,200,80,237),(2597,23,93,200,80,238),(2598,23,130,200,80,239),(2599,23,88,200,80,240),(2600,23,126,200,80,241),(2601,23,95,200,81,242),(2602,23,96,200,81,243),(2603,23,94,200,81,244),(2604,23,106,200,82,245),(2605,23,109,200,82,246),(2606,23,110,200,82,247),(2607,23,132,201,80,248),(2608,23,127,201,80,249),(2609,23,104,201,80,250),(2610,23,131,201,80,251),(2611,23,129,201,80,252),(2612,23,86,201,80,253),(2613,23,90,201,80,254),(2614,23,102,201,80,255),(2615,23,125,201,80,256),(2616,23,93,201,80,257),(2617,23,130,201,80,258),(2618,23,88,201,80,259),(2619,23,126,201,80,260),(2620,23,95,201,81,261),(2621,23,96,201,81,262),(2622,23,94,201,81,263),(2623,23,106,201,82,264),(2624,23,109,201,82,265),(2625,23,110,201,82,266),(2626,23,132,202,80,267),(2627,23,127,202,80,268),(2628,23,104,202,80,269),(2629,23,131,202,80,270),(2630,23,129,202,80,271),(2631,23,86,202,80,272),(2632,23,90,202,80,273),(2633,23,102,202,80,274),(2634,23,125,202,80,275),(2635,23,93,202,80,276),(2636,23,130,202,80,277),(2637,23,88,202,80,278),(2638,23,126,202,80,279),(2639,23,95,202,81,280),(2640,23,96,202,81,281),(2641,23,94,202,81,282),(2642,23,106,202,82,283),(2643,23,109,202,82,284),(2644,23,110,202,82,285),(2645,23,132,206,80,286),(2646,23,127,206,80,287),(2647,23,104,206,80,288),(2648,23,131,206,80,289),(2649,23,129,206,80,290),(2650,23,86,206,80,291),(2651,23,90,206,80,292),(2652,23,102,206,80,293),(2653,23,125,206,80,294),(2654,23,93,206,80,295),(2655,23,130,206,80,296),(2656,23,88,206,80,297),(2657,23,126,206,80,298),(2658,23,95,206,81,299),(2659,23,96,206,81,300),(2660,23,94,206,81,301),(2661,23,106,206,82,302),(2662,23,109,206,82,303),(2663,23,110,206,82,304);
/*!40000 ALTER TABLE `questionnaire_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `response`
--

DROP TABLE IF EXISTS `response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `response` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `response_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `response_createdBy_fk_idx` (`createdBy`),
  KEY `response_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `response_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `response_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `response_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `response`
--

LOCK TABLES `response` WRITE;
/*!40000 ALTER TABLE `response` DISABLE KEYS */;
INSERT INTO `response` VALUES (4,23,2,'2015-08-14 03:43:11',2,'2015-09-14 07:27:12'),(5,25,2,'2015-08-24 06:13:14',2,'2015-08-24 06:13:14');
/*!40000 ALTER TABLE `response` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `response_data`
--

DROP TABLE IF EXISTS `response_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `response_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireQuestionId` int(11) NOT NULL,
  `responseId` int(11) NOT NULL,
  `responseData` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UQ_response_data_questionnaireQuestionId_responseId` (`questionnaireQuestionId`,`responseId`),
  KEY `response_data_questionnaireId_fk_idx` (`questionnaireQuestionId`),
  KEY `response_data_responseId_fk_idx` (`responseId`),
  CONSTRAINT `response_data_questionnaireQuestionId_fk` FOREIGN KEY (`questionnaireQuestionId`) REFERENCES `questionnaire_question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `response_data_responseId_fk` FOREIGN KEY (`responseId`) REFERENCES `response` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=462 DEFAULT CHARSET=latin1 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `response_data`
--

LOCK TABLES `response_data` WRITE;
/*!40000 ALTER TABLE `response_data` DISABLE KEYS */;
INSERT INTO `response_data` VALUES (245,2360,4,'{\"responseText\":\"text \",\"score\":-1.0}'),(246,2361,4,'{\"quantifier\":10.0,\"responseNumber\":\"10\"}'),(247,2362,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Helpdesk\"},{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"batch execution\"}]}'),(248,2363,4,'{\"responseDate\":\"2015-09-17\",\"score\":-1.0}'),(249,2364,4,'{\"quantifier\":6.0,\"responseNumber\":\"6\"}'),(250,2365,4,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":7.0,\"text\":\"6-10\"}],\"otherOptionResponseText\":null,\"score\":null}'),(251,2366,4,'{\"options\":[],\"otherOptionResponseText\":\"hello text area\",\"score\":12.0}'),(252,2367,4,'{\"options\":[{\"sequenceNumber\":5,\"quantifier\":1.5,\"text\":\"Others\"}]}'),(253,2368,4,'{\"quantifier\":10.0,\"responseNumber\":\"10\"}'),(254,2369,4,'{\"responseParaText\":\"text aarea\",\"score\":4.0}'),(255,2370,4,'{\"quantifier\":41.0,\"responseNumber\":\"41\"}'),(256,2373,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(257,2374,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"Distributed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(258,2377,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"15 - 30%\"}],\"otherOptionResponseText\":null,\"score\":null}'),(259,2378,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Documentation standards defined but not followed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(260,2379,4,'{\"responseText\":\"test hello\",\"score\":5.0}'),(261,2380,4,'{\"quantifier\":45.0,\"responseNumber\":\"45\"}'),(262,2383,4,'{\"quantifier\":5.0,\"responseNumber\":\"5\"}'),(263,2385,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Minor functionality overlap\"}],\"otherOptionResponseText\":null,\"score\":null}'),(264,2404,4,'{\"options\":[],\"otherOptionResponseText\":\"text area 3\",\"score\":45.0}'),(265,2423,4,'{\"options\":[],\"otherOptionResponseText\":\"hi other text area 4 gh\",\"score\":null}'),(266,2382,4,'{\"responseDate\":\"2015-09-17\",\"score\":null}'),(267,2384,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"1-2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(268,2386,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Designer 2000\"},{\"sequenceNumber\":4,\"quantifier\":3.0,\"text\":\"System Architect\"}]}'),(269,2387,4,'{\"quantifier\":45.0,\"responseNumber\":\"45\"}'),(270,2388,4,'{\"responseParaText\":\"hello\",\"score\":458.0}'),(271,2389,4,'{\"quantifier\":234.0,\"responseNumber\":\"234\"}'),(272,2390,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(273,2391,4,'{\"quantifier\":45.0,\"responseNumber\":\"45\"}'),(274,2392,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(275,2393,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"Distributed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(276,2394,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(277,2371,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(278,2372,4,'{\"quantifier\":4445.0,\"responseNumber\":4445.0}'),(279,2375,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(280,2376,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(281,2381,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Hands-on support to some of the production activities like generating MIS reports\"},{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"batch execution\"}]}'),(282,2395,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(283,2396,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"15 - 30%\"}],\"otherOptionResponseText\":null,\"score\":null}'),(284,2397,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Documentation standards defined but not followed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(285,2398,4,'{\"responseText\":\"ttttt\",\"score\":null}'),(286,2399,4,'{\"quantifier\":6.0,\"responseNumber\":6.0}'),(287,2400,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Hands-on support to some of the production activities like generating MIS reports\"},{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"System maintenance support to maintain application\"}]}'),(288,2401,4,'{\"responseDate\":\"2015-09-16\",\"score\":null}'),(289,2402,4,'{\"quantifier\":5.0,\"responseNumber\":5.0}'),(290,2403,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"1-2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(291,2405,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Designer 2000\"},{\"sequenceNumber\":4,\"quantifier\":3.0,\"text\":\"System Architect\"}]}'),(292,2406,4,'{\"quantifier\":6.0,\"responseNumber\":6.0}'),(293,2407,4,'{\"responseParaText\":\"hello\",\"score\":null}'),(294,2408,4,'{\"quantifier\":44.0,\"responseNumber\":44.0}'),(295,2409,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(296,2410,4,'{\"quantifier\":9.0,\"responseNumber\":9.0}'),(297,2411,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(298,2412,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"Distributed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(299,2413,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(300,2414,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(301,2415,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"15 - 30%\"}],\"otherOptionResponseText\":null,\"score\":null}'),(302,2416,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Documentation standards defined but not followed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(303,2417,4,'{\"responseText\":\"yt\",\"score\":null}'),(304,2418,4,'{\"quantifier\":4.0,\"responseNumber\":4.0}'),(305,2419,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"batch execution\"}]}'),(306,2420,4,'{\"responseDate\":\"2015-09-22\",\"score\":null}'),(307,2421,4,'{\"quantifier\":8.0,\"responseNumber\":8.0}'),(308,2422,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"3-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(309,2424,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Designer 2000\"},{\"sequenceNumber\":4,\"quantifier\":3.0,\"text\":\"System Architect\"}]}'),(310,2425,4,'{\"quantifier\":5.0,\"responseNumber\":5.0}'),(311,2426,4,'{\"responseParaText\":\"uu\",\"score\":null}'),(312,2427,4,'{\"quantifier\":5.0,\"responseNumber\":5.0}'),(313,2428,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(314,2429,4,'{\"quantifier\":47.0,\"responseNumber\":47.0}'),(315,2430,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(316,2431,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Centrally\"}],\"otherOptionResponseText\":null,\"score\":null}'),(317,2432,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(318,2433,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(319,2434,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"5 - 15%\"}],\"otherOptionResponseText\":null,\"score\":null}'),(320,2435,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Documentation standards defined but not followed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(321,2436,4,'{\"responseText\":\"dd\",\"score\":null}'),(322,2437,4,'{\"quantifier\":4.0,\"responseNumber\":4.0}'),(323,2438,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Hands-on support to some of the production activities like generating MIS reports\"},{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"batch execution\"}]}'),(324,2439,4,'{\"responseDate\":\"2015-09-17\",\"score\":null}'),(325,2440,4,'{\"quantifier\":4.0,\"responseNumber\":4.0}'),(326,2441,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"3-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(327,2442,4,'{\"options\":[],\"otherOptionResponseText\":\"other test for PMD BLI\",\"score\":null}'),(328,2443,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":2.0,\"text\":\"Erwin\"},{\"sequenceNumber\":4,\"quantifier\":3.0,\"text\":\"System Architect\"}]}'),(329,2444,4,'{\"quantifier\":4.0,\"responseNumber\":4.0}'),(330,2445,4,'{\"responseParaText\":\"er\",\"score\":null}'),(331,2446,4,'{\"quantifier\":5.0,\"responseNumber\":5.0}'),(332,2447,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(333,2448,4,'{\"quantifier\":4.0,\"responseNumber\":4.0}'),(334,2449,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(335,2450,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"Others\"}],\"otherOptionResponseText\":null,\"score\":null}'),(336,2451,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(337,2452,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(338,2453,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"15 - 30%\"}],\"otherOptionResponseText\":null,\"score\":null}'),(339,2454,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Documentation standards defined but not followed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(340,2455,4,'{\"responseText\":\"trt\",\"score\":null}'),(341,2456,4,'{\"quantifier\":9.0,\"responseNumber\":9.0}'),(342,2457,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Hands-on support to some of the production activities like generating MIS reports\"},{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"batch execution\"}]}'),(343,2458,4,'{\"responseDate\":\"2015-09-17\",\"score\":null}'),(344,2459,4,'{\"quantifier\":6.0,\"responseNumber\":6.0}'),(345,2460,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"3-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(346,2461,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Minor functionality overlap\"}],\"otherOptionResponseText\":null,\"score\":null}'),(347,2462,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Designer 2000\"},{\"sequenceNumber\":4,\"quantifier\":3.0,\"text\":\"System Architect\"}]}'),(348,2466,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(349,2468,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(350,2469,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"Others\"}],\"otherOptionResponseText\":null,\"score\":null}'),(351,2470,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(352,2471,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(353,2472,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"15 - 30%\"}],\"otherOptionResponseText\":null,\"score\":null}'),(354,2473,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Documentation standards defined but not followed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(355,2476,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Hands-on support to some of the production activities like generating MIS reports\"},{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"batch execution\"}]}'),(356,2479,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"3-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(357,2480,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Minor functionality overlap\"}],\"otherOptionResponseText\":null,\"score\":null}'),(358,2481,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Designer 2000\"},{\"sequenceNumber\":4,\"quantifier\":3.0,\"text\":\"System Architect\"}]}'),(359,2485,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(360,2487,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(361,2488,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"Distributed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(362,2489,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(363,2490,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(364,2491,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"15 - 30%\"}],\"otherOptionResponseText\":null,\"score\":null}'),(365,2492,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Documentation standards defined but not followed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(366,2495,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Hands-on support to some of the production activities like generating MIS reports\"},{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"System maintenance support to maintain application\"}]}'),(367,2498,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"3-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(368,2499,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"No functionality overlap\"}],\"otherOptionResponseText\":null,\"score\":null}'),(369,2500,4,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":3.0,\"text\":\"System Architect\"},{\"sequenceNumber\":5,\"quantifier\":1.5,\"text\":\"Others\"}]}'),(370,2504,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(371,2506,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(372,2507,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"Distributed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(373,2508,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(374,2510,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"5 - 15%\"}],\"otherOptionResponseText\":null,\"score\":null}'),(375,2509,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(376,2511,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Documentation standards defined but not followed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(377,2514,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Helpdesk\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Hands-on support to some of the production activities like generating MIS reports\"}]}'),(378,2517,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"1-2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(379,2518,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Minor functionality overlap\"}],\"otherOptionResponseText\":null,\"score\":null}'),(380,2519,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":2.0,\"text\":\"Erwin\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Designer 2000\"}]}'),(381,2523,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(382,2525,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(383,2526,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"Distributed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(384,2528,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(385,2529,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"15 - 30%\"}],\"otherOptionResponseText\":null,\"score\":null}'),(386,2530,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Documentation standards defined but not followed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(387,2533,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Helpdesk\"},{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"batch execution\"}]}'),(388,2536,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"3-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(389,2537,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"No functionality overlap\"}],\"otherOptionResponseText\":null,\"score\":null}'),(390,2538,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Designer 2000\"},{\"sequenceNumber\":5,\"quantifier\":1.5,\"text\":\"Others\"}]}'),(391,2542,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(392,2544,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(393,2545,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"Distributed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(394,2546,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(395,2547,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(396,2548,4,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":6.0,\"text\":\"> 30%\"}],\"otherOptionResponseText\":null,\"score\":null}'),(397,2549,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Documentation standards not defined\"}],\"otherOptionResponseText\":null,\"score\":null}'),(398,2552,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Hands-on support to some of the production activities like generating MIS reports\"}]}'),(399,2555,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"3-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(400,2556,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"No functionality overlap\"}],\"otherOptionResponseText\":null,\"score\":null}'),(401,2557,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Designer 2000\"}]}'),(402,2561,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":5.0,\"text\":\"All envisaged functionality covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(403,2563,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(404,2564,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"Others\"}],\"otherOptionResponseText\":null,\"score\":null}'),(405,2565,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(406,2566,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(407,2567,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"15 - 30%\"}],\"otherOptionResponseText\":null,\"score\":null}'),(408,2568,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Documentation standards defined but not followed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(409,2571,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Hands-on support to some of the production activities like generating MIS reports\"},{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"batch execution\"}]}'),(410,2574,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"1-2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(411,2575,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Minor functionality overlap\"}],\"otherOptionResponseText\":null,\"score\":null}'),(412,2576,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Designer 2000\"},{\"sequenceNumber\":4,\"quantifier\":3.0,\"text\":\"System Architect\"}]}'),(413,2580,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(414,2582,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(415,2584,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(416,2585,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(417,2586,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"5 - 15%\"}],\"otherOptionResponseText\":null,\"score\":null}'),(418,2590,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Hands-on support to some of the production activities like generating MIS reports\"},{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"batch execution\"}]}'),(419,2593,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"3-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(420,2594,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"No functionality overlap\"}],\"otherOptionResponseText\":null,\"score\":null}'),(421,2595,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":2.0,\"text\":\"Erwin\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Designer 2000\"}]}'),(422,2599,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(423,2601,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(424,2602,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"Distributed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(425,2604,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(426,2603,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(427,2605,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"5 - 15%\"}],\"otherOptionResponseText\":null,\"score\":null}'),(428,2606,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Documentation standards defined but not followed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(429,2609,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Hands-on support to some of the production activities like generating MIS reports\"},{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"batch execution\"}]}'),(430,2612,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"1-2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(431,2613,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"No functionality overlap\"}],\"otherOptionResponseText\":null,\"score\":null}'),(432,2614,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Designer 2000\"},{\"sequenceNumber\":4,\"quantifier\":3.0,\"text\":\"System Architect\"}]}'),(433,2618,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(434,2620,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(435,2621,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"Distributed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(436,2623,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(437,2622,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(438,2624,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"15 - 30%\"}],\"otherOptionResponseText\":null,\"score\":null}'),(439,2625,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Documentation standards defined but not followed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(440,2628,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Hands-on support to some of the production activities like generating MIS reports\"}]}'),(441,2631,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"1-2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(442,2632,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Minor functionality overlap\"}],\"otherOptionResponseText\":null,\"score\":null}'),(443,2633,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Designer 2000\"},{\"sequenceNumber\":4,\"quantifier\":3.0,\"text\":\"System Architect\"}]}'),(444,2637,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(445,2639,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(446,2641,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(447,2640,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"Others\"}],\"otherOptionResponseText\":null,\"score\":null}'),(448,2642,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(449,2643,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"5 - 15%\"}],\"otherOptionResponseText\":null,\"score\":null}'),(450,2644,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Documentation standards defined but not followed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(451,2647,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Helpdesk\"}]}'),(452,2650,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"1-2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(453,2651,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Minor functionality overlap\"}],\"otherOptionResponseText\":null,\"score\":null}'),(454,2652,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Designer 2000\"}]}'),(455,2656,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(456,2658,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(457,2659,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"Distributed\"}],\"otherOptionResponseText\":null,\"score\":null}'),(458,2660,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(459,2661,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(460,2662,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"5 - 15%\"}],\"otherOptionResponseText\":null,\"score\":null}'),(461,2663,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Documentation standards defined but not followed\"}],\"otherOptionResponseText\":null,\"score\":null}');
/*!40000 ALTER TABLE `response_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(45) NOT NULL,
  `roleDescription` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (11,'ROLE_APP_USER',NULL),(12,'ROLE_APP_ADMIN',NULL),(13,'WORKSPACE_ADMIN',NULL),(14,'CONSULTANT',NULL),(15,'CUSTOMER',NULL);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_permission`
--

DROP TABLE IF EXISTS `role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_permission` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `permissionname` varchar(45) NOT NULL,
  `roleid` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_ROLE_PERMISSION` (`permissionname`,`roleid`),
  KEY `FK_ROLEPERM_ROLEID_idx` (`roleid`),
  CONSTRAINT `FK_ROLEPERM_ROLEID` FOREIGN KEY (`roleid`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_permission`
--

LOCK TABLES `role_permission` WRITE;
/*!40000 ALTER TABLE `role_permission` DISABLE KEYS */;
INSERT INTO `role_permission` VALUES (12,'APP_LOGIN',11),(34,'APP_LOGIN',12),(42,'ASSET_FILLDETAILS',12),(33,'QUESTIONNAIRE_MODIFY',12),(48,'QUESTIONNAIRE_RESPOND',12),(35,'QUESTIONNAIRE_VIEW',12),(40,'QUESTION_MODIFY',12),(41,'QUESTION_VIEW',12),(39,'REPORT_ASSET_GRAPH',12),(47,'REPORT_FACET_GRAPH',12),(43,'REPORT_SCATTER_GRAPH',12),(49,'TEMPLATE_MODIFY',12),(50,'TEMPLATE_VIEW',12),(36,'USER_MODIFY',12),(11,'USER_ROLE_MODIFY',11),(38,'USER_ROLE_MODIFY',12),(37,'USER_ROLE_VIEW',12),(46,'USER_VIEW',12),(44,'WORKSPACES_MODIFY',12),(45,'WORKSPACES_VIEW',12);
/*!40000 ALTER TABLE `role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `emailAddress` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `accountExpired` tinyint(1) NOT NULL DEFAULT '0',
  `accountLocked` tinyint(1) NOT NULL DEFAULT '0',
  `credentialExpired` tinyint(1) NOT NULL DEFAULT '0',
  `accountEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `emailAddress_UNIQUE` (`emailAddress`),
  UNIQUE KEY `UK_t40ack6c3x1y4m2nhaju018jg` (`emailAddress`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'System','System','thirdeye-system@birlasoft.com','b!rl@s0fT',0,1,1,0,0,-1,'2015-08-13 10:29:36',-1,'2015-08-13 10:29:36'),(2,'Dharmender','Kapoor','dharmender.k@birlasoft.com','welcome12#',0,0,0,0,0,1,'2015-08-13 10:45:54',3,'2015-08-26 09:35:27'),(3,'Samar','Gupta','samar.gupta@birlasoft.com','welcome12#',0,0,0,0,0,2,'2015-08-26 07:58:00',2,'2015-08-26 07:58:06');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_role_unique` (`userId`,`roleId`),
  KEY `roleIdFk_idx` (`roleId`),
  KEY `userIdFk_idx` (`userId`),
  CONSTRAINT `roleIdFk` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `userIdFk` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (9,2,11),(10,2,12),(6,3,11),(7,3,12);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_workspace`
--

DROP TABLE IF EXISTS `user_workspace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_workspace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_USER_WORKSPACE` (`userId`,`workspaceId`),
  KEY `FK_WORKSPACEID_WORKSPACEID_JOIN_idx` (`workspaceId`),
  CONSTRAINT `FK_USERID_USERID_JOIN` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_WORKSPACEID_WORKSPACEID_JOIN` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_workspace`
--

LOCK TABLES `user_workspace` WRITE;
/*!40000 ALTER TABLE `user_workspace` DISABLE KEYS */;
INSERT INTO `user_workspace` VALUES (1,2,1),(2,2,4);
/*!40000 ALTER TABLE `user_workspace` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workspace`
--

DROP TABLE IF EXISTS `workspace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workspace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workspaceName` varchar(45) NOT NULL,
  `workspaceDescription` text,
  `deletedStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_WORKSPACE_USER_idx` (`createdBy`),
  KEY `FK_WORKSPACE_USER_UPDATE_idx` (`updatedBy`),
  CONSTRAINT `FK_WORKSPACE_USER_CREATE` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_WORKSPACE_USER_UPDATE` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workspace`
--

LOCK TABLES `workspace` WRITE;
/*!40000 ALTER TABLE `workspace` DISABLE KEYS */;
INSERT INTO `workspace` VALUES (1,'Demo for Caterpillar','Workspace for Caterpillar',0,2,'2015-08-13 10:49:14',2,'2015-08-13 10:49:14'),(2,'Another workspace for testing','',0,2,'2015-08-28 11:12:16',2,'2015-08-28 11:12:16'),(3,'Another workspace for 2','',0,2,'2015-08-28 11:15:37',2,'2015-08-28 11:15:37'),(4,'123','',0,2,'2015-08-28 11:19:42',2,'2015-08-28 11:19:42');
/*!40000 ALTER TABLE `workspace` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-14 13:03:15
