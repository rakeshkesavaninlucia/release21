<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
 <style th:fragment="onPageStyles">
  </style>
<title>Fragments for BCM</title>
</head>
<body>
	<div th:fragment="createNewBCMTemplate">
		<div class="modal fade" id="createNewBCMTemplateModal" tabindex="-1" role="dialog" aria-labelledby="createNewBCMTemplateModal">
			<div class="modal-dialog" role="document" >
				    <div class="modal-content">
						<form th:action="@{/bcm/create/save}" method="post" th:object="${bcmTemplateForm}"  id="createNewBCMTemplateForm" onsubmit="submitButton.disabled = true; return true;">
								<input type="hidden" th:field="*{id}"/>
								
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" >Create BCM Template</h4>
						      </div>
						      <div class="modal-body">
						          <div class="form-group">
						            <label for="bcmName" class="control-label">Name:</label>
						            <input type="text" class="form-control" th:field="*{bcmName}" />
			            			<span th:if="${#fields.hasErrors('bcmName')}" th:errors="*{bcmName}" style="color: red;">Test</span>
						          </div>
						      </div>
						      <div class="modal-footer">
						        <input type="submit" th:value="*{id != null }? 'Edit BCM Template' : 'Create BCM Template'" class="btn btn-primary" name="submitButton"></input>
						      </div>
					    </form>
				    </div>
			 </div>
		 </div>
	</div>
	
	<div th:fragment="createWorkspaceBCM">
		<div class="modal fade" id="createWorkspaceBCMModal" tabindex="-1" role="dialog" aria-labelledby="createWorkspaceBCMModal">
			<div class="modal-dialog" role="document" >
				    <div class="modal-content">
						<form th:action="@{/wsbcm/create/save}" method="post" id="createWorkspaceBCMForm" th:object="${workspaceBcmForm}">
								
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" th:text="#{bcm.workspace.manageCapabilityMap}">Create / Edit Workspace BCM</h4>
						      </div>
						      <div class="modal-body">
						      	  <div class="form-group">
						            <label for="bcmName" class="control-label">Name:</label>
						            <input type="text" class="form-control" th:field="*{bcmName}" />
			            			<span th:if="${#fields.hasErrors('bcmName')}" th:errors="*{bcmName}" style="color: red;">Test</span>
						          </div>
						          <div class="form-group">
						            <label for="bcm" class="control-label">BCM Template Name:</label>
						            <select name="bcmTemplate" class="form-control">
						            <option value="-1">----Select------</option>
										<option th:each="bcmTemplate : ${listOfBCMTemplate}" th:value="${bcmTemplate.id}" th:text="${bcmTemplate.bcmName}">Item 1</option>
									</select>
									<span th:if="${#fields.hasErrors('bcm')}" th:errors="*{bcm}" style="color: red;">Test</span>
						          </div>
						      </div>
						      <div class="modal-footer">
						        <input type="submit" th:value="*{id != null }? 'Edit Workspace BCM' : 'Create Workspace BCM'" class="btn btn-primary"></input>
						      </div>
					    </form>
				    </div>
			 </div>
		 </div>
	</div>
	
</body>
</html>