<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity4"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.indexcreation.createindex.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
<div th:fragment="pageTitle"><span th:text="#{pages.indexcreation.createindex.title}"></span></div>
<div th:fragment="pageSubTitle"><span th:text="#{pages.indexcreation.createindex.subtitle}"></span></div>
	<div class="container" th:fragment="contentContainer">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
	               <form th:action="@{/index/createIndex}" method="post" id="createIndexForm" >
	                 <div class="box-body"><br/>
						<button type="submit" class="btn btn-primary">Create Index</button>	
					</div>
					 <div class="box-body" > 					                                                                                                                                                                                                                                                                                       
						<span  style="color: green;" th:text="${message}"></span> 
					 </div>
					</form>
				</div>
			</div>
		</div>
	  </div>	
</body>
</html>