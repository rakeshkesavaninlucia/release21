<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.workspacemanagement.workspaceuser.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<!-- Latest compiled and minified CSS -->
<link th:href="@{/static/css/ext/bootstrap-3.3.6.min.css}"
	rel="stylesheet" type="text/css" />
<!-- Latest compiled and minified JavaScript -->
<script th:src="@{/static/js/ext/jquery/2.1.4/jquery.min.js}"></script>
<script th:src="@{/static/js/ext/bootstrap/3.3.6/bootstrap.min.js}"></script>
</head>
<body>
	<div th:fragment="pageTitle">
		<span th:text="${viewWorkspace.workspaceName}"></span>
	</div>
	<div th:fragment="pageSubTitle">
		<span th:text="${viewWorkspace.workspaceDescription}"></span>
	</div>
	<div class="container" th:fragment="contentContainer">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-primary">
					<div  id = "boxbody1" class="box-body" data-module="module-deleteWorkspace">
						<div class="table-responsive" data-module="common-data-table">
							<div class="overlay">
								<i class="fa fa-refresh fa-spin"></i>
							</div>
							<table class="table table-bordered table-condensed table-hover workspacetable">
								<thead>
									<tr>
										<th><span th:text="#{workspace.fname}"></span></th>
										<th><span th:text="#{workspace.lname}"></span></th>
										<th><span th:text="#{workspace.email}"></span></th>
										<th><span th:text="#{workspace.workspaceList}"></span></th>
										<th><span th:text="#{workspace.select.workspace}"></span></th>
									</tr>
								</thead>
								<tbody>
									<tr th:each="oneUser : ${mapOfUserAndWorkspace}">
									<input type="hidden" th:value = "${oneUser.key.id}"/>
										<td><span th:text="${oneUser.key.firstName}">Test</span></td>
										<td><span th:text="${oneUser.key.lastName}">Test</span></td>
										<td><span th:text="${oneUser.key.emailAddress}">Test</span></td>
										<td class="col-md-4">
										
										<ul  th:each="oneWorkspace : ${oneUser.value}">
                                              <li>
                                            <span th:text="${oneWorkspace}" th:value="${oneUser.key.id}">Test</span>
                                                      </li>
                                            </ul>
											</td>
										<td>
											<div>
												<input type="checkbox" class = "userChecked" name ="checkedUserId"  th:value="${oneUser.key.id}"/>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
							<div class="box-footer clearfix">
								<div class="col-md-5" id ="workpsaceSelection">
									<select class="select2" name = "workpsaceId" placeholder = "Add Workspace for Selected User">
										<option value="-1"> Add Workspace for Selected User</option>
										<option th:each="oneWorkspace : ${adminListWorkspace}"	th:value="${oneWorkspace.id}" th:name ="wsID"
											th:text="${oneWorkspace.workspaceName}"/>
									</select>
								</div>
								<div>
									<button data-type="add" th:attr="data-wwsid=${viewWorkspace.id}"  class="btn btn-primary">Add Workspace</button>&nbsp;&nbsp;
									<button data-type = "create" class="btn btn-primary">Create WorkSpace</button>&nbsp;&nbsp;
									<button data-type="delete"  th:attr="data-wwsid=${viewWorkspace.id}"  th:name = "wsidd" class="btn btn-primary">Delete WorkSpace</button>
								</div>
								<div th:if = "${error eq 0}">
								 <script type="text/x-config" th:inline="javascript">{"error":"true"}</script>
								<span class = "error_msg" style = "display:none" th:text ="#{workspace.assign.workspace}">Test</span></div>
									<div th:if = "${error eq 1}">
								 <script type="text/x-config" th:inline="javascript">{"error":"false"}</script>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div th:fragment="scriptsContainer">
		<script th:src="@{/static/js/3rdEye/modules/module-deleteWorkspace.js}"></script>
	</div>

</body>
</html>