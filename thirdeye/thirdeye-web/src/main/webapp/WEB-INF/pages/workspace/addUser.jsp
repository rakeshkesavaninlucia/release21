<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle ='')">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
<div th:fragment="pageTitle"></div>
		<div class="container"  th:fragment="contentContainer">
			<div th:replace="questionnaire/modalFragment :: addUserModal">
			</div>		
		</div>
</body>
</html>