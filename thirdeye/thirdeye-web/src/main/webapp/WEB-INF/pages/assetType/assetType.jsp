<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.admin.assetTypeManagement.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
	<div th:fragment="pageTitle">
		<span th:text="#{pages.admin.assetTypeManagement.title}"></span>
	</div>
	<div th:fragment="pageSubTitle">
		<span th:text="#{pages.usermanagement.assettype.subtitle}"></span>
	</div>
	<div class="container" th:fragment="contentContainer">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-primary" data-module="module-assetType">
					<div class="box-body">
						<div class="table-responsive" data-module="common-data-table">
							<div class="overlay">
								<i class="fa fa-refresh fa-spin"></i>
							</div>
							<table class="table table-bordered table-condensed table-hover"
								id="assetTypeView">
								<thead>
									<tr>
										<th><span th:text="#{assetType.name}"></span></th>
										<th><span th:text="#{assetType.action}"></span></th>
									</tr>
								</thead>
								<tbody>
									<tr th:each="oneAssetType : ${assetTypeMap}">
									<input type="hidden" name="assetTypeId" th:value="${oneAssetType.key.id}" />
										<td><span th:text="${oneAssetType.key.assetTypeName}">Test</span></td>
										<td><a th:if= "${oneAssetType.value eq false}" class="deleteRow  fa fa-trash-o clickable" th:title="#{icon.title.delete}" sec:authorize="@securityService.hasPermission({'DELETE_ASSET_TYPE'})"></a>
										<a th:if= "${oneAssetType.value eq true}" class="fa fa-trash-o" th:title="#{icon.title.delete.assettype}" sec:authorize="@securityService.hasPermission({'DELETE_ASSET_TYPE'})"></a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer clearfix">
						<div>
						<a class="btn btn-primary" data-type = "newAssetType" >Add Asset Type</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div th:fragment="scriptsContainer"  th:remove="tag">
       <script	th:src="@{/static/js/3rdEye/modules/module-assetType.js}"></script>
   </div>
</body>
</html>