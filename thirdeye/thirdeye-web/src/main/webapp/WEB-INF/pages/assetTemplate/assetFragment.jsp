<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.templatemanagement.inventoryaddasset.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
<div th:fragment = "assetinformation">
		<form th:action="@{/templates/saveAssetDetails}" method="get" id="assetdetailForms">
			<div class="form-group">
				<table class="table table-bordered table-condensed table-hover">
					<tr th:each="assetData : ${asset.assetDatas}">
						<td class="col-md-4 horizontal"><span th:text="${assetData.name}"></span></td>
						<td class="col-md-8">
							<input class="assetdetails form-control" type="text" readonly="readonly" th:value="${assetData.data}" />
						</td>
					</tr>
				</table>
			</div>
		</form>
	</div>
	
	<div  th:fragment="editAssetDetails">
		<form th:action="@{/templates/form/saveAssetDetails}" method="post" id="assetdetailForm">
		<input type="hidden" name="templateId" th:value="${assetTemplateModel.id}" />
		<input type="hidden" name="id" th:value="${assetid}" />
			<div class="form-group">
				<table class="table table-bordered table-condensed table-hover">				
					<tr th:each="templatecol : ${assetTemplateModel.assetTemplateColumns}">
						<td class="col-md-4 horizontal"><span th:text="${templatecol.assetTemplateColName}"></span></td>
						<td class="col-md-8">
							<input th:if="${templatecol.dataType ne T(org.birlasoft.thirdeye.constant.DataType).DATE.name()}" th:name="${templatecol.id}" th:type="${templatecol.dataType}" class="form-control" th:maxlength="${templatecol.length}" th:required="${templatecol.mandatory}" th:value="${mapOfColIdAndData.get(templatecol.id)}"></input>
							<input th:if="${templatecol.dataType eq T(org.birlasoft.thirdeye.constant.DataType).DATE.name()}" th:name="${templatecol.id}" type="date" class="form-control" th:maxlength="${templatecol.length}" th:required="${templatecol.mandatory}" th:value="${mapOfColIdAndData.get(templatecol.id)}"></input>
						</td>
					</tr>
				</table>
				<span th:if="${error eq 1}" style="color: red;">Name already exists</span>
			</div>
		</form>
	</div>
	
	
</body>
</html>