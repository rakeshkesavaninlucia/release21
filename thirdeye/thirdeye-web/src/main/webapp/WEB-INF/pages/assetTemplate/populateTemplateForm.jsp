<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.templatemanagement.inventoryaddasset.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>

		<div th:fragment="pageTitle"><span th:text="#{enter.asset}"></span></div>
		<div th:fragment="pageSubTitle"><span th:text="${assetTemplateModel.assetTemplateName }"></span></div>
			
		<div class="container"  th:fragment="contentContainer">
			<div class="row">
				<div class="col-md-6">
					<div class="box box-primary">
		                <div class="box-body">
							<div th:replace="assetTemplate/editTemplateFragments :: populateAssetDataForm"></div>		
		                </div><!-- /.box-body -->
		              </div>
				</div>
			</div>
		</div>
	<div th:fragment="scriptsContainer" >
  		<script th:src="@{/static/js/3rdEye/modules/module-assetDataEntry.js}"></script>
  	</div>
</body>
</html>