<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.templatemanagement.inventoryasset.nav })">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
	<div th:fragment="pageTitle">
		<a class="pull-left fa fa-chevron-left btn btn-default"
			style="margin-left: 10px;" data-type="backButton" id="goBack"></a> <span
			th:text="${asset.shortName}"></span>
	</div>

	<div th:fragment="pageSubTitle">
		<span th:text="${asset.assetTemplateName} + ' / ' + ${asset.assetTypeName}"></span>
	</div>

	<div class="container" th:fragment="contentContainer">
	
		<div th:replace="assetTemplate/assetDropDownFragment :: assetDropDown"></div>
		
		<div class="row"    style=" margin-top: 10px;">
			<div class="col-md-6">
				<div class="box box-primary" data-module="module-editAssetDetails">
					<div class="box-header with-border">
						<h3 class="box-title" th:text="#{header.asset.details}"></h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<script type="text/x-config" th:inline="javascript">{"assetId":"[[${asset.id}]]"}</script>
					<div id="box-body1" class="box-body" style="width: 100%; height: 300px; overflow-y: scroll;"></div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title" th:text=" #{header.asset.parameter.health}"></h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="box-body" style="width: 100%; height: 300px; overflow-y: scroll;"
						data-module="module-parameterOfParameter">
						<div id="spiderChart"></div>
						<div th:replace="assetTemplate/assetParameterBarChart :: assetbarchart"></div>
					</div>
				</div>
			</div>
		</div>
	
		<div class="row">
			<div class="col-md-6">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title" th:text="#{header.data.completeness}"></h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="box-body" style="overflow: auto;">
						<div class="col-md-2"></div>
						<div class="col-md-4">
							<div data-module="module-circularProgress" th:attr="data-percent=${completionPercentage}"></div>
						</div>

						<div class="col-md-6 text-center">
							<label style="white-space: normal;"><span th:text="#{data.completness.numberOfQQ} + ':'"></span></label> 
							<span th:text="${NumberOfQuestions}"></span><br></br> <br></br>
							<label style="white-space: normal;">
							<span th:text="#{data.completness.numberOfRD} + ':'"></span></label> 
							<span th:text="${NumberOfResponses}"></span>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6" th:if="${not #lists.isEmpty(relationshipList)}">
				<div class="box box-primary" data-module='module-viewAsset'>
					<div class="box-header with-border">
						<h3 class="box-title" th:text="#{header.asset.relationship}"></h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="parameterTree box-body" style="overflow: auto;">
						<div th:replace="relationship/relationshipTemplateFragments :: oneRelationshipWithWrapper(listOfRelationships=${relationshipList})"></div>
					</div>
					<div class="box-footer clearfix">
						<div>
							<a class="btn btn-primary" data-type="addRelationshipButton" th:text="#{anchor.button.add.relationship}"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div data-module="module-drilldown-parameter"></div>
		<div class="modal fade" data-module='module-relationshipTemplateList'
			tabindex="-1" role="dialog" style="display: none;"></div>
	</div>

	<div th:fragment="scriptsContainer" th:remove="tag">
		<script th:src="@{/static/js/ext/d3/3.5.6/d3.min.js}"></script>
		<script th:src="@{/static/js/ext/nv.d3/1.8.1-dev/nv.d3.min.js}"></script>
		<script th:src="@{/static/js/ext/d3.tip/0.6.3/d3.tip.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/charting-spider.js}"></script>
		<script	th:src="@{/static/js/3rdEye/modules/module-drilldown-parameter.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-bulletChart.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-viewAsset.js}"></script>
		<script	th:src="@{/static/js/3rdEye/modules/module-circularProgress.js}"></script>
		<script	th:src="@{/static/js/3rdEye/modules/module-relationshipTemplateList.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-assetSelect2.js}"></script>
		<script	th:src="@{/static/js/3rdEye/modules/module-editAssetDetails.js}"></script>
		<script	th:src="@{/static/js/3rdEye/modules/module-parameterOfParameter.js}"></script>
		<link th:href="@{/static/css/3rdEye/barchart.css}" rel="stylesheet"	type="text/css" />
	</div>
</body>
</html>