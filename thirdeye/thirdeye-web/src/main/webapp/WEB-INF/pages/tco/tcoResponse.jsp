<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle=#{pages.tcomanagement.sresponse.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle"><span th:text="#{pages.tco.sresponse.title}"></span></div>
	<div th:fragment="pageSubTitle"><span th:text="#{pages.tco.sresponse.subtitle}"></span></div>
	<div th:fragment="contentContainer">
	<div class="row" th:if="${chartOfAccountId}">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			 <div th:replace="tco/tcoFragment :: exportImportCOAExcel"></div>
		</div>
	</div>
		<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="box box-primary userquestionnaire" data-module='module-questionnaireResponse'>
				<div class="box-header">
		        	<h3 class="box-title" th:text="${questionnaire.name}">This is the title of the questionnaire</h3>
		            <small th:text="${questionnaire.description}"></small>
		        </div>
				<div class="box-body">
					<div th:each="coa : ${listOfCoa}" class="col-md-12">
						<label th:text="'For '+ ${coa.asset.shortName}">Which asset is this for</label>
						<div th:each="cs : ${coa.listOfCostStructure}">
							<div class="containerParameterTreeView" id="container">
								<ul>
									<li><span th:text="${cs.displayName}"></span>
									<div
										th:replace="tco/tcoResponse :: oneCsWithWrapper(listOfCs=${cs.childCostStructure})"
										th:if="${not #lists.isEmpty(cs.childCostStructure)}"></div>
									<div
										th:replace="tco/tcoResponse :: oneCeWithWrapper(listOfCe=${cs.childCostElement})"
										th:if="${not #lists.isEmpty(cs.childCostElement)}"></div>
													
									</li>
								</ul>
							</div>
						
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>

	<ul th:fragment="oneCsWithWrapper(listOfCs)"
		th:if="${not #lists.isEmpty(listOfCs)}">
		<li th:each="oneCs : ${#sets.toSet(listOfCs)}"><span
			th:text="${oneCs.displayName}"></span> <!-- Process children -->
			<div
				th:replace="tco/tcoResponse :: oneCsWithWrapper(listOfCs=${oneCs.childCostStructure})"
				th:if="${not #lists.isEmpty(oneCs.childCostStructure)}"></div>
			<div
				th:replace="tco/tcoResponse :: oneCeWithWrapper(listOfCe=${oneCs.childCostElement})"
				th:if="${not #lists.isEmpty(oneCs.childCostElement)}"></div></li>
	</ul>
	
	<!-- The fragment for displaying the questions -->
	<ul th:fragment="oneCeWithWrapper(listOfCe)"
		th:if="${not #lists.isEmpty(listOfCe)}">
		<li th:each="oneCe : ${#sets.toSet(listOfCe)}"
			th:title="${oneCe.title}" data-toggle="popover"
			data-trigger="hover" data-container="body" data-placement="top" class="col-md-12">
			<div th:replace="tco/tcoResponse :: CURRENCY(oneQQ=${oneCe.qqBean})"></div></li>
			
	</ul>

	<form role="form" th:action="@{/questionnaire/{questionnaireId}/response/save(questionnaireId=${oneQQ.questionnaireId})}" th:attr="data-qqid=${oneQQ.questionnaireQuestionId}" th:fragment="CURRENCY(oneQQ)" class="editableForm col-md-6" method="POST">
		<div class="form-group">
			<input type="hidden" name="questionnaireQuestionId" th:value="${oneQQ.questionnaireQuestionId}" />
			<span th:text="${oneQQ.question.title }">Question Title</span>
			<span th:if="${not #maps.isEmpty(mapOfMandatoryQuestion) and #maps.containsKey(mapOfMandatoryQuestion, oneQQ.questionnaireQuestionId)}">
			<span style="color: red;">*</span></span>
			<p class="help-block" th:text="${oneQQ.question.helpText}">Help Text the question</p>
			<input type="number" th:min="${oneQQ.question.jsonNumberQuestionMapper.min}" th:max="${oneQQ.question.jsonNumberQuestionMapper.max}" class="form-control editableParameter" th:id="${oneQQ.questionnaireQuestionId}" th:name="'qq_' + ${oneQQ.questionnaireQuestionId}" th:value="${oneQQ.responseText}" placeholder="Enter your amount Here" />
		</div>
	</form>
	
	<div th:fragment="scriptsContainer"  th:remove="tag">
		<script th:src="@{/static/js/3rdEye/modules/module-questionnaireResponse.js}"></script>
		<script	th:src="@{/static/js/3rdEye/modules/module-exportImportCOA.js}"></script>  
	</div>
</body>
</html>