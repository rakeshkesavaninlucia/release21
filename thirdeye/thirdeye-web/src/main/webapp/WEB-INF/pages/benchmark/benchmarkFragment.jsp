<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="viewBenchmarks" class="benchmarksContainer">
		<select name="benchmarkId" class="form-control select2" data-type="fetchBenchmarkItem">
			<option value="-1" th:selected="${benchmarkQuestionBean.benchmarkId eq -1}">--Select--</option>
			<option th:each="oneBenchmark : ${listOfBenchmark}" th:value="${oneBenchmark.id}" th:text="${oneBenchmark.name}" th:selected="${benchmarkQuestionBean.benchmarkId eq oneBenchmark.id}"></option>
		</select>
		<span th:if="${benchmarkQuestionBean ne null and benchmarkQuestionBean.mapOfBenchmarkParam.get('benchmarkId') ne null}" th:text="${benchmarkQuestionBean.mapOfBenchmarkParam.get('benchmarkId')}" class="error_msg">Test</span>
	</div>
	
	<div th:fragment="viewBenchmarkItems" class="benchmarkItemsContainer">
		<div th:each="oneBenchmarkItem : ${listOfBenchmarkItem}">
			<input type="checkbox" name="benchmarkItem" th:value="${oneBenchmarkItem.id}" th:checked="${#sets.contains(setOfCheckedBenchmarkItemIds, oneBenchmarkItem.id)}" th:disabled="${#sets.contains(setOfCheckedBenchmarkItemIds, oneBenchmarkItem.id) and not #sets.isEmpty(questionForm.parameterFunctions)}"/>
			<input type="hidden" name="benchmarkItem" th:value="${oneBenchmarkItem.id}" th:if="${#sets.contains(setOfCheckedBenchmarkItemIds, oneBenchmarkItem.id) and not #sets.isEmpty(questionForm.parameterFunctions)}" />
			<span th:text="${oneBenchmarkItem.displayName}"></span>
		</div>
		<span th:if="${benchmarkQuestionBean ne null and benchmarkQuestionBean.mapOfBenchmarkParam.get('benchmarkItem') ne null}" th:text="${benchmarkQuestionBean.mapOfBenchmarkParam.get('benchmarkItem')}" class="error_msg">Test</span>
	</div>
	<!-- Sunil code start benchmark creation -->
	
	<div th:fragment="createBenchmark" class="benchmarkItemsContainer">	
			<form th:action="@{/benchmark/save}" method="post" class="benchMarkForm"  th:object="${benchmarkForm}">
			        <input type="hidden" name="benchmarkId" th:field="*{id}" />					
				    <div class="form-group">
				       <table class="table table-bordered table-condensed table-hover">
						    <tr>
								<td class="col-md-2 horizontal"><label for="workspace" class="control-label" th:text="#{benchmark.workspace}"></label></td>
								<td class="col-md-10">
									<div th:if="${benchmarkForm.id eq null}">
										<select id="userWorkSpaceId" name="workspace" class="form-control select2">
											<option th:selected="${benchmarkForm.id eq null or (benchmarkForm.workspace ne null and (benchmarkForm.workspace.id eq activeWorkspace.id))}" th:value="${activeWorkspace.id}" th:text="${activeWorkspace.workspaceName}" />
											<option th:selected="${benchmarkForm.id gt 0 and benchmarkForm.workspace eq null}" value="" th:text="#{option.systemlevel}">Independent from workspace</option>
										</select> 
									</div>
									<div th:if="${benchmarkForm.id ne null}">
										<span th:if="${benchmarkForm.workspace ne null}" th:text="${activeWorkspace.workspaceName}"></span>
										<span th:if="${benchmarkForm.workspace eq null}" th:text="#{option.systemlevel}"></span>
										<input th:if="${benchmarkForm.workspace ne null}" type="hidden" th:field="*{workspace}" />
									</div>
								</td>
							</tr>
							 <tr>
								<td class="col-md-2 horizontal"><label for="name" class="control-label" th:text="#{benchmark.name}"><span style="color: red;">*</span></label></td>
								<td class="col-md-10">
								   <div class="form-group">					                    
				                         <input type="text" th:field="*{name}" class="editableBenchmark form-control" />
				                         <span th:if="${#fields.hasErrors('name')}" th:errors="*{name}" class="error_msg">Test</span>
				                         <span class="error_msg"></span>
				                   </div>
								</td>
							</tr>
								
						</table>
					</div>
			</form>	
	</div>
	
	<div th:fragment="createBenchmarkItem"  class="table-responsive">
		 <table class="table table-bordered table-condensed table-hover" id="benchmarkColumns">
			<thead>
		    	<tr>
					<th>Base Name</th>
					<th>Version</th>
					<th>Display Name</th>					
					<th sec:authorize="@securityService.hasPermission({'BENCHMARK_MODIFY'})">Actions</th>
				</tr>
			</thead>
			<tbody >
		 	  <tr th:each="oneCol : ${benchmarkForm.benchmarkItems}" th:include="benchmark/benchmarkFragment :: viewBenchmarkItem"
		  	  	  th:with="colid=${oneCol.id},baseName=${oneCol.baseName},version=${oneCol.version},displayName=${oneCol.displayName},sequenceNumber=${oneCol.sequenceNumber},benchmarkId=${oneCol.benchmark.id}"/>
			</tbody>
		</table>	
		<span sec:authorize="@securityService.hasPermission({'BENCHMARK_MODIFY'})"><button id="addRowButton"  th:attr="data-benchmarkId=${benchmarkForm.id}" class="btn btn-primary" >Add Benchmark Item</button></span>
	</div>
	
	<div th:fragment="viewBenchmarkItem" th:remove="tag" >
		<tr th:id="${colid}" class="nodrag nodrop">
			<td><span th:text="${baseName}">Test</span>	</td>		
			<td><span th:text="${version}">Test</span></td>
			<td><span th:text="${displayName}">Test</span></td>
			<td sec:authorize="@securityService.hasPermission({'BENCHMARK_MODIFY'})"><a class="editRow fa fa-pencil-square-o" title="Edit" style="cursor: pointer;"></a>
			<a class="deleteRow  fa fa-trash-o" th:title="#{icon.title.delete}" style="cursor: pointer;"></a>
			<a th:href="@{/benchmarkItemScore/{id}/createItemScore(id=${colid})}" class="fa fa-plus-square-o" th:title="#{icon.title.score}"></a>			</td>
		</tr>
	</div>
	
	<tr th:fragment="editBenchmarkItem(oneRow)" class="dirtyrow" th:id="${oneRow.id}" th:object="${benchmarkItemBean}">
	        <input type="hidden"  name="benchmark.id" th:value="${oneRow.benchmark.id}" />		  
			<td><input type="text" name="baseName" th:value="${oneRow.baseName}"></input>
			<span class="asterisk">*</span>
			<span th:if="${#fields.hasErrors('baseName')}" th:errors="*{baseName}" class="error_msg">Test</span>
			</td>
			
			<td><input type="text" name="version" th:value="${oneRow.version}"></input>
			<span class="asterisk">*</span>
			<span th:if="${#fields.hasErrors('version')}" th:errors="*{version}" class="error_msg">Test</span>
			</td>
			<td><input type="text" name="displayName" th:value="${oneRow.displayName}"></input>
			</td>
			<td><a class="saveRow fa fa-check-square-o" title="Save"></a>
			<a class="deleteRow  fa fa-trash-o" th:title="#{icon.title.delete}"></a>
			<a th:href="@{/benchmarkItemScore/{id}/createItemScore(id=${oneRow.id})}" class="fa fa-plus-square-o" th:title="#{icon.title.score}"></a>
			</td>
	</tr>
	
	<tr th:fragment="createRow" class="dirtyrow" th:object="${benchmarkItemBean}">
		    <input type="hidden" th:field="*{benchmark.id}" />		
		    <td>		         
		      <input type="text" th:field="*{baseName}"></input>
			  <span class="asterisk">*</span>
			  <span th:if="${#fields.hasErrors('baseName')}" th:errors="*{baseName}" class="error_msg">Test</span>
			</td>			
			<td>
			  <input type="text" th:field="*{version}"></input>
			  <span class="asterisk">*</span>
			  <span th:if="${#fields.hasErrors('version')}" th:errors="*{version}" class="error_msg">Test</span>
			</td>
			<td><input type="text" th:field="*{displayName}"></input></td>
			<td><a class="saveRow fa fa-check-square-o" title="Save"></a></td>
	</tr>
	
	<!-- Sunil code end -->
</body>
</html>