<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.reports.assetgraph.viewgraph.nav})">
<head>
<meta charset="utf-8" />
<title>d3.js from Excel - ramblings.mcpher.com</title>
<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
  Remove this if you use the .htaccess -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<div th:fragment="onPageStyles" th:remove="tag">
	<link th:href="@{/static/css/3rdEye/charting-dependency.css}" rel="stylesheet"	type="text/css" />
	<style>
		svg {
			font: 10px sans-serif;
		}
		
		#chart {
			/* height: 600px; */
			border: 1px solid black;
			position: relative;
			cursor: pointer;
			cursor: hand;
		}
		
		.node rect {
			cursor: move;
			fill-opacity: .9;
			shape-rendering: crispEdges;
		}
		
		.node text {
			pointer-events: none;
			text-shadow: 0 1px 0 #fff;
		}
		
		.link {
			fill: none;
			stroke: #000;
			stroke-opacity: .2;
		}
		
		.link:hover {
			stroke-opacity: .5;
		}
		
		circle.node-dot {
			fill: DarkSlateGray;
			stroke: SlateGray;
			stroke-width: 1px;
		}
		
		line.link {
			fill: none;
			/* stroke: SlateGray; */
			stroke: Black;
			/* stroke-width: 1.5px; */
			stroke-width: 2px;
		}
		
		marker#defaultMarker {
			fill: SlateGray;
		}
		
		line.link.defaultMarker {
			stroke: Black;
		}
		
		circle {
			fill: #ccc;
			stroke: #333;
			stroke-width: 1.5px;
		}
		
		text {
			pointer-events: none;
		}
		
		text.shadow {
			stroke: #fff;
			stroke-width: 3px;
			stroke-opacity: .8;
		}
		
		.d3-tip {
			line-height: 1;
			font-weight: bold;
			padding: 12px;
			background: rgba(0, 0, 0, 0.8);
			color: #fff;
			border-radius: 2px;
		}
		
		/* Creates a small triangle extender for the tooltip */
		.d3-tip:after {
			box-sizing: border-box;
			display: inline;
			font-size: 10px;
			width: 100%;
			line-height: 1;
			color: rgba(0, 0, 0, 0.8);
			content: "\25BC";
			position: absolute;
			text-align: center;
		}
		
		/* Style northward tooltips differently */
		.d3-tip.n:after {
			margin: -1px 0 0 0;
			top: 100%;
			left: 0;
		}
		
		.tools {
			z-index: 100;
			position: absolute;
			right: 20px;
			top: 10px;
			padding: 10px;
			background-color: #f7f7f7;
		}
		
		#internalScale.slider {
			
		}
	</style>
</div>
</head>
<body>
	<div th:fragment="pageTitle">
		<div>
			<span th:text="#{pages.reports.assetgraph.viewgraph.viewadd.title}"></span><span data-toggle="control-sidebar" style="float:right;"><i class="glyphicon glyphicon-filter clickable btn btn-primary"></i></span>
		</div>
	</div>
	<div th:fragment="contentContainer">
		<div class="row" style="padding-bottom: 20px; padding-left: 20px;">
			<div style="padding-top: 15px;" data-module="module-filterTags">
				<div class="col-md-12">
					<div id="tags"></div>
				</div>
			</div>
		</div>

		<aside class="control-sidebar control-sidebar-dark" style="margin-top: 101px; padding-top: 0px;">
			<div class="tab-content" style="height: 39em; overflow-y: auto;">
				<div id="control-sidebar-theme-demo-options-tab" class="tab-pane active">
					<div style="text-align: center; text-decoration: underline;">
						<h4 th:text="#{filter.panel}"></h4>
					</div>
					<div data-module="module-assetTypeFacet"></div>
					<div data-module="module-facetUpdate"></div>
				</div>
			</div>
		</aside>
		
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div data-module="charting-dependency" th:attr="data-graphid=${graphId}">
						<div class="box-header with-border">
							
							<div class="box-tools pull-right">
								<button class="btn btn-box-tool" data-type="download" title = "Download">
									<i class="glyphicon glyphicon-download clickable"></i>
								</button>
							</div>
						</div>
						<div class="box-body">
							<script type="text/x-config">{"title":"Database process documentation","graph":{"linkDistance":100,"charge":-400,"height":1500,"numColors":12,"labelPadding":{"left":3,"right":3,"top":2,"bottom":2},"labelMargin":{"left":3,"right":3,"top":2,"bottom":2},"ticksWithoutCollisions":50},"types":"","constraints":[{"has":{"type":"Application"},"type":"position","y":0.1,"weight":0.8},{"has":{"type":"Server"},"type":"position","y":0.25,"weight":0.8},{"has":{"type":"Database"},"type":"position","y":0.4,"weight":0.8},{"has":{"type":"Person"},"type":"position","y":0.55,"weight":0.8},{"has":{"type":"Business Services"},"type":"position","y":0.7,"weight":0.5},{"has":{"type":"IT Services"},"type":"position","y":0.85,"weight":0.8},{"has":{"type":"report"},"type":"position","y":1,"weight":0.8},{"has":{"type":"report","group":"Intermediate"},"type":"position","y":0.8,"weight":0.8},{"has":{"type":"report","group":"Reporting"},"type":"position","x":0.9,"weight":0.8},{"has":{"type":"query","group":"Data1"},"type":"position","x":0.15,"weight":0.7},{"has":{"type":"query","group":"Data3"},"type":"position","x":0.2,"weight":0.7},{"has":{"type":"query","group":"Data4"},"type":"position","x":0.2,"weight":0.7},{"has":{"type":"query","group":"Data2"},"type":"position","x":0.5,"weight":0.4},{"has":{"type":"query","group":"Validation"},"type":"position","x":0.8,"y":0.7,"weight":0.7},{"has":{"type":"query","group":"Miscellaneous"},"type":"position","x":0.9,"y":0.5,"weight":0.7},{"has":{"type":"query","group":"Management"},"type":"position","x":0.95,"weight":0.7},{"has":{"type":"Application"},"type":"linkStrength","strength":0},{"has":{"type":"Server"},"type":"linkStrength","strength":0.5},{"has":{"type":"Database"},"type":"linkStrength","strength":0.5},{"has":{"type":"Person"},"type":"linkStrength","strength":0.5},{"has":{"type":"Business Services"},"type":"linkStrength","strength":0.5},{"has":{"type":"IT Services"},"type":"linkStrength","strength":0.5}],"jsonUrl":"json.php"}</script>
							<div id="graph-container" class="clickable">
								<div class="tools form-inline well" style="width: 200px; margin: 20px;">
									<input id="internalScale" type="range" min="0.1" max="3" value="1" step="0.01" class="slider"></input>
								</div>
								<div>
									<a class="left carousel-control"><span id="internalTranslateRight" class="fa fa-angle-left"></span></a>
									<a class="right carousel-control" style="margin-right: 10px; margin-right: 30px;"><span id="internalTranslateLeft" class="fa fa-angle-right"></span></a>
								</div>
								<div id="graph"></div>
							</div>
							<div id="docs-container">
								<div id="docs" class="docs"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div th:fragment="scriptsContainer" th:remove="tag">
		<script th:src="@{/static/js/ext/d3/3.5.6/d3.min.js}"></script>
		<script th:src="@{/static/js/ext/d3.tip/0.6.3/d3.tip.js}"></script>
		<script	th:src="@{/static/js/ext/jquery.browser/0.0.6/jquery.browser.min.js}"></script>
		<script th:src="@{/static/js/ext/colorbrewer/colorbrewer.js}"></script>
		<script th:src="@{/static/js/ext/geometry/geometry.js}"></script>
		<script th:src="@{/static/js/ext/thirdEye/charting-dependency.js}"></script>
		<script th:src="@{/static/js/ext/thirdEye/saveSvgAsPng.min.js}"></script>
	</div>

</body>
</html>