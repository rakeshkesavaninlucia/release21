<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = 'snow')">
<body>
	<div th:fragment="pageTitle">
		<div>
			<span th:text="#{pages.snow.title}"></span>
		</div>
	</div>
	<div class="container" th:fragment="contentContainer">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-primary">
					<div class="box-body">
						<div class="table-responsive" data-module="common-data-table">
							<table class="table table-bordered table-condensed table-hover"
								id="">
								<thead>
									<tr>
										<th><span th:text="#{snow.id}"></span></th>
										<th><span th:text="#{snow.priority}"></span></th>
										<th><span th:text="#{snow.category}"></span></th>
										<th><span th:text="#{snow.description}"></span></th>
									</tr>
								</thead>
								<tbody>
									<tr th:each="oneRecord : ${finalresponse.results}">
										<td class="IID"><span th:text="${oneRecord.incidentId}">Test</span></td>
										<td class="PID"><span th:text="${oneRecord.priority}">Test</span></td>
										<td class="CID"><span th:text="${oneRecord.category}">Test</span></td>
										<td class="DID"><span
											th:text="${oneRecord.incidentDescription}">Test</span></td>

									</tr>
								</tbody>

							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>


</body>
</html>
