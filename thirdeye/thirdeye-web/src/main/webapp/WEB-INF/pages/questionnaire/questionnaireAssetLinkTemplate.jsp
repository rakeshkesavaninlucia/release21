<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = ${pageTitle})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>

	<div th:fragment="pageTitle"><div></div>
	</div>
	<div class="container"  th:fragment="contentContainer">
			<div th:replace="questionnaire/questionnaireTemplateFragement :: createQuestionnaireAssetLink"></div>		
	</div>
	<div th:fragment="scriptsContainer">
		<script th:src="@{/static/js/ext/jquery.dataTables/jquery.dataTables.fnGetHiddenNodes.js}"></script>
     	<script th:src="@{/static/js/3rdEye/modules/module-questionnaireAsset.js}"></script>
	</div>
</body>
</html>