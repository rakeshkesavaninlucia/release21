<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = ${pageTitle})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
 
 <div th:fragment="pageTitle" >
       <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}" th:text="#{pages.questionnaire.viewquestionnaire.title}"></span>
       <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" th:text="#{pages.tco.viewtco.title}"></span>
 </div>
 <div th:fragment="pageSubTitle">
     <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}" th:text="#{pages.questionnaire.viewquestionnaire.subtitle}"></span>
     <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" th:text="#{pages.tco.viewtco.subtitle}"></span>
 </div>
 
	<div class="container" th:fragment="contentContainer">
	 <div class="row">
        	<div class="col-sm-12">
	        	<div class="box box-primary">
	                <div class="box-body" data-module = "module-listQuestionnaire">
 		<div class="table-responsive" data-module="common-data-table">
			<div class="overlay">
				<i class="fa fa-refresh fa-spin"></i>
			</div>
			<table class="table table-bordered table-condensed table-hover" id="templateColumnsofview">
				<thead>
					<tr>
						<th>
						  <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}" th:text="#{questionnaire.name.table}"></span>
						  <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" th:text="#{tco.name.table}"></span>
						</th>
						<th><span th:text="#{questionnaire.description.table}"></span></th>
						<th><span th:text="#{questionnaire.date.table}"></span></th>
						<th th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}"><span  th:text="#{questionnare.year.tco}"></span></th>
						<th><span th:text="#{questionnaire.action.table}"></span></th>
					</tr>
				</thead>
				<tbody>
					<tr th:each="oneOfQuestionnaire : ${questionnaireHashMap}" th:id="${oneOfQuestionnaire.key.id}">
					<td>
						<a th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}" th:href="@{/{action}/{id}/status(id=${oneOfQuestionnaire.key.id},action=${action})}" sec:authorize="@securityService.hasPermission({'QUESTIONNAIRE_MODIFY'})"><span th:text="${oneOfQuestionnaire.key.name}">Test</span></a>
						<a th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" th:href="@{/{action}/{id}/status(id=${oneOfQuestionnaire.key.id},action=${action})}" sec:authorize="@securityService.hasPermission({'MODIFY_TCO'})"><span th:text="${oneOfQuestionnaire.key.name}">Test</span></a>
						<span th:text="${oneOfQuestionnaire.key.name}" sec:authorize="@securityService.hasPermission({'MODIFY_TCO','QUESTIONNAIRE_MODIFY'}) == false">Test</span>
						
					</td>
					<td><span th:text="${oneOfQuestionnaire.key.description}">Test</span></td>
					<td><span th:text="${#dates.format(oneOfQuestionnaire.key.updatedDate, 'dd-MMM-yyyy')}">Test</span></td>
					<td th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" th:text="${oneOfQuestionnaire.key.year}"><span>Test</span></td>
					<td>
						<a th:href="@{/{action}/{id}/edit(id=${oneOfQuestionnaire.key.id},action=${action})}" class="fa fa-pencil-square-o" th:title="#{icon.title.edit}" sec:authorize="@securityService.hasPermission({'QUESTIONNAIRE_MODIFY'})"></a>
						<input type="hidden" name= "questionnaireId" th:value = "${oneOfQuestionnaire.key.id}"/>
						<a th:if ="${oneOfQuestionnaire.value eq false}" class="deleteRow  fa fa-trash-o clickable"	th:title="#{icon.title.delete}" sec:authorize="@securityService.hasPermission({'DELETE_QUESTIONNAIRE'})"></a>
						<a  th:if="${oneOfQuestionnaire.value eq true }"  class="fa fa-trash-o"	 th:title="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)} ? #{icon.title.delete.tco} : #{icon.title.delete.questionnaire}" sec:authorize="@securityService.hasPermission({'DELETE_QUESTIONNAIRE'})"></a>
					</td>
					</tr>					
				</tbody>
			</table>
		</div>
		</div>
		<div class="box-footer clearfix" sec:authorize="@securityService.hasPermission({'MODIFY_TCO','QUESTIONNAIRE_MODIFY'})">
	          <div>
		          <a th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}" class="btn btn-primary" th:href="@{/questionnaire/create}"><span th:text ="#{questionnaire.create}" sec:authorize="@securityService.hasPermission({'QUESTIONNAIRE_MODIFY'})"></span></a>
		          <a th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" class="btn btn-primary" th:href="@{/tco/create}"><span th:text ="#{tco.create.chartofaccount}" sec:authorize="@securityService.hasPermission({'MODIFY_TCO'})"></span></a>
		      </div>
	    </div>
		</div>
		</div>
		</div>
	</div>
	<div th:fragment="scriptsContainer"  th:remove="tag">
       <script	th:src="@{/static/js/3rdEye/modules/module-listQuestionnaire.js}"></script>
   </div>
</body>
</html>