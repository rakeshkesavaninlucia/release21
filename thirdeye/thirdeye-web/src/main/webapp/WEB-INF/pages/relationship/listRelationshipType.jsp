<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity4"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.configuration.relationshiptype.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
<div th:fragment="pageTitle"><span th:text="#{pages.configuration.relationshiptype.title}"></span></div>
<div th:fragment="pageSubTitle"><span th:text="#{pages.configuration.relationshiptype.subtitle}"></span></div>
	<div class="container" th:fragment="contentContainer">
	  <div class="row">
        	<div class="col-sm-12">
	        	<div class="box box-primary" data-module="module-modalCreation">
	                <div class="box-body">
	           			<div class="table-responsive" data-module="common-data-table">
							<div class="overlay">
								<i class="fa fa-refresh fa-spin"></i>
							</div>
							<table class="table table-bordered table-condensed table-hover" id="relationshipTypeList">
								<thead>
									<tr>
										<th><span th:text="#{relationship.parent.descriptor}"></span></th>
										<th><span th:text="#{relationship.child.descriptor}"></span></th>
										<th><span th:text="#{relationship.type.direction}"></span></th>
										<th><span th:text="#{tableheader.displayname}"></span></th>
										<th><span th:text="#{table.header.updated.date}"></span></th>
										<th><span th:text="#{table.header.action}"></span></th>
									</tr>
								</thead>
								<tbody>
									<tr th:each="oneRelationshipType : ${listOfRelationshipType}" th:id="${oneRelationshipType.id}">
										<td><span th:text="${oneRelationshipType.parentDescriptor}">Test</span></td>
										<td><span th:text="${oneRelationshipType.childDescriptor}">Test</span></td>
										<td><span th:if="${oneRelationshipType.direction ne null and !oneRelationshipType.direction.trim().isEmpty()}" th:text="${T(org.birlasoft.thirdeye.constant.InterfaceDirection).valueOf(oneRelationshipType.direction).getDescription()}">Test</span></td>
										<td>
											<span th:if="${oneRelationshipType.workspace eq null}" th:text="${oneRelationshipType.displayName}">System Level</span>
											<span th:if="${oneRelationshipType.workspace ne null}" th:text="${oneRelationshipType.displayName} + ' - *' ">System Level</span>
										</td>
										<td><span th:text="${#dates.format(oneRelationshipType.updatedDate,'dd-MMM-yyyy')}">Test</span></td>
										<td><a class="editRelationshipType fa fa-pencil-square-o" sec:authorize="@securityService.hasPermission('RELATIONSHIP_MODIFY')" th:attr="data-relationshiptypeid=${oneRelationshipType.id},data-id=${oneRelationshipType.id}" th:title="#{icon.title.edit}" data-type="editModal"></a></td>
									</tr>					
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer clearfix">
		                  <div>
					          <a class="createRelationshipType btn btn-primary" sec:authorize="@securityService.hasPermission('RELATIONSHIP_MODIFY')" th:text="#{relationship.type.create}" data-type="createModal"></a>
					          <script type="text/x-config" th:inline="javascript">{"url":"relationship/create", "modal": "createRelationshipTypeModal"}</script>
					      </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div th:fragment="scriptsContainer"  th:remove="tag">
		<script	th:src="@{/static/js/3rdEye/modules/module-modalCreation.js}"></script>
	</div>
</body>
</html>