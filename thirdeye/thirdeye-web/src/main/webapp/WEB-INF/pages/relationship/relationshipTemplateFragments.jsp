<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
 <style th:fragment="onPageStyles">
  </style>
<title>Relationship Template Fragments</title>
</head>
<body>
	<div th:if="${isRelationshipAssetType eq true}" th:fragment="createRelationshipTemplate" class="row" th:object="${assetTemplateBean}">
		<input type="hidden" th:field="*{relationshipTemplateId}" />
		<div class="col-md-4 form-group">
			<label for="parentAssetTypeId" class="control-label" th:text="#{rsuggestion.label.parent.assettype}"></label> <span class="asterisk">*</span>
			<select id="parentAssetTypeId" th:field="*{parentAssetTypeId}" class="form-control select2">
				<option value="-1">--Select--</option>
				<option th:each="assetType : ${listOfAssetType}" th:value="${assetType.id}" th:text="${assetType.assetTypeName}" />
			</select>
			<span th:if="${#fields.hasErrors('parentAssetTypeId')}" th:errors="*{parentAssetTypeId}" class="error_msg">Test</span>
		</div>
		<div class="col-md-4 form-group">
			<label for="relationshipTypeId" class="control-label" th:text="#{rsuggestion.label.rtype}"></label> <span class="asterisk">*</span>
			<select id="relationshipTypeId" th:field="*{relationshipTypeId}" class="form-control ajaxPopulated relationshipTypeSelector select2" th:attr="data-dependson=ws">
				<option value="-1">--Select--</option>
				<option th:each="rType : ${listOfRType}" th:value="${rType.id}">
					<span th:if="${rType.workspace ne null}" th:text="${rType.displayName} + ' - *'"></span>
					<span th:if="${rType.workspace eq null}" th:text="${rType.displayName}"></span>
				</option>
			</select>
			<span th:if="${#fields.hasErrors('relationshipTypeId')}" th:errors="*{relationshipTypeId}" class="error_msg">Test</span>
		</div>
		<div class="col-md-4 form-group">
			<label for="childAssetTypeId" class="control-label" th:text="#{rsuggestion.label.child.assettype}"></label> <span class="asterisk">*</span>
			<select id="childAssetTypeId" th:field="*{childAssetTypeId}" class="form-control select2">
				<option value="-1">--Select--</option>
				<option th:each="assetType : ${listOfAssetType}" th:value="${assetType.id}" th:text="${assetType.assetTypeName}" />
			</select>
			<span th:if="${#fields.hasErrors('childAssetTypeId')}" th:errors="*{childAssetTypeId}" class="error_msg">Test</span>
		</div>
		<div class="col-md-6 form-group">
			<input type="checkbox" th:field="*{hasTemplateColumn}" th:text="#{relationship.template.hasAttribute}"/>
		</div>
	</div>	
	
	<div th:fragment="relationshipTemplateListModal" >
		<div class="modal-dialog modal-sm" role="document">
			    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title">Available Relationship Template</h4>
				      </div>
				      <div class="modal-body">
				          <div class="table-responsive" data-module="common-data-table">
							<table class="table table-bordered table-condensed table-hover">
							    <thead>
									<tr>
										<th><span th:text="#{table.header.name}"></span></th>
									</tr>
								</thead>
								<tbody >
									<tr th:each="oneRelationshipTemplate : ${listOfRelationshipTemplate}" th:id="${oneRelationshipTemplate.id}">
										<td><a th:href="@{/templates/populateTemplateForm/{id}(id=${oneRelationshipTemplate.id})}" th:text="${oneRelationshipTemplate.assetTemplateName}"></a></td>
									</tr>			
								</tbody>
							</table>
						 </div>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				      </div>
			    </div>
		 </div>
	</div>
	
	<ul  class="parameterTreeMenu" id="assetRelationshipTree" th:fragment="oneRelationshipWithWrapper(listOfRelationships)" th:if="${not #lists.isEmpty(listOfRelationships)}">
		<li th:fragment="oneRelationshipAdded(listOfRelationship)" th:each="oneRelationship : ${listOfRelationships}" class="treeview">
			<a th:fragment="oneAnchor(oneRelationship)" th:id="${oneRelationship.id}" class="isParent"> 
				<i class="fa fa-plus-square-o pull-left"></i>
				<i class="fa fa-minus-square-o pull-left"></i>
				<span th:text="${oneRelationship.displayName}">Option in tree</span>
			</a>
			<div th:replace="relationship/relationshipTemplateFragments :: oneAssetWithWrapper(listOfAssets=${oneRelationship.listOfAssets})" th:if="${not #lists.isEmpty(oneRelationship.listOfAssets)}"></div>
		</li>
	</ul>
	
	<!-- The fragment is for displaying the assetss -->
	<ul class="treeview-menu" th:fragment="oneAssetWithWrapper(listOfAssets)" th:if="${not #lists.isEmpty(listOfAssets)}">
		<li th:fragment="oneAssetAdded(listOfAssets)" th:each="oneAsset : ${listOfAssets}" class="treeview">
			<a th:id="${oneAsset.id}" class="question"> 
				<span th:text="${oneAsset.shortName}">Leaf Node</span>
			</a>
		</li>
	</ul>
</body>
</html>