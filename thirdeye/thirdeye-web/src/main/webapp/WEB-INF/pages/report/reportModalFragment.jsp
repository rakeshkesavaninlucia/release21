<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
 <style th:fragment="onPageStyles">
  </style>
<title>Report Model Fragment</title>
</head>
<body>
	<div th:fragment="createReport">
		<div class="modal fade" id="saveReportModal" tabindex="-1" role="dialog" aria-labelledby="saveReportModal">
			<div class="modal-dialog" role="document" >
				    <div class="modal-content">
						<form th:action="@{/report/saveReport}" method="post" id="saveReportForm" th:object="${reportForm}">
							  <input type="hidden" th:field="*{id}" />
							  <input type="hidden" th:field="*{reportConfig}" />
							  <input type="hidden" th:field="*{reportType}" />
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" th:text="*{id != null }? 'Edit Report' : 'Save Report'"></h4>
						      </div>
						      <div class="modal-body">
						      	  <div class="form-group">
						            <label for="reportName" class="control-label" th:text="#{label.report.name}"></label>
						            <span class="asterisk">*</span>
						            <input type="text" class="form-control" th:field="*{reportName}" />
			            			<span th:if="${#fields.hasErrors('reportName')}" th:errors="*{reportName}" class="error_msg">Test</span>
						          </div>
						          <div class="form-group">
						            <label for="description" class="control-label" th:text="#{label.report.description}"></label>
						            <span class="asterisk"></span>
						            <input type="text" class="form-control" th:field="*{description}" />
			            			<span th:if="${#fields.hasErrors('description')}" th:errors="*{description}" class="error_msg">Test</span>
						          </div>
						      </div>
						      <div class="modal-footer">
						        <input type="submit" th:value="*{id != null }? 'Update Report' : 'Save Report'" class="btn btn-primary"></input>
						      </div>
					    </form>
				    </div>
			 </div>
		 </div>
	</div>
	<div th:fragment="viewReports" class="table-responsive" data-module="common-data-table">
		<div class="overlay">
			<i class="fa fa-refresh fa-spin"></i>
		</div>
		<table class="table table-bordered table-condensed table-hover" id="reportView">
				<thead>
					<tr>
						<th><span th:text="#{report.name}"></span></th>		
						<th><span th:text="#{report.description}"></span></th>	
						<th><span th:text="#{report.type}"></span></th>																						
						<th><span th:text="#{report.action}"></span></th>
						<th><span th:text="#{report.view.active}"></span></th>
					</tr>
				</thead>
				<tbody >
                    <tr th:each="oneReport : ${listOfReports}" th:include="report/reportModalFragment :: viewOneReport"
                     th:with="reportid=${oneReport.id},reportname=${oneReport.reportName},description=${oneReport.description},reporttype=${oneReport.reportType},reporstatus=${oneReport.status}" th:remove="tag"/>
                </tbody>
		</table>
	</div>
	
	<div th:fragment="viewOneReport" th:remove="tag" >
		<tr th:id="${reportid}"  th:name="${reportid}"  class="nodrag nodrop">
			<td><span th:text="${reportname}">Test</span></td>	
			<td><span th:text="${description}">Test</span></td>
			<td><span th:text="${reporttype}">Test</span></td>		
			<td>
			   <a th:href="@{/report/view/{id}(id=${reportid})}" class="fa fa-search" th:title="#{icon.title.view.report}"></a>
			</td>
			<td><input  type="radio" name="reportStatus" class="reportStatusClass" th:checked="${oneReport.status}" th:value="${oneReport.reportName}" th:id="${oneReport.id}"  th:attr="data-reporturl=${'/report/view/'+oneReport.id},data-idenfiervalue=${oneReport.reportName},data-checkedstatus=${oneReport.id}" data-type="reportStatusDataType"/></td>
			</tr>
	</div>
	
	<tr th:fragment="editOneReport(oneRow)" class="dirtyrow" th:id="${oneRow.id}" th:object="${reportConfigBean}">
			<td>
			    <input type="text" name="reportName" th:value="${oneRow.reportName}"></input>
				<span class="asterisk">*</span>
				<span th:if="${#fields.hasErrors('reportName')}" th:errors="*{reportName}" class="error_msg">Test</span>
			</td>
			<td>
			    <input type="text" name="version" th:value="${oneRow.description}"></input>
			</td>
			<td>
			    <span th:text="${oneRow.reportType}">Test</span>
			</td>
			<td>
			    <a class="saveRow fa fa-check-square-o" title="Save"></a>
			    <a class="deleteRow  fa fa-trash-o" th:title="#{icon.title.delete}"></a>
			</td>
	</tr>
	
</body>
</html>