/**
 * @author: ananta.sethi
 *  module for view asset template
 *  (functions-edit,delete,save)
 */
Box.Application.addModule('module-viewAssetTemplate',function(context) {
	'use strict';
	var $,commonUtils;
//	private methods here
	function captureDisplaySequence()
	{
		var idList = "";
		$("#templateColumns tbody tr").each(function(){
			if (typeof $(this).attr('id') != 'undefined'){
				idList = idList + $(this).attr('id') + ",";
			}
		});
		if (idList.length > 0){
			$("#sequenceNumber").val(idList.substring(0, (idList.length - 1)));
		} else{
			$("#sequenceNumber").val("");
		}
	} // [EOF captureDisplaySequence]

	function setNewSequence()
	{
		var dataObj = {};
		dataObj.sequence = $("#sequenceNumber").val();
		var deferred = $.Deferred();
		var postRequest = $.post(commonUtils.getAppBaseURL("templates/columns/updateSequence"),dataObj);
		postRequest.done(function(incomingData){
			deferred.resolve();
		})
		return deferred.promise();
	} // [EOF setNewSequence]
	
	function disableselect(){
		
		if( $("#dropdown").val() === "DATE"){
			$('#length, #Enter_Text').prop('disabled', 'disabled');
			$('#length, #Enter_Text').prop('value','1');
		}else
			$('#length, #Enter_Text').prop('disabled', false);
	}
	

	return {	

		// Initialize the page
		init : function() {

			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			// Accessing the service
			commonUtils = context.getService('commonUtils');
			
			$("#addRowButton").bind('click',function(){
				$(".saveRow").trigger('click');
				if ($(".dirtyrow").length < 1) 
				{
					commonUtils.addRowToTableFromURL("templateColumns",commonUtils.getAppBaseURL("templates/columns/newRow"));
				}
			});

			// Bind the click on the table
			// Base configuration for the click handler.
			var clickHandlerConfiguration = {};
			clickHandlerConfiguration.saveClass = "saveRow";
			clickHandlerConfiguration.saveURL = commonUtils.getAppBaseURL("templates/columns/saveRow");
			clickHandlerConfiguration.editClass = "editRow";
			clickHandlerConfiguration.editURL = commonUtils.getAppBaseURL("templates/columns/editRow/");
			clickHandlerConfiguration.deleteClass = "deleteRow";
			clickHandlerConfiguration.deleteURL = commonUtils.getAppBaseURL("templates/columns/deleteRow");

			$("#templateColumns").click(clickHandlerConfiguration,commonUtils.handleTableRowClick);
			// drag and drop rows of the table
			$('#templateColumns').tableDnD({
				onDragStart : function(table, row) {
					$(table).parent().find('.result').text('');
				},
				onDrop : function(table, row) {
					captureDisplaySequence();
					setNewSequence();
				}
			});
		},
		onchange:function(event,element,elementType){
			if(elementType === 'selecttype'){	
				disableselect();
			}
		},
		onclick:function(event,element,elementType){
			if(elementType === 'lengthType' && $('#datatype').val()==='DATE'){
				$('#lengthField').prop('readonly',true).val('1');
			}
		}
	}
});