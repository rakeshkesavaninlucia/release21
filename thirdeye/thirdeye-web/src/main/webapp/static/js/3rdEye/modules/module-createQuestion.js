/**
 * @author: dhruv.sood
 * function find fragment,addOption,remove option -used while creating a question 
 */
Box.Application.addModule('module-createQuestion', function(context) {
	'use strict';
	var $,commonUtils;
	var dataObj = {};
	// private methods here
	function findFragment(optionValue){
		if(optionValue === "-1")
			$( "#getFragment" ).empty();
		else{
			var $urlToHit = commonUtils.getAppBaseURL("questionType/"+optionValue);
			$.get( $urlToHit, function( newRowContent ) {
				$( "#getFragment" ).empty();
				$("#getFragment").append(newRowContent);
				if(optionValue === 'MULTCHOICE')
					$( "#divCheckbox" ).show();
				else {
					$('.benchmarkCheckboxMCQ').prop('checked', false);
					$( "#divCheckbox" ).hide();
				}
			});
		}
	}

	function addOption() {
		var $urlToHit = commonUtils.getAppBaseURL("questionType/addOption");
		$.get( $urlToHit, function( newRowContent ) {
			$("#getOption").append(newRowContent);
		});
	}

	function removeOption(spanTag){
		var $divTag = spanTag.target.closest('div');
		$divTag.remove();
	}
	
	return {
		behaviors : [ 'behavior-select2' ],
		init : function(){
			var _this = this;
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			// Accessing the service
			commonUtils = context.getService('commonUtils');
			
			_this._initPageBindings();
		}, 
		_initPageBindings: function(){	
			$("form").submit(function() {
				var $input = $(this).find("input[name='queTypeText']");
				var $inputHidden = $(this).find("input[name='queTypeTextHidden']");
				$.each($inputHidden, function(i){
					this.value = $input[i].value.replace(/,/g, "&#44;");
				});
			});
		},
		onclick:function(event,element,elementType){
			if(elementType === 'addOption'){
				addOption();
			}
			else if(elementType === 'removeOption'){
				removeOption(event);
			}
			else if(elementType === 'fetchBenchmark'){
				dataObj.workspaceId = $("#userWorkSpaceId").val();
				dataObj.isChecked = element.checked;
				context.broadcast('bq::fetchBenchmark',dataObj);
			}
		},
		onchange:function(event,element,elementType){
			if(elementType === 'questiontype'){		
				findFragment(element.value);
			} else if (elementType === 'fetchBenchmarkItem'){
				dataObj.benchmarkId = element.value;
				context.broadcast('bq::fetchBenchmarkItem',dataObj);
			}
		}
	};
});