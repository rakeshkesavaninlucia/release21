/**
 * @author samar.gupta
 * Create Parameter module
 */
Box.Application.addModule('module-createParameter', function(context) {
	
	var $, commonUtils;
	var dataObj = {};
	
	// private methods here
	function showButtonAfterEmptyAndAppendDiv(incomingData){
		$('.showButtonDependsOnParamType').empty();
		$('.showButtonDependsOnParamType').append(incomingData);
	}
	
	function showModalBasedOnParamType(modalUrl){
		removeSelectedChildEntities().then(function(){
			var postRequest = $.post( commonUtils.getAppBaseURL(modalUrl), dataObj);
			postRequest.done(function(incomingData){
				showButtonAfterEmptyAndAppendDiv(incomingData);
			})
		});
	}
	
	function removeSelectedChildEntities(){
		// Prepare the deferred call
		var deferred = $.Deferred();
		if($('.selectedChildEntities').length > 0 && confirm("Your data will be lost and cannot be recovered. Are you sure?") === true){
			var postRequest = $.post( commonUtils.getAppBaseURL("parameter/initialize"), dataObj);
			postRequest.done(function(){
				$('.selectedChildEntities').remove();
			})
		}
		deferred.resolve();
		return deferred.promise();
	}
	
return {
	
	behaviors : [ 'behavior-select2' ],
	
    init: function (){
    	// retrieve a reference to jQuery
        $ = context.getGlobal('jQuery');
        commonUtils = context.getService('commonUtils');
      },
      
      onchange: function(event, element, elementType) {
    	  if(elementType === "addButton"){
    		  var $selectedElementValue = element.value;
        	  if($selectedElementValue === "FC"){
        		  showModalBasedOnParamType("parameter/fc/showButton");
        	  }else if($selectedElementValue === "LP"){
        		  showModalBasedOnParamType("parameter/lp/showButton");
        	  }else if($selectedElementValue === "AP"){
        		  showModalBasedOnParamType("parameter/ap/showButton");
        	  }
    	  }
      },
      
      onclick: function(event, element, elementType){
    	  if(elementType === "buttonClick"){
    		  if($("a").hasClass("addFunctionalMap")){
    			  context.broadcast('pfmm::showFunctionalMapSelector', dataObj);
    		  }else if($("a").hasClass("addQuestion")){
    			  dataObj.workspaceId = $("#workspaceId").val();
    			  context.broadcast('plqm::showQuestionSelector', dataObj);
			}else if($("a").hasClass("addParameter")){
				  dataObj.workspaceId = $("#workspaceId").val();
	  			  context.broadcast('pam::showParameterSelector', dataObj);
			}
    	  }
      }
	};
});