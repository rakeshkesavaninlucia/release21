/**
 * @author: shaishav.dixit
 */
Box.Application.addModule('module-qualityGate', function(context) {

	var $, commonUtils,notifyService;
	var pageUrl;
	
	function bindQualityGateModalRequirements($newModal){
		$('#createQualityGateForm', $newModal).ajaxForm(function(htmlResponse) {
			
			if (htmlResponse.redirect){
				window.location.replace(commonUtils.getAppBaseURL(htmlResponse.redirect));
				//notify
				notifyService.setNotification("Saving...", "", "success");	
			} else{ 
				var $replacement = commonUtils.swapModalContent("createQualityGateModal", htmlResponse);
				$replacement.modal({backdrop:false});
				bindQualityGateModalRequirements($replacement);
				//notify qg name empty
				notifyService.setNotification("Fields Missing","", "error");	
			}
		}); 
	}
	
	function createQualityGateModal(){
		
    	var url = commonUtils.getAppBaseURL(pageUrl);
    	
		commonUtils.getHTMLContent(url).then(function(content){
			
			var $newModal = commonUtils.appendModalToPage("createQualityGateModal", content, true);
			
			bindQualityGateModalRequirements($newModal);
			
			$newModal.modal({backdrop:false,show:true});
		});
	}
	
	function addEditOrDeleteCondition(){
		window.location.href = commonUtils.getAppBaseURL(pageUrl); 
	}
	
    return {

        init: function (){
        	// retrieve a reference to jQuery
            $ = context.getGlobal('jQuery');
            commonUtils = context.getService('commonUtils');
            notifyService = context.getService('service-notify');
        },
    
        onclick: function(event, element, elementType) {
        	if (elementType === 'createQualityGate') {
        		pageUrl = "qualityGates/create";
        		createQualityGateModal();
        	} else if (elementType === 'editQualityGate') {
        		pageUrl = $(element).data("url");
        		createQualityGateModal();
        	} else if (elementType === 'editQualityGateCondition' || elementType === 'deleteQualityGate') {
        		pageUrl = $(element).data("url");
        		addEditOrDeleteCondition();
        		if(elementType === 'deleteQualityGate')
        		// notify
				notifyService.setNotification("Deleted", "", "success");
        	}
        }	
    };
});