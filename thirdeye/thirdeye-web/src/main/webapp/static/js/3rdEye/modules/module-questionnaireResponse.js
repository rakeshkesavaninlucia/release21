/**
 * @author samar.gupta
 */
Box.Application.addModule('module-questionnaireResponse', function(context) {
	'use strict';
	var $,$moduleDiv,notifyService;

	// private methods here

	function bindBlurToParameters(){
		$(".editableParameter", $(".userquestionnaire")).blur(function(){
			var $editedParam = $(this);
			var $formForParam = $editedParam.closest("form.editableForm");

			$formForParam.ajaxForm(function(htmlResponse) {							
				if (htmlResponse.indexOf("error") > -1 ) {								
					$("#"+htmlResponse.replace("error_", "")).html("Question Score can not be less than 1 and greater than 10.")
					.addClass("error_msg");
				}else{
					$("#"+htmlResponse.replace("success_", "")).empty();
				}						
			}); 

			$formForParam.submit();

		})
	}
	function tcoTable(){
		$('#coaList').DataTable({
			scrollY:    	500,
			scrollX:        true,
			scrollCollapse: true,
			paging:         false,
			bInfo : 		false,
			bLengthChange : false,
			bFilter :		false,
			ordering:		false,
			fixedColumns: true				
		} );
	}
	function onlyNumber(){
		$('.numeric').numeric();
	}

	return {
		behaviors : [ 'behavior-select2'],
		init : function(){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			$moduleDiv = context.getElement();
			notifyService = context.getService('service-notify');
			bindBlurToParameters();
			tcoTable();
			onlyNumber();
		} , // end of init
	};
});
