Box.Application.addModule('module-widgetDrag', function(context) {
	'use strict';
	
	var $, notifyService, commonUtils, $moduleDiv;
	
	function updateSequence(result){
		var dataObj = {};
		dataObj.sequence = result;
		var postRequest = $.post( commonUtils.getAppBaseURL("dashboard/" + $($moduleDiv).data('dashboardid') + "/updateSequence"), dataObj);
		postRequest.done(function(){
			notifyService.setNotification("Sequence Updated", "", "success");
		}).fail(function(){
			notifyService.setNotification("Something went wrong", "", "error");
		});
	};
	
	return{
		init:function(){
			$ = context.getGlobal('jQuery');
			commonUtils = context.getService('commonUtils');
			notifyService = context.getService('service-notify');
			$moduleDiv= context.getElement();
			
			var el = document.getElementById('wizDrag');
			Sortable.create(el, {
				animation: 150,
				draggable: '.widgetcontainer',
				handle: '.box-header',
				onSort: function (e) {
			        var items = e.to.children;
			        var result = [];
			        for (var i = 0; i < items.length; i++) {
			            result.push($(items[i].children[0]).data('id'));
			        }
			        /* Do ajax call here */
			        updateSequence(result);
			    }
			});
			
		}
	}
});