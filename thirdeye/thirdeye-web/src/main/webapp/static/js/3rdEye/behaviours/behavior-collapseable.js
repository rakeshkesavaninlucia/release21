/**
 * @author: ananta.sethi
 * Behavior for Collapse
 */
Box.Application.addBehavior('behavior-collapseable', function(context) {
	'use strict';
     var $;
	//private method
	function collapse(element) {
	//	var _this = this;

		// Find the box parent
		var box = element.parents(".box").first();
		// Find the body and the footer
		var box_content = box.find("> .box-body, > .box-footer");
		if (!box.hasClass("collapsed-box")) {
			// Convert minus into plus
			element.children(":first").removeClass('fa-minus').addClass(
					'fa-plus');
			// Hide the content
			box_content.slideUp(300, function() {
				box.addClass("collapsed-box");
			});
		} else {
			// Convert plus into minus
			element.children(":first").removeClass('fa-plus').addClass(
					'fa-minus');
			// Show the content
			box_content.slideDown(300, function() {
				box.removeClass("collapsed-box");
			});
		}
	}
return {
		init : function() {

			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			//var _this = this;
			$(context.getElement()).on("click", "[data-widget='collapse']", function(e) {
				// $(".box-tools").on('click','[data-widget="collapse"]',
				// function (e) {
				e.preventDefault();
				/*_this.*/collapse($(this));
			});
		},

	}
});