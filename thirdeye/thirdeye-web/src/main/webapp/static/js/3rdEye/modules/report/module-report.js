/**
 * This module is to populate the modal for saving reports.
 * @author: sunil1.gupta
 */
Box.Application.addModule('module-report', function(context) {
	 'use strict';
	 
	 var $, commonUtils, moduleConfig, notifyService;
	
    function popupSaveReportModal(url, modalId,data) {
		
	       commonUtils.getHTMLContent(commonUtils.getAppBaseURL(url), data).then(function(content){
			var $newModal = commonUtils.appendModalToPage(modalId, content, true);
			bindSaveReport($newModal, modalId);
			$newModal.modal({backdrop:false,show:true});
			
		});
	}
	
    function bindSaveReport ($newModal, modalId){
		// Bind the ajax form
		var formId = $newModal.find('form').attr('id');
		$("#"+formId, $newModal).ajaxForm(function(htmlResponse) {
			if(htmlResponse.redirect){
				notifyService.setNotification("Saved", "", "success");
				$newModal.modal('hide');
			}else{ 
				var $replacement = commonUtils.swapModalContent(modalId, htmlResponse);
				$replacement.modal({backdrop:false});
				bindSaveReport($replacement);
			}
		}); 
	}
    return {
    	messages: [ 'repo::savereport'],
		init: function(){
			$ = context.getGlobal('jQuery');
			// Accessing the service
			commonUtils = context.getService('commonUtils');
			moduleConfig = context.getConfig();
			notifyService = context.getService('service-notify');
		},
		onmessage: function(name, data) {
            if (name === 'repo::savereport') {
            	popupSaveReportModal(moduleConfig.url,moduleConfig.modal,data);
            }
        }
    };
});