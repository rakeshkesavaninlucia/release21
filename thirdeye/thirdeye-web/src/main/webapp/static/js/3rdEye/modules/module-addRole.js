/**
 * This module is to populate the modal for creating, viewing and editing the cost element modal.
 * @author: tanushree.mittal
 */
Box.Application.addModule('module-addRole', function(context) {

	 'use strict';

	var $;
	var commonUtils;
	var createUrl = "user/roles/lists";
	 
	function addNewRoleModal(){		
				
    	var url = commonUtils.getAppBaseURL(createUrl);
    	
		commonUtils.getHTMLContent(url).then(function(content){
			
			var $newModal = commonUtils.appendModalToPage("addNewRoleModal", content, true);
			bindNewRoleModalRequirements($newModal);
			$newModal.modal({backdrop:false,show:true});
		});
	}
	
	function bindNewRoleModalRequirements ($newModal){
		// Bind the ajax form
		$('#addNewRoleForm', $newModal).ajaxForm(function(htmlResponse) {

			if (htmlResponse.redirect){
				window.location.replace(commonUtils.getAppBaseURL(htmlResponse.redirect));
			} else{ 
				var $replacement = commonUtils.swapModalContent("addNewRoleModal", htmlResponse);
				$replacement.modal({backdrop:false});
				bindNewRoleModalRequirements($replacement);
			}
		}); 
	
	}
	
    return {

        init: function (){
        	// retrieve a reference to jQuery
            $ = context.getGlobal('jQuery');
            commonUtils = context.getService('commonUtils');  
            
            var clickHandlerConfiguration = {};
			clickHandlerConfiguration.deleteClass = "deleteRow";
			clickHandlerConfiguration.deleteURL = commonUtils.getAppBaseURL("user/deleteUserRole");
			$("#userManagementId").click(clickHandlerConfiguration,commonUtils.handleTableRowClick);
        },
            
        onclick: function(event, element, elementType) {
        	if (elementType === 'newRole') {           		
        	addNewRoleModal();
        	}
        }	
    };
});