/**
 * @author: ananta.sethi
 * module-to delete questionnaire 
 */
Box.Application.addModule('module-listQuestionnaire', function(context) {
	'use strict';
	var $,commonUtils;


	return {
		init : function() {
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			// Accessing the service
			commonUtils = context.getService('commonUtils');
			
			var clickHandlerConfiguration = {};
			clickHandlerConfiguration.deleteClass = "deleteRow";
			clickHandlerConfiguration.deleteURL = commonUtils.getAppBaseURL("questionnaire/delete");
			$("#templateColumnsofview").click(clickHandlerConfiguration,commonUtils.handleTableRowClick);
		}
	}
});
