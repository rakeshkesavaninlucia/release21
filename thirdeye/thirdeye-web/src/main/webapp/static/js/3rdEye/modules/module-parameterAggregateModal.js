/**
 * @author samar.gupta
 */

Box.Application.addModule('module-parameterAggregateModal', function(context) {

    // private methods here
	var $, commonUtils,moduleEl;
	var dataObj = {};
	var pageUrl = "parameter/ap/showModal";
	
	function showParameterSelectorModal(data){	
		 var modalId = moduleEl.id;
		 commonUtils.populateModalWithURL(modalId, commonUtils.getAppBaseURL(pageUrl),data)
		.then(function(){
			Box.Application.startAll(moduleEl);
			// Show the modal
			var $modalDiv = $(moduleEl).modal({
				keyboard: true
			})			
		});	
	}
	function hideParameterSelectorModal(){
		$(moduleEl).modal('hide');
	}
	function saveParametersTemporary(){		
		var table = $('#parameterListInModal').DataTable();
		var dataParameter = table.$('.parameterClass:checked').map(function() { return this.value; }).get();
		dataObj.parameter = dataParameter;
		var postRequest = $.post( commonUtils.getAppBaseURL("parameter/ap/saveTemporary"), dataObj);
		postRequest.done(function(htmlResponse){
			$('#selectedParameterTableId').remove();
			$( htmlResponse ).insertAfter( "#questionParamFooter" );
			$('.errorDiv').remove();
			hideParameterSelectorModal();
		})
	}
   
    return {

    	messages: [ 'pam::showParameterSelector' ],

        onmessage: function(name, data) {
            if (name === 'pam::showParameterSelector') {
            	showParameterSelectorModal(data);
            }
        },
        
        onclick: function(event, element, elementType) {        
        	if (elementType === 'saveParametersTemporary') {
        		saveParametersTemporary();
        		event.preventDefault();
        	}
        },
    
        init: function (){
        	// retrieve a reference to jQuery
            $ = context.getGlobal('jQuery');
            commonUtils = context.getService('commonUtils');
            moduleEl = context.getElement();
        },
        
        destroy: function() {
        	 dataObj = {};
        	 moduleEl= null;
       }
    };
});