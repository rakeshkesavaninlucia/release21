package org.birlasoft.thirdeye.constant;

public enum AssetTypes {

	RELATIONSHIP("Relationship");
	
	private String value;
	
	private AssetTypes(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
