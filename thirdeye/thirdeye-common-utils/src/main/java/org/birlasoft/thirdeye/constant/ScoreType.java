package org.birlasoft.thirdeye.constant;



/**
 * Enum for {@code question type} {@code selection}  
 */
public enum ScoreType {
	NA("N/A",-1),
	ONE("1", 1),
	TWO("2",2),
	THREE("3",3),
	FOUR("4",4),
	FIVE("5",5),
	SIX("6",6),
	SEVEN("7",7),
	EIGHT("8",8),
	NINE("9",9),
	TEN("10",10);	

	private  String value;
	private  int key;

	ScoreType(String value, int key){
		this.value = value;
		this.key = key;
	}

	public String getValue() {
        return value;
    } 
   
    public int getKey() {
        return key;
    }
   
}

