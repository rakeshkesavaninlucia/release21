package org.birlasoft.thirdeye.config;

public abstract class TransactionManagers {

	public static final String MASTERTRANSACTIONMANAGER = "masterTransactionManager";
	public static final String TENANTTRANSACTIONMANAGER = "tenantTransactionManager";
}
