package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Aid;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * @author samar.gupta
 *
 */
public interface AIDRepository extends JpaRepository<Aid, Serializable> {
	/**
	 * Find list of Aids by workspace In.
	 * @param workspaces
	 * @return {@code List<Aid>}
	 */
	public List<Aid> findByWorkspaceIn(Set<Workspace> workspaces);
	/**
	 * find list of aid by asset template.
	 * @param assetTemplate
	 * @return {@code List<Aid>}
	 */
	public List<Aid> findByAssetTemplate(AssetTemplate assetTemplate);
}
