package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;

import org.birlasoft.thirdeye.entity.Role;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository interface for role.
 */
public interface RoleRepository extends JpaRepository<Role, Serializable> {
	
	@EntityGraph(value = "Role.rolePermissions", type = EntityGraphType.LOAD)
	@Query("select r from Role r WHERE r.id = (:id)")
	public Role findByIdLoadedRolePermissions(@Param("id") Integer id);
	
	/** update delete status for role 
	 * @param userRoleId
	 */
	@Modifying
	@Query("UPDATE Role a SET a.deleteStatus=1 WHERE a.id =?1")
	public void updateDeleteStatus(Integer userRoleId);
	
	
	/** get role by delete status
	 * @param deleteStatus
	 * @return
	 */
	public List<Role> findByDeleteStatus(Boolean deleteStatus);
}
