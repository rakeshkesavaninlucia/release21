/**
 * 
 */
package org.birlasoft.thirdeye.repositories;

import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireParameterAsset;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author dhruv.sood
 *
 */
public interface QuestionnaireParameterAssetRepository extends JpaRepository<QuestionnaireParameterAsset, Integer> {

	public QuestionnaireParameterAsset findByQuestionnaireAndParameterAndAsset(Questionnaire questionnaire,
			Parameter parameter, Asset asset);
}
