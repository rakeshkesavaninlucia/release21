package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
/**
 * create {@code questionnaire}{@code repository}
 * @author sanjeev.mishra
 */
public interface QuestionnaireRepository extends JpaRepository<Questionnaire, Serializable> {
	
	/**
	 * List of all {@code questionnaire} within {@code user} {@code workspace}
	 * @param workspace
	 * @return
	 */
	public List<Questionnaire> findByWorkspaceIn(Set<Workspace> workspace);

	/**
	 * Find list of questionnaire by publish status and in active workspaces. 
	 * @param status
	 * @param workspaces
	 * @return {@code List<Questionnaire>}
	 */
	public List<Questionnaire> findByStatusAndWorkspaceIn(String status, Set<Workspace> workspaces);
	
	/**
	 * method to return list of {@code Questionnaire}
	 * @param status
	 * @return {@code List<Questionnaire>}
	 */
	public List<Questionnaire> findByStatus(String status);
	
	/**
	 * List of all {@code questionnaire} within {@code user} {@code workspace} and {@code questionnaireType}
	 * @param workspace
	 * @param questionnaireType
	 * @return {@code List<Questionnaire>}
	 */
	public List<Questionnaire> findByWorkspaceInAndQuestionnaireType(Set<Workspace> workspace,String questionnaireType);
	
	/**
	 * Find list of questionnaire by questionnaire type(default/TCO), publish status and in active workspaces
	 * @param type
	 * @param status
	 * @param workspaces
	 * @return
	 */
	public List<Questionnaire> findByQuestionnaireTypeAndStatusAndWorkspaceIn(String type, String status, Set<Workspace> workspaces);

	/** Find questionnaire by year and assetId 
	 * @param chartOfAccountYear
	 * @param assetTypeid
	 * @param workspace
	 * @return
	 */
	public Questionnaire findByYearAndAssetTypeIdAndWorkspace(String chartOfAccountYear, Integer assetTypeId,Workspace workSpace);
	
	/** update delete status
	 * @param questionnaireId
	 */
	@Modifying
	@Query("UPDATE Questionnaire a SET a.deleteStatus=1 WHERE a.id =?1")
	public void updateDeleteStatus(Integer questionnaireId);
	
	/** get list of questionnaire for a specific workspace,questioniare type and delete status
	 * @param workspace
	 * @param questionnaireType
	 * @param status
	 * @return
	 */
	public List<Questionnaire> findByWorkspaceInAndQuestionnaireTypeAndDeleteStatus(Set<Workspace> workspace, String questionnaireType,Boolean status);

}
	
	

