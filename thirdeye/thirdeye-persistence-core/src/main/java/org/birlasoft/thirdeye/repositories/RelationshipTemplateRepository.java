package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.RelationshipTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RelationshipTemplateRepository extends JpaRepository<RelationshipTemplate, Serializable> {
	public List<RelationshipTemplate> findByAssetTemplate(AssetTemplate assetTemplate);
	public List<RelationshipTemplate> findByAssetTemplateIn(Set<AssetTemplate> setOfAssetTemplate);
}
