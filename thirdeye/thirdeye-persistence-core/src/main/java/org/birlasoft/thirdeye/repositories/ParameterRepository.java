package org.birlasoft.thirdeye.repositories;

import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Workspace;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository interface for parameter.
 */
public interface ParameterRepository extends JpaRepository<Parameter, Integer> {
	
	/**
	 * Get Parameters by id's
	 * @param idsOfParameter
	 * @return
	 */
	public List<Parameter> findByIdIn(Set<Integer> idsOfParameter);
	
	/**
	 * Get Parameters by workspace or system level
	 * @param workspaces
	 * @return
	 */
	@EntityGraph(value = "Parameter.parameterFunctionsForParentParameterId", type = EntityGraphType.LOAD)
	public Set<Parameter> findByWorkspaceIsNullOrWorkspaceIn(Set<Workspace> workspaces);
	
	/**
	 * Get Parameters by workspace or system level. Workspace loaded
	 * @param workspace
	 * @return
	 */
	@Query("SELECT p FROM Parameter p LEFT JOIN FETCH p.parameter WHERE p.workspace is null or p.workspace = (:workspace)")
	public List<Parameter> findByWorkspaceIsNullOrWorkspaceLoaded(@Param("workspace") Workspace workspace);
	
	/**
	 * Get all system level parameters
	 * @return
	 */
	@Query("SELECT p FROM Parameter p LEFT JOIN FETCH p.parameter WHERE p.workspace is null")
	public List<Parameter> findByWorkspaceIsNull();
	
	/**
	 * Get parameters by workspace
	 * @param activeWorkSpaceForUser
	 * @return
	 */
	public List<Parameter> findByWorkspace(Workspace activeWorkSpaceForUser);
	
	/**
	 * Get list of parameters and parameter functions by parameter
	 * @param parentParameter
	 * @return
	 */
	@EntityGraph(value = "Parameter.parameterFunctionsForParentParameterId", type = EntityGraphType.LOAD)
	@Query("SELECT p FROM Parameter p WHERE p.parameter = (:param)")
	public Set<Parameter> findByParameterLoadedFunctions(@Param("param") Parameter parentParameter);

	/**
	 * Get parameter by workspace and type
	 * @param listOfWorkspaces
	 * @param type
	 * @return
	 */
	public Set<Parameter> findByWorkspaceInAndType(Set<Workspace> listOfWorkspaces, String type);
	
	/**
	 * Get parameter by workspace/system level and type not in.
	 * @param workspace
	 * @param type
	 * @return
	 */
	@Query("SELECT p FROM Parameter p LEFT JOIN FETCH p.parameter WHERE p.type in (:type) and (p.workspace is null or p.workspace = (:workspace))")
	public List<Parameter> findByWorkspaceIsNullOrWorkspaceLoadedAndType(@Param("workspace") Workspace workspace,@Param("type") List<String> type);
    
	/**
	 * Get system level parameters by type not in
	 * @param type
	 * @return
	 */
	@Query("SELECT p FROM Parameter p LEFT JOIN FETCH p.parameter WHERE p.workspace is null and p.type in (:type)")
	public List<Parameter> findByWorkspaceIsNullAndType(@Param("type") List<String> type);
	
	/**
	 * Fetch set of parameters by workspace is null or active workspace
	 * and parameter types
	 * @param workspaces
	 * @param types
	 * @return {@code Set<Parameter>}
	 */
	@Query("SELECT p FROM Parameter p LEFT JOIN FETCH p.parameterFunctionsForParentParameterId WHERE p.type in (:types) and (p.workspace is null or p.workspace in (:workspaces))")
	public Set<Parameter> findByWorkspaceIsNullOrWorkspaceInAndTypeIn(@Param("workspaces") Set<Workspace> workspaces, @Param("types") List<String> types);
}
