package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;

import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterQualityGate;
import org.birlasoft.thirdeye.entity.QualityGate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository interface for ParameterQualityGate.
 * @author shaishav.dixit
 *
 */
public interface ParameterQualityGateRepository extends JpaRepository<ParameterQualityGate, Serializable> {
	
	/**
	 * Get {@link ParameterQualityGate} by {@link Parameter}
	 * @param parameter
	 * @return
	 */
	public ParameterQualityGate findByParameter(Parameter parameter);
	
	/**
	 * Get {@link ParameterQualityGate} by {@link QualityGate}
	 * @param qualityGate
	 * @return
	 */
	public ParameterQualityGate findByQualityGate(QualityGate qualityGate);
	
	/**
	 * Get {@code List} of {@link QualityGate} from {@code ParameterQualityGate} table by {@code List} of {@link QualityGate}
	 * @param listOfQualityGates
	 * @return
	 */
	@Query("SELECT pqg.qualityGate FROM ParameterQualityGate pqg where pqg.qualityGate in (:qg) ")
	public List<QualityGate> findByQualityGateIn(@Param("qg") List<QualityGate> listOfQualityGates);

}
