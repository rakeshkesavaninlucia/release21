/**
 * 
 */
package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.FunctionalMap;
import org.birlasoft.thirdeye.entity.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author shaishav.dixit
 *
 */
public interface FunctionalMapRepository extends JpaRepository<FunctionalMap, Serializable> {
	
	/** 
	 * @param workspace
	 * @return
	 */
	public List<FunctionalMap> findByWorkspace(Workspace workspace);
	
	/** 
	 * @param workspace
	 * @param levelTypes
	 * @return
	 */
	public List<FunctionalMap> findByWorkspaceAndTypeNotIn(Workspace workspace, Set<String> levelTypes);
	/**
	 * find list of FunctionalMap object by asset template.
	 * @param assetTemplate
	 * @return {@code List<FunctionalMap>}
	 */
	public List<FunctionalMap> findByAssetTemplate(AssetTemplate assetTemplate);
}
