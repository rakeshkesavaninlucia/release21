package org.birlasoft.thirdeye.service.impl;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.constant.ParameterType;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterFunction;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.service.ParameterFunctionService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CostStructureServiceImplTest {

	private static final int ID = 1;
	
	//User
	private static final String FIRST_NAME = "Demo First";
	private static final String LAST_NAME = "Demo Last";
	private static final String EMAIL = "demo@birlasoft.com";
	private static final String PASSWORD = "welcome12#";
	
	@Mock
	private ParameterService parameterService;
	private ParameterFunctionService parameterFunctionService = Mockito.mock(ParameterFunctionService.class);
	@InjectMocks
	private CostStructureServiceImpl costStructureServiceImpl = new CostStructureServiceImpl();

	private Parameter dummyParameter;
	private User dummyCurrentUser;
	private ParameterBean dummyParameterBean;
	private ParameterFunction dummyParameterFunction;
	
	private List<ParameterFunction> dummyListOfParameterFunction;
	private Set<ParameterFunction> dummySetOfParameterFunction;

	@Before
	public void setup() {
		
		// Create user
		dummyCurrentUser = new User();
		dummyCurrentUser.setId(ID);
		dummyCurrentUser.setFirstName(FIRST_NAME);
		dummyCurrentUser.setLastName(LAST_NAME);
		dummyCurrentUser.setEmailAddress(EMAIL);
		dummyCurrentUser.setPassword(PASSWORD);
		
		// Create Parameter/Cost Structure
		dummyParameter = new Parameter();
		dummyParameter.setId(ID);
		dummyParameter.setType(ParameterType.TCOA.toString());
		
		// Create Parameter Bean
		dummyParameterBean = new ParameterBean();
		dummyParameterBean.setId(ID);
		dummyParameterBean.setDisplayName("Test Demo");
		dummyParameterBean.setDescription("Test Description");
		
		// Create Parameter Function
		dummyParameterFunction = new ParameterFunction();
		dummyParameterFunction.setId(ID);
		
		// Create list of parameter function
		dummyListOfParameterFunction = new ArrayList<>();
		dummyListOfParameterFunction.add(dummyParameterFunction);
		
		// Create set of parameter function
		dummySetOfParameterFunction = new HashSet<>();
		dummySetOfParameterFunction.add(dummyParameterFunction);
	}
	
	@Test
	public void testUpdateCostStructure(){
		Mockito.when(parameterService.findOne(ID)).thenReturn(dummyParameter);
		dummyParameter.setDisplayName(dummyParameterBean.getDisplayName());
		dummyParameter.setDescription(dummyParameterBean.getDescription());
		dummyParameter.setCreatedDate(new Date());
		dummyParameter.setUserByCreatedBy(dummyCurrentUser);
		Parameter returnedParameter = costStructureServiceImpl.updateCostStructure(dummyParameterBean, dummyCurrentUser);
		assertNotNull(returnedParameter);
	}
	
	@Test
	public void testDeleteParamFunctionByParent(){
		Mockito.when(parameterFunctionService.findByParameterByParentParameterId(dummyParameter)).thenReturn(dummyListOfParameterFunction);
		Mockito.doNothing().when(parameterFunctionService).deleteInBatch(dummyListOfParameterFunction);
		costStructureServiceImpl.deleteParamFunctionByParent(dummyParameter);
	}
	
	@Test
	public void testDeleteParamFunctionByChild(){
		Mockito.when(parameterFunctionService.findByParameterByChildparameterId(dummyParameter)).thenReturn(dummySetOfParameterFunction);
		Mockito.doNothing().when(parameterFunctionService).deleteInBatch(dummyListOfParameterFunction);
		costStructureServiceImpl.deleteParamFunctionByChild(dummyParameter);
	}
}
