package org.birlasoft.thirdeye.service.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;




import org.birlasoft.thirdeye.beans.JSONQualityGateDescriptionMapper;
import org.birlasoft.thirdeye.beans.JSONQualityGateDescriptionValueMapper;
import org.birlasoft.thirdeye.beans.QualityGateBean;


import org.birlasoft.thirdeye.entity.QualityGate;


import org.birlasoft.thirdeye.repositories.QualityGateRepository;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.QualityGateService;
import org.birlasoft.thirdeye.util.Utility;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class QualityGateDescriptionServiceImplTest {

	@Mock
	private CustomUserDetailsService customerUserDetailsService;
	@Mock
	private QualityGateRepository qualityGateRepository;
	@InjectMocks
	private QualityGateService qualityGateService = new QualityGateServiceImpl();
	
	private QualityGate qualityGate; 
	private QualityGate qualityGate2;
	private JSONQualityGateDescriptionMapper jSONQualityGateDescriptionMapper;
	private JSONQualityGateDescriptionValueMapper jSONQualityGateDescriptionValueMapper;
	private List<String> description = new ArrayList<String>();
	private List<String> color = new ArrayList<String>();
	private Integer id;

	@Before
	public void setup() {
		qualityGate = new QualityGate();
		qualityGate.setId(10);

		description.add("Poor");
		description.add("Medium");
		description.add("Good");

		color.add("Red");
		color.add("Yellow");
		color.add("Green");

		setUpQualityGateBean(description,color);
		
		id = 11;
	}

	private void setUpQualityGateBean(List<String> listOfDescription,List<String> listOfColors){
		qualityGate2 = new QualityGate();
		qualityGate2.setId(11);

		jSONQualityGateDescriptionMapper = new JSONQualityGateDescriptionMapper();
		int ctr = 0;
		for (String color : listOfColors) {
			jSONQualityGateDescriptionValueMapper = new JSONQualityGateDescriptionValueMapper();
			jSONQualityGateDescriptionValueMapper.setColor(color);
			jSONQualityGateDescriptionValueMapper.setDescription(listOfDescription.get(ctr));
			ctr++;
			jSONQualityGateDescriptionMapper.addOption(jSONQualityGateDescriptionValueMapper);
		}
		qualityGate2.setColorDescription(Utility.convertObjectToJSONString(jSONQualityGateDescriptionMapper));
	}
	
	private void mockServiceClassMethods() {
	Mockito.when(qualityGateRepository.findOne(id)).thenReturn(qualityGate2);
	}
	
	@Test
	public void insertDescriptionMapperList() {

		qualityGate = qualityGateService.insertDescriptionMapperList(qualityGate, description, color);
		Assert.assertFalse(qualityGate.getColorDescription().isEmpty());
	}

	@Test
	public void createQGBean() {

		QualityGateBean qualityGateBean2 = qualityGateService.createQGBean(qualityGate2);
		Assert.assertFalse(qualityGateBean2.getDescription().isEmpty());
	}
	
	@Test
	public void getQGBean() {
		mockServiceClassMethods();
		
		QualityGateBean qualityGateBean2 = qualityGateService.getQGBean(id);
		Assert.assertFalse(qualityGateBean2.getDescription().isEmpty());
	}
}
