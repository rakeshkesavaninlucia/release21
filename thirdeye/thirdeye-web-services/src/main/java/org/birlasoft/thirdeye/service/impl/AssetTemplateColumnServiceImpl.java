package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.comparator.SequenceNumberComparator;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.DataType;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.AssetTemplateColumn;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.repositories.AssetDataRepository;
import org.birlasoft.thirdeye.repositories.AssetTemplateColumnRepository;
import org.birlasoft.thirdeye.service.AssetTemplateColumnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service {@code class} for Asset template column save, update, delete and find.
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class AssetTemplateColumnServiceImpl implements AssetTemplateColumnService {

	private AssetTemplateColumnRepository assetTemplateColumnRepository;
	private AssetDataRepository assetDataRepository;
	
	/**
	 * Constructor autowiring of dependencies
	 * @param assetTemplateColumnRepository
	 * @param assetDataRepository
	 */
	@Autowired
	public AssetTemplateColumnServiceImpl(AssetTemplateColumnRepository assetTemplateColumnRepository,
			AssetDataRepository assetDataRepository) {
		this.assetTemplateColumnRepository = assetTemplateColumnRepository;
		this.assetDataRepository = assetDataRepository;
	}

	@Override
	public AssetTemplateColumn findOne(Integer id) {
		return assetTemplateColumnRepository.findOne(id);
	}

	@Override
	public AssetTemplateColumn save(AssetTemplateColumn assetTemplateColumn) {
		return assetTemplateColumnRepository.save(assetTemplateColumn);
	}

	@Override
	public int findMaxSequenceNumberInAssetTemplateColumn(AssetTemplate cachedTemplate) {
		// find Max sequence number in asset template columns
		int maxSequenceNumber = 0;
		Set<AssetTemplateColumn> assetTemplateColumns = cachedTemplate.getAssetTemplateColumns();
		for(AssetTemplateColumn assetTemplateCol : assetTemplateColumns){
			if(assetTemplateCol != null){
				int currentSequenceNumber = assetTemplateCol.getSequenceNumber();
				if(maxSequenceNumber < currentSequenceNumber)
					maxSequenceNumber = currentSequenceNumber;
			}
		}
		return maxSequenceNumber+1;
	}
	
	@Override
	public Set<AssetTemplateColumn> sortTemplateColumnsBySequenceNumber(Set<AssetTemplateColumn> assetTemplateColumns) {
		List<AssetTemplateColumn> sortedListOfAssetTemplateColumn = getSortedListOfTemplateColumn(assetTemplateColumns);
		return new LinkedHashSet<>(sortedListOfAssetTemplateColumn);
	}

	@Override
	public AssetTemplateColumn createAssetTemplateColumnObject(AssetTemplateColumn assetTemplateColumn, AssetTemplate cachedTemplate, User currentUser) {
		assetTemplateColumn.setSequenceNumber(findMaxSequenceNumberInAssetTemplateColumn(cachedTemplate));
		assetTemplateColumn.setAssetTemplate(cachedTemplate);
		assetTemplateColumn.setUserByCreatedBy(currentUser);
		assetTemplateColumn.setUserByUpdatedBy(currentUser);
		assetTemplateColumn.setCreatedDate(new Date());
		assetTemplateColumn.setUpdatedDate(new Date());
		return assetTemplateColumn;
	}

	@Override
	public List<AssetTemplateColumn> getSortedListOfTemplateColumn(Set<AssetTemplateColumn> assetTemplateColumns) {
		List<AssetTemplateColumn> listOfAssetTemplateColumn = new ArrayList<>(assetTemplateColumns);
		Collections.sort(listOfAssetTemplateColumn, new SequenceNumberComparator());
		return listOfAssetTemplateColumn;
	}

	@Override
	public AssetTemplateColumn createDefaultAssetTemplateColumn(AssetTemplate assetTemplate, User currentUser) {
		AssetTemplateColumn assetTemplateColumn = new AssetTemplateColumn();
		assetTemplateColumn.setAssetTemplateColName(Constants.DEFAULT_COLUMN_NAME);
		assetTemplateColumn.setDataType(DataType.TEXT.name());
		assetTemplateColumn.setLength(Constants.DEFAULT_COLUMN_LENGTH);
		assetTemplateColumn.setSequenceNumber(1);
		assetTemplateColumn.setAssetTemplate(assetTemplate);
		assetTemplateColumn.setMandatory(true);
		assetTemplateColumn.setCreatedDate(new Date());
		assetTemplateColumn.setUpdatedDate(new Date());
		assetTemplateColumn.setUserByCreatedBy(currentUser);
		assetTemplateColumn.setUserByUpdatedBy(currentUser);
		return assetTemplateColumn;
	}

	@Override
	public void deleteAssetData(Integer idOfTemplateColumn) {
		AssetTemplateColumn assetTemplateColumn = assetTemplateColumnRepository.findById(idOfTemplateColumn);
		if(!assetTemplateColumn.getAssetDatas().isEmpty()){
			assetDataRepository.deleteInBatch(assetTemplateColumn.getAssetDatas());
		}
	}

	@Override
	public void delete(AssetTemplateColumn assetTemplateColumn) {
		assetTemplateColumnRepository.delete(assetTemplateColumn);
	}

	@Override
	public void updateSequenceNumber(List<AssetTemplateColumn> listOfTemplateColumnToUpdate) {
		Collections.sort(listOfTemplateColumnToUpdate, new SequenceNumberComparator());
		int count = 1;
		for (AssetTemplateColumn atc : listOfTemplateColumnToUpdate) {
			atc.setSequenceNumber(count);
			count++;
		}
		saveInBatch(listOfTemplateColumnToUpdate);
	}

	@Override
	public List<AssetTemplateColumn> saveInBatch(List<AssetTemplateColumn> templateColumns) {
		return assetTemplateColumnRepository.save(templateColumns);
	}

	@Override
	public AssetTemplateColumn findOneLoadedAssetTemplate(Integer idOfTemplateColumn) {
		return assetTemplateColumnRepository.findOneLoadedAssetTemplate(idOfTemplateColumn);
	}

	@Override
	public List<AssetTemplateColumn> findByAssetTemplate(AssetTemplate assetTemplate) {
		return assetTemplateColumnRepository.findByAssetTemplate(assetTemplate);
	}

	@Override
	public void deleteInBatch(List<AssetTemplateColumn> deleteAssetTemplateColumns) {
		assetTemplateColumnRepository.deleteInBatch(deleteAssetTemplateColumns);

	}
}
