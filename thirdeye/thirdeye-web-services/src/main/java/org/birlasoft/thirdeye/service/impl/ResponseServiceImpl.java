package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.birlasoft.thirdeye.beans.JSONBenchmarkMultiChoiceQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONDateResponseMapper;
import org.birlasoft.thirdeye.beans.JSONMultiChoiceQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONMultiChoiceResponseMapper;
import org.birlasoft.thirdeye.beans.JSONNumberResponseMapper;
import org.birlasoft.thirdeye.beans.JSONParaTextResponseMapper;
import org.birlasoft.thirdeye.beans.JSONQuestionOptionMapper;
import org.birlasoft.thirdeye.beans.JSONTextResponseMapper;
import org.birlasoft.thirdeye.beans.QuestionnaireQuestionBean;
import org.birlasoft.thirdeye.beans.QuestionnaireQuestionGridBean;
import org.birlasoft.thirdeye.beans.QuestionnaireQuestionResponseBean;
import org.birlasoft.thirdeye.beans.QuestionnaireQuestionWrapper;
import org.birlasoft.thirdeye.beans.tco.ChartOfAccountGridBean;
import org.birlasoft.thirdeye.beans.tco.ChartOfAccountGridWrapper;
import org.birlasoft.thirdeye.beans.tco.ChartOfAccountResponseBean;
import org.birlasoft.thirdeye.comparator.SequenceNumberComparator;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionType;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.Response;
import org.birlasoft.thirdeye.entity.ResponseData;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.repositories.ResponseDataRepository;
import org.birlasoft.thirdeye.repositories.ResponseRepository;
import org.birlasoft.thirdeye.service.QuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.ResponseService;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author samar.gupta
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class ResponseServiceImpl implements ResponseService {

	@Autowired
	private ResponseRepository responseRepository;
	@Autowired
	private ResponseDataRepository responseDataRepository;
	@Autowired
	private QuestionnaireQuestionService questionnaireQuestionService;
	@Autowired
	private QuestionService questionService;


	@Override
	public List<Response> findByQuestionnaire(Questionnaire qe, boolean loaded) {
		List<Response> listOfResponse = responseRepository.findByQuestionnaire(qe);

		if(loaded){
			for (Response response : listOfResponse){
				Set<QuestionnaireParameter> questionnaireParameters = response.getQuestionnaire().getQuestionnaireParameters();
				response.getQuestionnaire().getQuestionnaireParameters().size();
				for (QuestionnaireParameter qp : questionnaireParameters) {
					qp.getParameterByParameterId().getDisplayName();
				}
				response.getQuestionnaire().getQuestionnaireQuestions().size();
				Set<QuestionnaireQuestion> questionnaireQuestions = response.getQuestionnaire().getQuestionnaireQuestions();
				for (QuestionnaireQuestion questionnaireQuestion : questionnaireQuestions) {
					questionnaireQuestion.getQuestion().getQuestionType();
					questionnaireQuestion.getQuestion().getTitle();
				}
				response.getQuestionnaire().getQuestionnaireAssets().size();
				response.getQuestionnaire().getQuestionnaireQuestions().size();
				Set<QuestionnaireQuestion> qqs = response.getQuestionnaire().getQuestionnaireQuestions();
				for (QuestionnaireQuestion qq : qqs) {
					qq.getResponseDatas().size();
				}
			}	
		}
		return listOfResponse;
	}

	@Override
	public Response save(Response r) {
		return responseRepository.save(r);
	}

	@Override
	public Response findOne(Integer responseId) {
		return responseRepository.findOne(responseId);
	}

	@Override
	public Response createNewResponseObject(Questionnaire qe, User currentUser) {
		// Create a new response and save it
		Response newResponse = new Response();
		newResponse.setCreatedDate(new Date());
		newResponse.setUpdatedDate(new Date());
		newResponse.setUserByCreatedBy(currentUser);
		newResponse.setUserByUpdatedBy(currentUser);
		newResponse.setQuestionnaire(qe);
		return newResponse;
	}

	@Override
	public Map<Integer, Integer> getMapForCompletionPercentageOfQe(List<Questionnaire> listOfquestionnaires) {
		Map<Integer,Integer> completionPercentage = new HashMap<>();

		for (Questionnaire oneQe : listOfquestionnaires){
			int numberOfResponses = oneQe.getResponses().size();
			List<QuestionnaireQuestion> qq = questionnaireQuestionService.findByQuestionnaire(oneQe, true);
			int numberOfQQ = qq.size();

			int numberOfResponseData = 0;
			for (QuestionnaireQuestion oneQQ : qq){
				numberOfResponseData = numberOfResponseData + oneQQ.getResponseDatas().size();
			}

			int finalPercentage = 0;
			if (numberOfResponses>0 && numberOfQQ >0){
				finalPercentage = (numberOfResponseData*100)/(numberOfQQ * numberOfResponses);
			}

			completionPercentage.put(oneQe.getId(), finalPercentage);
		}
		return completionPercentage;
	}

	private ResponseData insertResponseIntoResponseData(String[] newValuesToBeStored, QuestionnaireQuestion qqAnswered, ResponseData alreadyRespondedData, String otherOptionText) {
		// Need to find out options that were selected

		String jSONStringOfQuestion = qqAnswered.getQuestion().getQueTypeText();

		String queType = qqAnswered.getQuestion().getQuestionType();
		if (jSONStringOfQuestion == null){
			if(queType.equals(QuestionType.TEXT.toString())){
				JSONTextResponseMapper jsonTextResponseMapper = new JSONTextResponseMapper();
				jsonTextResponseMapper.setResponseText(alreadyRespondedData.getResponseData());
				alreadyRespondedData.setResponseData(Utility.convertObjectToJSONString(jsonTextResponseMapper));
			}else if (queType.equals(QuestionType.PARATEXT.toString())){
				JSONParaTextResponseMapper jsonParaTextResponseMapper = new JSONParaTextResponseMapper();
				jsonParaTextResponseMapper.setResponseParaText(alreadyRespondedData.getResponseData());
				alreadyRespondedData.setResponseData(Utility.convertObjectToJSONString(jsonParaTextResponseMapper));
			}else if (queType.equals(QuestionType.DATE.toString())){
				JSONDateResponseMapper jsonDateResponseMapper = new JSONDateResponseMapper();
				jsonDateResponseMapper.setResponseDate(alreadyRespondedData.getResponseData());
				alreadyRespondedData.setResponseData(Utility.convertObjectToJSONString(jsonDateResponseMapper));
			}
		}else {
			if(queType.equals(QuestionType.MULTCHOICE.toString())){
				JSONMultiChoiceQuestionMapper jsonMultiChoiceQuestionMapper;
				if(qqAnswered.getQuestion().getBenchmark() == null)
					jsonMultiChoiceQuestionMapper = Utility.convertJSONStringToObject(jSONStringOfQuestion, JSONMultiChoiceQuestionMapper.class);
				else {
					jsonMultiChoiceQuestionMapper = questionService.convertBenchmarkMCQintoNormalMCQ(Utility.convertJSONStringToObject(jSONStringOfQuestion, JSONBenchmarkMultiChoiceQuestionMapper.class));
				}
				JSONMultiChoiceResponseMapper jsonMultiChoiceResponseMapper = new JSONMultiChoiceResponseMapper();
				//for (ResponseData oneVal : alreadyRespondedData.getResponse()){
					// We need to see which options were selected
					for (JSONQuestionOptionMapper oneOption : jsonMultiChoiceQuestionMapper.getOptions()){
						if (oneOption.getText().equals(alreadyRespondedData.getResponseData())){
							jsonMultiChoiceResponseMapper.addOption(oneOption);
						}
					}
				//}
				if(alreadyRespondedData.getResponseData().equalsIgnoreCase(Constants.OTHER_TEXT)){
					jsonMultiChoiceResponseMapper.setOtherOptionResponseText(otherOptionText);
				}

				alreadyRespondedData.setResponseData(Utility.convertObjectToJSONString(jsonMultiChoiceResponseMapper));
			}
			else if(queType.equals(QuestionType.NUMBER.toString()) ){
				JSONNumberResponseMapper jsonNumberResponseMapper = new JSONNumberResponseMapper();
				jsonNumberResponseMapper.setResponseNumber(!alreadyRespondedData.getResponseData().isEmpty() ? Double.parseDouble(alreadyRespondedData.getResponseData()) : null);
				alreadyRespondedData.setResponseData(Utility.convertObjectToJSONString(jsonNumberResponseMapper));
			}
			else if(queType.equals(QuestionType.CURRENCY.toString())){
				JSONNumberResponseMapper jsonNumberResponseMapper = new JSONNumberResponseMapper();
				jsonNumberResponseMapper.setQuantifier(!alreadyRespondedData.getResponseData().isEmpty() ? Double.parseDouble(alreadyRespondedData.getResponseData()) : null);
				jsonNumberResponseMapper.setResponseNumber(!alreadyRespondedData.getResponseData().isEmpty() ? Double.parseDouble(alreadyRespondedData.getResponseData()):null);
				alreadyRespondedData.setResponseData(Utility.convertObjectToJSONString(jsonNumberResponseMapper));
			}
		}

		return alreadyRespondedData;
	}

	@Override
	public List<ResponseData> findByResponse(Response response) {
		return responseDataRepository.findByResponse(response);
	}

	@Override
	public List<QuestionnaireQuestionBean> prepareQQsListForNQRQ(Questionnaire qe) {
		List<QuestionnaireQuestionBean> qqbeanListForGUI = new ArrayList<>();
		List<Response> responses = this.findByQuestionnaire(qe, true);
		for (Response response : responses) {
			Set<ResponseData> responseDatas = response.getResponseDatas();
			for (ResponseData responseData : responseDatas) {
				qqbeanListForGUI.add(new QuestionnaireQuestionBean(responseData.getQuestionnaireQuestion()));
			}
		}

		Collections.sort(qqbeanListForGUI, new SequenceNumberComparator());
		return qqbeanListForGUI;
	}

	@Override
	public List<Response> findByQuestionnaireIn(List<Questionnaire> qes) {
		return responseRepository.findByQuestionnaireIn(qes);
	}

	@Override
	public Response findByIdLoadedResponseDatas(Integer id) {
		return responseRepository.findByIdLoadedResponseDatas(id);
	}

	@Override
	public List<ResponseData> getListOfResponseData(String[] newValuesToBeStored, Integer idOfQQ, ResponseData alreadyRespondedData, String otherOptionText){
		// get QuestionnaireQuestion
		QuestionnaireQuestion qq = questionnaireQuestionService.findOne(idOfQQ,true);

		// To set responseData for even the duplicate record (i.e duplicate questionId for one questionnaireassetId),
		// get all qq where group of questionId and questionnaireAssetId is same.  
		List<QuestionnaireQuestion> listOfqq = questionnaireQuestionService.findByQuestionIdAndQuestionnaireAssetId(qq.getQuestionnaireAsset(), qq.getQuestion());
		List<ResponseData> listOfResponseData =new ArrayList<>();

		ResponseData responseData;
		for(QuestionnaireQuestion oneqq: listOfqq){
			responseData = responseDataRepository.findByQuestionnaireQuestionId(oneqq);
			if(responseData != null){
				//update responseData even for duplicate QuestionnaireQuestion
				responseData.setResponseData(alreadyRespondedData.getResponseData());
			}
			else{
				//Set response and questionnaireQuestion if responseData is not for duplicate questionnaireQuestion
				responseData = new ResponseData();
				responseData.setQuestionnaireQuestion(oneqq);
				responseData.setResponse(alreadyRespondedData.getResponse());
			}
			listOfResponseData.add(insertResponseIntoResponseData(newValuesToBeStored, oneqq, responseData, otherOptionText ));
		}
		return listOfResponseData;
	}

	@Override
	public void saveOneQQResponse(HttpServletRequest request, Integer responseId, String otherOptionText) {
		// Pull the fully loaded response from the data base
		Response activeResponse = findByIdLoadedResponseDatas(responseId);
		// This will have the response data and QQ loaded		
		// Quick check on the qqid's which are already available
		// The map is of QQID and the associated response data
		Map<Integer, ResponseData> qqMap = new HashMap<>();
		for (ResponseData oneResponseData : activeResponse.getResponseDatas()){
			qqMap.put(oneResponseData.getQuestionnaireQuestion().getId(),oneResponseData);
		}		
		List<String> requestParameterNames = Collections.list((Enumeration<String>) request.getParameterNames());
		for(String oneParam : requestParameterNames){
			String[] responseString = request.getParameterValues(oneParam); 
			if (oneParam.startsWith("qq_")){
				// Find out which qq the user is answering
				String cleansedParam = oneParam.replaceAll("qq_", "");
				// The cleansed param is an Integer
				Integer idOfQQ = Integer.parseInt(cleansedParam);
				if (qqMap.containsKey(idOfQQ)){
					// We already have the response data and that has to be updated.
					ResponseData alreadyRespondedData = qqMap.get(idOfQQ);
					List<ResponseData> listOfAlreadyRespondedData= getListOfResponseData(responseString, idOfQQ, alreadyRespondedData, otherOptionText);
					saveOrDelete(responseString, listOfAlreadyRespondedData);
				}else{ 
					// create a new response data if responseString is not null or blank
					createNewResponseData(otherOptionText, activeResponse, responseString, idOfQQ);
				}
			}
		}
		activeResponse.setUpdatedDate(new Date());
		responseRepository.save(activeResponse);

	}

	/**
	 * Method to create a new response data if responseString is not null or blank
	 * @param otherOptionText
	 * @param activeResponse
	 * @param responseString
	 * @param idOfQQ
	 */
	private void createNewResponseData(String otherOptionText, Response activeResponse, String[] responseString,
			Integer idOfQQ) {
		if(responseString[0]!=null && !responseString[0].isEmpty() ){ 
			ResponseData newResponseData = new ResponseData();
			List<ResponseData> listOfResponseData = getListOfResponseData(responseString, idOfQQ, newResponseData, otherOptionText);
			for(ResponseData oneResponseData :listOfResponseData ){
				oneResponseData.setResponse(activeResponse);
				activeResponse.getResponseDatas().add(oneResponseData);
			}
			responseDataRepository.save(listOfResponseData);
		}
	}
	/**
	 * method to save or delete response data based on responseString is not null or empty
	 * @param responseString
	 * @param listOfAlreadyRespondedData
	 */
	private void saveOrDelete(String[] responseString, List<ResponseData> listOfAlreadyRespondedData) {
		if(responseString[0]!=null && !responseString[0].isEmpty() ){
			responseDataRepository.save(listOfAlreadyRespondedData);
		}else{
			responseDataRepository.delete(listOfAlreadyRespondedData);	
		}
	}

	@Override
	public List<Integer> saveTcoResponse(Integer responseId, ChartOfAccountGridWrapper chartOfAccountGridWraper) {

		// Pull the fully loaded response from the data base
		Response activeResponse = findByIdLoadedResponseDatas(responseId);
		List<ResponseData> listOfDeleteAlreadyRespondedData =  new ArrayList<>();
		//List<ResponseData> listOfAlreadyRespondedData= getListofChartOfAccountResponse(chartOfAccountGridWraper,activeResponse,listOfDeleteAlreadyRespondedData);
		Map<String,Object> tcoMap = getListofChartOfAccountResponse(chartOfAccountGridWraper,activeResponse,listOfDeleteAlreadyRespondedData) ;
		@SuppressWarnings("unchecked")
		List<ResponseData> listOfAlreadyRespondedData = (List<ResponseData>)tcoMap.get("responseList") ;
		responseDataRepository.save(listOfAlreadyRespondedData);

		if(!listOfDeleteAlreadyRespondedData.isEmpty())
			responseDataRepository.delete(listOfDeleteAlreadyRespondedData);

		activeResponse.setUpdatedDate(new Date());
		responseRepository.save(activeResponse);
		
		return (List<Integer>)tcoMap.get("assetIdLst") ;
	}
	private  Map<String,Object> getListofChartOfAccountResponse(ChartOfAccountGridWrapper chartOfAccountGridWraper,Response activeResponse,List<ResponseData> listOfDeleteAlreadyRespondedData){

		Map<String,Object> tcoMap = new HashMap<>() ;
		List<ResponseData> listOfResponseData =new ArrayList<>();
		List<Integer> assetIdLst = new ArrayList<>() ;
		for(ChartOfAccountResponseBean oneresponse:chartOfAccountGridWraper.getCostResponse()){
			for(ChartOfAccountGridBean onegridbean:oneresponse.getCostResponses()){
				ResponseData responseData= new ResponseData();
				// get QuestionnaireQuestion
				QuestionnaireQuestion oneQQ = questionnaireQuestionService.findOne(onegridbean.getQuestionnaireQuestionId(), true);
				assetIdLst.add(oneQQ.getQuestionnaireAsset().getAsset().getId()) ;
				// get already response data
				ResponseData alreadyResponsededData = responseDataRepository.findByQuestionnaireQuestionId(oneQQ);
				if(alreadyResponsededData==null && !onegridbean.getResponse().isEmpty()){
					responseData.setResponseData(onegridbean.getResponse());
					responseData.setQuestionnaireQuestion(oneQQ);
					responseData.setResponse(activeResponse);
					listOfResponseData.add(insertResponseIntoResponseData(null, oneQQ, responseData, null));

				}else if(alreadyResponsededData !=null && onegridbean.getResponse().isEmpty()){
					alreadyResponsededData.setResponseData(onegridbean.getResponse());
					listOfDeleteAlreadyRespondedData.add(insertResponseIntoResponseData(null, oneQQ, alreadyResponsededData, null));
				}
				else if(alreadyResponsededData !=null){
					alreadyResponsededData.setResponseData(onegridbean.getResponse());
					listOfResponseData.add(insertResponseIntoResponseData(null, oneQQ, alreadyResponsededData, null));
				}
			}
		}
		
		tcoMap.put("responseList", listOfResponseData) ;
		tcoMap.put("assetIdLst", assetIdLst) ;
		return tcoMap;
 }
	
	@Override
	public List<Integer> saveQuestionnaire(Integer responseId, QuestionnaireQuestionWrapper questionnaireQuestionWrapper) {
		// Pull the fully loaded response from the data base
				Response activeResponse = findByIdLoadedResponseDatas(responseId);
				List<ResponseData> listOfDeleteAlreadyRespondedData =  new ArrayList<>();
			//	List<ResponseData> listOfAlreadyRespondedData= getListofQuestionnaireResponse(questionnaireQuestionWrapper,activeResponse,listOfDeleteAlreadyRespondedData);
				Map<String,Object> tcoMap=getListofQuestionnaireResponse(questionnaireQuestionWrapper,activeResponse,listOfDeleteAlreadyRespondedData);
				List<ResponseData> listOfAlreadyRespondedData = (List<ResponseData>)tcoMap.get("responseList");
				responseDataRepository.save(listOfAlreadyRespondedData);

				if(!listOfDeleteAlreadyRespondedData.isEmpty())
					responseDataRepository.delete(listOfDeleteAlreadyRespondedData);

				activeResponse.setUpdatedDate(new Date());
				responseRepository.save(activeResponse);
				
				return (List<Integer>)tcoMap.get("assetIdLst") ;
	}
	
	private Map<String,Object> getListofQuestionnaireResponse(QuestionnaireQuestionWrapper questionnaireQuestionWrapper,Response activeResponse,List<ResponseData> listOfDeleteAlreadyRespondedData){
		
		Map<String,Object> tcoMap = new HashMap<>() ;
		List<ResponseData> listOfResponseData =new ArrayList<>();
		List<Integer> assetIdLst = new ArrayList<>() ;
		for(QuestionnaireQuestionResponseBean oneresponse:questionnaireQuestionWrapper.getQuestionnaireQuestionResponseBean()){
			for(QuestionnaireQuestionGridBean onegridbean:oneresponse.getQuestionnaireQuestionGridBean()){
				ResponseData responseData= new ResponseData();
				// get QuestionnaireQuestion
				QuestionnaireQuestion oneQQ = questionnaireQuestionService.findOne(onegridbean.getQuestionnaireQuestionId(), true);
				assetIdLst.add(oneQQ.getQuestionnaireAsset().getAsset().getId()) ;
				// get already response data
				ResponseData alreadyResponsededData = responseDataRepository.findByQuestionnaireQuestionId(oneQQ);
				if(alreadyResponsededData==null && (onegridbean.getResponse() != null && !onegridbean.getResponse().isEmpty())){
					responseData.setResponseData(onegridbean.getResponse());
					responseData.setQuestionnaireQuestion(oneQQ);
					responseData.setResponse(activeResponse);
					listOfResponseData.add(insertResponseIntoResponseData(null, oneQQ, responseData, null));

				}else if(alreadyResponsededData !=null && (onegridbean.getResponse().isEmpty()) || onegridbean.getResponse() == null){
					alreadyResponsededData.setResponseData(onegridbean.getResponse());
					listOfDeleteAlreadyRespondedData.add(insertResponseIntoResponseData(null, oneQQ, alreadyResponsededData, null));
				}
				else if(alreadyResponsededData !=null){
					alreadyResponsededData.setResponseData(onegridbean.getResponse());
					listOfResponseData.add(insertResponseIntoResponseData(null, oneQQ, alreadyResponsededData, null));
				}
			}
		}
		tcoMap.put("responseList", listOfResponseData) ;
		tcoMap.put("assetIdLst", assetIdLst) ;
		return tcoMap;
	}
}