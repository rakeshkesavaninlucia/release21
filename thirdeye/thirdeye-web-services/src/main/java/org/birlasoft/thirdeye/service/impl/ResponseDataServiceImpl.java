package org.birlasoft.thirdeye.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.Response;
import org.birlasoft.thirdeye.entity.ResponseData;
import org.birlasoft.thirdeye.repositories.ResponseDataRepository;
import org.birlasoft.thirdeye.service.ResponseDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author samar.gupta
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class ResponseDataServiceImpl implements ResponseDataService {
	
	@Autowired
	private ResponseDataRepository responseDataRepository;
	
	@Override
	public ResponseData save(ResponseData rd) {
		return responseDataRepository.save(rd);
	}
	
	@Override
	public List<ResponseData> save(List<ResponseData> responseList) {
		return  responseDataRepository.save(responseList);
	}

	@Override
	public List<ResponseData> findByResponse(Response response) {
		return responseDataRepository.findByResponse(response);
	}

	@Override
	public List<ResponseData> findByQuestionnaireQuestion(QuestionnaireQuestion questionnaireQuestion) {
		return responseDataRepository.findByQuestionnaireQuestion(questionnaireQuestion);
	}
	
	@Override
	public List<ResponseData> findByQuestionnaireQuestionIn(Collection<QuestionnaireQuestion> questionnaireQuestion){
	   return responseDataRepository.findByQuestionnaireQuestionIn(questionnaireQuestion);
	
    }
	
	@Override
	public List<QuestionnaireQuestion> findDistinctQuestionnaireQuestionByQuestionnaireQuestionId( Set<QuestionnaireQuestion> questionnaireQuestion){
	   return responseDataRepository.findDistinctQuestionnaireQuestionByQuestionnaireQuestionId(questionnaireQuestion);
	
    }
	@Override
	public ResponseData findOne(Integer id, boolean loaded) {
		ResponseData responseData = responseDataRepository.findOne(id);
		
		if(loaded){
			responseData.getQuestionnaireQuestion().getQuestion().getQuestionType();
		}
		
		return responseData;
	}

	@Override
	public ResponseData findByQuestionnaireQuestionAndResponse(QuestionnaireQuestion questionnaireQuestion, Response response, boolean loaded) {
		ResponseData responseData = responseDataRepository.findByQuestionnaireQuestionAndResponse(questionnaireQuestion, response);
		
		if(loaded){
			responseData.getQuestionnaireQuestion().getQuestion().getQuestionType();
		}
		
		return responseData;
	}
}
