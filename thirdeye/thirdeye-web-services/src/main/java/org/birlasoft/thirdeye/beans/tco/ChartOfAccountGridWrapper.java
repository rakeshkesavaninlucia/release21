package org.birlasoft.thirdeye.beans.tco;

import java.util.List;

public class ChartOfAccountGridWrapper {

	private List<ChartOfAccountResponseBean> costResponse;

	public List<ChartOfAccountResponseBean> getCostResponse() {
		return costResponse;
	}

	public void setCostResponse(List<ChartOfAccountResponseBean> costResponse) {
		this.costResponse = costResponse;
	}
}
