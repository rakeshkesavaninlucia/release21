/**
 * 
 */
package org.birlasoft.thirdeye.service;

import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireParameterAsset;

/**
 * @author dhruv.sood
 *
 */
public interface QuestionnaireParameterAssetService {

	/**Method to find one QPA based on Qe, P and A
	 * @author: dhruv.sood
	 * @param questionnaire
	 * @param parameter
	 * @param asset
	 * @return
	 */
	public QuestionnaireParameterAsset findByQuestionnaireAndParameterAndAsset(Questionnaire questionnaire, Parameter parameter, Asset asset);

	/** Method to save the QPA
	 * @author: dhruv.sood
	 * @param listOfQuestionnaireParameterAsset
	 * @return
	 */
	public QuestionnaireParameterAsset save(QuestionnaireParameterAsset questionnaireParameterAsset);
}
