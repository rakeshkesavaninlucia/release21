package org.birlasoft.thirdeye.service.impl;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.service.DataCompletnessService;
import org.birlasoft.thirdeye.service.QuestionnaireAssetService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.ResponseDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service to get Completeness Percentage for Data Completeness View
 * @author mehak.guglani
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class DataCompletnessServiceImpl implements DataCompletnessService {
	
	@Autowired
	private ResponseDataService responseDataService;
	@Autowired
	private QuestionnaireAssetService questionnaireAssetService;
	@Autowired
	private QuestionnaireQuestionService qqService;
	
	
	@Override
	public BigDecimal getDataCompletionViewForAsset(Integer numberOfQQ , Integer numberOfResponseData ) {
	  BigDecimal completionPercentage = new BigDecimal(0);
	  
	  //Check whether QuestionnaireQuestion Set and Response Data is empty 
	  if(numberOfQQ > 0 && numberOfResponseData > 0 ){
	       	 //calculate Percentage
		     Integer percentage = (numberOfResponseData*100)/(numberOfQQ);
		     completionPercentage = new BigDecimal(percentage);
      }		
	  return completionPercentage;
	}


	@Override
	public Set<QuestionnaireQuestion> getQuestionsForAsset(Asset asset) {
		Set<QuestionnaireQuestion> setOfQuestionnaireQuestions = new HashSet<>();

		// Get list of QA 
		List<QuestionnaireAsset> listOfQuestionnaireAssets = questionnaireAssetService.findByAsset(asset);

		// Check if listOfQuestionnaireQuestion is empty  
		if(!listOfQuestionnaireAssets.isEmpty()){
			//Get Set Of QuestionnaireQuestion Id 
			setOfQuestionnaireQuestions =  qqService.findByQuestionnaireAssetIn(new HashSet<>(listOfQuestionnaireAssets));
		}
		return setOfQuestionnaireQuestions;
	}


	@Override
	public int getNumberOfQuestionsResponded(Set<QuestionnaireQuestion> setOfQuestionnaireQuestions) {
		//Get Distinct QuestionnaireQuestion from Response Data Table
		List<QuestionnaireQuestion> distQQ = responseDataService.findDistinctQuestionnaireQuestionByQuestionnaireQuestionId(setOfQuestionnaireQuestions);
		return distQQ.size();
	}
}
