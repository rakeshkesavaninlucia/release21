package org.birlasoft.thirdeye.beans.aid;

import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.relationship.RelationshipAssetDataBean;

/**
 * Bean for AID graph for related assets. 
 *
 */
public class RelatedAssetBean {
	
	private AssetBean asset;
	
	private List<RelationshipAssetDataBean> listOfRelationshipAssetData = new ArrayList<>();
	
	/**
	 * Add {@link RelationshipAssetDataBean} to the {@code list}.
	 * @param oneRelatioship
	 */
	public void addRelationshipAssetData(RelationshipAssetDataBean oneRelatioship) {
		if(oneRelatioship != null){
			this.listOfRelationshipAssetData.add(oneRelatioship);
		}
	}

	public AssetBean getAsset() {
		return asset;
	}

	public void setAsset(AssetBean asset) {
		this.asset = asset;
	}

	public List<RelationshipAssetDataBean> getListOfRelationshipAssetData() {
		return listOfRelationshipAssetData;
	}

	public void setListOfRelationshipAssetData(
			List<RelationshipAssetDataBean> listOfRelationshipAssetData) {
		this.listOfRelationshipAssetData = listOfRelationshipAssetData;
	}

}
