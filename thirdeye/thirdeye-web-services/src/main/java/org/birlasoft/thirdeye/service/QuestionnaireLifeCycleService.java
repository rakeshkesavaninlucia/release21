package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.entity.Questionnaire;


/**
 * This is {@code service} interface for questionnaire life cycle.
 * 
 */
public interface QuestionnaireLifeCycleService {

	/**
	 * method to get the list of root parameter for Questionnaire.
	 * @param qe
	 * @return {@code List<ParameterBean>}
	 */
	public List<ParameterBean> getRootParameterForQuestionnaire(Questionnaire qe);

	public Map<String, Integer> getMapOfCategoryAndNoOfQuetion(Questionnaire qe);
	
	

	
}
