package org.birlasoft.thirdeye.beans;

public class JSONQualityGateDescriptionValueMapper {
	
		private String color;
		private String description;
		
		public String getColor() {
			return color;
		}
		
		public void setColor(String color) {
			this.color = color;
		}
		
		public String getDescription() {
			return description;
		}
		
		public void setDescription(String description) {
			this.description = description;
		}

	}

