package org.birlasoft.thirdeye.beans.fm;

import java.math.BigDecimal;
import java.util.Date;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.BcmLevelBean;
import org.birlasoft.thirdeye.beans.JSONMultiChoiceResponseMapper;
import org.birlasoft.thirdeye.entity.FunctionalMapData;
import org.birlasoft.thirdeye.util.Utility;

/**
 * 
 * Functional Map Data Bean
 *
 */
public class FunctionalMapDataBean {

	private Integer id;
	private AssetBean assetBean;
	private BcmLevelBean bcmLevelBean;
	private String jsonData;
	private Date updatedDate;
	private String responseData;
	private BigDecimal responseValue;
	
	/**
	 * Parameterized Constructor
	 * @param fmData
	 */
	public FunctionalMapDataBean(FunctionalMapData fmData) {
		this.id = fmData.getId();
		this.assetBean = new AssetBean(fmData.getAsset());
		this.bcmLevelBean = new BcmLevelBean(fmData.getBcmLevel());
		this.jsonData = fmData.getData();
		this.responseData = parseJsonData(jsonData);
		this.updatedDate = fmData.getUpdatedDate();
		this.responseValue = parseResponseValue(jsonData);
	}
	
	public BigDecimal getResponseValue() {
		return responseValue;
	}

	public void setResponseValue(BigDecimal responseValue) {
		this.responseValue = responseValue;
	}
	
	private String parseJsonData(String jsonData) {
		JSONMultiChoiceResponseMapper jsonMultiChoiceResponseMapper = Utility.convertJSONStringToObject(jsonData, JSONMultiChoiceResponseMapper.class);
		if (jsonMultiChoiceResponseMapper != null){	
			return jsonMultiChoiceResponseMapper.getOptions().get(0).getText();
		}
		return null;		
	}

	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public AssetBean getAssetBean() {
		return assetBean;
	}

	public void setAssetBean(AssetBean assetBean) {
		this.assetBean = assetBean;
	}

	public BcmLevelBean getBcmLevelBean() {
		return bcmLevelBean;
	}

	public void setBcmLevelBean(BcmLevelBean bcmLevelBean) {
		this.bcmLevelBean = bcmLevelBean;
	}	

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getResponseData() {
		return responseData;
	}

	public void setResponseData(String responseData) {
		this.responseData = responseData;
	}

	private BigDecimal parseResponseValue(String jsonData) {
		JSONMultiChoiceResponseMapper jsonMultiChoiceResponseMapper = Utility.convertJSONStringToObject(jsonData, JSONMultiChoiceResponseMapper.class);
		if (jsonMultiChoiceResponseMapper != null){	
			return new BigDecimal(jsonMultiChoiceResponseMapper.getOptions().get(0).getQuantifier());
		}
		return null;		
	}
}
