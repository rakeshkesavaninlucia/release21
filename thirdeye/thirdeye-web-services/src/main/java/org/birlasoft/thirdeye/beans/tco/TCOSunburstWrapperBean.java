/**
 * 
 */
package org.birlasoft.thirdeye.beans.tco;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Bean corresponding to JSON data to display Sunburst Map for TCO parameter. 
 * @author dhruv.sood
 *
 */
public class TCOSunburstWrapperBean {

	
	private String costStructureName;	
	private Integer costStructureId;  //CostStructure Id added for assetDetail
	private List<TCOSunburstWrapperBean> subCostStructureList = new ArrayList<>();
	private BigDecimal cost;
	
	/**
	 * @return the costStructureName
	 */
	public String getCostStructureName() {
		return costStructureName;
	}
	/**
	 * @param costStructureName the costStructureName to set
	 */
	public void setCostStructureName(String costStructureName) {
		this.costStructureName = costStructureName;
	}
	/**
	 * @return the subCostStructureList
	 */
	public List<TCOSunburstWrapperBean> getSubCostStructureList() {
		return subCostStructureList;
	}
	/**
	 * @param subCostStructureList the subCostStructureList to set
	 */
	public void setSubCostStructureList(
			List<TCOSunburstWrapperBean> subCostStructureList) {
		this.subCostStructureList = subCostStructureList;
	}
	/**
	 * @return the cost
	 */
	public BigDecimal getCost() {
		return cost;
	}
	/**
	 * @param cost the cost to set
	 */
	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
	
	public boolean addSubCostStructureList(TCOSunburstWrapperBean child) {
		if (child == null) {
			return false;
		}		
		return this.subCostStructureList.add(child);
	}
	
	public Integer getCostStructureId() {
		return costStructureId;
	}
	public void setCostStructureId(Integer costStructureId) {
		this.costStructureId = costStructureId;
	}
}
