package org.birlasoft.thirdeye.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.parameter.FunctionalCoverageParameterEvaluator;
import org.birlasoft.thirdeye.constant.ParameterType;
import org.birlasoft.thirdeye.constant.QuestionType;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterFunction;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.ResponseData;
import org.birlasoft.thirdeye.interfaces.ParameterEvaluator;
import org.birlasoft.thirdeye.interfaces.QuantifiableResponse;
import org.birlasoft.thirdeye.service.BCMLevelService;
import org.birlasoft.thirdeye.service.FunctionalMapService;
import org.birlasoft.thirdeye.service.ParameterBoostService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;

/**
 * Class for AP, LP, FC parameter evaluation and boosting.
 */
public class QuestionnaireParameterEvaluator implements ParameterEvaluator {

	private final List<Parameter> listOfParameters;
	private final Questionnaire questionnaire;
	
	private ParameterService parameterService;	
	private QuestionnaireQuestionService questionnaireQuestionService;
	private FunctionalMapService functionalMapService;
	private BCMLevelService bcmLevelService;
	private ParameterBoostService parameterBoostService;
	
	/**
	 * @param listOfParameters
	 * @param questionnaire
	 */
	public QuestionnaireParameterEvaluator(List<Parameter> listOfParameters,
			Questionnaire questionnaire) {
		this.listOfParameters = listOfParameters;
		this.questionnaire = questionnaire;
	}
	
	/**
	 * @param parameterService
	 * @param questionnaireQuestionService
	 * @param functionalMapService
	 * @param bcmLevelService
	 * @param listOfParameters
	 * @param questionnaire
	 * @param parameterBoostService
	 */
	public QuestionnaireParameterEvaluator(ParameterService parameterService,
			QuestionnaireQuestionService questionnaireQuestionService,
			FunctionalMapService functionalMapService,
			BCMLevelService bcmLevelService,
			List<Parameter> listOfParameters, Questionnaire questionnaire, ParameterBoostService parameterBoostService) {
		this.listOfParameters = listOfParameters;
		this.questionnaire = questionnaire;
		this.parameterService = parameterService;
		this.questionnaireQuestionService = questionnaireQuestionService;
		this.functionalMapService = functionalMapService;
		this.bcmLevelService = bcmLevelService;
		this.parameterBoostService=parameterBoostService;
	}

	@Override
	public List<ParameterEvaluationResponse> evaluateParameter(List<AssetBean> assets) {
		List<ParameterEvaluationResponse> listOfParameterResponses = new ArrayList<>();
		
		for (Parameter oneParameter : listOfParameters) {
			List<ParameterEvaluationResponse> responses = new ArrayList<>();
			if (oneParameter.getType().equals(ParameterType.AP.toString())){
				Map<ParameterType, List<ParameterBean>> mapOfChildParameters =  parameterService.getAllParametersInTree(oneParameter);
				List<ParameterBean> listOfEvaluatedChildParameters = new ArrayList<>();
				
				for (ParameterBean lpParameter : mapOfChildParameters.get(ParameterType.LP)) {
					ParameterEvaluationResponse parameterEvaluationResponse = evaluateLp(assets, lpParameter);
					responses.add(parameterEvaluationResponse);
					listOfEvaluatedChildParameters.add(lpParameter);
				}
				
				for (ParameterBean fcParameter : mapOfChildParameters.get(ParameterType.FC)) {
					List<Parameter> listOfParameter = new ArrayList<>();
					listOfParameter.add(parameterService.findOne(fcParameter.getId()));
					FunctionalCoverageParameterEvaluator fcParameterEvaluator = new FunctionalCoverageParameterEvaluator(listOfParameter, parameterService, functionalMapService, bcmLevelService, questionnaire);
					responses.addAll(fcParameterEvaluator.evaluateParameter(assets));
					listOfEvaluatedChildParameters.add(fcParameter);
				}
				
				List<ParameterEvaluationResponse> responsesOfAp = evaluateAggregateParameter(responses, listOfEvaluatedChildParameters, mapOfChildParameters.get(ParameterType.AP), assets);
				
				//Adding the parameter to be evaluated to the list
				ParameterBean parameterBean = getParameterBean(oneParameter);
				listOfParameterResponses.add(evaluateAp(assets, parameterBean, responsesOfAp));
			} else if(oneParameter.getType().equals(ParameterType.LP.toString())){
				//get list of questions
				ParameterBean parameterBean = getParameterBean(oneParameter);
				listOfParameterResponses.add(evaluateLp(assets, parameterBean));
			} else if(oneParameter.getType().equals(ParameterType.FC.toString())){
				List<Parameter> listOfParameter = new ArrayList<>();
				listOfParameter.add(oneParameter);
				FunctionalCoverageParameterEvaluator fcParameterEvaluator = new FunctionalCoverageParameterEvaluator(listOfParameter, parameterService, functionalMapService, bcmLevelService, questionnaire);
				listOfParameterResponses.addAll(fcParameterEvaluator.evaluateParameter(assets));
			}
		}
		
		//Boosting the evaluated value of the responses
		listOfParameterResponses = parameterBoostService.boostEvaluatedResponse(questionnaire, listOfParameterResponses);
		
		return listOfParameterResponses;
	}

	private ParameterBean getParameterBean(Parameter parameter) {
		ParameterBean parameterBean = new ParameterBean(parameter);
		for (ParameterFunction pf : parameter.getParameterFunctionsForParentParameterId()){
			if(pf.getParameterByChildparameterId() != null){
				parameterBean.addChildParameter(new ParameterBean(pf.getParameterByChildparameterId()));
				parameterBean.addParameterFunction(pf.getParameterByChildparameterId().getId(), pf);
			}
			if(pf.getQuestion() != null){
				parameterBean.addChildQuestion(new QuestionBean(pf.getQuestion()));
				parameterBean.addParameterFunction(pf.getQuestion().getId(), pf);
			}
		}
		return parameterBean;
	}

	private List<ParameterEvaluationResponse> evaluateAggregateParameter(List<ParameterEvaluationResponse> responseOfParameters, List<ParameterBean> listOfEvaluatedChildParameters, List<ParameterBean> listOfParametersToBeEvaluated, List<AssetBean> assets) {
		ParameterBean toBeRemoved = null;
		for (ParameterBean apParameter :  listOfParametersToBeEvaluated) {
			List<ParameterBean> childParameters = apParameter.getChildParameters();
			if(listOfEvaluatedChildParameters.containsAll(childParameters)){
				ParameterEvaluationResponse parameterEvaluationResponse = evaluateAp(assets, apParameter, responseOfParameters);
				toBeRemoved = apParameter;
				listOfEvaluatedChildParameters.add(apParameter);
				responseOfParameters.add(parameterEvaluationResponse);
				break;
			}
		}
		listOfParametersToBeEvaluated.remove(toBeRemoved);
		if(!listOfParametersToBeEvaluated.isEmpty()){
			return evaluateAggregateParameter(responseOfParameters, listOfEvaluatedChildParameters, listOfParametersToBeEvaluated, assets);
		}
		return responseOfParameters;
	}

	private ParameterEvaluationResponse evaluateAp(List<AssetBean> assets, ParameterBean parameter, List<ParameterEvaluationResponse> responsesOfLp) {
		ParameterEvaluationResponse parameterEvaluationResponse = new ParameterEvaluationResponse();
		parameterEvaluationResponse.setParameterBean(parameter);
		List<ParameterEvaluationResponse> listOfChildParameterResponses = new ArrayList<>();
		for (ParameterBean childParameter : parameter.childParameters) {
			for (ParameterEvaluationResponse oneChildParameterResponse : responsesOfLp) {
				if(childParameter.getId().equals(oneChildParameterResponse.getParameterBean().getId())){
					listOfChildParameterResponses.add(oneChildParameterResponse);
					break;
				}
			}
		}
		for (AssetBean oneAsset : assets) {
			Map<ParameterBean, BigDecimal> mapOfChildParameterResponse = new HashMap<>();
			for (ParameterEvaluationResponse childResponse : listOfChildParameterResponses) {
				mapOfChildParameterResponse.put(childResponse.getParameterBean(), childResponse.fetchParamValueForAsset(oneAsset));
			}
			parameterEvaluationResponse.addAssetEvaluation(oneAsset, parameter.evaluateAggregate(mapOfChildParameterResponse));
		}
		
		return parameterEvaluationResponse;
	}

	private ParameterEvaluationResponse evaluateLp(List<AssetBean> assets, ParameterBean parameter) {
		ParameterEvaluationResponse parameterEvaluationResponse = new ParameterEvaluationResponse();
		
		//get list of all questions in a parameter
		Map<Integer, QuestionBean> mapOfQuestions = prepareQuestionMap(parameter.getChildQuestions());
		
		Set<QuestionnaireQuestion> setOfQQ = questionnaireQuestionService.findByParameterAndQuestionnaire(parameter, questionnaire);
		for (AssetBean oneAsset : assets) {
			Set<QuestionnaireQuestion> questionsForOneAsset = extractQuestionnaireQuestionForAsset(setOfQQ, oneAsset);
			
			cleanseExistingResponses(mapOfQuestions.values());
			
			//plugin responses
			pluginResponses(mapOfQuestions, questionsForOneAsset);			
			parameterEvaluationResponse.addAssetEvaluation(oneAsset, parameter.evaluate());
		}
		
		parameterEvaluationResponse.setParameterBean(parameter);
		return parameterEvaluationResponse;
	}

	private void cleanseExistingResponses(Collection<QuestionBean> questionBeans) {
		for (QuestionBean qb : questionBeans) {
			qb.cleanResponses();
		}
	}

	private Map<Integer, QuestionBean> prepareQuestionMap(List<QuestionBean> listOfQuestions) {
		Map<Integer, QuestionBean> questionMap = new HashMap<>();
		for (QuestionBean qb : listOfQuestions) {
			questionMap.put(qb.getId(), qb);
		}
		return questionMap;
	}

	private void pluginResponses(Map<Integer, QuestionBean> mapOfQuestions, Set<QuestionnaireQuestion> questionsForOneAsset) {
		
		for (QuestionnaireQuestion oneQQ : questionsForOneAsset){
			QuestionType queType = QuestionType.valueOf(oneQQ.getQuestion().getQuestionType());
			for (ResponseData rd : oneQQ.getResponseDatas()){
				String responseJSON = rd.getResponseData();
				
				Object deserializedObject = queType.convertResponseFromJSONString(responseJSON);
				if (deserializedObject instanceof QuantifiableResponse){
					mapOfQuestions.get(oneQQ.getQuestion().getId()).addQuantifiableResponse((QuantifiableResponse)deserializedObject);
				}
			}
		}	
	}

	private Set<QuestionnaireQuestion> extractQuestionnaireQuestionForAsset(Set<QuestionnaireQuestion> setOfQQ, AssetBean oneAsset) {
		Set<QuestionnaireQuestion> questionsForOneAsset = new HashSet<>();
		
		List<QuestionnaireQuestion> qqToBeRemoved = new ArrayList<>();
		// Extracting  up all the questions for one Asset
		for (QuestionnaireQuestion oneQQ : setOfQQ){
			if (oneQQ.getQuestionnaireAsset().getAsset().getId().equals(oneAsset.getId())){
				questionsForOneAsset.add(oneQQ);
				qqToBeRemoved.add(oneQQ);
			}
		}
		
		setOfQQ.removeAll(qqToBeRemoved);
		
		return questionsForOneAsset;
	}

}
