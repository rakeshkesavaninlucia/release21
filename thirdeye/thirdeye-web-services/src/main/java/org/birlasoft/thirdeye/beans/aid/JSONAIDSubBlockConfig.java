package org.birlasoft.thirdeye.beans.aid;

import com.fasterxml.jackson.annotation.JsonIgnore;

//@JsonIgnoreProperties(ignoreUnknown = true, value={"subBlockIdentifier","parentBlockId"})
public class JSONAIDSubBlockConfig {
	
	
	private String subBlockIdentifier;
	private int parentBlockId;
	private String configurationString;

	private String questionString;
	private String parameterString;
	private String templateColumnString;
	
	// Start of Parameters for serialization
	
	private int templateColumnId;
	private String blockTitle;
	
	/**
	 * Parameter of the questionnaire which needs to be put
	 */
	private int paramQEId;
	private int paramId;
	
	/**
	 * Question id that needs to be put
	 */
	private int questionId;
	private int questionQEId;
	
	@JsonIgnore
	public String getSubBlockIdentifier() {
		return subBlockIdentifier;
	}
	public void setSubBlockIdentifier(String subBlockIdentifier) {
		this.subBlockIdentifier = subBlockIdentifier;
	}
	
	@JsonIgnore
	public int getParentBlockId() {
		return parentBlockId;
	}
	
	@JsonIgnore
	public String getConfigurationString() {
		return configurationString;
	}
	public void setConfigurationString(String configurationString) {
		this.configurationString = configurationString;
	}
	
	public void setParentBlockId(int parentBlockId) {
		this.parentBlockId = parentBlockId;
	}
	public int getTemplateColumnId() {
		return templateColumnId;
	}
	public void setTemplateColumnId(int templateColumnId) {
		this.templateColumnId = templateColumnId;
	}
	
	public void updateBeanForTemplateColumn() {
		this.paramId = 0;
		this.paramQEId = 0;
		this.questionId = 0;
		this.questionQEId = 0;
	}
	
	public String getBlockTitle() {
		return blockTitle;
	}
	public void setBlockTitle(String blockTitle) {
		this.blockTitle = blockTitle;
	}
	public int getParamQEId() {
		return paramQEId;
	}
	public void setParamQEId(int paramQEId) {
		this.paramQEId = paramQEId;
	}
	public int getParamId() {
		return paramId;
	}
	public void setParamId(int paramId) {
		
		this.paramId = paramId;
	}
	
	public void updateBeanForParameter() {
		this.questionId = 0;
		this.questionQEId = 0;
		this.templateColumnId = 0;
	}
	
	public int getQuestionId() {
		return questionId;
	}
	public void setQuestionId(int questionId) {
		
		this.questionId = questionId;
	}
	
	public void updateBeanForQuestion() {
		this.paramId = 0;
		this.paramQEId = 0;
		this.templateColumnId = 0;
	}
	public int getQuestionQEId() {
		return questionQEId;
	}
	public void setQuestionQEId(int questionQEId) {
		this.questionQEId = questionQEId;
	}
	
	@JsonIgnore
	public String getQuestionString() {
		return questionString;
	}
	public void setQuestionString(String questionTitle) {
		this.questionString = questionTitle;
	}
	
	@JsonIgnore
	public String getParameterString() {
		return parameterString;
	}
	public void setParameterString(String parameterName) {
		this.parameterString = parameterName;
	}
	
	@JsonIgnore
	public String getTemplateColumnString() {
		return templateColumnString;
	}
	
	public void setTemplateColumnString(String templateColumnName) {
		this.templateColumnString = templateColumnName;
	}

	@JsonIgnore
	public boolean isConfigured (){
		boolean isConfigure = false;
		if(questionId > 0 || paramId > 0 || templateColumnId > 0){
			isConfigure = true;
		} else if (configurationString != null && !configurationString.trim().isEmpty()){
			isConfigure = true;
		}
		return isConfigure;
	}
	
	@JsonIgnore
	public String getDisplayString(){
		if (paramId > 0){
			return getParameterString();
		} else if (questionId >0){
			return getQuestionString();
		} else if (templateColumnId > 0){
			return getTemplateColumnString();
		} else if (configurationString != null && !configurationString.trim().isEmpty()){
			return getConfigurationString();
		}
		
		return "";
	}
	
	public boolean usesParameter() {
		return getParamId() > 0 && getParamQEId() > 0;
	}
	
	public boolean usesQuestion() {
		return getQuestionId() > 0 && getQuestionQEId() > 0;
	}
	
	public boolean usesTemplateColumn() {
		return getTemplateColumnId() > 0;
	}
	
	
	
}
