package org.birlasoft.thirdeye.beans.report;

import java.util.Set;

import org.birlasoft.thirdeye.entity.Report;
import org.birlasoft.thirdeye.util.Utility;

/**
 * @author sunil1.gupta
 *
 */
public class HealthAnalysisJSONConfig  {
	
	private String questionnaireIds;
	private String parameterIds;
	private Integer qualityGateId;
	private Integer[] idsOfLevel1;
	private Integer[] idsOfLevel2;
	private Integer[] idsOfLevel3;
	private Integer[] idsOfLevel4;
	
	public HealthAnalysisJSONConfig() {
		
	}
	
	public HealthAnalysisJSONConfig(Report r) {
		HealthAnalysisJSONConfig haJSONConfig = Utility.convertJSONStringToObject(r.getReportConfig(), HealthAnalysisJSONConfig.class);
		if (haJSONConfig != null){
			this.questionnaireIds = haJSONConfig.getQuestionnaireIds();
			this.parameterIds = haJSONConfig.getParameterIds();
			this.qualityGateId = haJSONConfig.getQualityGateId();
			this.idsOfLevel1 = haJSONConfig.getIdsOfLevel1();
			this.idsOfLevel2 = haJSONConfig.getIdsOfLevel2();
			this.idsOfLevel3 = haJSONConfig.getIdsOfLevel3();
			this.idsOfLevel4 = haJSONConfig.getIdsOfLevel4();
		}	
	}

	public String getQuestionnaireIds() {
		return questionnaireIds;
	}

	public void setQuestionnaireIds(String questionnaireIds) {
		this.questionnaireIds = questionnaireIds;
	}

	public String getParameterIds() {
		return parameterIds;
	}

	public void setParameterIds(String parameterIds) {
		this.parameterIds = parameterIds;
	}

	public Integer getQualityGateId() {
		return qualityGateId;
	}

	public void setQualityGateId(Integer qualityGateId) {
		this.qualityGateId = qualityGateId;
	}

	public Integer[] getIdsOfLevel1() {
		return idsOfLevel1;
	}

	public void setIdsOfLevel1(Integer[] idsOfLevel1) {
		this.idsOfLevel1 = idsOfLevel1;
	}

	public Integer[] getIdsOfLevel2() {
		return idsOfLevel2;
	}

	public void setIdsOfLevel2(Integer[] idsOfLevel2) {
		this.idsOfLevel2 = idsOfLevel2;
	}

	public Integer[] getIdsOfLevel3() {
		return idsOfLevel3;
	}

	public void setIdsOfLevel3(Integer[] idsOfLevel3) {
		this.idsOfLevel3 = idsOfLevel3;
	}

	public Integer[] getIdsOfLevel4() {
		return idsOfLevel4;
	}

	public void setIdsOfLevel4(Integer[] idsOfLevel4) {
		this.idsOfLevel4 = idsOfLevel4;
	}
	
	
	
}
