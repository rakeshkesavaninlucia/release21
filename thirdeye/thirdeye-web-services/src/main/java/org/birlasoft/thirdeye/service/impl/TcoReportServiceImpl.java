package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.QuestionBean;
import org.birlasoft.thirdeye.beans.tco.TCOAssetWrapper;
import org.birlasoft.thirdeye.beans.tco.TCOSunburstWrapperBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterFunction;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;
import org.birlasoft.thirdeye.search.api.beans.IndexParameterBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.api.beans.SearchCostStructureBean;
import org.birlasoft.thirdeye.search.api.beans.SearchQuestionBean;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ParameterFunctionService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.TcoReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class TcoReportServiceImpl implements TcoReportService {

	private static Logger logger = LoggerFactory.getLogger(TcoReportServiceImpl.class);

	@Value("#{systemProperties['" + Constants.ELASTICSEARCH_HOST_NAME + "']}")
	private String elasticSearchHost;

	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private QuestionnaireParameterService questionnaireParameterService;
	@Autowired
	private QuestionnaireService questionnaireService;
	@Autowired
	private ParameterFunctionService parameterFunctionService;
	@Autowired
	private ParameterService parameterService;

	@Override
	public TCOSunburstWrapperBean getTcoSunburstWrapperBean(Integer chartOfAccountId) {

		Questionnaire questionnaire = questionnaireService.findOne(chartOfAccountId);
		List<QuestionnaireParameter> listOfQPs = questionnaireParameterService.findByQuestionnaire(questionnaire);
		Map<Integer, List<Integer>> mapOfQpIdAndQuestionIds = new HashMap<>();
		List<Integer> listOfParameterIds = new ArrayList<>();
		for (QuestionnaireParameter qp : listOfQPs) {
			listOfParameterIds.add(qp.getParameterByParameterId().getId());
			List<Integer> listOfQuestions = getListOfQuestions(qp);
			if(!listOfQuestions.isEmpty()){				
				mapOfQpIdAndQuestionIds.put(qp.getId(), getListOfQuestions(qp));
			}
		}

		List<SearchCostStructureBean> listOfSearchCostStructureBean = getCostStructureAggregate(listOfParameterIds);

		List<SearchQuestionBean> listOfSearchQuestionBean = new ArrayList<>();
		for (Entry<Integer, List<Integer>> entry : mapOfQpIdAndQuestionIds.entrySet()) {			
			listOfSearchQuestionBean.addAll(getCostElementAggregate(entry.getKey(), entry.getValue()));
		}

		return prepareTCOSunburstWrapper(questionnaire, listOfSearchCostStructureBean, listOfSearchQuestionBean);
	}

	private TCOSunburstWrapperBean prepareTCOSunburstWrapper(Questionnaire questionnaire, List<SearchCostStructureBean> listOfSearchCostStructureBean,
			List<SearchQuestionBean> listOfSearchQuestionBean) {
		Map<Integer, SearchCostStructureBean> mapOfCostStructure = new HashMap<>();
		for (SearchCostStructureBean csBean : listOfSearchCostStructureBean) {
			mapOfCostStructure.put(csBean.getId(), csBean);
		}

		List<QuestionnaireParameter> listOfQp = questionnaireParameterService.getListOfRootQuestionnaireParameters(questionnaire);
		List<ParameterBean> listOfParameterBean = new ArrayList<>();
		for (QuestionnaireParameter questionnaireParameter : listOfQp) {
			Parameter parameter = parameterService.findFullyLoaded(questionnaireParameter.getParameterByParameterId().getId());
			ParameterBean parameterBean = parameterService.generateParameterTree(parameter);
			listOfParameterBean.add(parameterBean);
		}

		return prepareWrapperTree(questionnaire, listOfParameterBean, mapOfCostStructure, listOfSearchQuestionBean);
	}

	private TCOSunburstWrapperBean prepareWrapperTree(Questionnaire questionnaire, List<ParameterBean> listOfParameterBean,
			Map<Integer, SearchCostStructureBean> mapOfCostStructure, List<SearchQuestionBean> listOfSearchQuestionBean) {
		TCOSunburstWrapperBean tcoSunburstWrapperBean = new TCOSunburstWrapperBean();
		tcoSunburstWrapperBean.setCostStructureName(questionnaire.getName());
		for (ParameterBean oneParam : listOfParameterBean) {
			TCOSunburstWrapperBean childBean = prepareTCOSunburstWrapperBeanFromParamBean(questionnaire, oneParam, mapOfCostStructure, listOfSearchQuestionBean);
			tcoSunburstWrapperBean.addSubCostStructureList(childBean);
		}
		return tcoSunburstWrapperBean;
	}

	private TCOSunburstWrapperBean prepareTCOSunburstWrapperBeanFromParamBean(Questionnaire questionnaire, ParameterBean oneParam, Map<Integer, SearchCostStructureBean> mapOfCostStructure,
			List<SearchQuestionBean> listOfSearchQuestionBean) {
		TCOSunburstWrapperBean tcoSunburstWrapperBean = new TCOSunburstWrapperBean();
		tcoSunburstWrapperBean.setCostStructureName(oneParam.getDisplayName());
		tcoSunburstWrapperBean.setCostStructureId(oneParam.getId()); // added ID in wrapper for assetDetail enhancement
		tcoSunburstWrapperBean.setCost(mapOfCostStructure.get(oneParam.getId()).getAggregate());
		//add child cost structure
		for (ParameterBean oneChildParam : oneParam.getChildParameters()) {
			tcoSunburstWrapperBean.addSubCostStructureList(prepareTCOSunburstWrapperBeanFromParamBean(questionnaire, oneChildParam, mapOfCostStructure, listOfSearchQuestionBean));
		}

		List<QuestionnaireParameter> questionnaireParameters = questionnaireParameterService.findByQuestionnaireAndParameter(questionnaire, parameterService.findOne(oneParam.getId()));
		//add child cost element
		for (QuestionBean oneChildQuestion : oneParam.getChildQuestions()) {

			tcoSunburstWrapperBean.addSubCostStructureList(prepareTCOSunburstWrapperBeanFromQuestionBean(questionnaireParameters.get(0), oneChildQuestion, listOfSearchQuestionBean));
		}
		return tcoSunburstWrapperBean;
	}

	private TCOSunburstWrapperBean prepareTCOSunburstWrapperBeanFromQuestionBean(QuestionnaireParameter questionnaireParameter, QuestionBean oneChildQuestion,
			List<SearchQuestionBean> listOfSearchQuestionBean) {
		TCOSunburstWrapperBean tcoSunburstWrapperBean = new TCOSunburstWrapperBean();
		for (SearchQuestionBean searchQuestionBean : listOfSearchQuestionBean) {
			if(searchQuestionBean.getId().equals(oneChildQuestion.getId()) && 
					searchQuestionBean.getQuestionnaireParameterId().equals(questionnaireParameter.getId())){				
				tcoSunburstWrapperBean.setCostStructureName(oneChildQuestion.getTitle());
				tcoSunburstWrapperBean.setCost(searchQuestionBean.getValue());
			}
		}
		return tcoSunburstWrapperBean;
	}

	private List<Integer> getListOfQuestions(QuestionnaireParameter qp) {
		List<ParameterFunction> listOfParameterFunction = parameterFunctionService.findByParameterByParentParameterId(qp.getParameterByParameterId());
		List<Integer> listOfQuestion = new ArrayList<>();
		for (ParameterFunction parameterFunction : listOfParameterFunction) {
			if(parameterFunction.getQuestion() != null){
				listOfQuestion.add(parameterFunction.getQuestion().getId());
			}
		}
		return listOfQuestion;
	}

	private List<SearchQuestionBean> getCostElementAggregate(Integer qpId, List<Integer> listOfQuestions) {
		final String csUri = elasticSearchHost + Constants.BASESEARCHURL + "/tco/ce";

		ObjectMapper mapper = new ObjectMapper();
		RestTemplate restTemplate = new RestTemplate();
		List<SearchQuestionBean> listOfBean = new ArrayList<>();
		List<SearchQuestionBean> list = new ArrayList<>();

		//Setting the search config object  
		SearchConfig searchConfig = setSearchConfig(listOfQuestions);
		searchConfig.setQpId(qpId);

		//Firing a rest request to elastic search server
		try {
			listOfBean = (List<SearchQuestionBean>) restTemplate.postForObject(csUri, searchConfig, List.class);
			list = mapper.convertValue(listOfBean, new TypeReference<List<SearchQuestionBean>>() { });
		} catch (RestClientException e) {				
			logger.error("Exception occured in TcoReportServiceImpl :: getCostElementAggregate() :" + e);
		}
		return list;
	}

	private List<SearchCostStructureBean> getCostStructureAggregate(List<Integer> listOfParameterIds){
		final String csUri = elasticSearchHost + Constants.BASESEARCHURL + "/tco/cs";

		ObjectMapper mapper = new ObjectMapper();
		RestTemplate restTemplate = new RestTemplate();
		List<SearchCostStructureBean> listOfBean = new ArrayList<>();
		List<SearchCostStructureBean> list = new ArrayList<>();

		//Setting the search config object  
		SearchConfig searchConfig = setSearchConfig(listOfParameterIds);

		//Firing a rest request to elastic search server
		try {
			listOfBean = (List<SearchCostStructureBean>) restTemplate.postForObject(csUri, searchConfig, List.class);
			list = mapper.convertValue(listOfBean, new TypeReference<List<SearchCostStructureBean>>() { });
		} catch (RestClientException e) {				
			logger.error("Exception occured in TcoReportServiceImpl :: getCostStructureAggregate() :" + e);
		}
		return list;
	}

	/**
	 * Set tenant and workspace in search config. Also set search config values for widgets.
	 * @param tenantId
	 * @param activeWorkspaceId
	 * @return
	 */
	private SearchConfig setSearchConfig(List<Integer> listOfIds) {
		String tenantId = customUserDetailsService.getCurrentAuthenticationDetails().getTenantURL();
		Integer activeWorkspaceId = customUserDetailsService.getActiveWorkSpaceForUser().getId();
		SearchConfig searchConfig = new SearchConfig();
		List<Integer> listOfWorkspace = new ArrayList<>();
		listOfWorkspace.add(activeWorkspaceId);
		searchConfig.setListOfWorkspace(listOfWorkspace);
		List<String>list = new ArrayList<>();
		list.add(tenantId);
		searchConfig.setTenantURLs(list);

		searchConfig.setListOfIds(listOfIds);
		return searchConfig;
	}

	@Override
	public List<TCOAssetWrapper> getAssetDetailsForTcoReport(Integer chartOfAccountId, Integer costStructureId) {
		 List<TCOAssetWrapper> tcoAssetWrappers =  new ArrayList<>();
		List<IndexAssetBean> listofAllAssetBean = getDataFromElasticsearch(chartOfAccountId);

		for(IndexAssetBean oneAssetBean : listofAllAssetBean){
			tcoAssetWrappers.add(extractAssetDetails(oneAssetBean, costStructureId));
		}

		return tcoAssetWrappers;
	}

	private TCOAssetWrapper extractAssetDetails(IndexAssetBean oneAssetBean, Integer costStructureId) {
		TCOAssetWrapper tcoAssetWrapper =  new TCOAssetWrapper();
		tcoAssetWrapper.setAssetId(oneAssetBean.getId());
		tcoAssetWrapper.setAssetName(oneAssetBean.getName());
		for(IndexParameterBean oneParameterBean :oneAssetBean.getCostStuctures()) {
			if(costStructureId.equals(oneParameterBean.getId())){
				tcoAssetWrapper.setCost(oneParameterBean.getCurrentValue().getValue());
				tcoAssetWrapper.setCostStructureId(oneParameterBean.getId());
				break;
			}
		}
		return tcoAssetWrapper;
	}

	private List<IndexAssetBean> getDataFromElasticsearch(Integer chartOfAccountId) {

		final String asseturi = elasticSearchHost + Constants.BASESEARCHURL + "/tco/asset";

		RestTemplate restTemplate = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		List<IndexAssetBean> indexAssetBeanList = new ArrayList<>();
		List<IndexAssetBean> assetList = new ArrayList<>();

		SearchConfig searchConfig = setSearchConfig(chartOfAccountId);

		try {
			indexAssetBeanList = (List<IndexAssetBean>) restTemplate.postForObject(asseturi, searchConfig, List.class);
			assetList = mapper.convertValue(indexAssetBeanList, new TypeReference<List<IndexAssetBean>>() { });

		} catch (RestClientException e) {
			logger.error("Exception occured in TCOServiceImpl :: getDataFromElasticsearch() :", e);
		}

		return assetList;
	}

	private SearchConfig setSearchConfig(Integer chartOfAccountId) {

		String tenantId = customUserDetailsService.getCurrentAuthenticationDetails().getTenantURL();
		Integer activeWorkspaceId = customUserDetailsService.getActiveWorkSpaceForUser().getId();
		SearchConfig searchConfig = new SearchConfig();
		List<Integer> listOfWorkspace = new ArrayList<>();
		listOfWorkspace.add(activeWorkspaceId);
		searchConfig.setListOfWorkspace(listOfWorkspace);
		List<String> list = new ArrayList<>();
		list.add(tenantId);
		searchConfig.setTenantURLs(list);
		searchConfig.setQuestionnaireId(chartOfAccountId);

		return searchConfig;
	}

}
