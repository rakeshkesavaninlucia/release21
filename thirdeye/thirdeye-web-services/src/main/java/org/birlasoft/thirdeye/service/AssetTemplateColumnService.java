package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.AssetTemplateColumn;
import org.birlasoft.thirdeye.entity.User;

/**
 * Service interface for asset template column.
 */
public interface AssetTemplateColumnService {
	
	/**
	 * Only get One AssetTemplateColumn with given Id
	 * @param id
	 * @return AssetTemplateColumn Object
	 */
	public AssetTemplateColumn findOne(Integer id);
	
	/**
	 * save AssetTemplateColumn Object
	 * @param assetTemplateColumn
	 * @return AssetTemplateColumn  Object
	 */
	public AssetTemplateColumn save(AssetTemplateColumn assetTemplateColumn);
	
	/**
	 * find maximum sequence number from set of asset template columns.
	 * @param cachedTemplate
	 * @return int
	 */
	public int findMaxSequenceNumberInAssetTemplateColumn(AssetTemplate cachedTemplate);	
	
	/**
	 * Sort asset template column by sequence number.
	 * @param assetTemplateColumns
	 * @return
	 */
	public Set<AssetTemplateColumn> sortTemplateColumnsBySequenceNumber(Set<AssetTemplateColumn> assetTemplateColumns);
	
	/**
	 * Create asset template column object.
	 * @param assetTemplateColumn
	 * @param cachedTemplate
	 * @param currentUser
	 * @return {@code AssetTemplateColumn}
	 */
	public AssetTemplateColumn createAssetTemplateColumnObject(AssetTemplateColumn assetTemplateColumn, AssetTemplate cachedTemplate, User currentUser);
	
	/**
	 * Get sorted list of template column.
	 * @param assetTemplateColumns
	 * @return {@code List<AssetTemplateColumn>}
	 */
	public List<AssetTemplateColumn> getSortedListOfTemplateColumn(Set<AssetTemplateColumn> assetTemplateColumns);

	/**
	 * Create a default column for an asset template
	 * @param assetTemplate
	 * @param user
	 * @return
	 */
	public AssetTemplateColumn createDefaultAssetTemplateColumn(AssetTemplate assetTemplate, User user);
	/**
	 * Delete asset data for template column
	 * @param idOfTemplateColumn
	 */
	public void deleteAssetData(Integer idOfTemplateColumn);
	/**
	 * Delete asset template column
	 * @param assetTemplateColumn
	 */
	public void delete(AssetTemplateColumn assetTemplateColumn);
	/**
	 * Update sequence of template columns
	 * @param listOfTemplateColumnToUpdate
	 */
	public void updateSequenceNumber(List<AssetTemplateColumn> listOfTemplateColumnToUpdate);
	/**
	 * Save {@code List} of {@link AssetTemplateColumn}
	 * @param templateColumns
	 * @return
	 */
	public List<AssetTemplateColumn> saveInBatch(List<AssetTemplateColumn> templateColumns);
	/**
	 * Get loaded {@link AssetTemplateColumn}
	 * @param idOfTemplateColumn
	 * @return
	 */
	public AssetTemplateColumn findOneLoadedAssetTemplate(Integer idOfTemplateColumn);
	/**
	 * Get {@code List} of {@link AssetTemplateColumn} by {@link AssetTemplate}
	 * @param assetTemplate
	 * @return
	 */
	public List<AssetTemplateColumn> findByAssetTemplate(AssetTemplate assetTemplate);
	
	/**
	 * @param findByAssetTemplate
	 */
	public void deleteInBatch(List<AssetTemplateColumn> findByAssetTemplate);
}
