package org.birlasoft.thirdeye.beans.tco;

import java.util.ArrayList;
import java.util.List;

public class CostStructureBean {
	
	private String displayName;
	
	CostStructureBean parent; 
	List<CostStructureBean> childCostStructure = new ArrayList<>();
	List<CostElementBean> childCostElement = new ArrayList<>();
	
	public String getDisplayName() {
		return displayName;
	}
	
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	public CostStructureBean getParent() {
		return parent;
	}
	
	public void setParent(CostStructureBean parent) {
		this.parent = parent;
	}
	
	public List<CostStructureBean> getChildCostStructure() {
		return childCostStructure;
	}
	
	public List<CostElementBean> getChildCostElement() {
		return childCostElement;
	}
	
	public boolean addChildCostElement(CostElementBean costElementBean) {
		if (costElementBean == null) {
			return false;
		}
		return this.childCostElement.add(costElementBean);
	}
	
	public boolean addChildCostStructure(CostStructureBean child) {
		if (child == null) {
			return false;
		}		
		child.setParent(this);
		return this.childCostStructure.add(child);
	}

}
