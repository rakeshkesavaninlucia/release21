package org.birlasoft.thirdeye.beans;

import java.util.List;

public class QuestionnaireQuestionWrapper {
	
	private List<QuestionnaireQuestionResponseBean> questionnaireQuestionResponseBean;

	public List<QuestionnaireQuestionResponseBean> getQuestionnaireQuestionResponseBean() {
		return questionnaireQuestionResponseBean;
	}

	public void setQuestionnaireQuestionResponseBean(
			List<QuestionnaireQuestionResponseBean> questionnaireQuestionResponseBean) {
		this.questionnaireQuestionResponseBean = questionnaireQuestionResponseBean;
	}



}
