package org.birlasoft.thirdeye.beans;

import java.util.Set;

public class QuestionnaireQuestionGridBean {
	
	private Integer questionnaireQuestionId; 
	private String response;
	private String questionType;
	private Integer questionId; 
	private Set<String> questionOptions;
	
	public Integer getQuestionnaireQuestionId() {
		return questionnaireQuestionId;
	}
	public void setQuestionnaireQuestionId(Integer questionnaireQuestionId) {
		this.questionnaireQuestionId = questionnaireQuestionId;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	public Set<String> getQuestionOptions() {
		return questionOptions;
	}
	public void setQuestionOptions(Set<String> questionOptions) {
		this.questionOptions = questionOptions;
	}


}
