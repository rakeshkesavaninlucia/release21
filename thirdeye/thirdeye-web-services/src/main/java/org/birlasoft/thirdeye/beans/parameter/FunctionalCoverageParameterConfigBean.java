package org.birlasoft.thirdeye.beans.parameter;

import java.io.Serializable;

import org.birlasoft.thirdeye.entity.FunctionalMap;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true,value={"parameterType","name"})
public class FunctionalCoverageParameterConfigBean extends ParameterBaseConfigBean implements Serializable{
	
	/**
	 * Default generated serial version UID
	 */
	private static final long serialVersionUID = 5929240214958691976L;
	private Integer functionalMapId;
	private String name;
	private String functionalMapLevelType;
	
	
	public FunctionalCoverageParameterConfigBean() {
		super();
	}

	public FunctionalCoverageParameterConfigBean(FunctionalMap fm) {
		this.functionalMapId = fm.getId();
		this.name = fm.getName();
	}

	public Integer getFunctionalMapId() {
		return functionalMapId;
	}

	public void setFunctionalMapId(Integer functionalMapId) {
		this.functionalMapId = functionalMapId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFunctionalMapLevelType() {
		return functionalMapLevelType;
	}

	public void setFunctionalMapLevelType(String functionalMapLevelType) {
		this.functionalMapLevelType = functionalMapLevelType;
	}
}
