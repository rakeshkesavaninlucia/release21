package org.birlasoft.thirdeye.beans.aid;

import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.entity.AidBlock;
import org.birlasoft.thirdeye.search.api.beans.IndexAidBlockBean;
import org.birlasoft.thirdeye.search.api.beans.IndexAidSubBlockBean;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class JSONAIDBlock2Bean extends JSONAIDBlockConfig{

	private enum Block2_SubBlocks {subBlock1}
	
	private JSONAIDSubBlockConfig subBlock1 = new JSONAIDSubBlockConfig();
	
	public JSONAIDBlock2Bean(){
		super();
	}
	
	
	public JSONAIDBlock2Bean(AidBlock oneEntity){
		super(oneEntity);
		
		
		if (!StringUtils.isEmpty(oneEntity.getBlockJsonconfig())){
			JSONAIDBlock1Bean deserialized = Utility.convertJSONStringToObject(oneEntity.getBlockJsonconfig(), JSONAIDBlock1Bean.class);
			if (deserialized != null){
				this.subBlock1 = deserialized.getSubBlock1();
				subBlock1.setSubBlockIdentifier(Block2_SubBlocks.subBlock1.name());
			}
		}
		
	}
	
	/**
	 * Constructor to configure values in BLOCK_2.
	 * @param indexAidBlockBean
	 */
	public JSONAIDBlock2Bean(IndexAidBlockBean indexAidBlockBean) {
		super(indexAidBlockBean);
		
		List<IndexAidSubBlockBean> listOfAidSubBlockBeans = indexAidBlockBean.getSubBlocks();
		for (IndexAidSubBlockBean indexAidSubBlockBean : listOfAidSubBlockBeans) {
			if(indexAidSubBlockBean.getSubBlockName() != null){
				Block2_SubBlocks subBlockId = Block2_SubBlocks.valueOf(indexAidSubBlockBean.getSubBlockName());
				if (Block2_SubBlocks.subBlock1.equals(subBlockId)) {
					subBlock1.setBlockTitle(indexAidSubBlockBean.getTitle());
					subBlock1.setConfigurationString(indexAidSubBlockBean.getConfigValue());
					break;
				}
			}
		}
	}

	public JSONAIDSubBlockConfig getSubBlock1() {
		return subBlock1;
	}

	public void setSubBlock1(JSONAIDSubBlockConfig subBlock1) {
		this.subBlock1 = subBlock1;
	}

	
	@Override
	public JSONAIDSubBlockConfig getSubBlockFromIdentifier(String subBlockIdentifier) {
		Block2_SubBlocks subBlockId = Block2_SubBlocks.valueOf(subBlockIdentifier);
		
		switch (subBlockId) {
		case subBlock1:
			return subBlock1;
		default:
			return new JSONAIDSubBlockConfig();
		}
	}
	
	@Override
	public void setSubBlockFromIdentifier(String subBlockIdentifier, JSONAIDSubBlockConfig subBlockConfig) {
		Block2_SubBlocks subBlockId = Block2_SubBlocks.valueOf(subBlockIdentifier);
		
		switch (subBlockId) {
		case subBlock1:
			this.subBlock1 = subBlockConfig;
			break;
		}
		
	}
	
	@JsonIgnore
	@Override
	public List<JSONAIDSubBlockConfig> getListOfBlocks() {
		List<JSONAIDSubBlockConfig> listOfSubBlocks = new ArrayList<JSONAIDSubBlockConfig>();
		
		for (Block2_SubBlocks subBlockId : Block2_SubBlocks.values()){
			listOfSubBlocks.add(getSubBlockFromIdentifier(subBlockId.name()));
		}
		return listOfSubBlocks;
	}
}
