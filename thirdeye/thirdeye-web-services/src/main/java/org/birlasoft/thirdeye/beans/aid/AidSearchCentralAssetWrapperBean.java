package org.birlasoft.thirdeye.beans.aid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.birlasoft.thirdeye.comparator.SequenceNumberComparator;

/**
 * Wrapper bean for central block of Aid report.
 * @author samar.gupta
 *
 */
public class AidSearchCentralAssetWrapperBean {

	private String centralAssetBean;
	private List<JSONAIDBlockConfig> listOfBlocksInMainApp = new ArrayList<>();
	private Integer id;
	
	/**
	 * To add block in listOfBlocksInMainApp.
	 * @param oneBlock
	 */
	public void addBlock(JSONAIDBlockConfig oneBlock){
		if (oneBlock != null){
			listOfBlocksInMainApp.add(oneBlock);
		}
	}

	public List<JSONAIDBlockConfig> getListOfBlocksInMainApp() {
		return listOfBlocksInMainApp;
	}
	
	/**
	 * Sort listOfBlocksInMainApp by sequence number.
	 */
	public void prepareForDisplay(){
		Collections.sort(listOfBlocksInMainApp, new SequenceNumberComparator());
	}
	
	public String getCentralAssetBean() {
		return centralAssetBean;
	}
	public void setCentralAssetBean(String centralAssetBean) {
		this.centralAssetBean = centralAssetBean;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
}
