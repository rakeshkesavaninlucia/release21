package org.birlasoft.thirdeye.beans;

import java.util.ArrayList;
import java.util.List;

public class JSONQualityGateDescriptionMapper {
	private List<JSONQualityGateDescriptionValueMapper> description = new ArrayList<>();

	public List<JSONQualityGateDescriptionValueMapper> getDescription() {
		return description;
	}

	public void setDescription(List<JSONQualityGateDescriptionValueMapper> description) {
		this.description = description;
	}

	/**
	 * Add one {@link JSONQualityGateDescriptionValueMapper} to {@link List}
	 * @param oneValue
	 */
	public void addOption (JSONQualityGateDescriptionValueMapper oneValue){
		if (oneValue != null){
			description.add(oneValue);
		}
	}

}
