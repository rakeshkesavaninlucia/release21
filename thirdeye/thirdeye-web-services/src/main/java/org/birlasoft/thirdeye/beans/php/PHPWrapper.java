package org.birlasoft.thirdeye.beans.php;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 
 * PHPWrapper class
 *
 */

public class PHPWrapper {

	private Set<PHPValueWrapper> wrappers = new HashSet<>();
	private List<PHPGroupWrapper> assets = new ArrayList<>();
	private List<PHPGroupWrapper> parameters = new ArrayList<>();

	public List<PHPGroupWrapper> getAssets() {
		return assets;
	}

	public void setAssets(List<PHPGroupWrapper> assets) {
		this.assets = assets;
	}

	public List<PHPGroupWrapper> getParameters() {
		return parameters;
	}

	public void setParameters(List<PHPGroupWrapper> parameters) {
		this.parameters = parameters;
	}

	public Set<PHPValueWrapper> getWrappers() {
		return wrappers;
	}

	public void setWrappers(Set<PHPValueWrapper> wrappers) {
		this.wrappers = wrappers;
	}

}
