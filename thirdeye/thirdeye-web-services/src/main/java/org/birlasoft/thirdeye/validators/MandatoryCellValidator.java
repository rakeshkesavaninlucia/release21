/**
 * 
 */
package org.birlasoft.thirdeye.validators;

import java.util.List;

import org.apache.poi.ss.usermodel.Cell;

/**
 * @author shaishav.dixit
 *
 */
public class MandatoryCellValidator implements ExcelCellValidator {
	
	private boolean flag;
	private boolean isMandatory;
	private static final String CELL_MANDATORY_ERROR = "Data is mandatory";

	public MandatoryCellValidator(boolean isMandatory) {
		this.isMandatory = isMandatory;
	}

	@Override
	public boolean validate(Cell cell, List<String> errors) {
		flag = false;
		if(isMandatory && cell.getCellType() == Cell.CELL_TYPE_BLANK){
			errors.add(CELL_MANDATORY_ERROR);
		} else {
			flag = true;			
		}
		return flag;
	}

}
