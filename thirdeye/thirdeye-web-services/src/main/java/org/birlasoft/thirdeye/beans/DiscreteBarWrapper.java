package org.birlasoft.thirdeye.beans;

import java.util.ArrayList;
import java.util.List;

public class DiscreteBarWrapper {

	private String key;
	private List<DiscreteBar> values = new ArrayList<>();

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public List<DiscreteBar> getValues() {
		return values;
	}

	public void setValues(List<DiscreteBar> values) {
		this.values = values;
	}
	
	public void addValue (DiscreteBar oneBar){
		if (oneBar != null){
			values.add(oneBar);
		}
	}
}
