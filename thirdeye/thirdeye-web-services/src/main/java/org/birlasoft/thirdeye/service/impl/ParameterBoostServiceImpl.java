package org.birlasoft.thirdeye.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.ParameterEvaluationResponse;
import org.birlasoft.thirdeye.beans.parameter.ParameterValueBoostBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.entity.QuestionnaireParameterAsset;
import org.birlasoft.thirdeye.service.AssetService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ParameterBoostService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterAssetService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of service class for ParameterBoostService.
 * @author dhruv.sood
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class ParameterBoostServiceImpl implements ParameterBoostService {

	@Autowired
	private ParameterService parameterService;
	@Autowired
	private AssetService assetService;
	@Autowired
	private QuestionnaireParameterService questionnaireParameterService;
	@Autowired
	private QuestionnaireParameterAssetService questionnaireParameterAssetService;
	@Autowired
	private QuestionnaireService questionnaireService;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	
	@Override
	public List<ParameterValueBoostBean> generatetListOfPVBbean(Questionnaire questionnaire, Integer assetId) {
		
		//Get the set of questionnaire asset form the questionnaire								
		List<ParameterValueBoostBean> listOfParameterValueBoostBean = new ArrayList<>();
		List<Parameter> listOfParameter =  new ArrayList<>();
		Set<Integer> setOfParameterId = new HashSet<>();	
		Asset asset = assetService.findOne(assetId);
		AssetBean assetBean = new AssetBean(asset);		
		Set<AssetBean> setOfAssetBean = new HashSet<>();
		setOfAssetBean.add(assetBean);			
		
		//Get all the parameters for questionnaire
		List<QuestionnaireParameter> setOfQP = questionnaireParameterService.findByQuestionnaire(questionnaire);
		for (QuestionnaireParameter oneQP : setOfQP) {
			listOfParameter.add(oneQP.getParameterByParameterId());
			setOfParameterId.add(oneQP.getParameterByParameterId().getId());
		}
		
		//Get the Evaluation value for that asset and parameters
		List<ParameterEvaluationResponse> listOfParameterEvaluationResponse = parameterService.evaluateParameters(setOfParameterId, questionnaire.getId(), setOfAssetBean);
			
		for (ParameterEvaluationResponse oneParameterEvaluationResponse : listOfParameterEvaluationResponse) {
			
			ParameterValueBoostBean pvb = new ParameterValueBoostBean();	
			pvb.setQeId(questionnaire.getId());
			pvb.setAssetId(assetId);
			pvb.setParameterId(oneParameterEvaluationResponse.getParameterBean().getId());
			pvb.setParameterName(oneParameterEvaluationResponse.getParameterBean().getDisplayName());					
			pvb.setEvaluatedValue(oneParameterEvaluationResponse.getAssetResponse().get(assetBean));			
			
			Parameter parameter = parameterService.findOne(oneParameterEvaluationResponse.getParameterBean().getId());
			
			//Now retrieve the value from the QPA table
			QuestionnaireParameterAsset questionnaireParameterAsset= questionnaireParameterAssetService.findByQuestionnaireAndParameterAndAsset(questionnaire, parameter, asset);
								
			//Check if boost value is there then set it else set it to 0
			if(questionnaireParameterAsset!=null){						
					pvb.setBoostPercentage(questionnaireParameterAsset.getBoostPercentage().toString());						
			}else{
				pvb.setBoostPercentage("0.00");
			}
		
			//Add the response to the list
			listOfParameterValueBoostBean.add(pvb);
		}		
		
		return listOfParameterValueBoostBean;
	}

	
	@Override
	public QuestionnaireParameterAsset validateNewBoostValue(Integer qeId, Integer parameterId, Integer assetId) {
		
		Questionnaire questionnaire = questionnaireService.findOne(qeId);
		Parameter parameter = parameterService.findOne(parameterId);
		Asset asset = assetService.findOne(assetId);
		
		QuestionnaireParameterAsset questionnaireParameterAsset = questionnaireParameterAssetService.findByQuestionnaireAndParameterAndAsset(questionnaire, parameter, asset);
		
		if(questionnaireParameterAsset==null){
			questionnaireParameterAsset = new QuestionnaireParameterAsset();
		}
		questionnaireParameterAsset.setQuestionnaire(questionnaire);
		questionnaireParameterAsset.setParameter(parameter);
		questionnaireParameterAsset.setAsset(asset);		
		questionnaireParameterAsset.setUserByCreatedBy(customUserDetailsService.getCurrentUser());
		questionnaireParameterAsset.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		questionnaireParameterAsset.setCreatedDate(new Date());
		questionnaireParameterAsset.setUpdatedDate(new Date());
		return questionnaireParameterAsset;		
	}
	
	
	@Override
	public List<ParameterEvaluationResponse> boostEvaluatedResponse(Questionnaire questionnaire, List<ParameterEvaluationResponse> listOfParameterResponses){
		
		
		List<ParameterEvaluationResponse> newListOfParameterResponses = new ArrayList<>();
		
		for (ParameterEvaluationResponse oneParameterEvaluationResponse : listOfParameterResponses) {
			
			//Create the Parameter Response Bean
			ParameterEvaluationResponse parameterEvaluationResponse = new ParameterEvaluationResponse();
			
			for (Entry<AssetBean, BigDecimal> entry : oneParameterEvaluationResponse.getAssetResponse().entrySet()){				
				
				//Get the boost percentage from db if exists
				QuestionnaireParameterAsset questionnaireParameterAsset = questionnaireParameterAssetService.findByQuestionnaireAndParameterAndAsset(questionnaire, parameterService.findOne(oneParameterEvaluationResponse.getParameterBean().getId()), assetService.findOne(entry.getKey().getId()));
				
				//Apply boost to existing value
				BigDecimal boostedValue = applyBoost(questionnaireParameterAsset, entry.getValue());
				
				parameterEvaluationResponse.setParameterBean(oneParameterEvaluationResponse.getParameterBean());
				parameterEvaluationResponse.addAssetEvaluation(entry.getKey(),boostedValue);								
			}
			//Add this to new list
			newListOfParameterResponses.add(parameterEvaluationResponse);			
		}
		return newListOfParameterResponses;
	}


	/**Method to apply boost to existing evaluated value 
	 * @author dhruv.sood
	 * @param questionnaireParameterAsset
	 * @param boostedValue
	 * @return
	 */
	private BigDecimal applyBoost(QuestionnaireParameterAsset questionnaireParameterAsset, BigDecimal boostedValue) {
		if(questionnaireParameterAsset!=null){
			  boostedValue = (boostedValue.add(boostedValue.multiply(questionnaireParameterAsset.getBoostPercentage().divide(new BigDecimal(100),2)))).setScale(2, BigDecimal.ROUND_HALF_EVEN);
			  if(boostedValue.intValue()>10 || boostedValue.intValue()==10 ){
					 boostedValue = new BigDecimal(10);
				 }else if(boostedValue.intValue()<0 || boostedValue.intValue()==0){
					 boostedValue = new BigDecimal(0);
				 }
		  }
		return boostedValue;
	}
	
}
