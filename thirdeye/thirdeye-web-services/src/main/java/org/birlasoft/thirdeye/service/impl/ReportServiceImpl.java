package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.birlasoft.thirdeye.beans.report.ReportConfigBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.Report;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.ReportRepository;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author sunil1.gupta
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class ReportServiceImpl implements ReportService {
	
	@Autowired
	private ReportRepository reportRepository;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	public ReportServiceImpl() {
		//Default constructor
	}
	
	@Override
	public Report saveOrUpdateReport(ReportConfigBean reportConfigBean) {
		return reportRepository.save(prepareReportBean(reportConfigBean));
	}
	
	
	
	private  Report prepareReportBean(ReportConfigBean reportConfigBean) {
		Report report = new Report();
		if(reportConfigBean.getId() != null)
		  report.setId(reportConfigBean.getId());
		report.setReportName(reportConfigBean.getReportName());
		report.setDescription(reportConfigBean.getDescription());
		report.setReportType(reportConfigBean.getReportType());
		report.setReportConfig(reportConfigBean.getReportConfig());
		report.setDeletedStatus(reportConfigBean.isDeletedStatus());
		report.setUserByCreatedBy(customUserDetailsService.getCurrentUser());
		report.setCreatedDate(new Date());
		report.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		report.setUpdatedDate(new Date());
		report.setWorkspace(customUserDetailsService.getActiveWorkSpaceForUser());
		
		return report;
	}
	
	@Override
	public void deleteReport(Integer reportId) {
		// TODO Auto-generated method stub
	}

	@Override
	public List<ReportConfigBean> findAllMySavedAndAssingedReport(Workspace activeWorkSpaceForUser, User currentUser) {
		List<Report> report = reportRepository.findByWorkspaceAndUserByCreatedBy(activeWorkSpaceForUser,currentUser);
		return report.stream().map(ReportConfigBean :: new).collect(Collectors.toList());
	}

	@Override
	public Report findReportById(Integer reportId) {
		return reportRepository.findOne(reportId);
	}	
	
	@Override
	public List<Report> isSavedReportStatusActiveToShowMessage(Integer idOfSavedReport) {
		Set<Workspace> listOfWorkspaces = new HashSet<> ();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
		
		List<Report> updatedListOfReport = new ArrayList<>();
		
		if(!listOfWorkspaces.isEmpty()){
			List<Report> listOfReport = this.findByWorkspaceIn(listOfWorkspaces);
			for (Report report : listOfReport) {
				if(report.getId().equals(idOfSavedReport)) {
					report.setStatus(true);
				} else {
					report.setStatus(false);
				}
				updatedListOfReport.add(report);
			}
		}
		return updatedListOfReport;
	}

	@Override
	public boolean isStatusActiveToShowMessage(Integer idOfSavedReport) {
		List<Report> updatedListOfReport = this.isSavedReportStatusActiveToShowMessage(idOfSavedReport);
		
		boolean isStatusActive = false;
		if(!updatedListOfReport.isEmpty()){
			List<Report> savedReport = this.saveAll(updatedListOfReport);
			if(savedReport.size() == updatedListOfReport.size()){
				isStatusActive = true;
			}
		}
		
		return isStatusActive;
	}
	@Override
	public List<Report> findByWorkspaceIn(Set<Workspace> workspaces) {
		// TODO Auto-generated method stub
		return reportRepository.findByWorkspaceIn(workspaces);
	}

	@Override
	public List<Report> saveAll(List<Report> reports) {
		// TODO Auto-generated method stub
		return reportRepository.save(reports);
	}
	
}
