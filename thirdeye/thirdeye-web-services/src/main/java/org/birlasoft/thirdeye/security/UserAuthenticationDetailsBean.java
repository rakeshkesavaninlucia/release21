package org.birlasoft.thirdeye.security;

import java.io.Serializable;

/**
 * Bean to hold the tenant details in the session.
 * @author shaishav.dixit
 *
 */
public class UserAuthenticationDetailsBean implements Serializable{

	private static final long serialVersionUID = 2101198823101985L;
	String accountId;
	String tenantURL;
	
	/**
	 * 
	 * @param accountId
	 * @param tenantURL
	 */
	public UserAuthenticationDetailsBean(String accountId, String tenantURL) {
		super();
		this.accountId = accountId;
		this.tenantURL = tenantURL;
	}

	public String getAccountId() {
		return accountId;
	}

	public String getTenantURL() {
		return tenantURL;
	}
	
	
}
