package org.birlasoft.thirdeye.colorscheme.bean;

/**
 * Wrapper {@code bean} to color any {@code bean} which extends this {@code bean class}
 * 
 * @author shaishav.dixit
 *
 */
public class CSSBean {

	private String hexColor;

	public String getHexColor() {
		return hexColor;
	}

	public void setHexColor(String hexColor) {
		this.hexColor = hexColor;
	}
	
}
