package org.birlasoft.thirdeye.colorscheme.constant;

public enum Condition {
	
	EQUAL("equals"), LESS("is less than"), GREATER("is greater than");
	
	private final String value;       

    private Condition(String value) {
        this.value = value;
    }

	public String getValue() {
		return value;
	}

}
