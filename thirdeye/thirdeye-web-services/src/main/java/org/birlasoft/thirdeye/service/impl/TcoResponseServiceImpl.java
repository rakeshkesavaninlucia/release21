package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.JSONNumberResponseMapper;
import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.QuestionBean;
import org.birlasoft.thirdeye.beans.QuestionnaireQuestionBean;
import org.birlasoft.thirdeye.beans.tco.ChartOfAccountBean;
import org.birlasoft.thirdeye.beans.tco.ChartOfAccountGridBean;
import org.birlasoft.thirdeye.beans.tco.ChartOfAccountGridWrapper;
import org.birlasoft.thirdeye.beans.tco.ChartOfAccountResponseBean;
import org.birlasoft.thirdeye.beans.tco.CostElementBean;
import org.birlasoft.thirdeye.beans.tco.CostStructureBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.Response;
import org.birlasoft.thirdeye.entity.ResponseData;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireAssetService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.ResponseDataService;
import org.birlasoft.thirdeye.service.TcoResponseService;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class TcoResponseServiceImpl implements TcoResponseService {

	@Autowired
	private QuestionnaireAssetService questionnaireAssetService;
	@Autowired
	private QuestionnaireQuestionService questionnaireQuestionService;
	@Autowired
	private QuestionnaireParameterService questionnaireParameterService;
	@Autowired
	private ParameterService parameterService;
	@Autowired
	private ResponseDataService responseDataService;

	@Override
	public List<ChartOfAccountBean> extractChartOfAccounts(Questionnaire questionnaire) {
		List<QuestionnaireParameter> listOfQp = questionnaireParameterService.getListOfRootQuestionnaireParameters(questionnaire);
		List<ParameterBean> listOfParameterBean = new ArrayList<>();
		for (QuestionnaireParameter questionnaireParameter : listOfQp) {
			Parameter parameter = parameterService.findFullyLoaded(questionnaireParameter.getParameterByParameterId().getId());
			ParameterBean parameterBean = parameterService.generateParameterTree(parameter);
			listOfParameterBean.add(parameterBean);
		}

		return this.prepareListOfChartOfAccounts(listOfParameterBean, questionnaire);
	}

	private List<ChartOfAccountBean> prepareListOfChartOfAccounts(
			List<ParameterBean> listOfParameterBean, Questionnaire questionnaire) {
		List<ChartOfAccountBean> listToReturn = new ArrayList<>();

		Set<QuestionnaireAsset> setOfQa = questionnaireAssetService.findByQuestionnaireLoadedAsset(questionnaire);
		for (QuestionnaireAsset questionnaireAsset : setOfQa) {
			if (!questionnaireAsset.getAsset().isDeleteStatus()) {
				ChartOfAccountBean accountBean = new ChartOfAccountBean();
				accountBean.setAsset(new AssetBean(questionnaireAsset.getAsset()));
				for (ParameterBean oneParam : listOfParameterBean) {
					CostStructureBean costStructureBean = prepareCostStructureBeanFromParamBean(oneParam,
							questionnaireAsset);
					accountBean.addCostStructure(costStructureBean);
				}
				listToReturn.add(accountBean);
			}
		}

		return listToReturn;
	}

	private CostStructureBean prepareCostStructureBeanFromParamBean(ParameterBean oneParam, QuestionnaireAsset questionnaireAsset) {
		CostStructureBean costStructureBean = new CostStructureBean();
		costStructureBean.setDisplayName(oneParam.getDisplayName());
		//add child cost structure
		for (ParameterBean oneChildParam : oneParam.getChildParameters()) {
			costStructureBean.addChildCostStructure(prepareCostStructureBeanFromParamBean(oneChildParam, questionnaireAsset));
		}

		//add child cost element
		for (QuestionBean oneChildQuestion : oneParam.getChildQuestions()) {
			costStructureBean.addChildCostElement(prepareCostElementBeanFromQuestionBean(oneChildQuestion, questionnaireAsset));
		}
		return costStructureBean;
	}

	private CostElementBean prepareCostElementBeanFromQuestionBean(QuestionBean question, QuestionnaireAsset questionnaireAsset) {
		CostElementBean costElementBean = new CostElementBean();
		costElementBean.setTitle(question.getTitle());
		List<QuestionnaireParameter> listOfQuestionnaireParameters = questionnaireParameterService.findByQuestionnaireAndParameter(questionnaireAsset.getQuestionnaire(), 
				parameterService.findOne(question.getParentParameter().getId()));

		QuestionnaireQuestion qq = questionnaireQuestionService.getQqForQuestion(question, questionnaireAsset, listOfQuestionnaireParameters);
		costElementBean.setQqBean(new QuestionnaireQuestionBean(qq));
		costElementBean.setQuestionParameterId(qq.getQuestionnaireParameter().getId());
		return costElementBean;
	}

	@Override
	public ChartOfAccountGridWrapper getCostStructureName(Questionnaire questionnaire,Response qResponse) {

		List<QuestionnaireQuestion> questionnaireQuestions = questionnaireQuestionService.findByQuestionnaire(questionnaire, true);
		//get Asset Name
		List<String> sortedList = getAssetName(questionnaireQuestions);
		//get question id and CostStructure Name
		Map<String, String>  costStructureName =getCostStructureTreeName(questionnaire);
		
		//response map of questionnaireassetId and questionnairequestionId and response
		Map<Integer, Map<Integer,String>> responseMap = new HashMap<>();
		if(qResponse != null){
			List<ResponseData> responseData = responseDataService.findByResponse(qResponse);			
			responseMap = getResponseData(responseData);	
		}
		List<ChartOfAccountResponseBean> listofCS = new ArrayList<>();       
		ChartOfAccountGridWrapper chartOfAccountGridWrapper = new ChartOfAccountGridWrapper();
		for(String csKey:costStructureName.keySet()){ 
			String tempArr[] = csKey.split("_");
			int coaquestionId = Integer.parseInt(tempArr[0]);
			int qpid = Integer.parseInt(tempArr[1]);
			List<ChartOfAccountGridBean> listOfChartOfAccountGridBean = new ArrayList<>();
			ChartOfAccountResponseBean chartOfAccountResponseBean = new ChartOfAccountResponseBean();
			for(String  quesAssetKey :sortedList){
				for (QuestionnaireQuestion qq : questionnaireQuestions) {
					if (quesAssetKey.equals(new AssetBean(qq.getQuestionnaireAsset().getAsset()).getShortName()) && coaquestionId==qq.getQuestion().getId() && qpid==qq.getQuestionnaireParameter().getId()){
						int qqId = qq.getId();
						String resp = null;
						Map<Integer,String> mapResp = responseMap!=null ? responseMap.get(qq.getQuestionnaireAsset().getId()) : null;
						if(mapResp !=null ){
							resp = mapResp.get(qqId);}
						ChartOfAccountGridBean chartOfAccountGridBean = new ChartOfAccountGridBean();
						chartOfAccountGridBean.setQuestionnaireQuestionId(qqId);
						chartOfAccountGridBean.setResponse(resp);   
						listOfChartOfAccountGridBean.add(chartOfAccountGridBean);
						break;
					}
				}                   
			}
			chartOfAccountResponseBean.setCostElement(costStructureName.get(csKey));
			chartOfAccountResponseBean.setCostResponses(listOfChartOfAccountGridBean);
			listofCS.add(chartOfAccountResponseBean);        
		}
			chartOfAccountGridWrapper.setCostResponse(listofCS);
		return chartOfAccountGridWrapper;
	}
	@Override
	public List<String> getAssetName(List<QuestionnaireQuestion> questionnaireQuestions) {
		// get asset name	
		Set<String> assetName = new HashSet<>();
		for (QuestionnaireQuestion qq : questionnaireQuestions) {
			assetName.add(new AssetBean(qq.getQuestionnaireAsset().getAsset()).getShortName());
		}
		List<String> sortedList = new ArrayList<>(assetName);
		Collections.sort(sortedList);
		return sortedList;
	}

	private Map<Integer, Map<Integer,String>>  getResponseData(List<ResponseData> responseData) {
		Map<Integer, Map<Integer,String>> responseMap = new HashMap<>();
		for(ResponseData rd:responseData)
		{
			Map<Integer,String> questionMap = new HashMap<>();
			for(ResponseData rd2:responseData)
			{  
				if(getQuestionnaireAssetId(rd) == getQuestionnaireAssetId(rd2))
				{
					processChartOfAccountResponseData(questionMap, rd2);	
					responseMap.put(getQuestionnaireAssetId(rd),questionMap);
				}
			}			
		}
		return  responseMap;
	}

private Map<String, String> getCostStructureTreeName(Questionnaire questionnaire) {
		
		Map<String, String> listOfCoststructureName = new HashMap<>();
		List<ChartOfAccountBean> listOfCoa  = extractChartOfAccounts(questionnaire);
		for(ChartOfAccountBean costStructure:listOfCoa){
		for(CostStructureBean coststructureNname : costStructure.getListOfCostStructure()){
			if(!coststructureNname.getChildCostStructure().isEmpty()){
				for(CostStructureBean childCostStructure : coststructureNname.getChildCostStructure()){
					if(!childCostStructure.getChildCostElement().isEmpty()){
						for(CostElementBean childCostElement : childCostStructure.getChildCostElement()){
							listOfCoststructureName.put(childCostElement.getQqBean().getQuestion().getId()+"_"+ childCostElement.getQuestionParameterId(), coststructureNname.getDisplayName()+"/"+ childCostStructure.getDisplayName()+"/"+ childCostElement.getTitle());
						}												
					}
				}
			}
		}
		break;	
	}
		return listOfCoststructureName;
	}

	/**
	 * @param rd
	 * @return
	 */
	private Integer getQuestionnaireAssetId(ResponseData rd) {
		return rd.getQuestionnaireQuestion().getQuestionnaireAsset().getId();
	}

	/**
	 * @param questionMap
	 * @param rd2
	 */
	private void processChartOfAccountResponseData(Map<Integer, String> questionMap, ResponseData rd2) {

		JSONNumberResponseMapper jsonNumberResponse = Utility.convertJSONStringToObject(rd2.getResponseData(), JSONNumberResponseMapper.class);
		if (jsonNumberResponse != null){
			questionMap.put(rd2.getQuestionnaireQuestion().getId(), String.valueOf(jsonNumberResponse.getResponseNumber())); 
		}
	}
}
