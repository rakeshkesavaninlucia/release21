package org.birlasoft.thirdeye.service.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.birlasoft.thirdeye.beans.JSONQualityGateDescriptionMapper;
import org.birlasoft.thirdeye.beans.JSONQualityGateDescriptionValueMapper;
import org.birlasoft.thirdeye.beans.QualityGateBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterQualityGate;
import org.birlasoft.thirdeye.entity.QualityGate;
import org.birlasoft.thirdeye.entity.QualityGateCondition;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.ParameterQualityGateRepository;
import org.birlasoft.thirdeye.repositories.QualityGateRepository;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.QualityGateConditionService;
import org.birlasoft.thirdeye.service.QualityGateService;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service {@code class} of {@link QualityGate} to {@code save, update, delete} , and 
 * find existing.
 * @author shaishav.dixit
 *
 */
@Service
@Transactional(value = TransactionManagers.TENANTTRANSACTIONMANAGER)
public class QualityGateServiceImpl implements QualityGateService {
	
	@Autowired
	private QualityGateRepository qualityGateRepository;
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	
	@Autowired
	private QualityGateConditionService qualityGateConditionService; 
	
	@Autowired
	private ParameterQualityGateRepository parameterQualityGateRepository;

	@Override
	public QualityGate findByNameAndWorkspace(String name, Workspace workspace) {
		return qualityGateRepository.findByNameAndWorkspace(name, workspace);
	}

	@Override
	public QualityGate createQualityGateNewObject(QualityGateBean qualityGateBean) {
		
		QualityGate qualityGate = new QualityGate();
		setQualityGateDetails(qualityGateBean, qualityGate);
		qualityGate.setWorkspace(customUserDetailsService.getActiveWorkSpaceForUser());
		setCreatedAndUpdatedDetailsForQualityGate(qualityGate);
		return qualityGate;
	}

	@Override
	public QualityGate save(QualityGate qualityGate) {
		return qualityGateRepository.save(qualityGate);
	}

	@Override
	public List<QualityGate> findByWorkspace(Workspace workspace) {
		return qualityGateRepository.findByWorkspace(workspace);
	}

	@Override
	public QualityGate findOne(Integer qualityGateId) {
		return qualityGateRepository.findOne(qualityGateId);
	}

	@Override
	public QualityGate updateQualityGateObject(QualityGateBean qualityGateBean) {
		User currentUser = customUserDetailsService.getCurrentUser();
		
		QualityGate qualityGate = this.findOne(qualityGateBean.getId());
		setQualityGateDetails(qualityGateBean, qualityGate);
		qualityGate.setUserByUpdatedBy(currentUser);
		qualityGate.setUpdatedDate(new Date());
		return qualityGate;
	}

	private void setQualityGateDetails(QualityGateBean qualityGateBean, QualityGate qualityGate) {
		qualityGate.setName(qualityGateBean.getName());
	}

	@Override
	public void delete(Integer idOfQualityGate) {
		QualityGate qualityGate = this.findOne(idOfQualityGate);
		//First delete all the associated conditions
		qualityGateConditionService.deleteInBatch(qualityGate.getQualityGateConditions());
		//After deleting the conditions delete quality gate
		qualityGateRepository.delete(qualityGate);
	}

	@Override
	public void createDefaultQualityGateForParameter(Parameter parameter) {
		QualityGate qualityGate = getDefaultQualityGateForParameter(parameter);
		//save quality gate
		this.save(qualityGate);
		
		//create default condition for the quality gate
		QualityGateCondition qualityGateCondition = qualityGateConditionService.getDefaultCondition(qualityGate, parameter);
		qualityGateConditionService.save(qualityGateCondition);
		
		//create a mapping for parameter and quality gate
		ParameterQualityGate parameterQualityGate = new ParameterQualityGate();
		parameterQualityGate.setParameter(parameter);
		parameterQualityGate.setQualityGate(qualityGate);
		
		//save this parameter qg
		parameterQualityGateRepository.save(parameterQualityGate);
		
	}

	/**
	 * Creates a default quality gate for a parameter
	 * @param parameter
	 * @return
	 */
	private QualityGate getDefaultQualityGateForParameter(Parameter parameter) {
		
		QualityGate qualityGate = new QualityGate();
		qualityGate.setName("QG_" + parameter.getUniqueName());
		qualityGate.setWorkspace(customUserDetailsService.getActiveWorkSpaceForUser());
		qualityGate.setColorDescription(getDefaultColorDescription());
		setCreatedAndUpdatedDetailsForQualityGate(qualityGate);
		return qualityGate;
	}

	private String getDefaultColorDescription() {
		List<String> listOfDefaultColors = Arrays.asList("#fc8d59","#ffffbf","#91cf60");
		List<String> listOfDefaultDescription = Arrays.asList("Poor","Medium","Good");
		return createJsonString(listOfDefaultDescription, listOfDefaultColors);
	}
	
	private String createJsonString(List<String> listOfDescription, List<String> listOfColors) {
		String jSONString;
		JSONQualityGateDescriptionMapper descriptionMapper = generateDescriptionJSONMapper(listOfDescription, listOfColors);
		jSONString = Utility.convertObjectToJSONString(descriptionMapper);
		return jSONString;
	}
	
	/**
	 * @param qualityGate
	 */
	private void setCreatedAndUpdatedDetailsForQualityGate(QualityGate qualityGate) {
		User currentUser = customUserDetailsService.getCurrentUser();
		qualityGate.setUserByCreatedBy(currentUser);
		qualityGate.setUserByUpdatedBy(currentUser);
		qualityGate.setCreatedDate(new Date());
		qualityGate.setUpdatedDate(new Date());
	}

	@Override
	public List<QualityGate> findAssetQualityGates() {
		List<QualityGate> listOfQualityGates = this.findByWorkspace(customUserDetailsService.getActiveWorkSpaceForUser());
		if(!listOfQualityGates.isEmpty()){			
			List<QualityGate> listOfParameterQualityGates = parameterQualityGateRepository.findByQualityGateIn(listOfQualityGates);
			//remove all parameter quality gates
			listOfQualityGates.removeAll(listOfParameterQualityGates);
		}
		return listOfQualityGates;
	}
    
	@Override
	public QualityGate insertDescriptionMapperList(QualityGate qualityGate, List<String> listOfDescription, List<String> listOfColors) {
		JSONQualityGateDescriptionMapper descriptionMapper = this.generateDescriptionJSONMapper(listOfDescription, listOfColors);
		qualityGate.setColorDescription(Utility.convertObjectToJSONString(descriptionMapper));
		qualityGate = updateQGDescriptionObject(qualityGate);
		
		return qualityGate;
	}
	
	/**
	 * This method generate a descriptionMapper object {@link JSONQualityGateDescriptionMapper} from 
	 * list of color and list of description.
	 * @param listOfDescription
	 * @param listOfColors
	 * @return JSONQualityGateDescriptionMapper
	 */
	private JSONQualityGateDescriptionMapper generateDescriptionJSONMapper(List<String> listOfDescription, List<String> listOfColors) {
		JSONQualityGateDescriptionMapper descriptionMapper = new JSONQualityGateDescriptionMapper();
		int ctr = 0;
		for (String color : listOfColors) {
			JSONQualityGateDescriptionValueMapper descriptionValueMapper = new JSONQualityGateDescriptionValueMapper();
			descriptionValueMapper.setColor(color);
			descriptionValueMapper.setDescription(listOfDescription.get(ctr));
			ctr++;
			descriptionMapper.addOption(descriptionValueMapper);
		}
		return descriptionMapper;
	}
	
	private QualityGate updateQGDescriptionObject(QualityGate qualityGate) {
		User currentUser = customUserDetailsService.getCurrentUser();

		qualityGate.setUpdatedDate(new Date());
		qualityGate.setUserByUpdatedBy(currentUser);
		return qualityGate;
	}
	
	@Override
	public QualityGateBean createQGBean(QualityGate qualityGate) {
		QualityGateBean QGBean = new QualityGateBean();
		QGBean.setId(qualityGate.getId());
		QGBean.setDescription(getQGDescription(qualityGate.getColorDescription()));
		return QGBean;
	}
	
	private List<JSONQualityGateDescriptionValueMapper> getQGDescription(String description) {
		List<JSONQualityGateDescriptionValueMapper> jsonDescription = null;
		if(description != null){
			JSONQualityGateDescriptionMapper qualityGateDescriptionMapper = getQualityGateDescriptionMapper(description);
			jsonDescription = qualityGateDescriptionMapper.getDescription();
		}
		return jsonDescription;
	}
	
	private JSONQualityGateDescriptionMapper getQualityGateDescriptionMapper(String description) {
		JSONQualityGateDescriptionMapper mappedObject = null;
		if (description != null){
			mappedObject = Utility.convertJSONStringToObject(description, JSONQualityGateDescriptionMapper.class);
		}
		return mappedObject;
	}
	
	@Override
	public QualityGateBean getQGBean(Integer idOfQualityGate) {
		QualityGate qualityGate = qualityGateRepository.findOne(idOfQualityGate);
		return createQGBean(qualityGate);
	}
}
