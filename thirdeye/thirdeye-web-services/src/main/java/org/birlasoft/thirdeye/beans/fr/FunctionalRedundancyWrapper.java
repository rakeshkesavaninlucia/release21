package org.birlasoft.thirdeye.beans.fr;

import java.util.ArrayList;
import java.util.List;

/**
 * @author samar.gupta
 *
 */
public class FunctionalRedundancyWrapper {
	
	List<BcmParentLevelWrapper> bcmDetails = new ArrayList<>();
	List<AssetValueWrapper> assetDetails = new ArrayList<>();
	
	public List<BcmParentLevelWrapper> getBcmDetails() {
		return bcmDetails;
	}
	public void setBcmDetails(List<BcmParentLevelWrapper> bcmDetails) {
		this.bcmDetails = bcmDetails;
	}
	public List<AssetValueWrapper> getAssetDetails() {
		return assetDetails;
	}
	public void setAssetDetails(List<AssetValueWrapper> assetDetails) {
		this.assetDetails = assetDetails;
	}
}
