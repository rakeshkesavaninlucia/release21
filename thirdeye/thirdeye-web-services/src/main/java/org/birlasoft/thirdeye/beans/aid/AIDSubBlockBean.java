package org.birlasoft.thirdeye.beans.aid;

public class AIDSubBlockBean {
	
	private Integer parameterId;
	private Integer questionaireId;
	
	public Integer getParameterId() {
		return parameterId;
	}
	
	public void setParameterId(Integer parameterId) {
		this.parameterId = parameterId;
	}
	
	public Integer getQuestionaireId() {
		return questionaireId;
	}
	
	public void setQuestionaireId(Integer questionaireId) {
		this.questionaireId = questionaireId;
	}	

}
