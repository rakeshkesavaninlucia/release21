package org.birlasoft.thirdeye.beans;

import java.util.List;

import org.birlasoft.thirdeye.colorscheme.constant.Condition;

public class QualityGateConditionBean {
	
	private Integer id;
	private Condition condition;
	private Integer parameterId;
	private Double weight;
	private Integer qualityGateId;
	private List<JSONQualityGateConditionValueMapper> valueMapper;
	private String parameterName;
	private boolean weighted;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Condition getCondition() {
		return condition;
	}
	
	public void setCondition(Condition condition) {
		this.condition = condition;
	}

	public Integer getParameterId() {
		return parameterId;
	}

	public void setParameterId(Integer parameterId) {
		this.parameterId = parameterId;
	}


	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Integer getQualityGateId() {
		return qualityGateId;
	}

	public void setQualityGateId(Integer qualityGateId) {
		this.qualityGateId = qualityGateId;
	}

	public List<JSONQualityGateConditionValueMapper> getValueMapper() {
		return valueMapper;
	}

	public void setValueMapper(List<JSONQualityGateConditionValueMapper> valueMapper) {
		this.valueMapper = valueMapper;
	}

	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	public boolean isWeighted() {
		return weighted;
	}

	public void setWeighted(boolean weighted) {
		this.weighted = weighted;
	}
}
