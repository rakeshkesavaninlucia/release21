package org.birlasoft.thirdeye.service;

import java.util.Date;
import java.util.Set;

import org.birlasoft.thirdeye.entity.BenchmarkItem;
import org.birlasoft.thirdeye.entity.BenchmarkItemScore;


/**
 * Service Interface for BenchmarkItemScore.
 */

public interface BenchmarkItemScoreService {

	 /**
	  * Get BenchmarkItem by BenchmarkItemScore id
	  * @param idOfBenchmarkItemScore
	  * @return {@code BenchmarkItemScore}
	  */
	public BenchmarkItemScore findById(Integer idOfBenchmarkItemScore);
	
    /**
     * Method to save or update {@code BenchmarkItemScore}
     * @param benchmarkItemScoreBean
     * @return {@code BenchmarkItemScore}
     */
	public BenchmarkItemScore saveOrUpdate(BenchmarkItemScore benchmarkItemScoreBean);

	

	/**
	 * method to delete benchmark item score
	 * @param idOfBenchmarkItemScore 
	 */
	public void deleteBenchmarkItemScore(Integer idOfBenchmarkItem);
	
	/**
	 * method to get list of benchmarkItemScore by BenchmarkItemId
	 * @param idOfBenchmarkItemScore
	 * @return {@code List<BenchmarkItemScore>}
	 */
	public Set<BenchmarkItemScore> findByBenchmarkItemId(Integer idOfBenchmarkItemScore);
	
	/**
	 * method to get benchmarkItemScore by BenchmarkItem
	 * @param benchmarkItem
	 * @return {@code BenchmarkItemScore}
	 */
	public BenchmarkItemScore findByBenchmarkItemAndCurrentDate(BenchmarkItem benchmarkItem, Date date);
}
