package org.birlasoft.thirdeye.beans;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class QuestionEvaluationResponse {

	private QuestionBean questionBean;
	private Map<AssetBean, BigDecimal> assetResponse = new HashMap<AssetBean, BigDecimal>();
	
	public void addAssetEvaluation (AssetBean ab, BigDecimal d){
		if (ab != null){
			assetResponse.put(ab, d);
		}
	}

	public Set<AssetBean> getAssets(){
		return assetResponse.keySet();
	}
	
	public BigDecimal fetchQuestionValueForAsset(AssetBean ab){
		return assetResponse.get(ab);
	}
	
	public QuestionBean getQuestionBean() {
		return questionBean;
	}

	public void setQuestionBean(QuestionBean questionBean) {
		this.questionBean = questionBean;
	}

	
	public Map<Integer, BigDecimal> fetchAssetMappedByAssetId(){
		 Map<Integer, BigDecimal> responseMap = new HashMap<Integer, BigDecimal>();
		 for (AssetBean oneBean : assetResponse.keySet() ){
			 responseMap.put(oneBean.getId(), assetResponse.get(oneBean));
		 }
		 
		 return responseMap;
	}

	
}
