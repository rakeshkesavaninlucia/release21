package org.birlasoft.thirdeye.beans;

import org.birlasoft.thirdeye.entity.ParameterFunction;

/**
 * Bean class for {@link ParameterFunction}
 *
 */
public class ParameterFunctionBean {

	private Double weight;
	private Integer constant;
	
	private QuestionBean question;
	private ParameterBean parameter;
	
	/**
	 * Constructor for child parameter and its weight
	 * @param weight
	 * @param constant
	 * @param parameter
	 */
	public ParameterFunctionBean(Double weight, int constant,
			ParameterBean parameter) {
		super();
		this.weight = weight;
		this.constant = constant;
		this.parameter = parameter;
	}

	/**
	 * Constructor for child questions and its weight
	 * @param weight
	 * @param constant
	 * @param question
	 */
	public ParameterFunctionBean(Double weight, int constant, QuestionBean question) {
		super();
		this.weight = weight;
		this.constant = constant;
		this.question = question;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Integer getConstant() {
		return constant;
	}

	public void setConstant(int constant) {
		this.constant = constant;
	}

	public QuestionBean getQuestion() {
		return question;
	}

	public void setQuestion(QuestionBean question) {
		this.question = question;
	}

	public ParameterBean getParameter() {
		return parameter;
	}

	public void setParameter(ParameterBean parameter) {
		this.parameter = parameter;
	}
	
}
