/**
 * 
 */
package org.birlasoft.thirdeye.beans;

import java.util.List;

import org.birlasoft.thirdeye.validators.ExcelCellValidator;

/**
 * @author ananta.sethi
 *
 */
public class ExcelCellBean {
	
	private String value;
	private Integer column;
	private Integer row;
	private List<String> errors;
	private List<ExcelCellValidator> cellValidators;
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public Integer getColumn() {
		return column;
	}
	
	public void setColumn(Integer column) {
		this.column = column;
	}
	
	public Integer getRow() {
		return row;
	}
	
	public void setRow(Integer row) {
		this.row = row;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public List<ExcelCellValidator> getCellValidators() {
		return cellValidators;
	}

	public void setCellValidators(List<ExcelCellValidator> cellValidators) {
		this.cellValidators = cellValidators;
	}
}
